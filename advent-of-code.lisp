(in-package :advent-of-code)
(5am:def-suite :aoc-all)
(5am:def-suite :aoc :in :aoc-all)

;;;; Setup

;;; Let lparallel use all of the available CPU cores
(setf lparallel:*kernel* (lparallel:make-kernel (cpus:get-number-of-processors)))

;;; Use GMP for bignums.  It's far faster than SBCL's native implementation.
(sb-gmp:install-gmp-funs)


;;; Config File

(defparameter *config-file* (merge-pathnames ".aocrc" (user-homedir-pathname))
  "The Advent of Code config file.

Contains information about how to fetch problem details automatically.  It
should be a TOML file with a \"session\" key and a \"cachedir\" key,
something like this:

    session = \"abcdef1234567...\"
    cachedir = \"/path/to/dir/\"

The value of \"session\" should be the value of your \"session\" cookie
when you're logged in to the Advent of Code website.  \"cachedir\" is a
directory that will be used to cache data downloaded from the website.

The cachedir value MUST end with a slash.")

(defparameter *config-key-session* "session")
(defparameter *config-key-cachedir* "cachedir")

(defun get-config-key (key)
  (gethash key (pp-toml:parse-toml (file-string *config-file*))))

(defun get-cachedir ()
  (get-config-key *config-key-cachedir*))

(defun file-string (filename)
  (with-open-file (stream filename)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))


;;; Data from package name

(defun get-current-year ()
  (multiple-value-bind (match groups)
      (ppcre:scan-to-strings "^ADVENT-OF-CODE-(\\d+)"
                             (package-name *package*))
    (when (not match)
      (error "Current package does not have an Advent of Code year: ~A" *package*))
    (parse-integer (svref groups 0))))

(defun get-current-day ()
  (multiple-value-bind (match groups)
      (ppcre:scan-to-strings "^ADVENT-OF-CODE-\\d+-(\\d+)"
                             (package-name *package*))
    (when (not match)
      (error "Current package does not have an Advent of Code day: ~A" *package*))
    (parse-integer (svref groups 0))))


;;; Tests

(5am:def-suite :aoc-2015 :in :aoc-all)
(5am:def-suite :aoc-2016 :in :aoc-all)
(5am:def-suite :aoc-2017 :in :aoc-all)
(5am:def-suite :aoc-2018 :in :aoc-all)
(5am:def-suite :aoc-2019 :in :aoc-all)
(5am:def-suite :aoc-2020 :in :aoc-all)
(5am:def-suite :aoc-2021 :in :aoc-all)
(5am:def-suite :aoc-2022 :in :aoc-all)
(5am:def-suite :aoc-2023 :in :aoc-all)
; DEFSUITE-MARKER - Do not edit this line.

(defun 5am-year-suite ()
  (intern (format nil "AOC-~4,'0D" (get-current-year)) 'keyword))

(defun 5am-day-suite ()
  (intern (format nil "AOC-~4,'0D-~2,'0D" (get-current-year) (get-current-day))))

(defun test (&optional suite)
  "Runs all tests for the given suite.  Default is the suite for the
current package."
  (5am:run! (or suite (5am-day-suite))))

(flet ((gen-answer-test (number answer)
         (let ((suite (5am-day-suite))
               (get-answer (intern (format nil "GET-ANSWER-~A" number))))
           `(5am:test (,(intern (format nil "ANSWER-~A" number))
                        :suite ,suite)
              (5am:is ,(cond
                        ((typep answer 'number)
                         `(= ,answer (,get-answer)))
                        ((typep answer 'string)
                         `(string= ,answer (,get-answer)))
                        (t
                         `(equalp ,answer (,get-answer)))))))))
  (defmacro define-day (answer-1 answer-2)
    "Sets up the test suites for the current day's package."
    (let ((suite (5am-day-suite)))
      `(progn
         (5am:def-suite ,suite :in ,(5am-year-suite))
         ,(when answer-1
                (gen-answer-test 1 answer-1))
         ,(when answer-2
                (gen-answer-test 2 answer-2))))))

(defmacro given (part-number &body body)
  "Sets up tests for the examples given in the day's problem."
  `(5am:test (,(intern (format nil "GIVEN-~D" part-number))
               :suite ,(5am-day-suite))
     ,@(loop for form in body
          collect `(5am:is ,form))))

(defmacro deftest (name &body body)
  "Defines a test, leaving all details up to the caller."
  `(5am:test (,name :suite ,(5am-day-suite))
     ,@body))


;;; Input and Parsing

(defun read-file (filename)
  (with-open-file (instream filename :direction :input)
    (let ((result (make-string (file-length instream))))
      (read-sequence result instream)
      result)))

(defun read-lines-from-file (filename)
  (let ((lines (iterate (for line in-file filename using #'read-line)
                        (collect line into lines)
                        (finally (return lines)))))
    (if (endp (cdr lines))
        (car lines)
        lines)))

(defun extract-ints (string)
  (let ((result (mapcar #'parse-integer
                        (ppcre:all-matches-as-strings "[-+]?\\d+" string
                                                      :sharedp t))))
    (if (endp (cdr result))
        (car result)
        result)))

(defun apply-parse-rule (parse-rule lines)
  (flet ((parse-line (line)
           (multiple-value-bind (result parsedp) (parseq:parseq parse-rule line)
             (when (not parsedp)
               (error (format nil "Unable to parse \"~A\" as ~A." line parse-rule)))
             result)))
    (if (consp lines)
        (mapcar #'parse-line lines)
        (parse-line lines))))

(defun user-agent-string ()
  "Phil! Gold's AoC Library (phil_g@pobox.com; https://gitlab.com/asciiphil/advent-of-code; Drakma; Common Lisp)")

(defun fetch-input (day year)
  "Gets the input for the given day and year.  You should probably use
`input' instead of this function, since `input' caches the downloaded
data."
  (let* ((session-cookie (make-instance 'drakma:cookie
                                        :name "session"
                                        :domain "adventofcode.com"
                                        :value (get-config-key *config-key-session*)))
         (cookie-jar (make-instance 'drakma:cookie-jar
                                    :cookies (list session-cookie)))
         (url (format nil "https://adventofcode.com/~A/day/~A/input" year day))
         (cache-file (merge-pathnames (format nil "input.~4,'0D-~2,'0D" year day)
                                      (get-cachedir))))
    (unless (probe-file cache-file)
      (ensure-directories-exist (get-cachedir))
      (multiple-value-bind (data return-code)
          (drakma:http-request url
                               :cookie-jar cookie-jar
                               :user-agent (user-agent-string))
        (assert (= 200 return-code)
                (day year)
                "Error code when fetching data for ~4,'0D-12-~2,'0D: ~A" year day data)
        (with-open-file (cache cache-file :direction :output)
          (write-string data cache))))
    cache-file))

(defun fetch-tk (filename)
  "Gets a file from tk's AOC 2021 Big Inputs."
  (let ((url (format nil "https://the-tk.com/files/aoc2021-bigboys/~A.in.xz" filename))
        (compressed-cache-file (merge-pathnames (format nil "tk.~A.xz" filename)
                                     (get-cachedir)))
        (cache-file (merge-pathnames (format nil "tk.~A" filename)
                                     (get-cachedir))))
    ;; It would be nice to automatically decompress the file, but the only
    ;; LZMA library in Quicklisp is cl-lzma.  Unfortunately, cl-lzma does
    ;; all decompression in memory, so it fails for larger files.  We work
    ;; around this by downloading to an intermediate file and then calling
    ;; the CLI `xz` program to decompress it.
    (unless (probe-file cache-file)
      (trivial-download:download url compressed-cache-file)
      (assert (zerop (nth-value 1 (external-program:run "/usr/bin/xz"
                                                        (list "-d" (uiop:native-namestring compressed-cache-file))
                                                        :output *standard-output*)))))
    cache-file))

;;; WARNING: This function works by using ASDF to get the root directory
;;; for the ADVENT-OF-CODE system.  It will only work if the system was
;;; loaded via ASDF.
(defun filename-relative-to-year (year filename)
  ;; Look what we have to do just to generically use a file relative to
  ;; another directory.
  (merge-pathnames filename
                   (asdf:system-relative-pathname 'advent-of-code
                                                  (format nil "~4,'0D/" year))))

(defun input (&key year day tk file parse parse-line)
  "Fetches and returns input for a problem.  Call with no parameters to
  fetch the Advent of Code input associated with the current package.


  ## Input Source Selection

  YEAR, DAY, TK, and FILE select the source of the input data.  DAY, TK,
  and FILE are mutually exclusive.

  If FILE is provided, it should be a pathname or namestring for a file
  existing on the system.  Relative pathnames will be resolved relative to
  the directory corresponding to the YEAR parameter.  If no YEAR parameter
  is proved, its value will be taken from the name of the current package.
  The contents of the file will be returned, after any requested
  parsing (for which see below).

  If TK is provided, it should be a string matching a filename from
  https://the-tk.com/project/aoc2021-bigboys.html *without* the .in.xz
  suffix.  The file will be downloaded, decompressed, and cached.  Its
  contents will be optionally parsed (see below) and returned.

  If DAY is provided, or if no source-selection-related parameters are
  provided, the official Advent of Code input for the given year and day
  will be downloaded and cached.  If YEAR and/or DAY is not provided,
  appropriate values will be extracted from the current package name.  The
  input will be optionally parsed (see below) and returned.


  ## Parsing

  If neither PARSE nor PARSE-LINE is provided, the data will be split at
  any newlines and returned as a list of strings.  If there are no
  newlines in the data, a single string will be returned (not in a list).

  If PARSE-LINE is provided, it should be a symbol for a parseq rule.  The
  input will be split at newlines (as above) and the parsing rule will be
  applied separately to each line.  If any line fails to parse, an error
  will be signaled.

  If PARSE is provided, it should be a symbol for a parseq rule.  The
  input will *not* be split into substrings.  The parseq rule will be
  applied to the input as a whole.  If parsing fails, an error will be
  signaled."
  (when (< 1 (count-if #'identity (list file tk day)))
    (error "FILE, DAY, and TK are mutually exclusive"))
  (when (and parse parse-line)
    (error "You cannot pass both PARSE and PARSE-LINE"))
  (let ((real-day  (or day  (get-current-day)))
        (real-year (or year (get-current-year))))
    (let ((input-file
            (cond
              (file (filename-relative-to-year real-year file))
              (tk (fetch-tk tk))
              (t (fetch-input real-day real-year)))))
      (cond
        (parse
         (multiple-value-bind (data parsedp) (parseq:parseq parse (read-file input-file))
           (if parsedp
               data
               (error (format nil "Unable to use rule ~A to parse data:~%~A" parse data)))))
        (parse-line
         (apply-parse-rule parse-line (read-lines-from-file input-file)))
        (t
         (read-lines-from-file input-file))))))

(defun submit (part answer &key day year)
  "Submits the answer for the given year, day, and part.  (DAY and YEAR
  are taken from the current package if not specified)."
  (let* ((real-day  (or day  (get-current-day)))
         (real-year (or year (get-current-year)))
         (url (format nil "https://adventofcode.com/~A/day/~A/answer" real-year real-day))
         (session-cookie (make-instance 'drakma:cookie
                                        :name "session"
                                        :domain "adventofcode.com"
                                        :value (get-config-key *config-key-session*)))
         (cookie-jar (make-instance 'drakma:cookie-jar
                                    :cookies (list session-cookie))))
    (multiple-value-bind (data return-code)
        (drakma:http-request
         url
         :method :post
         :parameters (list (cons "level" (format nil "~A" part))
                           (cons "answer" (format nil "~A" answer)))
         :cookie-jar cookie-jar
         :user-agent (user-agent-string))
      (assert (= 200 return-code)
              (real-day real-year part)
              "Error code when submitting answer for ~4,'0D-12-~2,'0D, part ~A: ~A"
              real-year real-day part data)
      (let ((docroot (lquery:$ (initialize data))))
        (lquery:$1 docroot "article" (render-text))))))

(parseq:defrule integer-string ()
    (and (? "-") (+ digit))
  (:string)
  (:function #'parse-integer))

;;; Handles patterns of the following forms:
;;;
;;;  * a
;;;  * a and b
;;;  * a and b and c
;;;  * a, b, and c
;;;  * a, b, c
;;;  * a, b and c
;;;
(parseq:defrule comma-list (subrule)
    (and (* (and subrule (and (or ", and" "," "," " and") (* " ")))) subrule)
  (:destructure (beginning last)
     (nconc (mapcar #'first beginning) (list last))))

(5am:test (comma-list :suite :aoc)
  (let ((pairs '(((1) "1")
                 ((1 2) "1 and 2")
                 ((1 2 3) "1 and 2 and 3")
                 ((1 2 3) "1, 2, and 3")
                 ((1 2 3) "1, 2, 3")
                 ((1 2 3) "1,2,3")
                 ((1 2 3) "1, 2 and 3")
                 ((1 2 3) "1,  2,  3"))))
    (iter (for (result input) in pairs)
          (5am:is (equal result (parseq:parseq '(comma-list integer-string) input))))))

(defun parse-grid-to-array (lines &key (key #'identity))
  "Parses a sequence of strings, where the lines form a 2D grid.  Returns a
  2D array of the values.  Note that the array will be row-major, with the
  y coordinate coming first.

  Will call KEY on each character to determine what value to put in the
  map."
  (let ((result (make-array (list (length lines) (length (first lines)))))
        (row 0))
    (dolist (line lines)
      (dotimes (col (length line))
        (setf (aref result row col) (funcall key (schar line col))))
      (incf row))
    result))

(defun parse-grid-to-map (lines &key (key #'identity))
  "Parses a sequence of strings, where the lines form a 2D grid.  Returns a
  map from points to values.

  Will call KEY on each character to determine what value to put in the
  map.  If KEY returns NIL, no entry will be made."
  (flet ((parse-line (line row)
           (reduce (lambda (result col)
                     (let ((value (funcall key (schar line col))))
                       (if value
                           (fset:with result
                                      (point:make-point col row)
                                      value)
                           result)))
                   (alexandria:iota (length line))
                   :initial-value (fset:empty-map))))
    (reduce #'fset:map-union (mapcar #'parse-line lines (alexandria:iota (length lines))))))

;;; Example Files

(defun read-example (&key year day filename)
  "Returns the contents of an example as a string.  Filenames are relative
  to the \"YEAR/examples/\" directory.  If no filename is given,
  \"DAY.txt\" will be used.  If year or day is needed but absent, they'll
  be derived from the current package."
  (let* ((real-year (or year (get-current-year)))
         (real-day (or day (get-current-day)))
         (year-path (asdf:system-relative-pathname 'advent-of-code (format nil "~A/" real-year)))
         (relative-filename (or filename (format nil "~2,'0D.txt" real-day)))
         (example-path (merge-pathnames (format nil "examples/~A" relative-filename) year-path)))
    (read-file example-path)))


;;; List Manipulation

(defun split-at (n list)
  (if (<= n 0)
      (list nil list)
      (destructuring-bind (head tail)
          (split-at (1- n) (cdr list))
        (list (cons (car list) head)
              tail))))


;;; FSet additions

(defun map-set-with (map key new-value)
  "If MAP contains sets, updates the set at KEY to also contain NEW-VALUE."
  (fset:with map key
             (fset:with (fset:lookup map key)
                        new-value)))

(defun rotate-seq (sequence &optional (count 1))
  (declare (type integer count))
  (cond
    ((zerop count)
     sequence)
    ((plusp count)
     (rotate-seq (fset:with-last (fset:less-first sequence) (fset:first sequence))
                 (1- count)))
    ((minusp count)
     (rotate-seq (fset:with-first (fset:less-last sequence) (fset:last sequence))
                 (1+ count)))))

(defun invert-map (map)
  "Inverts MAP.  If there are duplicate values, the result will be missing elements."
  (let ((result (fset:empty-map)))
    (fset:do-map (k v map)
      (setf (fset:lookup result v) k))
    result))

(defun arb-element (collection)
  "Returns an arbitrary element from any FSet collection"
  (if (or (typep collection 'fset:set)
          (typep collection 'fset:bag)
          (typep collection 'fset:map))
      (fset:arb collection)
      (fset:first collection)))

(defun iota (n &key (start 0) (step 1) (type 'list))
  "Like `aledandria:iota', collects N numbers from START, increasing by
  STEP.  Returns a value of type TYPE (which mush be something that
  `fset:convert' will recognize."
  (fset:convert type (alexandria:iota n :start start :step step)))


;;;; Matrixes

;;; It would be nice to use a library for this, but I ran into problems
;;; with all of the ones I found:
;;;  * lisp-matrix - no longer supported
;;;  * magicl - functions like matrix multiplication only worked for
;;;    floating-point matrixes, plus it didn't appear to have an arbitrary
;;;    precision matrix type
;;;  * numcl - Gave me wrong answers; I got too annoyed with it to try
;;;    figuring out why
;;;
;;; So here we are.

;;; Basic naive implementation.
(defun matrix-multiply (a b &optional result)
  "Multiplies matrixes A and B, returning a new matrix.  If RESULT is
  given, stores the result in that array rather than consing a new one."
  (cond
    ((not (= 2
             (array-rank a)
             (array-rank b)
             (array-rank (if result result a))))
     (error "Matrixes must be of rank 2."))
    ((/= (array-dimension a 1) (array-dimension b 0))
     (error "Matrixes are not compatible for multiplication."))
    ((not (and (eql (array-element-type a)
                    (array-element-type b))
               (eql (array-element-type a)
                    (array-element-type (if result result a)))))
     (error "Don't know how to mix array element types."))
    ((and result (not (and (= (array-dimension a 0) (array-dimension result 0))
                           (= (array-dimension b 1) (array-dimension result 1)))))
     (error "Result's dimensions do not match operands'.")))
  (let ((real-result (or result (make-array (list (array-dimension a 0) (array-dimension b 1))
                                            :element-type (array-element-type a)))))
    (let ((lp-channel (lparallel:make-channel)))
      (dotimes (i (array-dimension a 0))
        (dotimes (j (array-dimension b 1))
          (lparallel:submit-task lp-channel
                                 (lambda (a b i j)
                                   (let ((sum 0))
                                     (dotimes (k (array-dimension a 1))
                                       (incf sum
                                             (* (aref a i k)
                                                (aref b k j))))
                                     (list i j sum)))
                                 a b i j)))
      (dotimes (n (* (array-dimension a 0)
                     (array-dimension b 1)))
        (destructuring-bind (i j sum) (lparallel:receive-result lp-channel)
          (setf (aref real-result i j) sum))))
    real-result))

(defun matrix-add (a b &optional result)
  "Adds the cells of matrixes A and B, returning a new matrix.  If RESULT
  is given, stores the result in that array rather than consing a new
  one."
  (cond
    ((not (= 2
             (array-rank a)
             (array-rank b)
             (array-rank (if result result a))))
     (error "Matrixes must be of rank 2."))
    ((or (/= (array-dimension a 0) (array-dimension b 0))
         (/= (array-dimension a 1) (array-dimension b 1)))
     (error "Matrixes are not compatible for addition."))
    ((not (and (eql (array-element-type a)
                    (array-element-type b))
               (eql (array-element-type a)
                    (array-element-type (if result result a)))))
     (error "Don't know how to mix array element types."))
    ((and result (not (and (= (array-dimension a 0) (array-dimension result 0))
                           (= (array-dimension a 1) (array-dimension result 1)))))
     (error "Result's dimensions do not match operands'.")))
  (let ((real-result (or result (make-array (list (array-dimension a 0) (array-dimension a 1))
                                            :element-type (array-element-type a)))))
    (dotimes (i (array-dimension a 0))
      (dotimes (j (array-dimension b 1))
        (setf (aref real-result i j)
              (+ (aref a i j)
                 (aref b i j)))))
    real-result))

(defun matrix-eye (n &key (type t))
  "Returns an identity matrix of size N."
  (let ((result (make-array (list n n)
                            :element-type type
                            :initial-element 0)))
    (dotimes (i n)
      (setf (aref result i i) 1))
    result))

(defun matrix-exp (a power)
  "Raises matrix A to the power POWER.  A must be square."
  (cond
    ((minusp power)
     (error "Only positive powers are supported."))
    ((not (= 2 (array-rank a)))
     (error "Matrix is not rank 2."))
    ((not (= (array-dimension a 0) (array-dimension a 1)))
     (error "Matrix is not square.")))
  (labels ((matrix-exp-r (b e a)
             (if (zerop e)
                 a
                 (matrix-exp-r (matrix-multiply b b)
                                (truncate e 2)
                                (if (oddp e)
                                    (matrix-multiply b a)
                                    a)))))
    (matrix-exp-r a power (matrix-eye (array-dimension a 0)))))


;;;; Miscellaneous

(defmacro defmemo (name params &body body)
  "Creates a memoized function."
  (alexandria:with-gensyms (memos result foundp)
    `(let ((,memos (fset:empty-map)))
       (defun ,name ,params
         (multiple-value-bind (,result ,foundp)
             (fset:lookup ,memos (list ,@params))
           (if ,foundp
               (progn
                 ,result)
               (let ((,result (progn ,@body)))
                 (fset:adjoinf ,memos (list ,@params) ,result)
                 ,result)))))))

(defmacro-driver (FOR var DIGITS-OF number
                  &optional BASE (base 10) FROM (direction 'right))
  "Each digit of a number.  DIRECTION is either LEFT or RIGHT and indicates
   the direction in which the number's digits will be processed.  RIGHT is
   slightly more efficient."
  (when (and (string/= "RIGHT" (symbol-name direction))
             (string/= "LEFT" (symbol-name direction)))
    (error "DIRECTION must be either LEFT or RIGHT, not ~A." direction))
  (let ((b (if (integerp base)
               base
               (gensym "BASE")))
        (n (gensym "N"))
        (divisor (if (and (integerp base)
                          (string= "RIGHT" (symbol-name direction)))
                     base
                     (gensym "DIVISOR")))
        (init-n (if (integerp number)
                    number
                    (gensym "INIT-N")))
        (quotient (gensym "QUOTIENT"))
        (remainder (gensym "REMAINDER"))
        (kwd (if generate 'generate 'for))
        (from-right (string= "RIGHT" (symbol-name direction))))
    `(progn
       ,(when (symbolp b)
          `(with ,b = ,base))
       (with ,n = nil)
       ,(when (symbolp init-n)
         `(with ,init-n = ,number))
       ,(when (symbolp divisor)
         `(with ,divisor = ,(if from-right
                                b
                                `(expt ,b (floor (log ,init-n ,b))))))
       (,kwd ,var next (progn
                         (when (and ,n (zerop ,(if from-right
                                                   n
                                                   divisor)))
                           (terminate))
                         (multiple-value-bind (,quotient ,remainder)
                             (truncate (or ,n ,init-n)
                                       ,divisor)
                           ,(when (not from-right)
                                  `(setf ,divisor (truncate ,divisor ,b)))
                           (setf ,n ,(if from-right
                                         quotient
                                         remainder))
                           ,(if from-right
                                remainder
                                quotient)))))))

(defun digit-vector (num &optional (base 10))
  (declare (type (integer 0) num)
           (type (integer 2 36) base))
  (coerce (digit-list num base) 'simple-vector))

(defun digit-list (num &optional (base 10))
  (declare (type (integer 0) num)
           (type (integer 2 36) base))
  (iter (for d digits-of num base base)
        (declare (type (integer 0 35) d))
        (collecting d at beginning)))

(defun join-digit-list (list &optional (base 10))
  (iter (for digit in-sequence list)
        (for result first digit then (+ (* result base) digit))
        (finally (return result))))

(defun row-order< (complex-1 complex-2)
  (or (< (imagpart complex-1) (imagpart complex-2))
      (and (= (imagpart complex-1) (imagpart complex-2))
           (< (realpart complex-1) (realpart complex-2)))))

(defun vector-shared-subseq (vector start &optional end)
  "Returns a subsequence of VECTOR that shares structure with VECTOR."
  (let ((real-end (or end (length vector))))
    (make-array (- real-end start)
                :element-type (array-element-type vector)
                :displaced-to vector
                :displaced-index-offset start)))

(defun flatten-array (array)
  (make-array (array-total-size array)
              :element-type (array-element-type array)
              :displaced-to array))

(defun make-keyword (string)
  "Returns the :keyword equivalent of STRING."
  (intern (string-upcase string) 'keyword))

(defun sum-from-to (a b)
  "Inclusive."
  (declare (type integer a b))
  (if (< b a)
      0
      (/ (* (1+ (- b a))
            (+ a b))
         2)))

(defun distinct-map-union (map1 map2)
  (assert (fset:disjoint? (fset:domain map1)
                          (fset:domain map2))
          ()
          "Duplicate map keys: ~A" (fset:intersection (fset:domain map1)
                                                      (fset:domain map2)))
  (fset:map-union map1 map2))

(defun cref (array complex-number)
  "Use a complex number to index a two-dimensional array."
  (aref array (imagpart complex-number) (realpart complex-number)))

(defun (setf cref) (new-value array complex-number)
  (setf (aref array (imagpart complex-number) (realpart complex-number)) new-value))

(defun manhattan-distance (complex-number &optional (reference 0))
  (+ (abs (- (realpart complex-number) (realpart reference)))
     (abs (- (imagpart complex-number) (imagpart reference)))))

(defstruct bbox
  left
  bottom
  top
  right)

(defun find-bbox (points)
  "POINTS should be a list of complex numbers."
  (iter (for point in points)
        (minimizing (realpart point) into left)
        (minimizing (imagpart point) into bottom)
        (maximizing (realpart point) into right)
        (maximizing (imagpart point) into top)
        (finally (return (make-bbox :left left :right right
                                    :top top :bottom bottom)))))

(defun print-2d-hash-table (hash-table element-char-fn &key (stream t) (unknown #\ ) highlight)
  "Prints a representation of HASH-TABLE to STREAM.  The keys of
  HASH-TABLE should be complex numbers, with the real part giving the
  column (increasing to the right) and the imaginary part giving the
  row (increasing *down*).

  ELEMENT-CHAR-FN should be a function of one argument.  It will be called
  for each value of HASH-TABLE.  It should return a character, though
  anything that can be passed to FORMAT will work.

  UNKNOWN will be used for positions that are not present in HASH-TABLE.

  HIGHLIGHT is a position that is to be marked in some way to make it stand out."
  (let ((bbox (find-bbox (alexandria:hash-table-keys hash-table))))
    (iter (for row from (bbox-bottom bbox) to (bbox-top bbox))
          (iter (for col from (bbox-left bbox) to (bbox-right bbox))
                (for position = (complex col row))
                (for (values element foundp) = (gethash position hash-table))
                (format stream "~A" (if foundp
                                        (funcall element-char-fn element)
                                        unknown))
                (when (and highlight (= highlight position))
                  (princ #\U20DD stream)))
          (format stream "~%")))
  (values))


;;;; Bitmap output using braille

(defparameter +braille-offsets+
  (vector (point:make-point 0 0)
          (point:make-point 0 1)
          (point:make-point 0 2)
          (point:make-point 1 0)
          (point:make-point 1 1)
          (point:make-point 1 2)
          (point:make-point 0 3)
          (point:make-point 1 3)))

(defun braille-char-at-point (origin point-max predicate-fn)
  (iter (with result = 0)
        (for offset in-vector +braille-offsets+ downto 0)
        (for point = (point:+ origin offset))
        (when (and (point:< point point-max)
                   (funcall predicate-fn point))
          (incf result))
        (setf result (* 2 result))
        (finally (return (code-char (+ #x2800 (truncate result 2)))))))

(defun print-braille (width height predicate-fn &key (stream t))
  "Prints a single-color bitmap using braille characters to STREAM.
  PREDICATE-FN will be passed a POINT.  It should return a true value if a
  dot should be drawn at the passed POINT.  The X values of the point will
  range from 0 to WIDTH-1.  The Y values will range from 0 to HEIGHT-1."
  (iter (for y from 0 below height by 4)
        (iter (for x from 0 below width by 2)
              (princ (braille-char-at-point (point:make-point x y)
                                            (point:make-point width height)
                                            predicate-fn)
                     stream))
        (princ #\Newline stream)))

(defun print-set-braille (points &key (stream t))
  "Convenience function to wrap `print-braille' for a set of points."
  (multiple-value-bind (min-point max-point) (point:bbox points)
    (let ((dims (point:+ #<1 1> (point:- max-point min-point))))
      (print-braille (point:x dims) (point:y dims)
                     (lambda (point)
                       (fset:lookup points (point:+ point min-point)))
                     :stream stream))))

(defun print-map-braille (map &key (key #'identity) (stream t))
  "Convenience function to wrap `print-braille' for an FSet map.  The
  domain of MAP should be points.  KEY will be passed a value and should
  return true for pixels that are on and false for pixels that are
  off."
  (multiple-value-bind (min-point max-point) (point:bbox (fset:domain map))
    (let ((dims (point:+ #<1 1> (point:- max-point min-point))))
      (print-braille (point:x dims) (point:y dims)
                     (lambda (point)
                       (funcall key (fset:lookup map (point:+ point min-point))))
                     :stream stream))))

(defun print-array-braille (array &key (key #'identity) (stream t))
  "Convenience function to wrap `print-braille' for a 2D array.  KEY
  should return true for pixels that are on and false for pixels that are
  off.  (#'plusp works well for arrays of bits.)"
  (print-braille (array-dimension array 1)
                 (array-dimension array 0)
                 (lambda (point)
                   (funcall key (point:aref array point)))
                 :stream stream))


;;;; SVG


(defun show-svg (svg-string)
  "Displays the SVG in SVG-STRING in the SLIME REPL in Emacs."
  (swank:eval-in-emacs `(slime-media-insert-image (create-image ,svg-string 'svg t) "svg"))
  (values))


(defmacro with-svg-surface ((surface width height) &body body)
  "Creates an anonymous Cairo SVG surface (not backed by a file).  SURFACE
  should be a symbol used in BODY to reference the surface.

  Returns the SVG as a string."
  (alexandria:with-gensyms ((width-sym "WIDTH")
                            (height-sym "HEIGHT")
                            svgfile)
    `(let ((,width-sym ,width)
           (,height-sym ,height))
       (cl-fad:with-open-temporary-file (,svgfile :direction :io)
         (cairo:with-surface (,surface (cairo:create-svg-surface ,svgfile ,width-sym ,height-sym))
           ,@body)
         (file-position ,svgfile 0)
         (read-file ,svgfile)))))

(defparameter *default-svg-grid-width* 512)
(defparameter *default-svg-grid-format*
  '(:shape :square :color :dark-primary :rotation 0 :font-color :dark-secondary))

(defun calculate-cell-size (width)
  "WIDTH is in cells."
  (max 1 (truncate *default-svg-grid-width* width)))

(defun svg-get-format (default-format format key)
  (or (getf format key)
      (getf default-format key)
      (getf *default-svg-grid-format* key)))

(defun svg-draw-cell-shape (shape)
  "Draws the shape in a 1x1 square centered on the origin."
  (ecase shape
    (:square (cairo:rectangle -1/2 -1/2 1 1))
    (:circle (cairo:arc 0 0 1/2 0 (* 2 pi)))
    (:triangle (progn
                 (cairo:move-to 0.5 0)
                 (cairo:line-to -0.36  0.5)
                 (cairo:line-to -0.36 -0.5)
                 (cairo:close-path)))
    (:line (cairo:rectangle -1/2 -1/8 1 1/4))
    (:corner (progn
               (cairo:move-to -1/8 -1/2)
               (cairo:line-to -1/8 1/8)
               (cairo:line-to 1/2 1/8)
               (cairo:line-to 1/2 -1/8)
               (cairo:line-to 1/8 -1/8)
               (cairo:line-to 1/8 -1/2)
               (cairo:close-path))))
  (cairo:fill-path))

(defun svg-draw-text (string)
  "Centers the text on the origin."
  (cairo:save)
  (multiple-value-bind (x-bearing y-bearing width height) (cairo:text-extents string)
    (let* ((max-extent (max width height))
           (scale (if (<= max-extent 1)
                      1
                      (/ max-extent))))
      (cairo:scale scale scale)
      (cairo:move-to (- 0 (/ width 2) x-bearing)
                     (- 0 (/ height 2) y-bearing)))
    (cairo:show-text string))
  (cairo:restore))

(defun svg-draw-cell (default-format point format)
  "Assumes a Cairo context is already set up."
  (when format
    (let ((real-format (if (consp format)
                           format
                           nil)))
      (cairo:save)
      (cairo:translate (+ 1/2 (point:x point))
                       (+ 1/2 (point:y point)))
      (let ((rotation (svg-get-format default-format real-format :rotation)))
        (when rotation
          (cairo:rotate (cairo:deg-to-rad rotation))))
      (viz:set-color (svg-get-format default-format real-format :color))
      (svg-draw-cell-shape (svg-get-format default-format real-format :shape))
      (let ((character (if (or (consp format)
                               (typep format 'boolean))
                           (svg-get-format default-format real-format :character)
                           (or (getf real-format :character)
                               format))))
        (when character
          (viz:set-color (svg-get-format default-format real-format :font-color))
          (cairo:set-font-size 1)
          (svg-draw-text (format nil "~A" character))))
      (cairo:restore))))

(defun draw-svg-grid (width height cell-fn &key cell-size (display t) format)
  "Renders tabular data into an SVG.  WIDTH and HEIGH should be the
  dimensions of the data.  CELL-SIZE gives the size of each cell in the
  SVG; if unset, the program will try to pick a reasonable value.

  If DISPLAY is true (the default), the SVG will be displayed in the SLIME
  REPL.  Otherwise, the SVG will be returned as a string.

  CELL-FN should be a function of one argument.  It will be passed a
  point.  If it returns a true value, something will be drawn at that
  point.

  If CELL-FN returns a list, it should be a plist, with one or more of the
  following keys:
   * :shape - One of the following:
     * :square
     * :circle
     * :triangle, which points to the right
     * :line, running left to right
     * :corner, a right angle line connecting the top and right sides of the cell
   * :color - a keyword understood by `viz:set-color'
   * :rotation - in degrees
   * :character - a character to draw on top of the cell
   * :font-color - the color to use for the character, if any

  If CELL-FN returns a true value that is not T or a list, the value will
  be drawn on top of the cell as if it had been given as a :character
  directive.  Note that long strings will be scaled down to fit in the
  cell.  This may affect readability.

  FORMAT gives the format directives (the same as CELL-FN's return values)
  to use as the defaults."
  (let* ((real-cell-size (or cell-size (calculate-cell-size width)))
         (image-width (* width real-cell-size))
         (image-height (* height real-cell-size)))
    (let ((svg
            (with-svg-surface (surface image-width image-height)
              (viz:clear-surface surface :dark-background)
              (cairo:with-context-from-surface (surface)
                (cairo:scale real-cell-size real-cell-size)
                (dotimes (y height)
                  (dotimes (x width)
                    (let ((point (point:make-point x y)))
                      (svg-draw-cell format point (funcall cell-fn point)))))))))
      (if display
          (show-svg svg)
          svg))))


(defun draw-set-svg-grid (points &key (display t) format)
  "Convenience function to wrap `draw-svg-grid' for a set of points.

  FORMAT will give the formatting for each point.  See `draw-svg-grid' for
  its structure."
  (multiple-value-bind (min-point max-point) (point:bbox points)
    (let ((dims (point:+ #<1 1> (point:- max-point min-point))))
      (draw-svg-grid (point:x dims) (point:y dims)
                     (lambda (point)
                       (fset:lookup points (point:+ point min-point)))
                     :display display
                     :format format))))

(defun draw-map-svg-grid (map &key (key #'identity) (display t) format)
  "Convenience function to wrap `draw-svg-grid' for an FSet map.  The
  domain of MAP should be points.  KEY will be passed a value and should
  return true for pixels to be drawn.  As with `draw-svg-grid', KEY can
  return a list of format directives."
  (multiple-value-bind (min-point max-point) (point:bbox (fset:domain map))
    (let ((dims (point:+ #<1 1> (point:- max-point min-point))))
      (draw-svg-grid (point:x dims) (point:y dims)
                     (lambda (point)
                       (funcall key (fset:lookup map (point:+ point min-point))))
                     :display display
                     :format format))))

(defun draw-array-svg-grid (array &key (key #'identity) (display t) format)
  "Convenience function to wrap `draw-svg-grid' for a 2D array.  KEY
  should return true for pixels that are on and false for pixels that are
  off.  (#'plusp works well for arrays of bits.)

  FORMAT will give the formatting for each point.  See `draw-svg-grid' for
  its structure."
  (draw-svg-grid (array-dimension array 1)
                 (array-dimension array 0)
                 (lambda (point)
                   (funcall key (point:aref array point)))
                 :display display
                 :format format))


;;;; Shortest Path

(defun shortest-path (start option-fn &key end finishedp test heuristic)
  "Finds the shortest path from START to END.  OPTION-FN should be a
  function that accepts a state and returns a list of `(cost state)` pairs
  signifying next moves from the given state.  Returns two values: a list
  of states from START to END, and the total cost of the path.  TEST
  determines how the states are compared, in case `fset:compare' is
  insufficient.  It should be a function that receives two states as
  parameters and returns a true value if the states are equal.  The
  optional HEURISTIC is a function that is called with a state and returns
  an estimate of the minimum cost from that state to END.

  In place of END, you can give FINISHEDP, which is a function that will
  be called on each state.  It should return true if the state is the end
  of the path and false otherwise.

  The states (START and the values returned from OPTION-FN) must be usable
  in FSet collections.  If they're not natively supported by FSet, you'll
  need to make sure you've defined `fset:compare' for them."
  (when (and (not end)
             (not finishedp))
    (error "Must give either END or FINISHEDP."))
  (when (and end finishedp)
    (error "Cannot give both END and FINISHEDP."))
  (let ((finished-fn (or finishedp
                         (if test
                             (alexandria:curry test end)
                             (lambda (state)
                               (eq :equal (fset:compare end state)))))))
    (if (funcall finished-fn start)
        (values (list start) 0)
        (let ((visited (fset:map (start (list start))))
              (options (fpq:empty-queue :pred #'<=
                                        :key (if heuristic
                                                 (lambda (edge)
                                                   (destructuring-bind (cost from-node to-node) edge
                                                     (declare (ignore from-node))
                                                     (+ cost (funcall heuristic to-node))))
                                                 #'first))))
          (find-shortest-path
           finished-fn option-fn visited
           (shortest-path-add-edges 0 options start (funcall option-fn start)))))))

(defun find-shortest-path (finished-fn option-fn visited options)
  (if (fpq:empty? options)
      (values nil nil)
      (multiple-value-bind (new-options edge) (fpq:delete-root options)
        (destructuring-bind (cost from-node to-node) edge
          (cond
            ((fset:lookup visited to-node)
             (find-shortest-path finished-fn option-fn visited new-options))
            ((funcall finished-fn to-node)
             (values (reverse (cons to-node (fset:lookup visited from-node)))
                     cost))
            (t
             (let ((next-visited (fset:with visited to-node
                                            (cons to-node (fset:lookup visited from-node))))
                   ;; Even though the algorithm skips visited edges
                   ;; elsewhere, removing them here cuts down on the work
                   ;; needed down the road.  But!  Removing them here
                   ;; doesn't remove the need to check for them later, too;
                   ;; a node may be visited between being added to the
                   ;; options and being extracted as the next candidate.
                   (next-edges (remove-if (lambda (edge) (fset:lookup visited (second edge)))
                                          (funcall option-fn to-node))))
               (find-shortest-path
                finished-fn option-fn next-visited
                (shortest-path-add-edges cost new-options to-node next-edges)))))))))

(defun shortest-path-add-edges (base-cost options start-node edges)
  "Adds all of EDGES to OPTIONS."
  (reduce (lambda (result edge)
            (destructuring-bind (cost node) edge
              (fpq:insert result
                          (list (+ base-cost cost)
                                start-node
                                node))))
          edges
          :initial-value options))


;;;; Permutations

(defgeneric visit-subsets (visit-fn collection subset-size)
  (:documentation
   "VISIT-FN will be called once for each subset.  It will be passed an
   FSet bag."))

(defmethod visit-subsets (visit-fn (collection fset:seq) subset-size)
  (declare (type function visit-fn)
           (type (integer 0) subset-size))
  (labels ((visit-r (remaining-items remaining-size result)
             (if (zerop remaining-size)
                 (funcall visit-fn result)
                 (fset:do-seq (element remaining-items :index i :end (1+ (- (fset:size remaining-items) remaining-size)))
                   (visit-r (fset:subseq remaining-items (1+ i))
                            (1- remaining-size)
                            (fset:with result element))))))
    (visit-r collection subset-size (fset:empty-bag))
    (values)))

(defmethod visit-subsets (visit-fn (collection fset:set) subset-size)
  (declare (type function visit-fn)
           (type (integer 0) subset-size))
  (assert nil nil "FIXME: `visit-subsets' on a set visits a number of combinations more than once.")
  (labels ((visit-r (remaining-items remaining-size result)
             (if (zerop remaining-size)
                 (funcall visit-fn result)
                 (fset:do-set (element remaining-items)
                   (visit-r (fset:less remaining-items element)
                            (1- remaining-size)
                            (fset:with result element))))))
    (visit-r collection subset-size (fset:empty-bag))
    (values)))

(defmethod visit-subsets (visit-fn (collection vector) subset-size)
  (declare (type function visit-fn)
           (type (integer 0) subset-size))
  (labels ((visit-r (remaining-items remaining-size result)
             (if (zerop remaining-size)
                 (funcall visit-fn result)
                 (iter (for element
                            in-vector remaining-items
                            with-index i
                            to (- (length remaining-items) remaining-size))
                   (visit-r (vector-shared-subseq remaining-items (1+ i))
                            (1- remaining-size)
                            (fset:with result element))))))
    (visit-r collection subset-size (fset:empty-bag))
    (values)))

;;; FIXME: For legacy reasons, this specialization passes a list to
;;; VISIT-FN, not a bag.  It and all of its callers should be updated.
(defmethod visit-subsets (visit-fn (sequence cons) subset-size)
  (declare (type function visit-fn)
           (type (integer 0) subset-size))
  (labels ((visit-r (remaining-items remaining-size result)
             (if (zerop remaining-size)
                 (funcall visit-fn result)
                 (mapl (lambda (subset-remaining-items)
                         (visit-r (cdr subset-remaining-items)
                                  (1- remaining-size)
                                  (cons (car subset-remaining-items) result)))
                       remaining-items))))
    (visit-r sequence subset-size nil)
    (values)))


;;; Uses Knuth's Algorithm L from TAOCP Volume 4, Chapter 7.2.1.2.
(defun visit-permutations-lexicographic (visit-func sequence &key (< #'<) (copy t))
  "VISIT-FUNC will be called once for each permutation of SEQUENCE.  The
   permutations will be visited in lexicographic order.  < is used for
   comparisons.  Sequences with equal elements are handled properly.

  If your sequence does not have repeated elements and the permutations do
  not need to be visited in lexicographic order, please see
  `visit-permutations', which is slightly more efficient in those cases."
  (declare (type (function (simple-vector)) visit-func)
           (type sequence sequence)
           (type (function (* *)) <))
  (let ((n (1- (length sequence)))
        (work-seq (make-array (length sequence) :initial-contents (sort (copy-seq sequence) <))))
    (declare (type (integer 0 #.(1- array-dimension-limit)) n)
             (type simple-vector work-seq))
    (labels ((find-j (i)
               (declare (type (integer 0 #.(- array-dimension-limit 2)) i))
               (if (funcall < (svref work-seq i) (svref work-seq (1+ i)))
                   i
                   (if (plusp i)
                       (find-j (1- i))
                       nil)))
             (find-l (aj i)
               (declare (type (integer 0 #.(1- array-dimension-limit)) i))
               (if (funcall < aj (svref work-seq i))
                   i
                   (find-l aj (1- i))))
             (reverse-section (k l)
               (declare (type (integer 0 #.(1- array-dimension-limit)) k l))
               (when (< k l)
                 (rotatef (svref work-seq k) (svref work-seq l))
                 (reverse-section (1+ k) (1- l))))
             (visit-r ()
               (funcall visit-func
                        (if copy
                            (coerce (copy-seq work-seq) (type-of sequence))
                            work-seq))
               (let ((j (find-j (1- n))))
                 (when j
                   (let ((l (find-l (svref work-seq j) n)))
                     (rotatef (svref work-seq j) (svref work-seq l))
                     (reverse-section (1+ j) n)
                     (visit-r))))))
      (visit-r)))
  (values))

;;; Based on the plain-change permutation algorithm from TAOCP Volume 4A
;;; §7.2.1.2.
;;;
;;; The essence of the algorithm is that you can work your way through all
;;; n! permutations of a set of n elements by making n!-1 swaps of
;;; adjacent elements, if you do the swaps in the right order.  Obviously,
;;; this is very efficient, since you're not moving much data around in
;;; memory and the amount of work between each pair of permutations is
;;; minimized.
;;;
;;; The algorithm operates mostly recursively.  It considers _inversions_
;;; of the elements to be the number of times each element has been
;;; shifted to the left relative to its original position.  With this
;;; definition, each element has a maximum inversion level equal to its
;;; zero-based index within the original sequence.
;;;
;;; Each inversion change can only happen once the element's successor has
;;; gone through a complete sequence of inversion changes, resulting in it
;;; either being fully inverted or fully uninverted.  Once an element has
;;; gone through a complete cycle, it passes its successor through one
;;; last complete cycle.  Thus, the recursive algorithm for inverting
;;; element j is, roughly:
;;;
;;;  * Fully invert element j+1
;;;  * Move element j one step closer toward full inversion.
;;;  * Fully uninvert element j+1
;;;  * If element j is not fully inverted:
;;;    * Move element j one step closer to full inversion
;;;    * Fully invert element j+1
;;;    * If element j is not fully inverted:
;;;      * Move element j one step closer to full inversion
;;;      * Fully uninvert element j+1
;;;      * etc.
;;;
;;; Uninverting the element follows roughly the same process.  If the last
;;; step of inversion was to invert element j+1, then the uninverson of
;;; element j must start with the _uninversion_ of element j+1.
;;;
;;; One wrinkle while shifting the position of an element is that its
;;; expected position in the vector might be offset by elements normally
;;; to its right that have been fully inverted and are now on its left.
;;; Fortunately a few properties make this easy to deal with:
;;;
;;;  * The last step undertaken by the uninversion of element j is to
;;;    uninvert element j+1.  This means that after an uninversion the
;;;    offset is always 0.
;;;  * Elements in odd-numbered positions (zero-based) always end their
;;;    inversion processes by uninverting their successors, so they only
;;;    apply an offset of 1 to their predecessors when they're fully
;;;    inverted.
;;;  * Elements in even-numbered positions (zero-based) always end their
;;;    inversion process by inverting their successors.  That means they
;;;    add one to the offset given by their successors.  Since their
;;;    successors are odd-numbered, their predecessors will always receive
;;;    an offset of 2.
;;;  * An exception is the penultimate element in the list.  Its offset
;;;    can only ever be 1, because there's only one element that might be
;;;    inverted past it.
;;;
;;; All told, that leads to the following rules:
;;;
;;;  * After uninverting its successor, an element's offset is always 0.
;;;  * The final element's offset is always 0.
;;;  * After inverting its successor, an element's offset is:
;;;    * 2 if the element's position is even _and_ it's not the penultimate
;;;      element, and
;;;    * 1 otherwise.
;;;
;;; In the code below inversion is referred to as "descent" and
;;; uninversion is referred to as "ascent".  Mostly because "uninversion"
;;; is a weird-sounding word.
;;;
(defun visit-permutations (visit-func sequence &key (copy t))
  "VISIT-FUNC will be called once for each permutation of SEQUENCE.  Order
  of permutations is not guaranteed.  All elements of SEQUENCE must be
  distinct; otherwise, duplicate permutations will be visited.  (For
  sequences with repeated elements, see `visit-permutations-lexicographic'.)

  If COPY is false, VISIT-FUNC will be passed the simple vector used
  internally by this function.  This is much faster than copying it, but
  VISIT-FUNC *must not* modify the vector.  If COPY is true (the default),
  VISIT-FUNC will be passed a sequence of the same type as SEQUENCE."
  (let* ((n (length sequence))
         (result (make-array n :initial-contents sequence)))
    (labels ((visit ()
               (funcall visit-func
                        (if copy
                            (coerce (copy-seq result) (type-of sequence))
                            result)))
             (inner-descent ()
               (do ((j (1- n) (1- j)))
                   ((zerop j))
                 (visit)
                 (rotatef (svref result j) (svref result (1- j)))))
             (inner-ascent ()
               (do ((j 1 (1+ j)))
                   ((<= n j))
                 (visit)
                 (rotatef (svref result j) (svref result (1- j)))))
             (make-middle (direction position next-descent next-ascent)
               (let ((offset (if (and (oddp position) (< position (- n 2)))
                                 2
                                 1)))
                 (if (eq direction :descent)
                     (lambda ()
                       (do ((j position (1- j))
                            (count 0 (1+ count)))
                           ((zerop j))
                         (if (evenp count)
                             (progn
                               (funcall next-descent)
                               (visit)
                               (rotatef (svref result (+ offset j))
                                        (svref result (+ offset j -1))))
                             (progn
                               (funcall next-ascent)
                               (visit)
                               (rotatef (svref result j)
                                        (svref result (1- j))))))
                       (if (evenp position)
                           (funcall next-descent)
                           (funcall next-ascent)))
                     (lambda ()
                       (do ((j 1 (1+ j))
                            (count (if (evenp position)
                                       1
                                       0)
                                   (1+ count)))
                           ((< position j))
                         (if (evenp count)
                             (progn
                               (funcall next-descent)
                               (visit)
                               (rotatef (svref result (+ offset j))
                                        (svref result (+ offset j -1))))
                             (progn
                               (funcall next-ascent)
                               (visit)
                               (rotatef (svref result j)
                                        (svref result (1- j))))))
                       (funcall next-ascent)))))
             (make-outer (next-descent next-ascent)
               (let ((j (if (= n 3)
                            2
                            3)))
                 (lambda ()
                   (funcall next-descent)
                   (visit)
                   (rotatef (svref result j)
                            (svref result (1- j)))
                   (funcall next-ascent)
                   (visit))))
             (build-func-tree (position next-descent next-ascent)
               (cond
                 ((null next-ascent)
                  (build-func-tree (1- position) #'inner-descent #'inner-ascent))
                 ((= position 1)
                  (make-outer next-descent next-ascent))
                 (t
                  (let ((new-descent (make-middle :descent position next-descent next-ascent))
                        (new-ascent (make-middle :ascent position next-descent next-ascent)))
                    (build-func-tree (1- position) new-descent new-ascent))))))
      (declare (inline visit))
      (case n
        ((0 1)
         (visit))
        (2
         (visit)
         (rotatef (svref result 1) (svref result 0))
         (visit))
        (t
         (funcall (build-func-tree (1- n) nil nil))))
      (values))))

;;; Based on TAOCP Volume 4A §7.2.1.4 Algorithm H.
(defun visit-n-integer-partitions (visit-func num-partitions sum &key (copy t))
  "Calls VISIT-FUNC once for every distinct division of SUM into
  NUM-PARTITIONS integer pieces.  VISIT-FUNC will be passed a vector.  If
  COPY is true, the vector will be a copy of the working vector.  Making
  COPY false is faster, but in that case VISIT-FUNC *must not* modify the
  vector and must assume that the vector will change after VISIT-FUNC
  returns."
  (if (or (< sum num-partitions)
          (< num-partitions 2))
      (error "Requirement: (>= SUM NUM-PARTITIONS 2)"))
  (let ((a (make-array num-partitions :element-type '(integer 1) :initial-element 1)))
    (setf (svref a 0) (1+ (- sum num-partitions)))
    (labels ((visit ()
               (funcall visit-func
                        (if copy
                            (copy-seq a)
                            a)))
             (visit-first-two ()
               (visit)
               (when (< (svref a 1) (1- (svref a 0)))
                 (decf (svref a 0))
                 (incf (svref a 1))
                 (visit-first-two)))
             ;; We *could* calculate j and s separately, but that would
             ;; involve two loops across the vector, so why not just get
             ;; them both in the same traversal?
             (find-j-and-s (ref j s)
               (cond
                 ((<= num-partitions j)
                  nil)
                 ((< (svref a j) ref)
                  (values j s))
                 (t
                  (find-j-and-s ref (1+ j) (+ s (svref a j))))))
             (visit-r ()
               (visit-first-two)
               (multiple-value-bind (j s)
                   (find-j-and-s (1- (svref a 0)) 2 (+ -1 (svref a 0) (svref a 1)))
                 (when j
                   (incf (svref a j))
                   (let ((x (svref a j)))
                     (setf (svref a 0) (- s (* x (1- j))))
                     (do ((jp 1 (1+ jp)))
                         ((= jp j))
                       (setf (svref a jp) x)))
                   (visit-r)))))
      (declare (inline visit))
      (visit-r)
      (values))))
