(in-package :aoc-2016-19)

(aoc:define-day 1816277 1410967)


;;;; Input

(defparameter *elf-count* (aoc:input :parse-line 'aoc:integer-string))


;;;; Part 1

;;; Each elf is a cons with the car being the elf number and the cdr being
;;; the number of presents the elf has.

(defun init-elves (elf-count)
  (labels ((init-r (queue elf-number)
             (if (< elf-count elf-number)
                 queue
                 (init-r (queue:snoc queue (cons elf-number 1))
                         (1+ elf-number)))))
    (init-r (queue:make-queue) 1)))

(defun take-turn (queue)
  (let ((next-queue (queue:tail queue)))
    (destructuring-bind (elf-number . present-count) (queue:head queue)
      (queue:snoc (queue:tail next-queue)
                  (cons elf-number (+ present-count (cdr (queue:head next-queue))))))))

(defun find-winner (queue)
  (if (= 1 (queue:size queue))
      (car (queue:head queue))
      (find-winner (take-turn queue))))

(defun get-answer-1 (&optional (elf-count *elf-count*))
  (find-winner (init-elves elf-count)))


;;;; Part 2

(defun init-elves-2 (elf-count)
  (labels ((init-r (elves elf-number)
             (if (< elf-count elf-number)
                 elves
                 (init-r (fset:with-last elves (cons elf-number 1))
                         (1+ elf-number)))))
    (init-r (fset:empty-seq) 1)))

(defun take-turn-2 (elves)
  (let ((loser-index (truncate (fset:size elves) 2)))
    (destructuring-bind (elf-number . present-count) (fset:first elves)
      (fset:with-last
          (fset:less-first (fset:less elves loser-index))
          (cons elf-number (+ present-count (cdr (fset:lookup elves loser-index))))))))

(defun find-winner-2 (elves)
  (if (= 1 (fset:size elves))
      (car (fset:first elves))
      (find-winner-2 (take-turn-2 elves))))

(defun get-answer-2 (&optional (elf-count *elf-count*))
  (find-winner-2 (init-elves-2 elf-count)))
