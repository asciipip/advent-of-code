(in-package :aoc-2016-03)

(aoc:define-day 869 1544)


;;;; Input

(defparameter *triangles*
  (mapcar #'aoc:extract-ints (aoc:input)))


;;;; Part 1

(defun triangle-valid-p (triangle)
  (let ((sorted-triangle (sort (copy-list triangle) #'<)))
    (< (third sorted-triangle) (+ (first sorted-triangle) (second sorted-triangle)))))

(defun list-valid-triangles (triangles)
  (remove-if-not #'triangle-valid-p triangles))

(defun get-answer-1 (&optional (triangles *triangles*))
  (length (list-valid-triangles triangles)))


;;;; Part 2

(defun transpose-triangles (triangles)
  (if (endp triangles)
      nil
      (nconc (iter (for x from 0 to 2)
                   (collecting (iter (for y from 0 to 2)
                                     (collecting (elt (elt triangles y) x)))))
             (transpose-triangles (nthcdr 3 triangles)))))

(defun get-answer-2 (&optional (triangles *triangles*))
  (length (list-valid-triangles (transpose-triangles triangles))))
