(in-package :aoc-2016-09)

(aoc:define-day 102239 10780403063)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Parsing

(defun count-uncompressed-chars (string &key (recurse nil))
  (labels ((count-r (pos count)
             (cond
               ((<= (length string) pos)
                count)
               ((char= #\( (char string pos))
                (multiple-value-bind (chars new-pos)
                    (decompressed-sequence-length string pos recurse)
                  (count-r new-pos (+ chars count))))
               (t
                (count-r (1+ pos) (1+ count))))))
    (count-r 0 0)))

(defun read-compression-header (string pos)
  (assert (char= #\( (char string pos)))
  (multiple-value-bind (sequence-length x-pos)
      (parse-integer string :start (1+ pos) :junk-allowed t)
    (assert (char= #\x (char string x-pos)))
    (multiple-value-bind (repetitions end-pos)
        (parse-integer string :start (1+ x-pos) :junk-allowed t)
      (assert (char= #\) (char string end-pos)))
      (values sequence-length repetitions (1+ end-pos)))))

(defun decompressed-sequence-length (string pos recurse)
  (multiple-value-bind (sequence-length repetitions new-pos)
      (read-compression-header string pos)
    (values (if recurse
                (* repetitions (count-uncompressed-chars
                                (subseq string new-pos (+ new-pos sequence-length))
                                :recurse t))
                (* repetitions sequence-length))
            (+ new-pos sequence-length))))


;;;; Part 1

(aoc:given 1
  (= 6 (count-uncompressed-chars "ADVENT"))
  (= 7 (count-uncompressed-chars "A(1x5)BC"))
  (= 9 (count-uncompressed-chars "(3x3)XYZ"))
  (= 6 (count-uncompressed-chars "(6x1)(1x3)A"))
  (= 18 (count-uncompressed-chars "X(8x2)(3x3)ABCY")))

(defun get-answer-1 (&optional (input *input*))
  (count-uncompressed-chars input))


;;;; Part 2

(aoc:given 2
  (= 9 (count-uncompressed-chars "(3x3)XYZ" :recurse t))
  (= 20 (count-uncompressed-chars "X(8x2)(3x3)ABCY" :recurse t))
  (= 241920 (count-uncompressed-chars "(27x12)(20x12)(13x14)(7x10)(1x12)A" :recurse t))
  (= 445 (count-uncompressed-chars "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN" :recurse t)))

(defun get-answer-2 (&optional (input *input*))
  (count-uncompressed-chars input :recurse t))
