(in-package :aoc-2016-01)

(aoc:define-day 271 153)


;;;; Input

(defstruct instruction
  turn
  walk)

(defmethod print-object ((instruction instruction) stream)
  (format stream "#<INSTRUCTION ~A~A>"
          (if (plusp (imagpart (instruction-turn instruction)))
              #\L
              #\R)
          (instruction-walk instruction)))

(defun parse-instruction (instruction-string)
  (let ((turn (if (char= #\L (char instruction-string 0))
                  #C(0  1)
                  #C(0 -1)))
        (walk (aoc:extract-ints instruction-string)))
    (make-instruction :turn turn :walk walk)))

(defun parse-instructions (string)
  (mapcar #'parse-instruction (ppcre:split ", " string)))

(defparameter *instructions* (parse-instructions (aoc:input)))


;;;; Part 1

(defstruct (state)
  (direction)
  (position))

(defparameter *start-point* (make-state :direction #C(0 1) :position #C(0 0)))

(defun state-distance (state)
  (+ (abs (realpart (state-position state)))
     (abs (imagpart (state-position state)))))

(defun take-step (instruction state)
  (with-slots (turn walk) instruction
    (with-slots (direction position) state
      (values (make-state :direction (* direction turn)
                          :position (+ position (* direction turn walk)))
              (alexandria:iota walk
                               :start (+ position (* direction turn))
                               :step (* direction turn))))))

(defun follow-instructions (instructions)
  (reduce #'(lambda (state step) (take-step step state))
          instructions
          :initial-value *start-point*))

(aoc:given 1
  (= 5 (state-distance (follow-instructions (parse-instructions "R2, L3"))))
  (= 2 (state-distance (follow-instructions (parse-instructions "R2, R2, R2"))))
  (= 12 (state-distance (follow-instructions (parse-instructions "R5, L5, R5, R3")))))

(defun get-answer-1 ()
  (state-distance (follow-instructions *instructions*)))


;;;; Part 2

(defun first-duplicate (instructions)
  (let ((visited (make-hash-table)))
    (labels ((first-duplicate-r (remaining state)
               (multiple-value-bind (new-state steps) (take-step (car remaining) state)
                 (iter (for step in steps)
                       (when (gethash step visited)
                         (return-from first-duplicate-r (make-state :position step)))
                       (setf (gethash step visited) t))
                 (first-duplicate-r (cdr remaining) new-state))))
      (setf (gethash (state-position *start-point*) visited) t)
      (first-duplicate-r instructions *start-point*))))

(aoc:given 2
  (= 4 (state-distance (first-duplicate (parse-instructions "R8, R4, R4, R8")))))

(defun get-answer-2 ()
  (state-distance (first-duplicate *instructions*)))
