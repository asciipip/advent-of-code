(in-package :aoc-2016-04)

(aoc:define-day 185371 984)


;;;; Input

(defparameter *example-real-1* "aaaaa-bbb-z-y-x-123[abxyz]")
(defparameter *example-real-2* "a-b-c-d-e-f-g-h-987[abcde]")
(defparameter *example-real-3* "not-a-real-room-404[oarel]")
(defparameter *example-decoy* "totally-real-room-200[decoy]")

(defparameter *rooms* (aoc:input))


;;;; Parsing

(parseq:defrule encrypted-name ()
    (+ (char "a-z-"))
  (:string)
  (:function (lambda (s) (subseq s 0 (1- (length s))))))

(parseq:defrule sector-id ()
    (+ digit)
  (:string)
  (:function #'parse-integer))

(parseq:defrule checksum ()
    (rep 5 (char "a-z"))
  (:string))

;; Weirdly, this rule needs to be reloaded by hand after loading all of
;; the Advent of Code packages.  Otherwise, the parsing fails.
(parseq:defrule room ()
    (and encrypted-name sector-id "[" checksum "]")
  (:choose 0 1 3))


;;;; Part 1

(defun checksum-char-< (e1 e2)
  (or (> (cdr e1) (cdr e2))
      (and (= (cdr e1) (cdr e2))
           (char< (car e1) (car e2)))))

(defun count-chars (string)
  (labels ((count-r (pos seen)
             (if (<= (length string) pos)
                 seen
                 (count-r (1+ pos) (fset:with seen (schar string pos))))))
    (count-r 0 (fset:bag))))

(defun calc-checksum (encrypted-name)
  (flet ((remove-dashes (collection)
           (fset:filter (lambda (e) (not (char= #\- e))) collection))
         (sort-for-checksum (aseq)
           (fset:sort aseq #'checksum-char-<))
         (extract-chars (aseq)
           (fset:image #'car aseq))
         (truncate-char-seq (seq)
           (fset:subseq seq 0 5))
         (convert-to-map (collection)
           (fset:convert 'fset:map collection))
         (convert-to-seq (collection)
           (fset:convert 'fset:seq collection))
         (convert-to-string (collection)
           (fset:convert 'string collection)))
    (convert-to-string
     (truncate-char-seq
      (extract-chars
       (sort-for-checksum
        (convert-to-seq
         (convert-to-map
          (remove-dashes
           (count-chars encrypted-name))))))))))

(defun room-real-p (string)
  (destructuring-bind (encrypted-name sector-id checksum)
      (parseq:parseq 'room string)
    (declare (ignore sector-id))
    (string= checksum (calc-checksum encrypted-name))))

(aoc:deftest given-1
  (5am:is-true (room-real-p *example-real-1*))
  (5am:is-true (room-real-p *example-real-2*))
  (5am:is-true (room-real-p *example-real-3*))
  (5am:is-false (room-real-p *example-decoy*)))

(defun list-real-rooms (&optional (rooms *rooms*))
  (remove-if-not #'room-real-p rooms))

(defun get-answer-1 (&optional (rooms *rooms*))
  (reduce #'+
          (mapcar (lambda (r) (second (parseq:parseq 'room r)))
                  (list-real-rooms rooms))))


;;;; Part 2

(defun decrypt-name (name offset)
  (iter (for c in-string name)
        (for new-c = (if (char= c #\-)
                         #\ 
                         (code-char (+ (char-code #\a)
                                       (mod (+ (- (char-code c)
                                                  (char-code #\a))
                                               offset)
                                            26)))))
        (collecting new-c result-type 'string)))

(defun print-room-names (&key (rooms *rooms*) (print-real t) (print-decoy t))
  (let ((parsed (sort (mapcar (lambda (r)
                                (cons (room-real-p r) (parseq:parseq 'room r)))
                              rooms)
                      #'<
                      :key #'third)))
    (mapcan (lambda (r)
              (destructuring-bind (real encrypted-name sector-id checksum) r
                (declare (ignore checksum))
                (when (or (and real print-real)
                          (and (not real) print-decoy))
                  (format t "~A: ~A (~A)~%"
                          (if real "real" "fake")
                          (decrypt-name encrypted-name sector-id)
                          sector-id))))
            parsed)))

(defun get-answer-2 (&optional (rooms *rooms*))
  (iter (for room in rooms)
        (for (encrypted-name sector-id checksum) = (parseq:parseq 'room room))
        (for name = (decrypt-name encrypted-name sector-id))
        (finding sector-id such-that (and (room-real-p room)
                                          (string= "northpole object storage" name)))))
