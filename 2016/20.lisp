(in-package :aoc-2016-20)

(aoc:define-day 19449262 119)


;;;; Parsing

(parseq:defrule range ()
    (and aoc:integer-string "-" aoc:integer-string)
  (:choose 0 2)
  (:function #'cons))


;;;; Input

(defparameter *ranges* (aoc:input :parse-line 'range))


;;;; Part 1

(defun merge-ranges (ranges)
  "RANGES must be sorted"
  (cond
    ((endp ranges)
     nil)
    ((endp (cdr ranges))
     ranges)
    (t
     (destructuring-bind (r1 r2 &rest rs) ranges
       (if (<= (car r2) (1+ (cdr r1)))
           (merge-ranges (cons (cons (car r1) (max (cdr r1) (cdr r2))) rs))
           (cons r1 (merge-ranges (cons r2 rs))))))))

(defun simplify-ranges (ranges)
  (merge-ranges (sort (copy-list ranges) #'< :key #'car)))

(defun get-answer-1 (&optional (ranges *ranges*))
  (let ((simplified-ranges (simplify-ranges ranges)))
    (assert (zerop (car (first simplified-ranges))))
    (1+ (cdr (first simplified-ranges)))))


;;;; Part 2

(defun count-absent-ranges (total-ips ranges)
  (if (endp ranges)
      total-ips
      (destructuring-bind ((low . hi) &rest rs) ranges
        (count-absent-ranges (- total-ips (1+ (- hi low)))
                             rs))))

(defun get-answer-2 (&optional (ranges *ranges*) (max 4294967295))
  (count-absent-ranges (1+ max) (simplify-ranges ranges)))
