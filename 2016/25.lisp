(in-package :aoc-2016-25)

;; As usual, part 2 is just completing all forty-nine of the other stars
;; for the year.
(aoc:define-day 192 nil)


;;;; Input

(defparameter *program* (aoc:input))


;;;; Part 1

;; See `25.md' for mor of an explanation of what's going on here.
(defun get-answer-1 (&optional (program *program*))
  (let ((initial-registers (assembunny:run (assembunny:compile (subseq program 0 3)))))
    (- #b101010101010
       (* (fset:lookup initial-registers :b)
          (fset:lookup initial-registers :c)))))
