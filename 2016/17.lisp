(in-package :aoc-2016-17)

(aoc:define-day "DDRUDLRRRD" 398)


;;; Input

(defparameter *passcode* (aoc:input))


;;; Part 1

(defparameter +door-directions+ (fset:set '(0 u) '(1 d) '(2 l) '(3 r)))
(defparameter +door-vectors+
  (fset:map ('u #<0 -1>)
            ('d #<0 1>)
            ('l #<-1 0>)
            ('r #<1 0>)))

(defparameter *vault-size* #<4 4>)

(defun path-to-coords (path)
  (reduce (lambda (position direction)
            (point:+ position (fset:lookup +door-vectors+ direction)))
          path
          :initial-value #<0 0>))

(defun valid-dirs-at-coords (point)
  (fset:reduce (lambda (result direction vector)
                 (if (point:< (point:make-point -1 -1)
                              (point:+ point vector)
                              *vault-size*)
                     (fset:with result direction)
                     result))
               +door-vectors+
               :initial-value (fset:empty-set)))

(defun door-open-p (digit)
  (< 10 digit))

(defun hash-door-digit (hash door-number)
  (mod (ash (aref hash (truncate door-number 2))
            (- (* 4 (mod (1+ door-number) 2))))
       16))

(defun get-open-doors (passcode steps)
  "Returns a set of 'u 'd 'l 'r giving the adjacent doors that are open."
  (let ((hash (md5:md5sum-string (format nil "~A~{~A~}" passcode steps))))
    (fset:intersection
     (valid-dirs-at-coords (path-to-coords steps))
     (fset:image (lambda (door-pair)
                   (destructuring-bind (idx sym) door-pair
                     (and (door-open-p (hash-door-digit hash idx))
                          sym)))
                 +door-directions+))))

(defun make-path-options-fn (passcode)
  (lambda (path)
    (let ((open-doors (get-open-doors passcode path)))
      (fset:image (lambda (direction)
                    (list 1 (append path (list direction))))
                  (fset:convert 'list open-doors)))))

(defun find-path (passcode)
  (car
   (last
    (aoc:shortest-path '()
                       (make-path-options-fn passcode)
                       :finishedp (lambda (path)
                                    (point:= (path-to-coords path)
                                             (point:+ #<-1 -1> *vault-size*)))))))

(defun get-answer-1 (&optional (passcode *passcode*))
  (format nil "~{~A~}" (find-path passcode)))


;;; Part 2

;;; Gotta just try all the paths.  Recursion handles depth-first traversal
;;; easily.

(defun take-step (passcode path longest-path)
  (if (point:= (path-to-coords path)
               (point:+ #<-1 -1> *vault-size*))
      (if (< (length longest-path) (length path))
          path
          longest-path)
      (let ((open-doors (get-open-doors passcode path)))
        (fset:reduce (lambda (result direction)
                       (let ((new-path (append path (list direction))))
                         (take-step passcode new-path result)))
                     open-doors
                     :initial-value longest-path))))

(defun get-answer-2 (&optional (passcode *passcode*))
  (length (take-step passcode '() '())))
