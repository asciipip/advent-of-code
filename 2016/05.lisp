(in-package :aoc-2016-05)

(aoc:define-day "F77A0E6E" "999828EC")


;;;; Input

(defparameter *example* "abc")
(defparameter *input* (aoc:input))


;;;; Part 1

(defun next-character-p (string)
  (let ((md5sum (md5:md5sum-string string)))
    (when (and (zerop (aref md5sum 0))
               (zerop (aref md5sum 1))
               (< (aref md5sum 2) 16))
      (values (aref md5sum 2)
              (truncate (aref md5sum 3) 16)))))

(defun find-next-character (door-id &optional (index 0))
  (multiple-value-bind (char-1 char-2) (next-character-p (format nil "~A~D" door-id index))
    (if char-1
        (values char-1 char-2 index)
        (find-next-character door-id (1+ index)))))

(defun find-characters (door-id character-count &optional (index 0))
  (if (<= character-count 0)
      nil
      (multiple-value-bind (char-1 char-2 new-index)
          (find-next-character door-id index)
        (declare (ignore char-2))
        (cons char-1 (find-characters door-id (1- character-count) (1+ new-index))))))

(defun get-answer-1 (&optional (input *input*))
  (format nil "~{~X~}" (find-characters input 8)))


;;;; Part 2

(defun find-characters-by-pos (door-id character-count &key (print nil))
  (let ((result (make-array character-count :initial-element nil)))
    (iter (while (position nil result))
          (for (values position char index)
               first (find-next-character door-id)
               then (find-next-character door-id (1+ index)))
          (when (and (< -1 position character-count)
                     (null (svref result position)))
            (setf (svref result position) char)
            (when print
            (format t "~{~:[_~;~:*~X~]~} (~A)~%" (coerce result 'list) index))))
    (format nil "~{~X~}" (coerce result 'list))))

(defun get-answer-2 (&key (print nil))
  (find-characters-by-pos *input* 8 :print print))
