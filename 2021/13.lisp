(in-package :aoc-2021-13)

;;; Part two's answer is "KJBKEUBG", but requires OCR, which I haven't
;;; implemented yet.
(aoc:define-day 788 nil)


;;;; Parsing

(parseq:defrule instructions ()
    (and points #\Newline (+ (and fold (? #\Newline))))
  (:choose 0 2)
  (:lambda (points folds)
    (list points (mapcar #'first folds))))

(parseq:defrule points ()
    (+ (and point #\Newline))
  (:lambda (&rest points)
    (fset:convert 'fset:set (mapcar #'first points))))

(parseq:defrule point ()
    (aoc:comma-list aoc:integer-string)
  (:function #'point:make-point))

(parseq:defrule fold ()
    (and "fold along " (char "xy") "=" aoc:integer-string)
  (:choose 1 3)
  (:lambda (axis value)
    (list (ecase axis
            (#\x :x)
            (#\y :y))
          value)))


;;;; Input

(defparameter *instructions* (aoc:input :parse 'instructions))


;;;; Part 1

(defun do-fold-point (fold points point)
  (destructuring-bind (axis line) fold
    (ecase axis
      (:x (if (< line (point:x point))
              (fset:with (fset:less points point)
                         (point:make-point (- (* 2 line) (point:x point)) (point:y point)))
              points))
      (:y (if (< line (point:y point))
              (fset:with (fset:less points point)
                         (point:make-point (point:x point) (- (* 2 line) (point:y point))))
              points)))))

(defun do-fold (points fold)
  (fset:reduce (alexandria:curry #'do-fold-point fold)
               points
               :initial-value points))

(defun get-answer-1 (&optional (instructions *instructions*))
  (destructuring-bind (points folds) instructions
    (fset:size (do-fold points (first folds)))))


;;;; Part 2

(defun do-folds (instructions)
  (destructuring-bind (points folds) instructions
    (fset:reduce #'do-fold
                 folds
                 :initial-value points)))

(defun print-instruction-result (instructions)
  (aoc:print-set-braille (do-folds instructions)))


;;;; Other

(defun flip-point (axis line point)
  (ecase axis
    (:x (point:make-point (- (* 2 line) (point:x point)) (point:y point)))
    (:y (point:make-point (point:x point) (- (* 2 line) (point:y point))))))

(defun unfold-point (axis line points point)
  (let ((r (random 1.0)))
    (cond
      ((<= r 0.33)
       points)
      ((<= r 0.66)
       (fset:with points (flip-point axis line point)))
      (t
       (fset:with (fset:less points point)
                  (flip-point axis line point))))))

(defun unfold (instructions &key axis line)
  (when (and (not (eq axis :x))
             (not (eq axis :y))
             (not (eq axis nil)))
    (error "Unknown axis: ~A" axis))
  (destructuring-bind (points folds) instructions
    (multiple-value-bind (min-point max-point) (point:bbox points)
      (let* ((point-span (point:- max-point min-point))
             (real-axis (or axis
                            (if (< (point:y point-span) (point:x point-span))
                                :y
                                :x)))
             (real-line (or line
                            (ecase real-axis
                              (:x (1+ (point:x max-point)))
                              (:y (1+ (point:y max-point)))))))
        (list (fset:reduce (alexandria:curry #'unfold-point real-axis real-line)
                           points
                           :initial-value points)
              (cons (list real-axis real-line)
                    folds))))))
