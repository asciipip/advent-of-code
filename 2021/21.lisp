(in-package :aoc-2021-21)

(aoc:define-day 897798 48868319769358)


;;;; Data Structures

;;; NOTE: In the description, the board spaces, player numbers, and die
;;; values are one-based.  To make them easier to work with, all are
;;; *zero-based* in code below.  They're converted to one-based for
;;; presentation.

(defparameter *board-spaces* 10)

(defun add-bags (bag1 bag2 &key mod)
  "Adds every value in BAG1 to every value in BAG2.  If MOD is provided,
  addition is performed modulo MOD."
  (let ((result (fset:empty-bag)))
    (fset:do-bag-pairs (b1 b1-count bag1)
      (fset:do-bag-pairs (b2 b2-count bag2)
        (let ((sum (if mod
                       (mod (+ b1 b2) mod)
                       (+ b1 b2))))
          (fset:adjoinf result sum (* b1-count b2-count)))))
    result))

(defun bag-multiply (bag multiplier)
  "Multiplies the multiplicity of every element of BAG by MULTIPLIER."
  (assert (plusp multiplier))
  (labels ((multiply-r (multiples-remaining result)
             (if (= 1 multiples-remaining)
                 result
                 (multiply-r (1- multiples-remaining)
                             (fset:bag-sum result bag)))))
    (multiply-r multiplier bag)))

(defclass game ()
  ((scores :initarg :scores
           :initform (fset:empty-seq (fset:empty-seq (fset:empty-bag))))
   (die-state :initarg :die
              :initform 0)
   (next-player :initarg :next-player
                :initform 0)
   (wins :initarg :wins
         :initform (fset:empty-seq 0))))

(defmethod print-object ((game game) stream)
  (print-unreadable-object (game stream :type t)
    (with-slots (scores die-state next-player wins) game
      (format stream "~%  ~A~%" scores)
      (format stream "  w:~A~%" wins)
      (format stream "  n:~A d:~A" (1+ next-player) (1+ die-state)))))

(defun initial-score (starting-position)
  (fset:with (fset:convert 'fset:seq (make-list 10 :initial-element (fset:empty-bag)))
             starting-position
             (fset:bag 0)))

(defun make-game (positions)
  (assert (= 2 (fset:size positions)))
  (let ((scores (fset:image #'initial-score positions)))
    (make-instance 'game :scores scores)))

(defun update-game (game &key scores die next-player wins)
  (with-slots ((old-scores scores)
               (old-die-state die-state)
               (old-next-player next-player)
               (old-wins wins))
      game
    (make-instance 'game
                   :scores (or scores old-scores)
                   :die (or die old-die-state)
                   :next-player (or next-player old-next-player)
                   :wins (or wins old-wins))))

(defun game-update-die (game new-state)
  (update-game game :die new-state))

(defun add-scores (scores moves)
  (let ((new-scores (fset:empty-seq (fset:empty-bag))))
    (fset:do-seq (score-bag scores :index i)
      (fset:do-bag-pairs (move move-count moves)
        (let ((new-pos (mod (+ i move) *board-spaces*)))
          (fset:unionf (fset:lookup new-scores new-pos)
                       (bag-multiply (fset:image (alexandria:curry #'+ (1+ new-pos))
                                                 score-bag)
                                     move-count)))))
    new-scores))

(defun count-winners (scores)
  (fset:size (fset:filter (alexandria:curry #'<= *winning-score*)
                          (fset:reduce #'fset:union scores))))

(defun remove-winners (scores)
  (fset:image (lambda (score-bag)
                (fset:filter (alexandria:curry #'> *winning-score*)
                             score-bag))
              scores))

(defun game-move-player (game move-distances)
  "Updates the next player's position and score."
  (with-slots (scores next-player wins) game
    (let ((new-scores (add-scores (fset:lookup scores next-player)
                                  move-distances)))
      (update-game game
                   :scores (fset:with scores next-player
                                      (remove-winners new-scores))
                   :wins (fset:with wins next-player
                                    (+ (fset:lookup wins next-player)
                                       (count-winners new-scores)))
                   :next-player (mod (1+ next-player)
                                     (fset:size scores))))))


;;;; Parsing

(parseq:defrule positions ()
    (+ player-position)
  (:lambda (&rest positions)
    (fset:convert 'fset:seq positions)))

(parseq:defrule player-position ()
    (and "Player " aoc:integer-string " starting position: " aoc:integer-string #\Newline)
  (:choose 3)
  (:function #'1-))


;;;; Input

(defparameter *example* (fset:seq 3 7))
(defparameter *positions* (aoc:input :parse 'positions))


;;;; Part 1

(defparameter *winning-score* 1000)
(defparameter *die-type* :practice)

(defun roll-die (game)
  "Returns two values: a bag of possible roll outcomes and the new game state."
  (ecase *die-type*
    (:practice
     (with-slots (die-state) game
       (values (fset:bag (1+ die-state))
               (game-update-die game (mod (1+ die-state) 100)))))
    (:dirac
     (values (fset:bag 1 2 3)
             game))))

(defun roll-die-multiple (game rolls &optional (sums (fset:empty-bag)))
  "Returns two values: a bag of possible roll outcomes and the new game state."
  (if (zerop rolls)
      (values sums game)
      (multiple-value-bind (roll-bag new-game) (roll-die game)
        (roll-die-multiple new-game (1- rolls)
                           (if (fset:empty? sums)
                               roll-bag
                               (add-bags sums roll-bag))))))

(defun player-move (game)
  "Returns the new state."
  (multiple-value-bind (rolls game-with-rolled-die)
      (roll-die-multiple game 3)
    (game-move-player game-with-rolled-die rolls)))

(defun find-winning-player (game)
  (with-slots (wins) game
    (fset:position-if #'plusp wins)))

(defun play-until-win (game &optional (moves 0))
  (if (find-winning-player game)
      (values game moves)
      (play-until-win (player-move game) (1+ moves))))

(defun get-answer-1 (&optional (positions *positions*))
  (multiple-value-bind (game-state moves)
      (play-until-win (make-game positions))
    (with-slots (scores wins) game-state
      (let ((loser-scores (fset:lookup scores (fset:lookup wins 0))))
        (* (fset:arb (fset:reduce #'fset:union loser-scores))
           (* 3 moves))))))


;;;; Part 2

;; #[ (id pos score) (id pos score) ]
(defun make-game-2 (positions)
  (assert (= 2 (fset:size positions)))
  (fset:seq (list 0 (fset:first positions) 0)
            (list 1 (fset:last positions) 0)))

(defun make-die-fn (die-type)
  (ecase die-type
    (:practice
     (let ((value -1))
       (lambda ()
         (fset:bag (1+ (setf value (mod (1+ value) 100)))))))
    (:dirac (lambda () (fset:bag 1 2 3)))))

(defun roll-die-2 (die-fn rolls)
  (assert (not (minusp rolls)))
  (if (zerop rolls)
      (fset:bag 0)
      (add-bags (funcall die-fn)
                (roll-die-2 die-fn (1- rolls)))))

(defun player-win-p (game)
  (if (<= *winning-score* (fset:last (fset:last game)))
      (fset:bag (fset:first (fset:last game)))
      nil))

(defun player-move-2 (game roll)
  (destructuring-bind (id pos score) (fset:first game)
    (let ((new-pos (mod (+ pos roll) *board-spaces*)))
      (fset:with-last (fset:less-first game)
                      (list id new-pos (+ score 1 new-pos))))))

(aoc:defmemo game-move-2 (game die-fn)
  (if (player-win-p game)
      (fset:seq 0 1)
      (let ((rolls (roll-die-2 die-fn 3)))
        (fset:reduce (lambda (scores roll)
                       (let ((wins (game-move-2 (player-move-2 game roll)
                                                die-fn)))
                         (gmap:gmap :seq
                                    #'+
                                    (:seq scores)
                                    (:seq (aoc:rotate-seq wins)))))
                     rolls
                     :initial-value (fset:convert 'fset:seq '(0 0))))))

(defun get-answer-2 (&optional (positions *positions*))
  (let ((*winning-score* 21))
    (fset:reduce #'max (game-move-2 (make-game-2 positions)
                                    (make-die-fn :dirac)))))
