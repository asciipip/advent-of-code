(in-package :aoc-2021-24)

(aoc:define-day 29991993698469 14691271141118)


;;;; Input

;;; See 24.md for more information.

(defparameter *parameters*
  ;; zd   xa  ya
  '(( 1   15   9)
    ( 1   11   1)
    ( 1   10  11)
    ( 1   12   3)
    (26  -11  10)
    ( 1   11   5)
    ( 1   14   0)
    (26   -6   7)
    ( 1   10   9)
    (26   -6  15)
    (26   -6   4)
    (26  -16  10)
    (26   -4   4)
    (26   -2   9)))

(defun digit-value (last-value parameters digit)
  (destructuring-bind (zd xa ya) parameters
    (let ((x (+ (mod last-value 26)
                xa)))
      (if (= digit x)
          (truncate last-value zd)
          (+ (* 26 (truncate last-value zd))
             digit
             ya)))))


;;;; Part 1

(defun possible-digits (parameters last-value)
  (destructuring-bind (zd xa ya) parameters
    (declare (ignore ya))
    (if (= 1 zd)
        '(1 2 3 4 5 6 7 8 9)
        (let ((possible-digit (+ (mod last-value 26)
                                 xa)))
          (if (<= 1 possible-digit 9)
              (list possible-digit)
              nil)))))

(defun visit-model-numbers (visit-fn parameters &optional (last-value 0) previous-digits)
  (if (endp parameters)
      (funcall visit-fn (reverse previous-digits))
      (dolist (d (possible-digits (car parameters) last-value))
        (visit-model-numbers visit-fn
                             (cdr parameters)
                             (digit-value last-value (car parameters) d)
                             (cons d previous-digits)))))

(defun model-number-to-int (model-number &optional (n 0))
  (if (endp model-number)
      n
      (model-number-to-int (cdr model-number)
                           (+ (car model-number)
                              (* 10 n)))))

(defun get-answer-1 (&optional (parameters *parameters*))
  (let ((result 0))
    (visit-model-numbers (lambda (m)
                           (let ((n (model-number-to-int m)))
                             (when (< result n)
                               (setf result n))))
                         parameters)
    result))


;;;; Part 2

(defun get-answer-2 (&optional (parameters *parameters*))
  (let ((result))
    (visit-model-numbers (lambda (m)
                           (let ((n (model-number-to-int m)))
                             (when (or (null result)
                                       (< n result))
                               (setf result n))))
                         parameters)
    result))
