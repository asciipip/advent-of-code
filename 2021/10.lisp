(in-package :aoc-2021-10)

(aoc:define-day 318081 4361305341)


;;;; Input

(defparameter *example* (aoc:input :file "examples/10.txt"))
(defparameter *nav-subsystem* (aoc:input))


;;;; Part 1

(defparameter +char-pairs+
  (fset:map (#\( #\))
            (#\[ #\])
            (#\{ #\})
            (#\< #\>)))
(defparameter +char-points+
  (fset:map (#\) 3)
            (#\] 57)
            (#\} 1197)
            (#\> 25137)))

(defun corrupted-p (string &optional (start 0))
  "Returns two values: the mismatched character and the position after the
  current character."
  (labels ((parse-subsequence (substart)
             (if (<= (length string) substart)
                 (values nil substart)
                 (let ((subchar (schar string substart)))
                   (if (fset:contains? (fset:domain +char-pairs+) subchar)
                       ;; Subsequence start
                       (multiple-value-bind (mismatch end)
                           (corrupted-p string substart)
                         (if mismatch
                             (values mismatch end)
                             (parse-subsequence end)))
                       ;; Sequence end
                       (values nil substart))))))
    (if (<= (length string) start)
        (values nil start)
        (let ((start-char (schar string start)))
          (assert (fset:contains? (fset:domain +char-pairs+) start-char))
          (multiple-value-bind (mismatch end)
              (parse-subsequence (1+ start))
            (cond
              (mismatch
               (values mismatch end))
              ((<= (length string) end)
               (values nil end))
              (t
               (let ((end-char (schar string end)))
                 (if (char= end-char (fset:lookup +char-pairs+ start-char))
                     (values nil (1+ end))
                     (values end-char end))))))))))

(defun find-corrupted (strings)
  (fset:filter #'identity (fset:image #'corrupted-p strings)))

(defun score-chars (chars)
  (fset:image (alexandria:curry #'fset:lookup +char-points+) chars))

(defun get-answer-1 (&optional (nav-subsystem *nav-subsystem*))
  (fset:reduce #'+ (score-chars (find-corrupted nav-subsystem))))


;;;; Part 2

(defparameter +autocomplete-points+
  (fset:map (#\) 1)
            (#\] 2)
            (#\} 3)
            (#\> 4)))

;;; This is a mess.  I copied and pasted `corrupted-p` from part one and
;;; then modified it until I worked.  `parse-subsequence` is probably
;;; redundant because the main function now eats chained sequences.  But
;;; I'm tired and I don't feel like messing with this any more.  I got my
;;; answers for the day.  If I'm lucky, I'll come back and clean this up
;;; later.  Otherwise, this terrible code will be enshrined in git for
;;; eternity.
(defun completion-seq (string &optional (start 0))
  "Returns two values: the sequence and the position after the current
  character."
  (labels ((parse-subsequence (substart)
             (if (<= (length string) substart)
                 (values (fset:empty-seq) substart)
                 (let ((subchar (schar string substart)))
                   (if (fset:contains? (fset:domain +char-pairs+) subchar)
                       ;; Subsequence start
                       (multiple-value-bind (end-seq end)
                           (completion-seq string substart)
                         (if end-seq
                             (values end-seq end)
                             (parse-subsequence end)))
                       ;; Sequence end
                       (values nil substart))))))
    (if (<= (length string) start)
        (values (fset:empty-seq) start)
        (let ((start-char (schar string start)))
          (assert (fset:contains? (fset:domain +char-pairs+) start-char))
          (multiple-value-bind (end-seq end)
              (parse-subsequence (1+ start))
            (cond
              (end-seq
               (values (fset:with-last end-seq (fset:lookup +char-pairs+ start-char))
                       end))
              ((<= (length string) end)
               (values (fset:seq (fset:lookup +char-pairs+ start-char)) end))
              (t
               (let ((end-char (schar string end)))
                 (assert (char= end-char (fset:lookup +char-pairs+ start-char)))
                 (if (and (< (1+ end) (length string))
                          (fset:contains? (fset:domain +char-pairs+)
                                          (schar string (1+ end))))
                     (completion-seq string (1+ end))
                     (values nil (1+ end)))))))))))

(defun score-autocomplete (chars)
  (fset:reduce (lambda (score char)
                 (+ (* 5 score)
                    (fset:lookup +autocomplete-points+ char)))
               chars
               :initial-value 0))

(defun score-autocomplete-set (set)
  (let ((sorted-scores (fset:sort (fset:image #'score-autocomplete
                                              (fset:convert 'fset:seq set))
                                  #'<)))
    (assert (oddp (fset:size sorted-scores)))
    (fset:lookup sorted-scores (truncate (fset:size sorted-scores) 2))))

(defun get-answer-2 (&optional (nav-subsystem *nav-subsystem*))
  (let* ((uncorrupted-strings (fset:filter (lambda (ns) (not (corrupted-p ns)))
                                           nav-subsystem))
         (autocomplete-set (fset:image #'completion-seq uncorrupted-strings)))
    (score-autocomplete-set autocomplete-set)))


;;;; Part 1, but with native parsing

;;; Uses +char-pairs+ from above

(defun read-alt-delimited-list (stream char)
  (read-delimited-list (fset:lookup +char-pairs+ char) stream t))

(defun add-other-braces-to-readtable ()
  (fset:do-set (char (fset:less (fset:domain +char-pairs+) #\())
    (set-macro-character char #'read-alt-delimited-list)
    (set-macro-character (fset:lookup +char-pairs+ char)
                         (get-macro-character #\)))))

;;; Bleah.  There might not be enough information in the condition to get
;;; the point in the string that caused problems.
(defun find-invalid-char (string)
  (handler-case
      (read-from-string string)
    (sb-int:simple-reader-error (condition)
      (assert (string= "unmatched-close-parenthesis"
                       (slot-value condition 'format-control)))
      (slot-value condition 'stream ))))
