(in-package :aoc-2021-07)

(aoc:define-day 329389 86397080)


;;;; Input

(defparameter *crabs* (fset:convert 'fset:seq (aoc:input :parse-line '(aoc:comma-list aoc:integer-string))))


;;;; Part 1

(defun median (numbers)
  (let ((sorted (fset:sort numbers #'<)))
    (if (oddp (fset:size sorted))
        (fset:lookup sorted (floor (fset:size sorted) 2))
        (/ (+ (fset:lookup sorted (floor (fset:size sorted) 2))
              (fset:lookup sorted (1+ (floor (fset:size sorted) 2))))
           2))))

(defun mean (numbers)
  (/ (fset:reduce #'+ numbers) (fset:size numbers)))

(defun fuel-used (part point-a point-b)
  (ecase part
    (1 (abs (- point-a point-b)))
    (2 (let ((n (abs (- point-a point-b))))
         (/ (* n (1+ n)) 2)))))

(defun distance-from-point (part point numbers)
  (fset:reduce #'+ (fset:image (alexandria:curry #'fuel-used part point) numbers)))

(defun find-center (part numbers)
  (let* ((center (ecase part
                   (1 (median numbers))
                   (2 (mean numbers))))
         (left (floor center))
         (right (ceiling center)))
    (if (integerp center)
        (values center (distance-from-point part center numbers))
        (let ((left-fuel (distance-from-point part left numbers))
              (right-fuel (distance-from-point part right numbers)))
          (if (< left-fuel right-fuel)
              (values left left-fuel)
              (values right right-fuel))))))

(defun get-answer-1 (&optional (numbers *crabs*))
  (nth-value 1 (find-center 1 numbers)))


;;;; Part 2

(defun get-answer-2 (&optional (numbers *crabs*))
  (nth-value 1 (find-center 2 numbers)))


;;;; Easter Egg!

(defun easter-egg (&optional (numbers *crabs*))
  (coerce (mapcar #'code-char (intcode:run (intcode:load-program (fset:convert 'list numbers))))
          'string))
