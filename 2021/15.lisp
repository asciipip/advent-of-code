(in-package :aoc-2021-15)

(aoc:define-day 447 2825)


;;;; Parsing

(parseq:defrule digit-grid ()
    (+ digit-line)
  (:lambda (&rest lines)
    (let ((result (make-array (list (length lines)
                                    (length (first lines))))))
      (series:iterate ((line (series:scan lines))
                       (y (series:scan-range)))
        (series:iterate ((digit (series:scan line))
                         (x (series:scan-range)))
          (setf (aref result y x) digit)))
      result)))

(parseq:defrule digit-line ()
    (and (+ digit) (? #\Newline))
  (:choose 0)
  (:lambda (&rest chars)
    (mapcar (lambda (c) (- (char-code c) (char-code #\0))) chars)))


;;;; Input

(defparameter *map* (aoc:input :parse 'digit-grid))


;;;; Part 1

(defparameter +neighbor-points+
  (list (point:make-point -1  0)
        (point:make-point  1  0)
        (point:make-point  0 -1)
        (point:make-point  0  1)))

(defun neighbor-list (array point &key (scale 5))
  (fset:reduce (lambda (r dp)
                 (let ((p (point:+ point dp)))
                   (if (and (< -1 (point:x p) (* scale (array-dimension array 1)))
                            (< -1 (point:y p) (* scale (array-dimension array 0))))
                       (cons p r)
                       r)))
               +neighbor-points+
               :initial-value nil))

(defun risk-level (map point)
  (multiple-value-bind (x-mult x-base)
      (truncate (point:x point) (array-dimension map 1))
    (multiple-value-bind (y-mult y-base)
        (truncate (point:y point) (array-dimension map 0))
      (let ((base-risk (aref map y-base x-base)))
        (1+ (mod (1- (+ base-risk x-mult y-mult))
                 9))))))

(defun get-answer-1 (&optional (map *map*) (scale 1))
  (let ((end-point (point:make-point (1- (* scale (array-dimension map 1)))
                                     (1- (* scale (array-dimension map 0))))))
    (nth-value 1 (aoc:shortest-path (point:make-point 0 0)
                                    (lambda (point)
                                      (mapcar (lambda (neighbor)
                                                (list (risk-level map neighbor) neighbor))
                                              (neighbor-list map point :scale scale)))
                                    :end end-point
                                    :heuristic (lambda (point) (point:manhattan-distance point end-point))))))


;;;; Part 2

(defun get-answer-2 ()
  (get-answer-1 *map* 5))
