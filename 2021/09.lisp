(in-package :aoc-2021-09)

(aoc:define-day 456 1047744)


;;;; Parsing

;;; This seems easier to do without parseq.

(defun parse-dem (strings)
  (let ((result (make-array (list (length strings) (length (first strings))))))
    (iter (for string in strings)
          (for y from 0)
          (iter (for c in-string string)
                (for x from 0)
                (for e = (- (char-code c) (char-code #\0)))
                (setf (aref result y x) e)))
    result))


;;;; Input

(defparameter *example* (parse-dem (aoc:input :file "examples/09.txt")))
(defparameter *dem* (parse-dem (aoc:input)))


;;;; Part 1

(defun neighbor-set (dem point)
  (let ((result (fset:empty-set)))
    (dolist (y (fset:image (alexandria:curry #'+ (point:y point)) '(-1 1)))
      (when (< -1 y (array-dimension dem 0))
        (fset:adjoinf result (point:make-point (point:x point) y))))
    (dolist (x (fset:image (alexandria:curry #'+ (point:x point)) '(-1 1)))
      (when (< -1 x (array-dimension dem 1))
        (fset:adjoinf result (point:make-point x (point:y point)))))
    result))

(defun low-point-p (dem point)
  (flet ((neighbor-high-p (neighbor)
           (< (point:aref dem point) (point:aref dem neighbor))))
    (gmap:gmap :and #'neighbor-high-p (:set (neighbor-set dem point)))))

(defun find-low-points (dem)
  (iter outer
        (for y from 0 below (array-dimension dem 0))
        (iter (for x from 0 below (array-dimension dem 1))
              (for point = (point:make-point x y))
              (when (low-point-p dem point)
                (in outer (collecting point))))))

(defun risk-level (dem point)
  (1+ (point:aref dem point)))

(defun get-answer-1 (&optional (dem *dem*))
  (fset:reduce #'+ (fset:image (alexandria:curry #'risk-level dem)
                               (find-low-points dem))))


;;;; Part 2

(defun dem-point-set (dem)
  (let ((result (fset:empty-set)))
    (dotimes (y (array-dimension dem 0))
      (dotimes (x (array-dimension dem 1))
        (fset:adjoinf result (point:make-point x y))))
    result))

(defun collect-basin-group (dem starting-point)
  (labels ((collect-r (unchecked-points basin)
             (let ((point (fset:arb unchecked-points)))
               (cond
                 ((fset:empty? unchecked-points)
                  basin)
                 ((= 9 (point:aref dem point))
                  (collect-r (fset:less unchecked-points point)
                             basin))
                 (t
                  (collect-r (fset:union (fset:set-difference (neighbor-set dem point)
                                                              basin)
                                         (fset:less unchecked-points point))
                             (fset:with basin point)))))))
    (collect-r (fset:set starting-point) (fset:empty-set))))

(defun basin-groups (dem)
  (labels ((basin-groups-r (unchecked-points basins)
             (if (fset:empty? unchecked-points)
                 basins
                 (let ((point (fset:arb unchecked-points)))
                   (if (= 9 (point:aref dem point))
                       (basin-groups-r (fset:less unchecked-points point)
                                       basins)
                       (let ((basin (collect-basin-group dem point)))
                         (basin-groups-r (fset:set-difference unchecked-points basin)
                                         (fset:with-last basins basin))))))))
    (basin-groups-r (dem-point-set dem) (fset:empty-seq))))

(defun get-answer-2 (&optional (dem *dem*))
  (let ((basins (fset:sort (fset:image #'fset:size (basin-groups dem))
                           #'>)))
    (fset:reduce #'* (fset:subseq basins 0 3))))


;;;; Visualization

(defparameter *elevation-colors*
  '((0   5 112 176)
    (8 116 169 207)
    (9 241 238 246)))

(defun save-hypsorelief (dem filename &key method)
  (let ((dem-path (merge-pathnames "aoc.aig" filename))
        (shade-path (merge-pathnames "shade.tif" filename))
        (color-path (merge-pathnames "color.tif" filename))
        (color-txt-path (merge-pathnames "color.txt" filename)))
    (save-dem (ecase method
                (:blob (upscale-dem-blob (aoc-2020-20::scale3x (aoc-2020-20::scale3x dem))
                                         2 :p 2.7 :radius 4.5))
                (:sharp (upscale-dem-blob (upscale-dem-sharp dem 9)
                                         2 :p 2.7 :radius 4.5)))
              (uiop:native-namestring dem-path)
              :cellsize 1/18)
    (when (probe-file shade-path)
      (delete-file shade-path))
    (external-program:run "gdaldem"
                          (list "hillshade"
                                (uiop:native-namestring dem-path)
                                (uiop:native-namestring shade-path))
                          :output *standard-output*)
    (when (probe-file color-path)
      (delete-file color-path))
    (with-open-file (output color-txt-path :direction :output
                                           :if-exists :supersede
                                           :if-does-not-exist :create)
      (format output "~{~{~A ~}~%~}" *elevation-colors*))
    (external-program:run "gdaldem"
                          (list "color-relief"
                                (uiop:native-namestring dem-path)
                                (uiop:native-namestring color-txt-path)
                                (uiop:native-namestring color-path))
                          :output *standard-output*)
    (external-program:run
     "convert"
     (list (uiop:native-namestring color-path) "-modulate" "120"
           "(" (uiop:native-namestring shade-path) "-level" "70,95%" "+level" "0%,80%" ")"
           "-compose" "screen" "-composite"
           "(" (uiop:native-namestring shade-path) "-level" "0,75%" "+level" "40%,100%" ")"
           "-compose" "multiply" "-composite"
           "-modulate" "92" "-define" "png:color-type=2"
           (uiop:native-namestring filename))
     :output *standard-output*)))

(defun fill-dem! (dem base)
  (dotimes (y (array-dimension dem 0))
    (dotimes (x (array-dimension dem 1))
      (when (< (aref dem y x) base)
        (setf (aref dem y x) base))))
  dem)

(defun write-hypsorelief-files (dem directory &key method count)
  (let ((dem-path (merge-pathnames "aoc.aig" directory))
        (shade-path (merge-pathnames "shade.tif" directory))
        (color-path (merge-pathnames "color.tif" directory))
        (color-txt-path (merge-pathnames "color.txt" directory)))
    (let ((upscaled-dem (ecase method
                          (:blob (upscale-dem-blob (aoc-2020-20::scale3x (aoc-2020-20::scale3x dem))
                                                   2 :p 2.7 :radius 4.5))
                          (:sharp (upscale-dem-blob (upscale-dem-sharp dem 9)
                                                    2 :p 2.7 :radius 4.5)))))
      
      (cl-progress-bar:with-progress-bar (count "frames")
        (dotimes (frame count)
          (let ((filled-dem (fill-dem! upscaled-dem (* frame (/ 8 count)))))
            (save-dem filled-dem
                      (uiop:native-namestring dem-path)
                      :cellsize 1/18)
            (when (probe-file shade-path)
              (delete-file shade-path))
            (external-program:run "gdaldem"
                                  (list "hillshade"
                                        (uiop:native-namestring dem-path)
                                        (uiop:native-namestring shade-path)))
            (when (probe-file color-path)
              (delete-file color-path))
            (with-open-file (output color-txt-path :direction :output
                                                   :if-exists :supersede
                                                   :if-does-not-exist :create)
              (format output "0   5 112 176~%8 116 169 207~%9 241 238 246~%"))
            (external-program:run "gdaldem"
                                  (list "color-relief"
                                        (uiop:native-namestring dem-path)
                                        (uiop:native-namestring color-txt-path)
                                        (uiop:native-namestring color-path)))
            (external-program:run
             "convert"
             (list (uiop:native-namestring color-path) "-modulate" "120"
                   "(" (uiop:native-namestring shade-path) "-level" "70,95%" "+level" "0%,80%" ")"
                   "-compose" "screen" "-composite"
                   "(" (uiop:native-namestring shade-path) "-level" "0,75%" "+level" "40%,100%" ")"
                   "-compose" "multiply" "-composite"
                   "-modulate" "92" "-define" "png:color-type=2"
                   (uiop:native-namestring (merge-pathnames (format nil "~4,'0D.png" frame)
                                                            directory)))))
          (cl-progress-bar:update 1))))))

;; Copied to 2022-12
(defun save-dem (dem filename &key (cellsize 1.0))
  "Writes DEM to FILENAME in Arc/Info ASCII Grid format."
  (with-open-file (output filename :direction :output :if-does-not-exist :create :if-exists :supersede)
    (format output "ncols ~A~%" (array-dimension dem 1))
    (format output "nrows ~A~%" (array-dimension dem 1))
    (format output "xllcorner 0.0 ~%yllcorner 0.0~%")
    (format output "cellsize ~F~%" cellsize)
    (format output "NODATA_value -9999~%")
    (dotimes (y (array-dimension dem 0))
      (dotimes (x (array-dimension dem 1))
        (format output "~F " (aref dem y x)))
      (format output "~%"))))

(defun inverse-distance-average (dem point p)
  (let ((value-sum 0)
        (weight-sum 0))
    (dotimes (y (array-dimension dem 0))
      (dotimes (x (array-dimension dem 1))
        (let ((weight (/ (expt (+ (expt (- x (point:x point)) 2)
                                  (expt (- y (point:y point)) 2))
                               (/ p 2.0)))))
          (incf weight-sum weight)
          (incf value-sum (* weight (aref dem y x))))))
    (/ value-sum weight-sum)))

(defun neighbor-distance-average (dem point p radius)
  (let ((value-sum 0)
        (weight-sum 0))
    (dolist (y (alexandria:iota (+ 2 (ceiling (* 2 radius)))
                                :start (floor (- (point:y point) radius))))
      (dolist (x (alexandria:iota (+ 2 (ceiling (* 2 radius)))
                                  :start (floor (- (point:x point) radius))))
        (when (and (< -1 y (array-dimension dem 0))
                   (< -1 x (array-dimension dem 1)))
          (let* ((distance (sqrt (+ (expt (- x (point:x point)) 2)
                                    (expt (- y (point:y point)) 2))))
                 (weight (if (zerop distance)
                             1
                             (/ (expt distance p)))))
            (when (<= distance radius)
              (incf weight-sum weight)
              (incf value-sum (* weight (aref dem y x))))))))
    (/ value-sum weight-sum)))

(defun upscale-dem-blob (dem factor &key (p 2.7) (radius 1.5))
  (let ((result (make-array (list (1- (* factor (array-dimension dem 0)))
                                  (1+ (* factor (1- (array-dimension dem 1))))))))
    (let ((lp-channel (lparallel:make-channel)))
      (cl-progress-bar:with-progress-bar ((array-total-size result) "upscaling")
        (dotimes (y (array-dimension result 0))
          (dotimes (x (array-dimension result 1))
            (lparallel:submit-task lp-channel
                                   (lambda (x y dem point p radius)
                                     (list x y (neighbor-distance-average dem point p radius)))
                                   x y dem (point:/ (point:make-point x y) factor) p radius))
          (dotimes (x (array-dimension result 1))
            (destructuring-bind (x y average) (lparallel:receive-result lp-channel)
              (setf (aref result y x) average)
              (cl-progress-bar:update 1))))))
    result))

(defun triangle-decompose (point &key ul ur ll lr)
  "Returns a list of three points consisting of the best triangle
  surrounding POINT.  All points should be 3D, though the z dimension
  doesn't matter for POINT.  UL, UR, LL, and LR should be 3D."
  (let ((ullr (point:norm (point:- ul lr)))
        (llur (point:norm (point:- ll ur))))
    ;; Pick the shortest diagonal to divide the square.
    ;; If the diagonals are equal size, pick the highest one.
    ;; If the heights are the same, pick the ll-ur one.
    (if (or (< ullr llur)
            (and (= ullr llur)
                 (< (+ (point:z ll) (point:z ur))
                    (+ (point:z ul) (point:z lr)))))
        (if (< (point:norm (point:- ll (point:make-point (point:x point) (point:y point) (point:z ll))))
               (point:norm (point:- ur (point:make-point (point:x point) (point:y point) (point:z ur)))))
            (fset:set ul lr ll)
            (fset:set ul lr ur))
        (if (< (point:norm (point:- ul (point:make-point (point:x point) (point:y point) (point:z ul))))
               (point:norm (point:- lr (point:make-point (point:x point) (point:y point) (point:z lr)))))
            (fset:set ll ur ul)
            (fset:set ll ur lr)))))

(defun plane-coefficients (points)
  "Returns four values: A, B, C, and D for the planar equation ax + by +
  cz = d.  POINTS should contain three 3D points."
  (destructuring-bind (p1 p2 p3) (fset:convert 'list points)
    (let ((a (- (* (- (point:y p2) (point:y p1))
                   (- (point:z p3) (point:z p1)))
                (* (- (point:y p3) (point:y p1))
                   (- (point:z p2) (point:z p1)))))
          (b (- (* (- (point:z p2) (point:z p1))
                   (- (point:x p3) (point:x p1)))
                (* (- (point:z p3) (point:z p1))
                   (- (point:x p2) (point:x p1)))))
          (c (- (* (- (point:x p2) (point:x p1))
                   (- (point:y p3) (point:y p1)))
                (* (- (point:x p3) (point:x p1))
                   (- (point:y p2) (point:y p1))))))
      (values a b c
              (+ (* a (point:x p1))
                 (* b (point:y p1))
                 (* c (point:z p1)))))))

(defun point-from-dem (dem x y)
  (point:make-point x y (aref dem y x)))

(defun interpolate-point (dem point)
  (let ((min-x (max (floor (point:x point)) 0))
        (max-x (min (ceiling (point:x point))
                    (1- (array-dimension dem 1))))
        (min-y (max (floor (point:y point)) 0))
        (max-y (min (ceiling (point:y point))
                    (1- (array-dimension dem 0)))))
    (assert (and (<= min-x max-x)
                 (<= min-y max-y)))
    (cond
      ((and (= min-x max-x)
            (= min-y max-y))
       ;; original point
       (aref dem min-y min-x))
      ((= min-x max-x)
       ;; on vertical line
       (+ (* (- 1 (- (point:y point) min-y)) (aref dem min-y min-x))
          (* (- 1 (- max-y (point:y point))) (aref dem max-y min-x))))
      ((= min-y max-y)
       ;; on horizontal line
       (+ (* (- 1 (- (point:x point) min-x)) (aref dem min-y min-x))
          (* (- 1 (- max-x (point:x point))) (aref dem min-y max-x))))
      (t
       (let ((plane-points (triangle-decompose (point:make-point (point:x point) (point:y point) 0)
                                               :ul (point-from-dem dem min-x min-y)
                                               :ll (point-from-dem dem min-x max-y)
                                               :ur (point-from-dem dem max-x min-y)
                                               :lr (point-from-dem dem max-x max-y))))
         (multiple-value-bind (a b c d) (plane-coefficients plane-points)
           (/ (- d
                 (* a (point:x point))
                 (* b (point:y point)))
              c)))))))

;;; The "sharp" mechanism decomposes each grid square into triangles and
;;; interpolates across thos triangles.  This leads to very sharp-looking
;;; ridged and corners, hence the name.
(defun upscale-dem-sharp (dem factor)
  (let ((result (make-array (list (1- (* factor (array-dimension dem 0)))
                                  (1+ (* factor (1- (array-dimension dem 1))))))))
    (let ((lp-channel (lparallel:make-channel)))
      (cl-progress-bar:with-progress-bar ((array-total-size result) "upscaling")
        (dotimes (y (array-dimension result 0))
          (dotimes (x (array-dimension result 1))
            (lparallel:submit-task lp-channel
                                   (lambda (x y dem point)
                                     (list x y (interpolate-point dem point)))
                                   x y dem (point:/ (point:make-point x y) factor)))
          (dotimes (x (array-dimension result 1))
            (destructuring-bind (x y average) (lparallel:receive-result lp-channel)
              (setf (aref result y x) average)
              (cl-progress-bar:update 1))))))
    result))
