(in-package :aoc-2021-05)

(aoc:define-day 5294 21698)


;;;; Parsing

;; Horizontal and diagonal lines always go left to right, vertical always
;; top to bottom.
(parseq:defrule line ()
    (and point " -> " point)
  (:choose 0 2)
  (:lambda (a b)
    (if (or (< (point:x b) (point:x a))
            (and (= (point:x b) (point:x a))
                 (< (point:y b) (point:y a))))
        (list b a)
        (list a b))))

(parseq:defrule point ()
    (and aoc:integer-string "," aoc:integer-string)
  (:choose 0 2)
  (:function #'point:make-point))


;;;; Input

(defparameter *example* (aoc:input :parse-line 'line :file "examples/05.txt"))
(defparameter *lines* (aoc:input :parse-line 'line))


;;;; Part 1

(defun filter-straight-lines (lines)
  (fset:filter (lambda (line)
                 (destructuring-bind (start end) line
                   (or (= (point:x start) (point:x end))
                       (= (point:y start) (point:y end)))))
               lines))

(defun points-on-line (line)
  (destructuring-bind (start end) line
    (let ((delta (point:make-point (signum (- (point:x end) (point:x start)))
                                   (signum (- (point:y end) (point:y start))))))
      (labels ((points-on-line-r (point result)
                 (if (point:= point end)
                     (cons point result)
                     (points-on-line-r (point:+ point delta)
                                       (cons point result)))))
        (points-on-line-r start nil)))))

(defun count-covered-points (lines)
  (multiple-value-bind (min-point max-point)
      (point:bbox (alexandria:flatten lines))
    (let* ((span (point:- max-point min-point))
           (field (make-array (list (1+ (point:y span)) (1+ (point:x span)))
                              :element-type (list 'integer 0 (length lines))
                              :initial-element 0)))
      (dolist (line lines)
        (dolist (point (points-on-line line))
          (incf (point:aref field (point:- point min-point)))))
      field)))

(defun count-overlaps (points threshold)
  (count-if (lambda (c) (<= threshold c)) (aoc:flatten-array points)))

(defun get-answer-1 (&optional (lines *lines*))
  (count-overlaps (count-covered-points (filter-straight-lines lines)) 2))


;;;; Part 2

(defun get-answer-2 (&optional (lines *lines*))
  (count-overlaps (count-covered-points lines) 2))
