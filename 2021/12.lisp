(in-package :aoc-2021-12)

(aoc:define-day 4241 122134)


;;;; Parsing

(parseq:defrule edge ()
    (and cave "-" cave)
  (:choose 0 2))

(parseq:defrule cave ()
    (+ alpha)
  (:string)
  (:lambda (s)
    (list (intern (string-upcase s))
          (if (upper-case-p (schar s 0))
              :big
              :small))))

(defclass graph ()
  ((edges :initarg :edges)
   (small-caves :initarg :small-caves)))

(defun build-graph (unparsed-edges)
  (let ((edges (fset:empty-map (fset:empty-set)))
        (small-caves (fset:empty-set)))
    (dolist (edge unparsed-edges)
      (destructuring-bind ((cave1 c1size) (cave2 c2size)) edge
        (unless (or (eq cave1 'end)
                    (eq cave2 'start))
          (fset:adjoinf (fset:lookup edges cave1) cave2))
        (unless (or (eq cave2 'end)
                    (eq cave1 'start))
          (fset:adjoinf (fset:lookup edges cave2) cave1))
        (when (eq c1size :small)
          (fset:adjoinf small-caves cave1))
        (when (eq c2size :small)
          (fset:adjoinf small-caves cave2))))
    (make-instance 'graph :edges edges :small-caves small-caves)))


;;;; Input

(defparameter *graph* (build-graph (aoc:input :parse-line 'edge)))
(defparameter *example1* (build-graph (aoc:input :parse-line 'edge :file "examples/12-1.txt")))


;;;; Part 1

(defun visited-small-caves (small-caves path free-visits)
  (let* ((path-bag (fset:convert 'fset:bag path))
         (small-caves-visited (fset:bag-product small-caves path-bag)))
    (if (<= free-visits
            (- (fset:size small-caves-visited) (fset:set-size small-caves-visited)))
        (fset:convert 'fset:set small-caves-visited)
        (fset:empty-set))))

(defun extend-paths (graph partial-paths completed-paths free-visits)
  (if (endp partial-paths)
      completed-paths
      (let ((path (car partial-paths)))
        (if (eq 'end (car path))
            (extend-paths graph
                          (cdr partial-paths)
                          (cons path completed-paths)
                          free-visits)
            (with-slots (edges small-caves) graph
              (let* ((visitable-caves
                       (fset:set-difference (fset:lookup edges (car path))
                                            (visited-small-caves small-caves path
                                                                 free-visits)))
                     (new-partial-paths
                       (fset:reduce (lambda (paths cave)
                                      (cons (cons cave path) paths))
                                    visitable-caves
                                    :initial-value (cdr partial-paths))))
                (extend-paths graph
                              new-partial-paths
                              completed-paths
                              free-visits)))))))

(defun find-paths (graph &key (free-visits 0))
  (extend-paths graph (list (list 'start)) nil free-visits))

(defun get-answer-1 (&optional (graph *graph*))
  (fset:size (find-paths graph)))


;;;; Part 2

(defun get-answer-2 (&optional (graph *graph*))
  (fset:size (find-paths graph :free-visits 1)))


;;;; Visualization

(defun write-dot (graph filename)
  (with-open-file (output filename
                          :direction :output
                          :if-does-not-exist :create
                          :if-exists :supersede)
    (format output "graph aoc202112 {~%")
    (fset:do-map (origin targets (slot-value graph 'edges))
      (fset:do-set (target targets)
        (when (or (string< (symbol-name origin) (symbol-name target))
                  (eq origin 'start)
                  (eq target 'end))
          (format output "  ~A -- ~A~%" origin target))))
    (format output "}~%")))
