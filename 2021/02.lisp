(in-package :aoc-2021-02)

(aoc:define-day 1804520 1971095320)


;;;; Parsing

(parseq:defrule instruction ()
    (or vertical horizontal))

(parseq:defrule vertical ()
    (and (or "up" "down") " " aoc:integer-string)
  (:choose 0 2)
  (:lambda (dir dy)
    (point:make-point 0 (ecase dir
                          ("up" (- dy))
                          ("down" dy)))))

(parseq:defrule horizontal ()
    (and "forward " aoc:integer-string)
  (:choose 1)
  (:lambda (dx)
    (point:make-point dx 0)))


;;;; Input

(defparameter *instructions* (aoc:input :parse-line 'instruction))
(defparameter *example*
  (mapcar (lambda (s) (parseq:parseq 'instruction s))
          '("forward 5"
            "down 5"
            "forward 8"
            "up 3"
            "down 8"
            "forward 2")))

;;;; Part 1

(defun move (instructions)
  (reduce #'point:+ instructions :initial-value (point:make-point 0 0)))

(defun get-answer-1 (&optional (instructions *instructions*))
  (let ((position (move instructions)))
    (* (point:x position) (point:y position))))


;;;; Part 2

(defun move-2 (instructions)
  (labels ((move-2-r (position aim remaining)
             (if (endp remaining)
                 position
                 (let ((instruction (car remaining)))
                   (if (zerop (point:x instruction))
                       (move-2-r position (point:+ aim instruction) (cdr remaining))
                       (move-2-r (point:+ position
                                          (point:make-point (point:x instruction)
                                                            (* (point:x instruction) (point:y aim))))
                                 aim
                                 (cdr remaining)))))))
    (move-2-r (point:make-point 0 0) (point:make-point 0 0) instructions)))

(defun get-answer-2 (&optional (instructions *instructions*))
  (let ((position (move-2 instructions)))
    (* (point:x position) (point:y position))))
