(in-package :aoc-2021-16)

(aoc:define-day 883 1675198555015)


;;;; Parsing

(defun deposit-nybble! (new-nybble nybblespec bit-array)
  "Like DPB, but works on nybbles instead of bytes, and bit-arrays instead
  of integers.  Destructively modifies BIT-ARRAY.  Returns the modified
  BIT-ARRAY."
  (labels ((deposit-r (nybble-part idx remaining-bits)
             (when (plusp remaining-bits)
               (multiple-value-bind (new-part bit) (truncate nybble-part 2)
                 (setf (sbit bit-array idx) bit)
                 (deposit-r new-part (1- idx) (1- remaining-bits))))))
    (deposit-r new-nybble (+ 3 (* 4 nybblespec)) 4))
  bit-array)

(defun hex-to-bits (string)
  (let ((result (make-array (* 4 (length string)) :element-type 'bit)))
    (dotimes (i (length string))
      (deposit-nybble! (parse-integer (string (schar string i))
                                      :radix 16)
                       i
                       result))
    result))


;;;; Input

(defparameter *example-literal* (hex-to-bits "D2FE28"))
(defparameter *example-operator-0* (hex-to-bits "38006F45291200"))
(defparameter *example-operator-1* (hex-to-bits "EE00D40C823060"))
(defparameter *message* (hex-to-bits (aoc:input)))


;;;; Part 1

(defconstant +literal-id+ 4)

(defconstant +length-type-bits+ 0)
(defconstant +length-type-count+ 1)

(defun read-bits (message start count)
  "Returns the integer represented by COUNT bits from START."
  (assert (plusp count))
  (assert (<= 0 start (+ start count) (length message)))
  (labels ((read-r (i result)
             (if (<= (+ start count) i)
                 result
                 (read-r (1+ i)
                         (+ (* 2 result)
                            (sbit message i))))))
    (read-r start 0)))

(defun packet-version (message &optional (packet-start 0))
  (read-bits message packet-start 3))

(defun packet-type (message &optional (packet-start 0))
  (read-bits message (+ 3 packet-start) 3))

(defun parse-literal (message &optional (packet-start 0) version-only)
  "Returns two values: the value of the packet and the position of the
  next bit after the packet."
  (assert (= +literal-id+ (packet-type message packet-start)))
  (labels ((literal-r (pos result)
             (let ((new-result (+ (* (expt 2 4) result)
                                  (read-bits message (1+ pos) 4))))
               (if (zerop (read-bits message pos 1))
                   (values (if version-only
                               (packet-version message packet-start)
                               new-result)
                           (+ pos 5))
                   (literal-r (+ 5 pos) new-result)))))
    (literal-r (+ 6 packet-start) 0)))

(defun parse-packet (message &optional (packet-start 0) version-only)
  "Returns two values: the parsed packet and the position of the next bit
  after the packet."
  (if (= +literal-id+ (packet-type message packet-start))
      (parse-literal message packet-start version-only)
      (parse-operator message packet-start version-only)))

(defun packet-length-type (message &optional (packet-start 0))
  (assert (/= +literal-id+ (packet-type message packet-start)))
  (read-bits message (+ packet-start 6) 1))

(defun packet-contents-length (message &optional (packet-start 0))
  (assert (/= +literal-id+ (packet-type message packet-start)))
  (assert (= +length-type-bits+ (packet-length-type message packet-start)))
  (read-bits message (+ packet-start 7) 15))

(defun packet-contents-count (message &optional (packet-start 0))
  (assert (/= +literal-id+ (packet-type message packet-start)))
  (assert (= +length-type-count+ (packet-length-type message packet-start)))
  (read-bits message (+ packet-start 7) 11))

(defun parse-operator (message &optional (packet-start 0) version-only)
  "Returns two values: a list of the operator and its contents and the
  position in MESSAGE of the next bit after the operator."
  (assert (/= +literal-id+ (packet-type message packet-start)))
  (alexandria:eswitch ((packet-length-type message packet-start))
    (+length-type-bits+
     (parse-operator-by-size message
                             (packet-contents-length message packet-start)
                             packet-start
                             version-only))
    (+length-type-count+
     (parse-operator-by-count message
                              (packet-contents-count message packet-start)
                              packet-start
                              version-only))))

(defun operator-symbol (message packet-start version-only)
  (if version-only
      (packet-version message packet-start)
      (fset:lookup +op-symbols+ (packet-type message packet-start))))

(defun parse-operator-by-size (message payload-size &optional (packet-start 0) version-only)
  "Returns two values: a list of the operator and its contents and the
  position in MESSAGE of the next bit after the operator."
  (let ((payload-start (+ packet-start (+ 3 3 1 15))))
    (labels ((parse-r (pos result)
               (if (< pos (+ payload-start payload-size))
                   (multiple-value-bind (new-packet new-pos) (parse-packet message pos version-only)
                     (parse-r new-pos (cons new-packet result)))
                   (values (cons (operator-symbol message packet-start version-only)
                                 (reverse result))
                           pos))))
      (parse-r payload-start nil))))

(defun parse-operator-by-count (message packet-count &optional (packet-start 0) version-only)
  "Returns two values: a list of the operator and its contents and the
  position in MESSAGE of the next bit after the operator."
  (labels ((parse-r (pos result)
             (if (< (length result) packet-count)
                 (multiple-value-bind (new-packet new-pos) (parse-packet message pos version-only)
                   (parse-r new-pos (cons new-packet result)))
                 (values (cons (operator-symbol message packet-start version-only)
                               (reverse result))
                         pos))))
    (parse-r (+ packet-start (+ 3 3 1 11)) nil)))

(defun get-answer-1 (&optional (message *message*))
  (reduce #'+ (alexandria:flatten (parse-packet message 0 t))))


;;;; Part 2

(defparameter +op-symbols+
  (fset:map (0 '+)
            (1 '*)
            (2 'min)
            (3 'max)
            (5 'gt-bit)
            (6 'lt-bit)
            (7 'eq-bit)))

(defun gt-bit (a b) (if (> a b) 1 0))
(defun lt-bit (a b) (if (< a b) 1 0))
(defun eq-bit (a b) (if (= a b) 1 0))

(defun get-answer-2 (&optional (message *message*))
  (eval (parse-packet message)))
