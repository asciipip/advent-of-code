(in-package :aoc-2021-14)

(aoc:define-day 2360 2967977072188)


;;;; Parsing

(defclass instructions ()
  ((first-element :initarg :first)
   (template-pairs :initarg :template)
   (pair-rules :initarg :rules)))

(defun elements-to-indices (elements)
  (fset:reduce (lambda (map element)
                 (fset:with map element (fset:size map)))
               (fset:convert 'fset:set elements)
               :initial-value (fset:empty-map)))

(parseq:defrule instructions ()
    (and template (rep 2 #\Newline) (+ (and pair-insertion (? #\Newline))))
  (:choose 0 2)
  (:lambda (template insertions)
    (let* ((char-mapping
             (elements-to-indices
              (alexandria:flatten (list template (mapcar #'first insertions)))))
           (rules (make-array (list (expt (fset:size char-mapping) 2)
                                    (expt (fset:size char-mapping) 2))
                              :initial-element 0))
           (initial-state (make-array (list 1
                                            (expt (fset:size char-mapping) 2))
                                      :initial-element 0)))
      (dolist (rule (mapcar #'first insertions))
        (destructuring-bind ((c1 . c2) ct) rule
          (setf (aref rules
                      (+ (* (fset:size char-mapping) (fset:@ char-mapping c1))
                         (fset:@ char-mapping c2))
                      (+ (* (fset:size char-mapping) (fset:@ char-mapping c1))
                         (fset:@ char-mapping ct)))
                1)
          (setf (aref rules
                      (+ (* (fset:size char-mapping) (fset:@ char-mapping c1))
                         (fset:@ char-mapping c2))
                      (+ (* (fset:size char-mapping) (fset:@ char-mapping ct))
                         (fset:@ char-mapping c2)))
                1)))
      (mapc (lambda (c1 c2)
              (incf (aref initial-state
                          0
                          (+ (* (fset:size char-mapping) (fset:@ char-mapping c1))
                             (fset:@ char-mapping c2)))))
            template
            (cdr template))
      (make-instance 'instructions
                     :first (fset:lookup char-mapping (first template))
                     :template initial-state
                     :rules rules))))

(parseq:defrule template ()
    (+ alpha))

(parseq:defrule pair-insertion ()
    (and alpha alpha " -> " alpha)
  (:choose 0 1 3)
  (:lambda (c1 c2 cn)
    (list (cons c1 c2)
          cn)))


;;;; Input

(defparameter *instructions* (aoc:input :parse 'instructions))
(defparameter *example* (aoc:input :file "examples/14.txt" :parse 'instructions))


;;;; Part 1


(defun step-polymer (instructions count)
  (with-slots (template-pairs pair-rules) instructions
    (aoc:matrix-multiply template-pairs (aoc:matrix-exp pair-rules count))))

(defun accumulate-elements (element-pairs)
  "Returns a vector of the element countss in the polymer represented by
  POLYMER-FN."
  (let* ((element-count (isqrt (array-dimension element-pairs 1)))
         (start-elements (make-array element-count :initial-element 0))
         (end-elements (make-array element-count :initial-element 0)))
    (dotimes (j (array-dimension element-pairs 1))
      (incf (svref start-elements (truncate j element-count))
            (aref element-pairs 0 j))
      (incf (svref end-elements (mod j element-count))
            (aref element-pairs 0 j)))
    (map 'simple-vector #'max start-elements end-elements)))

(defun get-answer-1 (&optional (instructions *instructions*) (steps 10))
  (let* ((elements (accumulate-elements (step-polymer instructions steps)))
         (min-count (reduce #'min elements))
         (max-count (reduce #'max elements)))
    (- max-count min-count)))


;;;; Part 2

(defun get-answer-2 ()
  (get-answer-1 *instructions* 40))
