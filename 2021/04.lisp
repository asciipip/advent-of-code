(in-package :aoc-2021-04)

(aoc:define-day 8580 9576)


;;;; Data Structures

;;; We're being generic, so each board carries its own neighbor map around
;;; with it.  That way we don't have to hardcode board sizes anywhere.
(defclass board ()
  ;; size included for ease of processing later
  ((size
    :documentation "the number of cells on each side of the board"
    :initarg :size)
   (cell-numbers
    :documentation "a map from numbers to the cell each number is in"
    :initarg :numbers)
   (cells
    :documentation "a map returning T for cells that have been picked"
    :initarg :cells)
   (neighbors
    :documentation
    "A map returning a set of sets.  Each subset is a line across the board."
    :initarg :neighbors)))

;;; But we'll save time and space by returning the same neighbor map for
;;; all boards of the same size.
(let ((memoized (fset:empty-map)))
  (defun make-neighbors (size)
    (or (fset:lookup memoized size)
        (let ((neighbors (fset:empty-map (fset:empty-set))))
          (iter (for x from 0 below size)
            (iter (for y from 0 below size)
                  (for point = (point:make-point x y))
                  ;; columns
                  (iter (for x1 from 0 below size)
                        (with subset = (fset:empty-set))
                        (fset:adjoinf subset
                                      (point:make-point x1 y))
                        (finally (fset:adjoinf (fset:lookup neighbors point)
                                               subset)))
                  ;; rows
                  (iter (for y1 from 0 below size)
                        (with subset = (fset:empty-set))
                        (fset:adjoinf subset
                                      (point:make-point x y1))
                        (finally (fset:adjoinf (fset:lookup neighbors point)
                                               subset)))))
          (setf (fset:lookup memoized size) neighbors)))))

(defun make-board (lines)
  (assert (= (length lines) (length (first lines))))
  (let ((numbers (fset:empty-map)))
    (iter (for y from 0)
          (for line in lines)
          (iter (for x from 0)
                (for number in line)
                (setf (fset:lookup numbers number)
                      (point:make-point x y))))
    (make-instance 'board
                   :size (length lines)
                   :numbers numbers
                   :cells (fset:empty-map)
                   :neighbors (make-neighbors (length lines)))))

(defun update-board (board cells)
  (with-slots (size neighbors cell-numbers) board
    (make-instance 'board
                   :size size
                   :numbers cell-numbers
                   :cells cells
                   :neighbors neighbors)))

(defun print-board (board &optional (stream t))
  (with-slots (size cell-numbers cells) board
    (let* ((max-num (fset:reduce #'max (fset:domain cell-numbers)))
           (digits (ceiling (log max-num 10)))
           (number-cells (aoc:invert-map cell-numbers)))
      (iter (for y from 0 below size)
            (iter (for x from 0 below size)
                  (for point = (point:make-point x y))
                  (format stream "~vD~A "
                          digits (fset:lookup number-cells point)
                          (if (fset:lookup cells point)
                              "*"
                              " ")))
            (format stream "~%")))))


;;;; Parsing

(parseq:defrule input ()
    (and numbers (+ board)))

(parseq:defrule numbers ()
    (and (aoc:comma-list aoc:integer-string) #\Newline)
  (:choose 0))

(parseq:defrule board ()
    (and #\Newline (+ board-line))
  (:choose 1)
  (:lambda (&rest lines) (make-board lines)))

(parseq:defrule board-line ()
    (and (* " ") aoc:integer-string (+ (and (+ " ") aoc:integer-string)) (? #\Newline))
  (:choose 1 2)
  (:lambda (n0 ns) (cons n0 (mapcar #'second ns))))


;;;; Input

(defparameter *example* (aoc:input :parse 'input :file "examples/04.txt"))
(defparameter *example-numbers* (first *example*))
(defparameter *example-boards* (second *example*))

(defparameter *input* (aoc:input :parse 'input))
(defparameter *numbers* (first *input*))
(defparameter *boards* (second *input*))

(defparameter *test-board*
  (make-board '((1 2 3)
                (4 5 6)
                (7 8 9))))

;;;; Part 1

(defun mark-cell (number board)
  "Returns the new board"
  (with-slots (cells cell-numbers) board
    (if (fset:lookup cell-numbers number)
        (update-board board (fset:with cells (fset:lookup cell-numbers number) t))
        board)))

(defun has-row-p (number board)
  "Returns true if BOARD has a cell with NUMBER and that cell is part of a
  completed row.  Note: Call MARK-CELL for NUMBER before calling this."
  (with-slots (neighbors cell-numbers cells) board
    (let ((cell (fset:lookup cell-numbers number)))
      (and cell
           (gmap:gmap :or (lambda (row) (row-full-p cells row))
                      (:set (fset:lookup neighbors cell)))))))

(defun row-full-p (cells row)
  (gmap:gmap :and (lambda (cell) (fset:lookup cells cell))
             (:set row)))

(defun board-score (number board)
  (with-slots (cell-numbers cells) board
    (let* ((unmarked-numbers (fset:filter (lambda (_ c)
                                            (declare (ignore _))
                                            (not (fset:lookup cells c)))
                                          cell-numbers))
           (unmarked-sum (fset:reduce #'+ (fset:domain unmarked-numbers))))
      (* unmarked-sum number))))

(defun find-winning-boards (numbers boards &optional past-winners-reversed)
  (if (or (endp numbers) (endp boards))
      (reverse past-winners-reversed)
      (let* ((number (first numbers))
             (new-boards (mapcar (alexandria:curry #'mark-cell number)
                                 boards))
             (winning-boards (fset:filter (alexandria:curry #'has-row-p number)
                                          new-boards)))
        (if (endp winning-boards)
            (find-winning-boards (cdr numbers) new-boards past-winners-reversed)
            (find-winning-boards (cdr numbers)
                                 (set-difference new-boards winning-boards)
                                 (append (mapcar (alexandria:curry #'list number) winning-boards)
                                         past-winners-reversed))))))

(defun get-answer-1 (&optional (numbers *numbers*) (boards *boards*))
  (destructuring-bind (number board) (first (find-winning-boards numbers boards))
    (values (board-score number board)
            board
            number)))


;;;; Part 2

(defun get-answer-2 (&optional (numbers *numbers*) (boards *boards*))
  (destructuring-bind (number board) (car (last (find-winning-boards numbers boards)))
    (values (board-score number board)
            board
            number)))
