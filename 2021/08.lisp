(in-package :aoc-2021-08)

(aoc:define-day 390 1011785)


;;;; Parsing

(parseq:defrule entry ()
    (and (rep 10 (and pattern " ")) "|" (rep 4 (and " " pattern)))
  (:choose 0 2)
  (:lambda (patterns output)
    (list (fset:convert 'fset:set (mapcar #'first patterns))
          (fset:convert 'fset:seq (mapcar #'second output)))))

(parseq:defrule pattern ()
    (+ segment)
  (:lambda (&rest segments)
    (let ((result (make-array 7 :initial-element 0 :element-type 'bit)))
      (dolist (segment segments)
        (setf (sbit result segment) 1))
      result)))

(parseq:defrule segment ()
    (char "abcdefg")
  (:lambda (c) (- (char-code c) (char-code #\a))))


;;;; Input

(defparameter *example* (parseq:parseq 'entry "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"))
(defparameter *notes* (aoc:input :parse-line 'entry))


;;;; Part 1

(defparameter +segment-digits+
  (fset:map (#*1110111 0)
            (#*0010010 1)
            (#*1011101 2)
            (#*1011011 3)
            (#*0111010 4)
            (#*1101011 5)
            (#*1101111 6)
            (#*1010010 7)
            (#*1111111 8)
            (#*1111011 9)))

;;; A permutation is a mapping of indices from the input to expected
;;; output.
;;;
;;; e.g.
;;; input #*1011011 + permutation #(6 5 4 3 2 1 0) -> output #*1101101

(defun permute-signal (permutation pattern)
  (assert (= (length permutation) (length pattern)))
  (let ((result (make-array (length pattern) :initial-element 0 :element-type 'bit)))
    (dotimes (i (length pattern))
      (when (plusp (sbit pattern i))
        (setf (sbit result (svref permutation i)) 1)))
    result))

(defun apply-permutation (permutation patterns)
  (fset:image (alexandria:curry #'permute-signal permutation) patterns))

(defun get-digits (patterns)
  (fset:image (alexandria:curry #'fset:lookup +segment-digits+) patterns))

(defun invert-permutation (permutation)
  (let ((result (make-array (length permutation))))
    (dotimes (i (length permutation))
      (setf (svref result (svref permutation i)) i))
    result))

(defparameter +digits-to-permutation+
  (let ((result (fset:empty-map)))
    (aoc:visit-permutations (lambda (permutation)
                              (setf (fset:lookup result (apply-permutation permutation (fset:domain +segment-digits+)))
                                    (invert-permutation permutation)))
                            #(0 1 2 3 4 5 6)
                            :copy t)
    result))

(defun solve-entry (entry)
  (get-digits (apply-permutation (fset:lookup +digits-to-permutation+ (first entry))
                                 (second entry))))

(defun get-answer-1 (&optional (notes *notes*))
  (let ((digits (fset:empty-map 0)))
    (dolist (note notes)
      (fset:do-seq (digit (solve-entry note))
        (incf (fset:lookup digits digit))))
    (fset:reduce #'+ (fset:image (alexandria:curry #'fset:lookup digits) '(1 4 7 8)))))


;;;; Part 2

(defun digit-seq-to-number (digit-seq)
  (fset:reduce (lambda (r d) (+ (* r 10) d))
               digit-seq
               :initial-value 0))

(defun get-answer-2 (&optional (notes *notes*))
  (fset:reduce #'+ (fset:image (alexandria:compose #'digit-seq-to-number #'solve-entry) notes)))


;;;; tk's big input

(defun get-answer-tk ()
  (let ((tk-input (time (aoc:input :tk "8-100000" :parse-line 'entry))))
    (values (time (get-answer-1 tk-input))
            (time (get-answer-2 tk-input)))))

