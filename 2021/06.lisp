(in-package :aoc-2021-06)

(aoc:define-day 366057 1653559299811)


;;;; Input

(defparameter *fish* (aoc:input :parse-line '(aoc:comma-list aoc:integer-string)))


;;;; Part 1

(defparameter *step-matrix*
  #2A((0 0 0 0 0 0 1 0 1)
      (1 0 0 0 0 0 0 0 0)
      (0 1 0 0 0 0 0 0 0)
      (0 0 1 0 0 0 0 0 0)
      (0 0 0 1 0 0 0 0 0)
      (0 0 0 0 1 0 0 0 0)
      (0 0 0 0 0 1 0 0 0)
      (0 0 0 0 0 0 1 0 0)
      (0 0 0 0 0 0 0 1 0)))

(defun group-fish (fish)
  (let ((result (make-array (array-dimension *step-matrix* 0)
                            :initial-element 0)))
    (dolist (f fish)
      (assert (<= f (length result)))
      (incf (svref result f)))
    (make-array (list 1 (length result)) :displaced-to result)))

(defun step-days (days fish-array)
  (aoc:matrix-multiply fish-array (aoc:matrix-exp *step-matrix* days)))

(defun get-answer-1 (&key (fish *fish*) (days 80))
  (reduce #'+ (aoc:flatten-array (step-days days (group-fish fish)))))


;;;; Part 2

(defun get-answer-2 (&key (fish *fish*) (days 256))
  (get-answer-1 :fish fish :days days))


;;;; Old way

;;; Preserved for posterity.  It actually competes pretty well with the
;;; matrix solution.  The only reason the matrix approach is faster is
;;; that I do the matrix multiplication in parallel.  Without that, the
;;; matrix approach would be slower, just bacause it has to do a lot more
;;; bignum multiplication.

(defun group-fish-clist (fish &key (cycle-days 7) (youth-days 2))
  (let ((fish-seq (fset:with (fset:empty-seq 0) (1- (+ cycle-days youth-days)) 0)))
    (dolist (f fish)
      (assert (<= f (+ cycle-days youth-days)))
      (incf (fset:lookup fish-seq f)))
    fish-seq))

(defun step-days-clist (days fish-seq &optional (idx 0))
  (if (zerop days)
      (aoc:rotate-seq fish-seq idx)
      (let ((adult-idx (mod (- idx 2) (fset:size fish-seq))))
        (step-days-clist (1- days)
                         (fset:with fish-seq adult-idx
                                    (+ (fset:lookup fish-seq adult-idx)
                                       (fset:lookup fish-seq idx)))
                         (mod (1+ idx) (fset:size fish-seq))))))

(defun get-answer-1-clist (&key (fish *fish*) (days 80))
  (fset:reduce #'+ (step-days-clist days (group-fish-clist fish))))
