(in-package :aoc-2021-22)

(aoc:define-day 607657 1187742789778677)


;;;; Data Structures

;;; The "on" cubes will be a set of cuboids.
;;;
;;; Each cuboid will be a single cons cell containing the min and max
;;; points of the cuboid, representing a half-open range.


;;;; Parsing

(parseq:defrule reboot-step ()
    (and onoff " " range)
  (:choose 0 2))

(parseq:defrule onoff ()
    (or "on" "off")
  (:function #'aoc:make-keyword))

(parseq:defrule range ()
    (and "x=" aoc:integer-string ".."  aoc:integer-string
         ",y="  aoc:integer-string ".."  aoc:integer-string
         ",z="  aoc:integer-string ".."  aoc:integer-string)
  (:choose 1 3 5 7 9 11)
  (:lambda (x0 x1 y0 y1 z0 z1)
    (cons (point:make-point x0 y0 z0)
          (point:make-point (1+ x1) (1+ y1) (1+ z1)))))


;;;; Input

(defparameter *example-small*
  (aoc:input :parse-line 'reboot-step :file "examples/22-small.txt"))
(defparameter *example-large*
  (aoc:input :parse-line 'reboot-step :file "examples/22-large.txt"))
(defparameter *reboot-steps* (aoc:input :parse-line 'reboot-step))


;;;; Part 1

(defun cuboid-small-p (step)
  (destructuring-bind (min . max) (second step)
    (point:<= (point:make-point -50 -50 -50)
              min max
              (point:make-point 50 50 50))))

(defun filter-small-cuboids (steps)
  (fset:filter #'cuboid-small-p steps))

;;; Cuboid predicates

(defun cuboids-overlap-p (cuboid1 cuboid2)
  (destructuring-bind (min1 . max1) cuboid1
    (destructuring-bind (min2 . max2) cuboid2
      (and (and (< (point:x min1) (point:x max2))
                (< (point:x min2) (point:x max1)))
           (and (< (point:y min1) (point:y max2))
                (< (point:y min2) (point:y max1)))
           (and (< (point:z min1) (point:z max2))
                (< (point:z min2) (point:z max1)))))))

(defun cuboid-contains-p (this other)
  "Returns true if OTHER is entirely contained within THIS."
  (destructuring-bind (min1 . max1) this
    (destructuring-bind (min2 . max2) other
      (point:<= min1 min2 max2 max1))))

(defun cuboid-intersection (cuboid1 cuboid2)
  "Returns the cubeoid marking the intersection between the two cubeoids"
  (destructuring-bind (min1 . max1) cuboid1
    (destructuring-bind (min2 . max2) cuboid2
      (let* ((points (fset:seq min1 max1 min2 max2))
             (xs (fset:sort (fset:image #'point:x points) #'<))
             (ys (fset:sort (fset:image #'point:y points) #'<))
             (zs (fset:sort (fset:image #'point:z points) #'<)))
        (cons (point:make-point (fset:@ xs 1) (fset:@ ys 1) (fset:@ zs 1))
              (point:make-point (fset:@ xs 2) (fset:@ ys 2) (fset:@ zs 2)))))))

(defun cuboid-difference (cuboid1 cuboid2)
  "Returns a set of cuboids representing the parts of CUBOID1 that do not
  intersect with CUBOID2."
  (let ((xs (list-internal-coordinates cuboid1 cuboid2 0))
        (ys (list-internal-coordinates cuboid1 cuboid2 1))
        (zs (list-internal-coordinates cuboid1 cuboid2 2))
        (result (fset:empty-set)))
    (series:iterate ((x1 (series:scan xs)) (x2 (series:scan (cdr xs))))
      (series:iterate ((y1 (series:scan ys)) (y2 (series:scan (cdr ys))))
        (series:iterate ((z1 (series:scan zs)) (z2 (series:scan (cdr zs))))
          (fset:adjoinf result (cons (point:make-point x1 y1 z1)
                                     (point:make-point x2 y2 z2))))))
    (fset:less result (cuboid-intersection cuboid1 cuboid2))))

(defun list-internal-coordinates (cuboid1 cuboid2 axis)
  "Returns a list of coordinates along AXIS from CUBOID1 and the
  coordinates from CUBOID2 that lie between CUBOID1's coordinates."
  (destructuring-bind (min1 . max1) cuboid1
    (destructuring-bind (min2 . max2) cuboid2
      (remove nil (list (point:elt min1 axis)
                        (and (< (point:elt min1 axis)
                                (point:elt min2 axis)
                                (point:elt max1 axis))
                             (point:elt min2 axis))
                        (and (< (point:elt min1 axis)
                                (point:elt max2 axis)
                                (point:elt max1 axis))
                             (point:elt max2 axis))
                        (point:elt max1 axis))))))

;;; Cuboid merging

(defun merge-cuboids (lit-cubes step)
  (ecase (first step)
    (:on (merge-lit-cuboids lit-cubes (fset:set (second step))))
    (:off (remove-unlit-cuboid lit-cubes (second step)))))

(defun merge-lit-cuboids (lit-cubes new-cuboids)
  (if (fset:empty? new-cuboids)
      lit-cubes
      (let* ((new-cuboid (fset:arb new-cuboids))
             (intersector (fset:find-if (alexandria:curry #'cuboids-overlap-p new-cuboid)
                                        lit-cubes)))
        (if intersector
            (cond
              ((cuboid-contains-p new-cuboid intersector)
               (merge-lit-cuboids (fset:less lit-cubes intersector)
                                  new-cuboids))
              ((cuboid-contains-p intersector new-cuboid)
               (merge-lit-cuboids lit-cubes
                                  (fset:less new-cuboids new-cuboid)))
              (t
               (merge-lit-cuboids lit-cubes
                                  (fset:union (fset:less new-cuboids new-cuboid)
                                              (cuboid-difference new-cuboid intersector)))))
            (merge-lit-cuboids (fset:with lit-cubes new-cuboid)
                               (fset:less new-cuboids new-cuboid))))))

(defun remove-unlit-cuboid (lit-cubes unlit-cubeoid)
  (fset:reduce (lambda (result cube)
                 (if (cuboids-overlap-p unlit-cubeoid cube)
                     (if (cuboid-contains-p unlit-cubeoid cube)
                         (fset:less result cube)
                         (fset:union (fset:less result cube)
                                     (cuboid-difference cube unlit-cubeoid)))
                     result))
               lit-cubes
               :initial-value lit-cubes))

;;; Final glue

(defun follow-steps (steps)
  (fset:reduce #'merge-cuboids steps :initial-value (fset:empty-set)))

(defun count-lit-cubes (lit-cubes)
  (fset:reduce (lambda (sum cuboid)
                 (destructuring-bind (min . max) cuboid
                   (let ((span (point:- max min)))
                     (+ (fset:reduce #'* (point:list-coordinates span))
                      sum))))
               lit-cubes
               :initial-value 0))

(defun get-answer-1 (&optional (steps *reboot-steps*))
  (count-lit-cubes (follow-steps (filter-small-cuboids steps))))


;;;; Part 2

(defun get-answer-2 (&optional (steps *reboot-steps*))
  (count-lit-cubes (follow-steps steps)))
