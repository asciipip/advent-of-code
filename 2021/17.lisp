(in-package :aoc-2021-17)

(aoc:define-day 23005 2040)


;;;; Parsing

;;; Overkill, but, hey.

(parseq:defrule target ()
    (and "target area: x=" aoc:integer-string ".." aoc:integer-string ", y=" aoc:integer-string ".." aoc:integer-string (? #\Newline))
  (:choose 1 3 5 7)
  (:lambda (x0 x1 y0 y1)
    (list (point:make-point x0 y0)
          (point:make-point x1 y1))))


;;;; Input

(defparameter *example* (parseq:parseq 'target "target area: x=20..30, y=-10..-5"))
(defparameter *target-area* (aoc:input :parse 'target))


;;;; Part 1

(defun triangular-number (n)
  (/ (* n (1+ n)) 2))

(defun inverse-triangular-number (n)
  (/ (1- (sqrt (1+ (* 8 n))))
     2))

(defun x-pos-at-time (initial-dx time)
  "TIME is number of steps."
  (if (<= initial-dx time)
      (triangular-number initial-dx)
      (- (triangular-number initial-dx)
         (triangular-number (- initial-dx time)))))

(defun y-pos-at-time (initial-dy time)
  "TIME is number of steps."
  (cond
    ((<= time initial-dy)
     (- (triangular-number initial-dy)
        (triangular-number (- initial-dy time))))
    ((< (1+ initial-dy) time)
     (- (triangular-number initial-dy)
        (triangular-number (- time initial-dy 1))))
    (t
     (triangular-number initial-dy))))

(defun min-dx-for-target (target-area)
  (let ((min-point (first target-area)))
    (ceiling (inverse-triangular-number (point:x min-point)))))

(defun valid-dx? (target-area dx)
  (destructuring-bind (min-point max-point) target-area
    (series:collect-first
     (series:choose-if (alexandria:curry #'<= (point:x min-point))
                       (series:until-if (alexandria:curry #'< (point:x max-point))
                                        (series:map-fn 'integer
                                                       (alexandria:curry #'x-pos-at-time dx)
                                                       (series:scan-range :from 1
                                                                          :upto dx)))))))

(defun list-valid-dx (target-area)
  (let ((max-point (second target-area)))
    (series:collect
        (series:choose-if (alexandria:curry #'valid-dx? target-area)
                          (series:scan-range :from (min-dx-for-target target-area)
                                             :upto (point:x max-point))))))

(defun valid-dy? (target-area dy)
  (destructuring-bind (min-point max-point) target-area
    (series:collect-first
     (series:choose-if (alexandria:curry #'>= (point:y max-point))
                       (series:until-if (alexandria:curry #'> (point:y min-point))
                                        (series:map-fn 'integer
                                                       (alexandria:curry #'y-pos-at-time dy)
                                                       (series:scan-range :from 1
                                                                          :upto (* 2 (- (point:y min-point))))))))))

(defun list-valid-dy (target-area)
  (let ((min-point (first target-area)))
    (series:collect
        (series:choose-if (alexandria:curry #'valid-dy? target-area)
                          (series:scan-range :from (point:y min-point)
                                             :upto (- (point:y min-point)))))))

(defun valid-vector? (target-area dx dy)
  (destructuring-bind (min-point max-point) target-area
    (flet ((vector-at-time (time)
             (point:make-point (x-pos-at-time dx time)
                               (y-pos-at-time dy time))))
      (series:collect-first
       (series:choose-if (lambda (point)
                           (and (<= (point:x min-point) (point:x point))
                                (<= (point:y point) (point:y max-point))))
                         (series:until-if (lambda (point)
                                            (or (< (point:x max-point) (point:x point))
                                                (< (point:y point) (point:y min-point))))
                                          (series:map-fn 'point:point
                                                         #'vector-at-time
                                                         (series:scan-range :from 1
                                                                            :upto (* 2 (- (point:y min-point)))))))))))

(defun list-valid-vectors (target-area)
  ;; Tired of arguing with Series for today.
  (iter outer
    (for dy in (list-valid-dy target-area))
    (iter (for dx in (list-valid-dx target-area))
      (when (valid-vector? target-area dx dy)
        (in outer (collecting (point:make-point dx dy)))))))

(defun find-peak-y (vectors)
  (iter (for vector in vectors)
    (maximizing (y-pos-at-time (point:y vector) (point:y vector)))))

(defun get-answer-1 (&optional (target-area *target-area*))
  (find-peak-y (list-valid-vectors target-area)))


;;;; Part 2

(defun get-answer-2 (&optional (target-area *target-area*))
  (length (list-valid-vectors target-area)))
