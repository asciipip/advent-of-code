#!/usr/bin/env python3
"""Just for fun, I decided to solve this using NumPy.  The regularity of
the data made it possible to easily process all of the Bingo boards at
once."""

import numpy as np


# Boring stuff to fetch the input into a local cache and return it.
import pathlib
import requests
import toml
AOCRC_FILE = pathlib.Path.home().joinpath('.aocrc')
def fetch_input():
    config = toml.load(AOCRC_FILE)
    #cache_file = pathlib.Path(config['cachedir']).expanduser().joinpath('input.2021-04')
    cache_file = pathlib.Path('/tmp/4-900-15.in')
    if not cache_file.exists():
        url = 'https://adventofcode.com/2021/day/4/input'
        cookies = {'session': config['session']}
        r = requests.get(url, cookies=cookies)
        with open(cache_file, 'w') as input_file:
            input_file.write(r.text)
    with open(cache_file) as input_file:
        return input_file.read()

# Returns a tuple of the numbers as a list, and the Bingo boards as a
# NumPy array.  The array will be Nx5x5, where N is the number of boards
# supplied.
def parse_input(data):
    numbers = [int(n) for n in data.split('\n\n', 1)[0].split(',')]
    cells = np.asarray([int (n) for n in data.split('\n\n', 1)[1].split()])
    cells = cells.reshape((len(cells) // 25, 5, 5))
    return (numbers, cells)

# Replaces all occurrences of the number in all of the boards with the
# sentinel value -1.
def mark_cells(number, cells):
    cells[cells == number] = -1

# Returns a NumPy vector with the indices of all winning boards.  Since
# marked cells have the value -1, winning rows and columns will sum to -5.
# (And *only* winning rows and columns will have that total, since all
# initial cell values are positive.)
def winning_boards(cells):
    boards = np.concatenate([
        np.argwhere(cells.sum(1) == -5)[:,0],
        np.argwhere(cells.sum(2) == -5)[:,0],
    ])
    return boards

# For the score, add all unmarked cell values (and then multiply by the
# last-picked number).  Easy: just remove all the negative numbers and sum
# the rest of the values.
def board_score(n, board):
    return n * (board[board > 0].sum())


if __name__ == '__main__':
    numbers, cells = parse_input(fetch_input())

    scores = []
    for n in numbers:
        mark_cells(n, cells)
        winners = winning_boards(cells)
        for w in winners:
            scores.append(board_score(n, cells[w]))
        cells = np.delete(cells, winners, axis=0)

    print("Part 1:", scores[0])
    print("Part 2:", scores[-1])
