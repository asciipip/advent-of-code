(in-package :aoc-2021-20)

(aoc:define-day 5475 17548)


;;;; Data Structures

#|

The enhancement algorithm is just a 512-element bit array.

The image is an FSet map from points to bits.  The map's default is what
the unlisted points are.  This might change from step to step, depending
on the algorithm's zero element.

|#


;;;; Parsing

(parseq:defrule input ()
    (and algorithm (+ #\Newline) image)
  (:choose 0 2))

(parseq:defrule algorithm ()
    (rep 512 bit)
  (:lambda (&rest bits)
    (make-array 512
                :element-type 'bit
                :initial-contents bits)))

(parseq:defrule image ()
    (+ (and (+ bit) (? #\Newline)))
  (:lambda (&rest lines)
    (let ((result (fset:empty-map 0)))
      (series:iterate ((line (series:scan lines))
                       (y (series:scan-range)))
        (series:iterate ((bit (series:scan (first line)))
                         (x (series:scan-range)))
          (fset:adjoinf result (point:make-point x y) bit)))
      result)))

(parseq:defrule bit ()
    (char "#.")
  (:lambda (char)
    (ecase char
      (#\. 0)
      (#\# 1))))

;;;; Input

(defparameter *example* (aoc:input :parse 'input :file "examples/20.txt"))
(defparameter *input* (aoc:input :parse 'input))


;;;; Part 1

;;; The order here matters!  It should go from LSB to MSB of the algorithm
;;; index.
(defparameter +neighbor-vectors+
  (let ((result nil))
    (dolist (y '(1 0 -1))
      (dolist (x '(1 0 -1))
        (push (point:make-point x y) result)))
    result))

(defun print-image (image)
  (aoc:print-map-braille image :key #'plusp))

(defun point-algorithm-index (image point)
  (let ((result 0))
    (dolist (vector +neighbor-vectors+)
      (setf result
            (+ (* result 2)
               (fset:lookup image (point:+ point vector)))))
    result))

(defun algorithm-aref (algorithm image point)
  (sbit algorithm (point-algorithm-index image point)))

(defun enhance-step (algorithm image)
  (let ((new-image (fset:empty-map (sbit algorithm
                                         (if (zerop (fset:map-default image))
                                             0
                                             511)))))
    (multiple-value-bind (min-point max-point)
        (point:bbox (fset:domain image))
      (series:iterate ((x (series:scan-range :from (1- (point:x min-point))
                                             :upto (1+ (point:x max-point)))))
        (series:iterate ((y (series:scan-range :from (1- (point:y min-point))
                                               :upto (1+ (point:y max-point)))))
          (let ((point (point:make-point x y)))
            (fset:adjoinf new-image point (algorithm-aref algorithm image point))))))
    new-image))

(defun enhance (algorithm image steps)
  (if (plusp steps)
      (enhance algorithm (enhance-step algorithm image) (1- steps))
      image))

(defun get-answer-1 (&optional (input *input*) (steps 2))
  (fset:reduce (lambda (count _ bit)
                 (declare (ignore _))
                 (+ count bit))
               (enhance (first input) (second input) steps)
               :initial-value 0))


;;;; Part 2

(defun get-answer-2 ()
  (get-answer-1 *input* 50))
