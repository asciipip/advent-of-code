(in-package :aoc-2021-25)

;;; It's day 25, so part 2 is a gimme.
(aoc:define-day 530 nil)


;;;; Data Structures

(defclass region ()
  ((east-herd :initarg :east-herd
              :initform (fset:empty-set))
   (south-herd :initarg :south-herd
               :initform (fset:empty-set))
   (size :initarg :size)))

(defun update-region (region &key east-herd south-herd)
  (with-slots ((old-east-herd east-herd) (old-south-herd south-herd) size) region
    (make-instance 'region
                   :east-herd (or east-herd old-east-herd)
                   :south-herd (or south-herd old-south-herd)
                   :size size)))

(defmethod print-object ((region region) stream)
  (with-slots (east-herd south-herd size) region
    (print-unreadable-object (region stream :type t)
      (princ #\Newline stream)
      (dotimes (y (point:y size))
        (princ "  " stream)
        (dotimes (x (point:x size))
          (let ((point (point:make-point x y)))
            (cond
              ((fset:contains? east-herd point)
               (princ #\> stream))
              ((fset:contains? south-herd point)
               (princ #\v stream))
              (t
               (princ #\. stream)))))
        (princ #\Newline stream)))))

(defun region= (region1 region2)
  (with-slots ((e1 east-herd) (s1 south-herd)) region1
    (with-slots ((e2 east-herd) (s2 south-herd)) region2
      (and (fset:equal? e1 e2)
           (fset:equal? s1 s2)))))


;;;; Parsing

(defun parse-map (lines)
  (let ((east-herd (fset:empty-set))
        (south-herd (fset:empty-set)))
    (series:iterate ((line (series:scan lines)) (y (series:scan-range)))
      (series:iterate ((char (series:scan 'string line)) (x (series:scan-range)))
        (case char
          (#\> (fset:adjoinf east-herd (point:make-point x y)))
          (#\v (fset:adjoinf south-herd (point:make-point x y))))))
    (make-instance 'region
                   :east-herd east-herd
                   :south-herd south-herd
                   :size (point:make-point (length (first lines)) (length lines)))))


;;;; Input

(defparameter *example* (parse-map (aoc:input :file "examples/25.txt")))
(defparameter *region* (parse-map (aoc:input)))


;;;; Part 1

(defun move-east-cucumber (region point &optional (delta 1))
  (with-slots (size) region
    (point:make-point (mod (+ delta (point:x point)) (point:x size))
                      (point:y point))))

(defun move-east (region)
  (with-slots (east-herd south-herd) region
    (let* ((proposed-positions (fset:image (alexandria:curry #'move-east-cucumber region)
                                           east-herd))
           (available-positions (fset:set-difference (fset:set-difference
                                                      proposed-positions east-herd)
                                                     south-herd))
           (moving-cucumbers (fset:image (lambda (point)
                                           (move-east-cucumber region point -1))
                                         available-positions)))
      (update-region region
                     :east-herd (fset:union (fset:set-difference east-herd
                                                                 moving-cucumbers)
                                            available-positions)))))

(defun move-south-cucumber (region point &optional (delta 1))
  (with-slots (size) region
    (point:make-point (point:x point)
                      (mod (+ delta (point:y point)) (point:y size)))))

(defun move-south (region)
  (with-slots (east-herd south-herd) region
    (let* ((proposed-positions (fset:image (alexandria:curry #'move-south-cucumber region)
                                           south-herd))
           (available-positions (fset:set-difference (fset:set-difference
                                                      proposed-positions south-herd)
                                                     east-herd))
           (moving-cucumbers (fset:image (lambda (point)
                                           (move-south-cucumber region point -1))
                                         available-positions)))
      (update-region region
                     :south-herd (fset:union (fset:set-difference south-herd
                                                                  moving-cucumbers)
                                             available-positions)))))

(defun find-stasis (region &optional (steps 0) last-region)
  (if (and last-region
           (region= region last-region))
      (values steps region)
      (find-stasis (move-south (move-east region))
                   (1+ steps)
                   region)))

(defun get-answer-1 (&optional (region *region*))
  (nth-value 0 (find-stasis region)))
