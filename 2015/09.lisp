(in-package :aoc-2015-09)

(aoc:define-day 207 804)


;;;; Input

(defparameter *input* (aoc:input))
(defparameter *test-input*
  '("London to Dublin = 464"
    "London to Belfast = 518"
    "Dublin to Belfast = 141"))


;;;; Part 1

(defstruct segment
  start end distance)

(defun parse-segment (segment-str)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings "^(.+) to (.+) = (\\d+)$" segment-str))))
    (let ((city1 (svref groups 0))
          (city2 (svref groups 1))
          (distance (parse-number:parse-number (svref groups 2))))
      (values (make-segment :start city1 :end city2 :distance distance)
              (make-segment :end city1 :start city2 :distance distance)))))

(defun parse-input (input)
  (iter (for i in input)
        (for (values e1 e2) = (parse-segment i))
        (collect e1)
        (collect e2)))

(defun segments-from (city segments)
  (iter (for segment in segments)
        (when (string= city (segment-start segment))
          (collect segment))))

(defun available-cities (segments)
  (remove-duplicates (mapcar #'segment-start segments) :test #'string=))

(defun distance (start end segments)
  (iter (for segment in segments)
        (when (and (string= start (segment-start segment))
                   (string= end (segment-end segment)))
          (return (segment-distance segment)))
        (finally (assert nil
                         (start end)
                         "Unable to find route ~A -> ~A in ~A" start end segments))))

(defun all-permutations (xs)
  (if (endp xs)
      '(())
      (iter (for e in xs)
            (appending (iter (for rs in (all-permutations (remove e xs)))
                             (collecting (cons e rs)))))))

(defun route-distance (route segments)
  (iter (for s in route)
        (for e in (cdr route))
        (summing (distance s e segments))))

(defun find-minimum-route (segments)
  (values-list
   (iter (for route in (all-permutations (available-cities segments)))
         (for route-distance = (route-distance route segments))
         (finding (list route-distance route) minimizing route-distance))))

(aoc:given 1
  (= 605 (find-minimum-route (parse-input *test-input*))))

(defun get-answer-1 ()
  (find-minimum-route (parse-input *input*)))


;;;; Part 2

(defun find-maximum-route (segments)
  (values-list
   (iter (for route in (all-permutations (available-cities segments)))
         (for route-distance = (route-distance route segments))
         (finding (list route-distance route) maximizing route-distance))))

(aoc:given 2
  (= 982 (find-maximum-route (parse-input *test-input*))))

(defun get-answer-2 ()
  (find-maximum-route (parse-input *input*)))
