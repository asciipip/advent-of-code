(in-package :aoc-2015-16)

(aoc:define-day 103 405)


;;;; Input

(defparameter *target*
  '((:children . 3)
    (:cats . 7)
    (:samoyeds . 2)
    (:pomeranians . 3)
    (:akitas . 0)
    (:vizslas . 0)
    (:goldfish . 5)
    (:trees . 3)
    (:cars . 2)
    (:perfumes . 1)))

(defun parse-aunt (string)
  (labels ((parse-thing (thing)
             (cons (alexandria:make-keyword (string-upcase (first (ppcre:split ": " thing))))
                   (aoc:extract-ints thing))))
    (destructuring-bind (name-string thing-string) (ppcre:split ": " string :limit 2)
      (cons (aoc:extract-ints name-string)
            (mapcar #'parse-thing (ppcre:split ", " thing-string))))))

(defparameter *aunts* (mapcar #'parse-aunt (aoc:input)))


;;;; Part 1

(defun things-match-p (reference other)
  "Returns true if all items in OTHER are also present in REFERENCE.  Not
  ever item in REFERENCE need be in OTHER.  Both items should be alists."
  (iter (for thing in other)
        (always (equal thing (assoc (car thing) reference)))))

(defun get-answer-1 ()
  (iter (for aunt in *aunts*)
        (finding (car aunt) such-that (things-match-p *target* (cdr aunt)))))


;;;; Part 2

(defun things-match-2-p (reference other)
  "Returns true if the things in OTHER match REFERENCE according to the
  rules in part 2 of the problem."
  (iter (for thing in other)
        (always
         (case (car thing)
           ((:cats :trees)
            (< (cdr (assoc (car thing) reference)) (cdr thing)))
           ((:pomeranians :goldfish)
            (< (cdr thing) (cdr (assoc (car thing) reference))))
           (t
            (= (cdr thing) (cdr (assoc (car thing) reference))))))))

(defun get-answer-2 ()
  (iter (for aunt in *aunts*)
        (finding (car aunt) such-that (things-match-2-p *target* (cdr aunt)))))
