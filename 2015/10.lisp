(in-package :aoc-2015-10)

(aoc:define-day 329356 4666278)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Part 1

(defclass string-generator ()
  ((string
    :initarg :string)
   (index
    :initform 0)))

(defclass composite-generator ()
  ((input-generator
    :initarg :input)
   (last-char
    :initform nil)
   (pending-string
    :initform nil)
   (pending-index)
   (finished
    :initform nil)))

(defgeneric next-char (generator))

(defmethod next-char ((generator string-generator))
  (with-slots (string index) generator
      (if (< index (length string))
          (prog1
              (schar string index)
            (incf index))
          nil)))

(defun composite-next-pending-char (generator)
  (with-slots (pending-string pending-index) generator
    (if (and pending-string
             (< pending-index (length pending-string)))
        (prog1
            (schar pending-string pending-index)
          (incf pending-index))
        nil)))

(defun composite-make-pending-string (generator)
  (with-slots (input-generator last-char pending-string pending-index finished) generator
    (let ((current-char (or last-char (next-char input-generator)))
          (char-count 1))
      (if (null current-char)
          (setf finished t)
          (progn
            (iter (for c = (next-char input-generator))
                  (if (not (eq current-char c))
                      (progn
                        (setf last-char c)
                        (finish))
                      (incf char-count)))
            (setf pending-string (format nil "~A~A" char-count current-char))
            (setf pending-index 0))))))

(defmethod next-char ((generator composite-generator))
  (with-slots (input-generator last-char pending-string pending-index finished) generator
    (cond
      (finished nil)
      ((and pending-string
            (< pending-index (length pending-string)))
       (composite-next-pending-char generator))
      (t (progn
           (composite-make-pending-string generator)
           (composite-next-pending-char generator))))))

(defun iterate-string-expansion (string n)
  (let ((generator (iter (repeat n)
                         (for result
                              initially (make-instance 'string-generator :string string)
                              then (make-instance 'composite-generator :input result))
                         (finally (return result)))))
    (iter (for c = (next-char generator))
          (while c)
          (collecting c result-type 'string))))

(defun iterate-string-length (string n)
  (let ((generator (iter (repeat n)
                         (for result
                              initially (make-instance 'string-generator :string string)
                              then (make-instance 'composite-generator :input result))
                         (finally (return result)))))
    (iter (for c = (next-char generator))
          (while c)
          (summing 1))))

(aoc:given 1
  (string= "1" (iterate-string-expansion "1" 0))
  (string= "11" (iterate-string-expansion "1" 1))
  (string= "21" (iterate-string-expansion "1" 2))
  (string= "1211" (iterate-string-expansion "1" 3))
  (string= "111221" (iterate-string-expansion "1" 4))
  (string= "312211" (iterate-string-expansion "1" 5)))

(defun get-answer-1 (&optional (iterations 40))
  (iterate-string-length *input* iterations))


;;;; Part 2

(defun get-answer-2 (&optional (iterations 50))
  (get-answer-1 iterations))
