(in-package :aoc-2015-18)

(aoc:define-day 814 924)


;;;; Input

(defparameter *test-input*
  '(".#.#.#"
    "...##."
    "#....#"
    "..#..."
    "#.#..#"
    "####.."))

(defun parse-grid (lines)
  (let ((grid (make-array (list (length lines) (length (first lines)))
                          :element-type 'bit
                          :initial-element 0)))
    (iter (for line in lines)
          (for i from 0)
          (iter (for c in-string line with-index j)
                (when (char= #\# c)
                  (setf (aref grid i j) 1))))
    grid))

(defparameter *test-grid* (parse-grid *test-input*))
(defparameter *grid* (parse-grid (aoc:input)))

(defvar *stuck-corners* nil)


;;;; Part 1

(defun turn-on-corners (grid)
  (let ((new-grid (make-array (aops:dims grid) :element-type 'bit)))
    (aops:copy-into new-grid grid)
    (iter (for i in (list 0 (1- (aops:dim grid 0))))
          (iter (for j in (list 0 (1- (aops:dim grid 1))))
                (setf (aref new-grid i j) 1)))
    new-grid))

(defun neighbors-on (grid i j)
  (iter outer
        (for ip from (max 0 (1- i)) to (min (1+ i) (1- (aops:dim grid 0))))
        (iter (for jp from (max 0 (1- j)) to (min (1+ j) (1- (aops:dim grid 1))))
              (format t "")
              (when (or (/= ip i)
                        (/= jp j))
                (in outer
                    (summing (aref grid ip jp)))))))

(defun turn-on (grid i j)
  (or (and *stuck-corners*
           (or (zerop i) (= i (1- (aops:dim grid 0))))
           (or (zerop j) (= j (1- (aops:dim grid 1)))))
      (let ((neighbors (neighbors-on grid i j)))
        (or (and (= 1 (aref grid i j))
                 (<= 2 neighbors 3))
            (and (zerop (aref grid i j))
                 (= neighbors 3))))))

(defun step-grid (grid)
  (let ((new-grid (make-array (aops:dims grid) :element-type 'bit :initial-element 0)))
    (iter (for i from 0 below (aops:dim grid 0))
          (iter (for j from 0 below (aops:dim grid 1))
                (when (turn-on grid i j)
                  (setf (aref new-grid i j) 1))))
    new-grid))

(defun step-grid-n (grid n)
  (if (<= n 0)
      grid
      (step-grid-n (step-grid grid) (1- n))))

(defun lights-after-n-steps (grid n)
  (reduce #'+ (aops:flatten (step-grid-n grid n))))

(aoc:given 1
  (= 4 (lights-after-n-steps *test-grid* 4)))

(defun get-answer-1 ()
  (lights-after-n-steps *grid* 100))


;;;; Part 2

(aoc:deftest given-2
  (let ((*stuck-corners* t))
    (= 17 (lights-after-n-steps (turn-on-corners *test-grid*) 4))))

(defun get-answer-2 ()
  (let ((*stuck-corners* t))
    (lights-after-n-steps (turn-on-corners *grid*) 100)))
