(in-package :aoc-2015-12)

(aoc:define-day 119433 68466)


;;;; Input

(defparameter *document* (yason:parse (aoc:input)))


;;;; Part 1

(defun document-sum (document &optional exclude)
  (cond
    ((null document)
     0)
    ((numberp document)
     document)
    ((consp document)
     (+ (document-sum (car document) exclude)
        (document-sum (cdr document) exclude)))
    ((hash-table-p document)
     (iter (for (k v) in-hashtable document)
           (when (equal exclude v)
             (leave 0))
           (summing (document-sum v exclude))))
    (t
     0)))

(defmacro test-sums (sum-list)
  `(progn
     ,@(iter (for (string expected-result) in sum-list)
             (collecting `(5am:is (= ,expected-result
                                     (document-sum (yason:parse ,string))))))))

(aoc:deftest given-1
  (test-sums (("[1,2,3]" 6)
              ("{\"a\":2,\"b\":4}" 6)
              ("[[[3]]]" 3)
              ("{\"a\":{\"b\":4},\"c\":-1}" 3)
              ("{\"a\":[-1,1]}" 0)
              ("[-1,{\"a\":1}]" 0)
              ("[]" 0)
              ("{}" 0))))

(defun get-answer-1 ()
  (document-sum *document*))


;;;; Part 2

(defmacro test-excluded-sums (exclude sum-list)
  `(progn
     ,@(iter (for (string expected-result) in sum-list)
             (collecting `(5am:is (= ,expected-result
                                     (document-sum (yason:parse ,string)
                                                   ,exclude)))))))

(aoc:deftest given-2
  (test-excluded-sums
   "red"
   (("[1,2,3]" 6)
    ("[1,{\"c\":\"red\",\"b\":2},3]" 4)
    ("{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}" 0)
    ("[1,\"red\",5]" 6))))

(defun get-answer-2 ()
  (document-sum *document* "red"))
