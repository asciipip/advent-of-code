(in-package :aoc-2015-17)

(aoc:define-day 654 57)


;;;; Input

(defparameter *test-containers* '(20 15 10 5 5))
(defparameter *containers* (mapcar #'aoc:extract-ints (aoc:input)))


;;;; Part 1

;;; TAOCP Volume 4A §7.2.1.3 Algorithm F
(defun visit-constrained-subsets (visit-func containers capacity)
  (let ((n (length containers)))
    (let ((ws (make-array n :element-type '(integer 1) :initial-contents (sort (copy-seq containers) #'<)))
          (ds (make-array n :element-type '(integer 0))))
      (iter (for w in-vector ws with-index j from 1)
            (setf (svref ds j) (- (svref ws j) (svref ws (1- j)))))
      (labels ((fill-r (cs r)
                 (funcall visit-func (- capacity r) (mapcar (lambda (c) (svref ws c)) cs))
                 (cond
                   ((and (/= 0 (car cs))
                         (<= (svref ws 0) r))
                    (fill-r (cons 0 cs) (- r (svref ws 0))))
                   (t
                    (next-container cs r))))
               (next-container (cs r)
                 (cond
                   ((endp cs)
                    (values))
                   ((and (< (car cs) (1- n))
                         (or (and (endp (cdr cs)))
                             (< (1+ (first cs)) (second cs)))
                         (<= (svref ds (1+ (car cs))) r))
                    (fill-r (cons (1+ (car cs)) (cdr cs))
                            (- r (svref ds (1+ (car cs))))))
                   (t
                    (next-container (cdr cs) (+ r (svref ws (car cs))))))))
        (fill-r (list 0) (- capacity (svref ws 0)))))))

(defun fill-eggnog (containers capacity)
  (let ((count 0))
    (visit-constrained-subsets (lambda (c cs)
                                 (declare (ignore cs))
                                 (when (= c capacity)
                                   (incf count)))
                               containers capacity)
    count))

(aoc:given 1
  (= 4 (fill-eggnog *test-containers* 25)))

(defun get-answer-1 ()
  (fill-eggnog *containers* 150))


;;;; Part 2

(defun count-minimum-combinations (containers capacity)
  (let ((counts (make-hash-table)))
    (visit-constrained-subsets (lambda (c cs)
                                 (when (= c capacity)
                                   (incf (gethash (length cs) counts 0))))
                               containers capacity)
    (iter (for (k v) in-hashtable counts)
          (finding v minimizing k))))

(aoc:given 2
  (= 3 (count-minimum-combinations *test-containers* 25)))

(defun get-answer-2 ()
  (count-minimum-combinations *containers* 150))
