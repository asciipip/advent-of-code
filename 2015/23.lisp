(in-package :aoc-2015-23)

(aoc:define-day 307 160)

;;;; Input

(defparameter *example*
  '("inc a"
    "jio a, +2"
    "tpl a"
    "inc a"))

(defparameter *program* (aoc:input))


;;;; Instructions

(defstruct machine
  (registers (vector 0 0))
  (ip 0)
  instructions)

(defun register-index (register)
  (ecase register
    (a 0)
    (b 1)))

(eval-when (:compile-toplevel :load-toplevel)
  (defparameter *instructions* (make-hash-table)))

(defmacro instruction (name params &body body)
  (let* ((param-gensyms (make-hash-table))
         (param-symbols (iter (for p in params)
                              (collecting
                                (if (consp p)
                                    (progn
                                      (assert (eq :register (first p)))
                                      (setf (gethash (second p) param-gensyms)
                                            (gensym (symbol-name (second p)))))
                                    p)))))
    (alexandria:with-gensyms (machine
                              offset)
      `(setf (gethash ',name *instructions*)
             (lambda ,param-symbols
               (lambda (,machine)
                 (flet ((jump (,offset)
                          (incf (machine-ip ,machine) (1- ,offset))))
                   (declare (ignorable (function jump)))
                   (symbol-macrolet
                       (,@(iter (for (param symbol) in-hashtable param-gensyms)
                                (collecting `(,param (svref (machine-registers ,machine)
                                                            (register-index ,symbol))))))
                     ,@body))))))))

(instruction hlf ((:register r))
  (setf r (truncate r 2)))

(instruction tpl ((:register r))
  (setf r (* 3 r)))

(instruction inc ((:register r))
  (setf r (1+ r)))

(instruction jmp (offset)
  (jump offset))

(instruction jie ((:register r) offset)
  (when (zerop (mod r 2))
    (jump offset)))

(instruction jio ((:register r) offset)
  (when (= 1 r)
    (jump offset)))


;;;; Compilation

(defun tokenize-string (string)
  (flet ((tokenize-element (elt)
           (handler-case
               (parse-integer elt)
             (sb-int:simple-parse-error () (intern (string-upcase elt))))))
    (mapcar #'tokenize-element (cl-ppcre:split ",? +" string))))

(defun compile-program (instruction-strings)
  (iter (for string in instruction-strings)
        (destructuring-bind (instruction &rest params) (tokenize-string string)
          (collecting (apply (gethash instruction *instructions*) params)
                      result-type vector))))

(defun load-program (instructions)
  (make-machine :instructions instructions))

(defun step-instruction (machine)
  (funcall (svref (machine-instructions machine)
                  (machine-ip machine))
           machine)
  (incf (machine-ip machine))
  (values))

(defun run-program (instruction-strings &key register-a)
  (let ((machine (load-program (compile-program instruction-strings))))
    (when register-a
      (setf (svref (machine-registers machine) (register-index 'a))
             register-a))
    (iter (while (< -1 (machine-ip machine) (length (machine-instructions machine))))
          (step-instruction machine))
    machine))


;;;; Part 1

(aoc:deftest given-1
  (let ((example-run (run-program *example*)))
    (5am:is (= 2 (svref (machine-registers example-run) (register-index 'a))))))

(defun get-answer-1 ()
  (let ((final-state (run-program *program*)))
    (svref (machine-registers final-state) (register-index 'b))))


;;;; Part 2

(defun get-answer-2 ()
  (let ((final-state (run-program *program* :register-a 1)))
    (svref (machine-registers final-state) (register-index 'b))))


;;;; Analysis

#|

My input program simplifies to the following:

if a != 1:
    a = 26623
else:
    a = 31911
while a != 1:
    b += 1
    if a % 2 == 1:
        a *= 3
        a += 1
    else:
        a /= 2

In other words, the initial value of register A causes the program to
choose one of two initialization values.  Then it runs that value through
a loop.  If the value is even, it's divided in half.  (NB: This is the
only use of the division operation, so the issue of how to implement
incomplete integer division is sidestepped.)  If the value is odd, it's
multiplied by three (preserving its oddness) then one is added to
it (making it even).

Register B simply counts how many times through the loop were necessary
for the value in register A to reach one.

|#
