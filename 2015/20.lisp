(in-package :aoc-2015-20)

(aoc:define-day 665280 705600)


;;;; Input

(defparameter *limit* (parse-integer (aoc:input)))


;;;; Part 1

;;; This could be done a little faster by calculating σ₁(n) rather than
;;; listing all the divisors separately.  But that requires more
;;; scaffolding to get the prime factorization of n first.  This was
;;; easier to code.  Also, it wouldn't apply cleanly to part 2.
;;;
;;; Many participants calculated the result faster by making a large array
;;; and simulating the present deliveries.  This seems algorithmically
;;; worse on the face of it, but is faster because the process of getting
;;; a number's divisors is pretty costly.  I didn't bother to go back and
;;; do it this way because the approach below is fas enough.  (<10s on my
;;; laptop at the time.)

(defun list-divisors (num)
  (iter (for i from 1 to (floor (sqrt num)))
        (when (zerop (mod num i))
          (collecting i)
          (when (/= i (/ num i))
            (collecting (/ num i))))))

(defun presents-per-house (house-number)
  (* 10 (apply #'+ (list-divisors house-number))))

(aoc:given 1
  (= 10 (presents-per-house 1))
  (= 30 (presents-per-house 2))
  (= 40 (presents-per-house 3))
  (= 70 (presents-per-house 4))
  (= 60 (presents-per-house 5))
  (= 120 (presents-per-house 6))
  (= 80 (presents-per-house 7))
  (= 150 (presents-per-house 8))
  (= 130 (presents-per-house 9)))

(defun get-answer-1 ()
  (iter (for i from 1)
        (finding i such-that (<= *limit* (presents-per-house i)))))


;;;; Part 2

(defun presents-per-house-2 (house-number)
  (let ((elf-numbers (remove-if (lambda (n) (< (* n 50) house-number))
                                (list-divisors house-number))))
    (* 11 (apply #'+ elf-numbers))))

(defun get-answer-2 ()
  (iter (for i from 1)
        (finding i such-that (<= *limit* (presents-per-house-2 i)))))
