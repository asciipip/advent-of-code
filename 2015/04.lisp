(in-package :aoc-2015-04)

(aoc:define-day 282749 9962624)


;;;; Input

(defvar *input* (aoc:input))


;;;; Part 1

(defun hash-matches-p (hash zeroes)
  (iter (for v in-vector hash)
        (for z from zeroes above 0 by 2)
        (always (or (and (<= 2 z)
                         (= v 0))
                    (and (= 1 z)
                         (< v 16))))))

(defun find-hash (key zeroes)
  (labels ((try-number (i)
             (if (hash-matches-p (md5:md5sum-string (concatenate 'string key (write-to-string i)))
                                 zeroes)
                 i
                 (try-number (1+ i)))))
    (try-number 1)))

(aoc:given 1
  (= 609043 (find-hash "abcdef" 5))
  (= 1048970 (find-hash "pqrstuv" 5)))

(defun get-answer-1 ()
  (find-hash *input* 5))


;;;; Part 2

(defun get-answer-2 ()
  (find-hash *input* 6))
