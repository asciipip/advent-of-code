(in-package :aoc-2015-03)

(aoc:define-day 2081 2341)


;;;; Input

(defvar *input* (aoc:input))


;;;; Part 1

(defun move (old-loc direction)
  (cond
    ((char= direction #\^) (+ old-loc #C( 0  1)))
    ((char= direction #\v) (+ old-loc #C( 0 -1)))
    ((char= direction #\>) (+ old-loc #C( 1  0)))
    ((char= direction #\<) (+ old-loc #C(-1  0)))))

(defun walk-instructions (directions)
  (let ((visited (make-hash-table :test #'equal))
        (loc 0))
    (setf (gethash loc visited) 1)
    (iter (for c in-string directions)
          (setf loc (move loc c))
          (setf (gethash loc visited)
                (1+ (gethash loc visited 0))))
    visited))

(defun houses-visited (instructions)
  (hash-table-count (walk-instructions instructions)))

(aoc:given 1
   (= 2 (houses-visited ">"))
   (= 4 (houses-visited "^>v<"))
   (= 2 (houses-visited "^v^v^v^v^v")))

(defun get-answer-1 ()
  (houses-visited *input*))


;;;; Part 2

(defun every-other (string &optional (start 0))
  (iter (for c in-string string from start by 2)
        (collect c into result result-type string)
        (finally (return result))))

(defun robo-houses-visited (instructions)
  (let ((santa-houses (every-other instructions))
        (robo-houses (every-other instructions 1)))
    (let ((house-visits (walk-instructions santa-houses)))
      (iter (for (house visits) in-hashtable (walk-instructions robo-houses))
            (incf (gethash house house-visits 0) visits))
      (hash-table-count house-visits))))

(aoc:given 2
   (= 3 (robo-houses-visited "^v"))
   (= 3 (robo-houses-visited "^>v<"))
   (= 11 (robo-houses-visited "^v^v^v^v^v")))

(defun get-answer-2 ()
  (robo-houses-visited *input*))
