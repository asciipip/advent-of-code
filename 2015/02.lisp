(in-package :aoc-2015-02)

(aoc:define-day 1598415 3812909)


;;;; Input

(defvar *input* (aoc:input))


;;;; Part 1

(defun wrapping-paper-area-ints (l w h)
  (let* ((min-dim (min l w h))
         (mid-dim (apply #'min (remove min-dim (list l w h) :count 1))))
    (+ (* 2 l w)
       (* 2 w h)
       (* 2 h l)
       (* min-dim mid-dim))))

(defun wrapping-paper-area (dimension-str)
  (let ((dimensions (aoc:extract-ints dimension-str)))
    (apply #'wrapping-paper-area-ints dimensions)))

(aoc:given 1
   (= 58 (wrapping-paper-area "2x3x4"))
   (= 43 (wrapping-paper-area "1x1x10")))

(defun get-answer-1 ()
  (apply #'+ (mapcar #'wrapping-paper-area *input*)))


;;;; Part 2

(defun ribbon-length-ints (l w h)
  (let* ((min-dim (min l w h))
         (mid-dim (apply #'min (remove min-dim (list l w h) :count 1))))
    (+ (* 2 min-dim)
       (* 2 mid-dim)
       (* l w h))))

(defun ribbon-length (dimension-str)
  (let ((dimensions (aoc:extract-ints dimension-str)))
    (apply #'ribbon-length-ints dimensions)))

(aoc:given 2
  (= 34 (ribbon-length "2x3x4"))
  (= 14 (ribbon-length "1x1x10")))

(defun get-answer-2 ()
  (apply #'+ (mapcar #'ribbon-length *input*)))

