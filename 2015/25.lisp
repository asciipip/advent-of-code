(in-package :aoc-2015-25)

(aoc:define-day 8997277 nil)


;;;; Input

(defparameter *coords* (aoc:extract-ints (aoc:input)))


;;;; Part 1

(defun coords-to-pos (row column)
  (let ((row-basis (1+ (aoc:sum-from-to 1 (1- row))))
        (col-basis (1+ row)))
    (+ row-basis (aoc:sum-from-to col-basis (+ col-basis column -2)))))

(defun sequence-at (pos &key (seed 20151125) (multiplicand 252533) (divisor 33554393))
  (iter (for value first seed then (mod (* value multiplicand) divisor))
        (for index from 1)
        (finding value such-that (= index pos))))

(defun get-answer-1 ()
  (sequence-at (apply #'coords-to-pos *coords*)))


;;;; Part 2

;;; Awarded automatically for completing all other days.
