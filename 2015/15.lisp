(in-package :aoc-2015-15)

(aoc:define-day 21367368 1766400)


;;;; Input

(defparameter *test-input*
  '("Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8"
    "Cinnamon: capacity 2, durability 3, flavor -2, texture -1, caolries 3"))

(defun parse-ingredient (string)
  (destructuring-bind (name property-string) (ppcre:split ": " string)
    (cons name
          (mapcar (lambda (p)
                    (destructuring-bind (property-name-string property-value-string)
                        (ppcre:split " " p)
                      (cons (alexandria:make-keyword (string-upcase property-name-string))
                            (parse-integer property-value-string))))
                  (ppcre:split ", " property-string)))))

(defparameter *test-ingredients* (mapcar #'parse-ingredient *test-input*))
(defparameter *ingredients* (mapcar #'parse-ingredient (aoc:input)))


;;;; Part 1

;;; ASSUMPTION: Ingredient properties are always in the same order.
(defun make-ingredient-matrix (ingredients)
  (let ((matrix (make-array (list (length ingredients) (length (cdr (first ingredients))))
                            :element-type 'integer)))
    (iter (for ingredient in ingredients)
          (for i from 0)
          (iter (for (property . value) in (cdr ingredient))
                (for j from 0)
                (setf (aref matrix i j) value)))
    matrix))

(defun recipe-score (ingredient-matrix ingredient-amounts)
  (values (iter (for j below (1- (aops:dim ingredient-matrix 1)))
                (for property-score = (iter (for i below (aops:dim ingredient-matrix 0))
                                            (summing (* (svref ingredient-amounts i)
                                                        (aref ingredient-matrix i j)))))
                (when (<= property-score 0)
                  (leave 0))
                (multiplying property-score))
          (iter (for i below (aops:dim ingredient-matrix 0))
                (summing (* (svref ingredient-amounts i)
                            (aref ingredient-matrix i (1- (aops:dim ingredient-matrix 1))))))))

(defun best-recipe-score (ingredients &optional calorie-requirement)
  (let ((matrix (make-ingredient-matrix ingredients))
        (best-score 0)
        (best-recipe nil))
    (labels ((visit-permutation (p)
               (multiple-value-bind (score calories) (recipe-score matrix p)
                 (when (and (< best-score score)
                            (or (null calorie-requirement)
                                (= calories calorie-requirement)))
                   (setf best-score score
                         best-recipe p))))
             (visit-partition (p)
               (aoc:visit-permutations-lexicographic #'visit-permutation p)))
      (aoc:visit-n-integer-partitions #'visit-partition (aops:dim matrix 0) 100)
      (values best-score best-recipe))))

(aoc:given 1
  (= 62842880 (recipe-score (make-ingredient-matrix *test-ingredients*) #(44 56)))
  (equalp (list 62842880 #(44 56)) (multiple-value-list (best-recipe-score *test-ingredients*))))

(defun get-answer-1 ()
  (best-recipe-score *ingredients*))


;;;; Part 2

(aoc:given 2
  (equalp (list 57600000 500)
          (multiple-value-list (recipe-score (make-ingredient-matrix *test-ingredients*) #(40 60))))
  (equalp (list 57600000 #(40 60))
          (multiple-value-list (best-recipe-score *test-ingredients* 500))))

(defun get-answer-2 ()
  (best-recipe-score *ingredients* 500))
