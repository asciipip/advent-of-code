(in-package :aoc-2015-22)

;; Part 1 is 900, but it takes five minutes
;; Part 2 is 1216, but it takes an inordinate amount of time
(aoc:define-day nil nil)


;;;; Basics

(defstruct state
  player
  boss
  effects)

(defun state= (state other)
  (and (state-p state)
       (state-p other)
       (equalp (state-player state) (state-player other))
       (equalp (state-boss state) (state-boss other))
       (every #'eq (mapcar #'class-of (state-effects state)) (mapcar #'class-of (state-effects other)))))

(defun sxhash-state (state)
  (with-slots (player boss effects) state
    (sxhash (list player boss (mapcar #'class-of effects)))))
(sb-ext:define-hash-table-test state= sxhash-state)

(defun player-hp (state)
  (entity-hp (state-player state)))

(defun boss-hp (state)
  (entity-hp (state-boss state)))

(defmethod print-object ((state state) stream)
  (with-slots (player boss effects) state
    (format stream "#<P(~Ahp ~Aa ~Am) B(~Ahp ~Ad)"
            (entity-hp player) (entity-armor player) (entity-mana player)
            (entity-hp boss) (entity-damage boss))
    (when (not (endp effects))
      (format stream "~%  ~{~A~^, ~}" effects))
    (format stream ">")))

(defun apply-effects (state)
  (let ((new-state (reduce (lambda (s e) (apply-effect e s)) (state-effects state)
                           :initial-value state)))
    (setf (state-effects new-state)
          (remove-if-not #'identity
                         (mapcar (lambda (e)
                                   (if (= 1 (spell-turns e))
                                       nil
                                       (make-instance (class-of e)
                                                      :turns (1- (spell-turns e)))))
                                 (state-effects new-state))))
    new-state))

(defun boss-damage (state)
  (with-slots (player boss) state
    (max 1 (- (entity-damage boss) (entity-armor player)))))

(defun boss-attack (state)
  (with-slots (player boss effects) state
    (make-state :player (if (<= (entity-hp boss) 0)
                            player
                            (do-damage player (boss-damage state)))
                :boss boss
                :effects effects)))


(defstruct entity
  role
  hp
  mana
  armor
  damage)

(defun use-mana (entity mana)
  (let ((new-entity (copy-structure entity)))
    (decf (entity-mana new-entity) mana)
    new-entity))

(defun do-damage (entity damage)
  (let ((new-entity (copy-structure entity)))
    (decf (entity-hp new-entity) damage)
    new-entity))


(defclass spell ()
  ((cost :reader spell-cost
         :allocation :class)
   (turns :initarg :turns
          :reader spell-turns
          :initform nil)))

(defmethod print-object ((spell spell) stream)
  (format stream "#<~A" (class-name (class-of spell)))
  (when (spell-turns spell)
    (format stream "(~A)" (spell-turns spell)))
  (format stream ">"))

(defgeneric cast (spell state))
(defgeneric apply-effect (spell state))
(defgeneric print-effect (spell))

(defmethod cast ((spell spell) state)
  (cond
    ((spell-turns spell)
     (with-slots (player boss effects) state
       (let ((new-player (use-mana player (spell-cost spell))))
         (make-state :player new-player
                     :boss boss
                     :effects (cons spell effects)))))))

(defclass magic-missile (spell)
  ((cost :initform 53)))

(defmethod cast ((spell magic-missile) state)
  (with-slots (player boss effects) state
    (make-state :player (use-mana player (spell-cost spell))
                :boss (do-damage boss 4)
                :effects effects)))

(defclass drain (spell)
  ((cost :initform 73)))

(defmethod cast ((spell drain) state)
  (with-slots (player boss effects) state
    (let ((new-player (use-mana player (spell-cost spell))))
      (incf (entity-hp new-player) 2)
      (make-state :player new-player
                  :boss (do-damage boss 2)
                  :effects effects))))

(defclass shield (spell)
  ((cost :initform 113)
   (turns :initform 6)))

(defmethod cast ((spell shield) state)
  (with-slots (player boss effects) state
    (let ((new-player (use-mana player (spell-cost spell))))
      (incf (entity-armor new-player) 7)
      (make-state :player new-player
                  :boss boss
                  :effects (cons spell effects)))))

(defmethod apply-effect ((spell shield) state)
  (if (= 1 (spell-turns spell))
      (let ((new-state (copy-structure state))
            (new-player (copy-structure (state-player state))))
        (decf (entity-armor new-player) 7)
        (setf (state-player new-state) new-player)
        new-state)
      state))

(defmethod print-effect ((spell shield))
  (format t "Shield's timer is now ~A.~%" (1- (spell-turns spell))))

(defclass poison (spell)
  ((cost :initform 173)
   (turns :initform 6)))

(defmethod apply-effect ((spell poison) state)
  (with-slots (player boss effects) state
    (make-state :player player
                :boss (do-damage boss 3)
                :effects effects)))

(defmethod print-effect ((spell poison))
  (format t "Poison deals 3 damage; its timer is now ~A.~%" (1- (spell-turns spell))))

(defclass recharge (spell)
  ((cost :initform 229)
   (turns :initform 5)))

(defmethod apply-effect ((spell recharge) state)
  (with-slots (player boss effects) state
    (make-state :player (use-mana player -101)
                :boss boss
                :effects effects)))

(defmethod print-effect ((spell recharge))
  (format t "Recharge provides 101 mana; its timer is now ~A.~%" (1- (spell-turns spell))))


;; WARNING: SBCL-specific MOP call.
(defparameter *spells*
  (sort (sb-mop:class-direct-subclasses (find-class 'spell))
        #'>
        :key (lambda (s) (spell-cost (make-instance s)))))


;;;; Input

(defparameter *test-state-1*
  (make-state :player (make-entity :role :player :hp 10 :mana 250 :armor 0)
              :boss (make-entity :role :boss :hp 13 :damage 8)
              :effects nil))
(defparameter *test-state-2*
  (make-state :player (make-entity :role :player :hp 10 :mana 250 :armor 0)
              :boss (make-entity :role :boss :hp 14 :damage 8)
              :effects nil))
(defparameter *initial-state*
  (destructuring-bind (boss-hp boss-damage) (mapcar #'aoc:extract-ints (aoc:input))
    (make-state :player (make-entity :role :player :hp 50 :mana 500 :armor 0)
                :boss (make-entity :role :boss :hp boss-hp :damage boss-damage)
                :effects nil)))


;;;; Part 1

(defun available-spells (state)
  (with-slots (player effects) state
    (with-slots (mana) player
      (remove-if (lambda (s) (member (class-of s) effects :key #'class-of))
                 (remove-if-not (lambda (s) (<= (spell-cost s) mana))
                                (mapcar #'make-instance *spells*))))))

(defun next-options (state)
  (labels ((use-spell (spell)
             (list (spell-cost spell)
                   (apply-effects (boss-attack (apply-effects (cast spell state)))))))
    (let* ((spells (available-spells state)))
      (if (or (<= (player-hp state) 0)
              (endp spells))
          nil
          (mapcar #'use-spell spells)))))

(defun finishedp (state)
  (and (<= (boss-hp state) 0)
       (< 0 (player-hp state))))

(defun least-mana-to-win (state)
  (nth-value 1 (aoc:shortest-path state
                                  #'next-options
                                  :finishedp #'finishedp)))

(aoc:given 1
  (= 226 (least-mana-to-win *test-state-1*))
  (= 641 (least-mana-to-win *test-state-2*)))

(defun apply-spells (initial-state spells &key (print nil))
  (labels ((print-state (state)
             (with-slots (player boss effects) state
               (format t "- Player has ~A hit points, ~A armor, ~A mana~%- Boss has ~A hit points~%"
                       (entity-hp player) (entity-armor player) (entity-mana player)
                       (entity-hp boss))
               (mapc #'print-effect effects))))
    (when (not (endp spells))
      (when print
        (format t "-- Player turn --~%")
        (print-state initial-state))
      (let ((state-with-effects (apply-effects initial-state)))
        (if (<= (boss-hp state-with-effects) 0)
            (progn
              (when print
                (format t "The boss was killed by poison.~%"))
              state-with-effects)
            (let ((state-after-cast (cast (make-instance (car spells)) state-with-effects)))
              (when print
                (format t "Player casts ~A.~%" (car spells)))
              (if (<= (boss-hp state-after-cast) 0)
                  (progn
                    (when print
                      (format t "The boss was killed."))
                    state-after-cast)
                  (let ((boss-turn-with-effects (apply-effects state-after-cast)))
                    (when print
                      (format t "~%-- Boss turn --~%")
                      (print-state state-after-cast))
                    (if (<= (boss-hp boss-turn-with-effects) 0)
                        (progn
                          (when print
                            (format t "The boss was killed by poison.~%"))
                          boss-turn-with-effects)
                        (let ((state-after-boss-attack (boss-attack boss-turn-with-effects)))
                          (when print
                            (format t "Boss attacks for ~A damage.~%" (boss-damage boss-turn-with-effects)))
                          (if (<= (player-hp state-after-boss-attack) 0)
                              (progn
                                (when print
                                  (format t "The player was killed.~%"))
                                state-after-boss-attack)
                              (progn
                                (when print
                                  (format t "~%"))
                                (apply-spells state-after-boss-attack (cdr spells) :print print)))))))))))))

(defun get-answer-1 ()
  (least-mana-to-win *initial-state*))


;;;; Part 2

(defun damage-player (state amount)
  (make-state :player (do-damage (state-player state) amount)
              :boss (state-boss state)
              :effects (state-effects state)))

(defun next-options-hard (state)
  (labels ((use-spell (spell)
             (list (spell-cost spell)
                   (apply-effects (damage-player (boss-attack (apply-effects (cast spell state)))
                                                 1)))))
    (let* ((spells (available-spells state)))
      (if (or (<= (player-hp state) 0)
              (endp spells))
          nil
          (mapcar #'use-spell spells)))))

(defun least-mana-to-win-hard (state)
  (nth-value 1 (aoc:shortest-path (damage-player state 1)
                                  #'next-options-hard
                                  :finishedp #'finishedp)))

(defun get-answer-2 ()
  (least-mana-to-win-hard *initial-state*))
