(in-package :aoc-2015-21)

(aoc:define-day 78 148)


;;;; Basics

(defstruct (item
             (:constructor make-item (type name cost damage armor)))
  type
  name
  cost
  damage
  armor)

(defparameter *weapons*
  (list (make-item :weapon "Dagger"       8  4  0)
        (make-item :weapon "Shortsword"  10  5  0)
        (make-item :weapon "Warhammer"   25  6  0)
        (make-item :weapon "Longsword"   40  7  0)
        (make-item :weapon "Greataxe"    74  8  0)))
(defparameter *armor*
  (list (make-item :armor  "Leather"     13  0  1)
        (make-item :armor  "Chainmail"   31  0  2)
        (make-item :armor  "Splintmail"  53  0  3)
        (make-item :armor  "Bandedmail"  75  0  4)
        (make-item :armor  "Platemail"  102  0  5)))
(defparameter *rings*
  (list (make-item :ring   "Damage +1"   25  1  0)
        (make-item :ring   "Damage +2"   50  2  0)
        (make-item :ring   "Damage +3"  100  3  0)
        (make-item :ring   "Defense +1"  20  0  1)
        (make-item :ring   "Defense +2"  40  0  2)
        (make-item :ring   "Defense +3"  80  0  3)))

(defstruct entity
  role
  (hp 100)
  (damage 0)
  (armor 0))

(defun use-item (entity item)
  (make-entity :role (entity-role entity)
               :hp (entity-hp entity)
               :damage (+ (entity-damage entity) (item-damage item))
               :armor (+ (entity-armor entity) (item-armor item))))

(defun attack (attacker defender)
  "Returns the new state of DEFENDER."
  (let ((damage (max 1 (- (entity-damage attacker) (entity-armor defender)))))
    (make-entity :role (entity-role defender)
                 :hp (- (entity-hp defender) damage)
                 :damage (entity-damage defender)
                 :armor (entity-armor defender))))


;;;; Input

(defparameter *player* (make-entity :role :player))
(defparameter *boss*
  (destructuring-bind (hp damage armor) (mapcar #'aoc:extract-ints (aoc:input))
    (make-entity :role :boss :hp hp :damage damage :armor armor)))


;;;; Part 1

(defun battle (attacker defender)
  (if (<= (entity-hp attacker) 0)
      defender
      (battle (attack attacker defender) attacker)))

(aoc:given 1
  (equalp (battle (make-entity :role :player :hp 8 :damage 5 :armor 5)
                  (make-entity :role :boss :hp 12 :damage 7 :armor 2))
          (make-entity :role :player :hp 2 :damage 5 :armor 5)))

(defun visit-weapon-combinations (visit-func)
  (labels ((visit-r (remaining)
             (when (not (endp remaining))
               (funcall visit-func (car remaining))
               (visit-r (cdr remaining)))))
    (visit-r *weapons*)))

(defun visit-armor-combinations (visit-func)
  "VISIT-FUNC must accept NIL for \"No armor\"."
  (labels ((visit-r (remaining)
             (when (not (endp remaining))
               (funcall visit-func (car remaining))
               (visit-r (cdr remaining)))))
    (visit-r (cons nil *armor*))))

;;; 0-2 rings
(defun visit-ring-combinations (visit-func)
  "VISIT-FUNC is passed a list of 0-2 elements."
  (labels ((visit-1 (remaining)
             (when (not (endp remaining))
               (visit-2 (car remaining) (cdr remaining))
               (visit-1 (cdr remaining))))
           (visit-2 (elt-1 remaining)
             (when (not (endp remaining))
               (funcall visit-func (remove-if-not #'identity (list elt-1 (car remaining))))
               (visit-2 elt-1 (cdr remaining)))))
    (funcall visit-func nil)
    (visit-1 (cons nil *rings*))))

(defun visit-items (visit-func)
  (visit-weapon-combinations
   (lambda (weapon)
     (visit-armor-combinations
      (lambda (armor)
        (visit-ring-combinations
         (lambda (rings)
           (let* ((items (remove-if-not #'identity (append (list weapon armor) rings)))
                  (player (reduce #'use-item items :initial-value *player*))
                  (cost (reduce #'+ items :key #'item-cost)))
             (funcall visit-func items player cost)))))))))

(defun get-answer-1 ()
  (let ((lowest-cost nil)
        (best-combination nil))
    (visit-items (lambda (items player cost)
                   (when (and (eq :player (entity-role (battle player *boss*)))
                              (or (null lowest-cost)
                                  (< cost lowest-cost)))
                     (setf lowest-cost cost
                           best-combination items))))
    (values lowest-cost best-combination)))


;;;; Part 2

(defun get-answer-2 ()
  (let ((highest-cost nil)
        (worst-combination nil))
    (visit-items (lambda (items player cost)
                   (when (and (eq :boss (entity-role (battle player *boss*)))
                              (or (null highest-cost)
                                  (< highest-cost cost)))
                     (setf highest-cost cost
                           worst-combination items))))
    (values highest-cost worst-combination)))
