(in-package :aoc-2022-12)

(aoc:define-day 350 349)


;;; Parsing

(defun parse-dem (lines)
  (make-array (list (length lines) (length (first lines)))
              :element-type '(integer 0 26)
              :initial-contents (mapcar #'parse-dem-line lines)))

(defun parse-dem-line (line)
  (mapcar (lambda (c)
            (case c
              (#\S 0)
              (#\E 25)
              (t (- (char-code c) (char-code #\a)))))
          (coerce line 'list)))

(defun find-position (char lines)
  (let* ((y (position-if (alexandria:curry #'find char) lines))
         (x (position char (nth y lines))))
    (point:make-point x y)))


;;; Input

;; Calling AOC:INPUT multiple times is fine because the data is cached locally.
(defparameter *dem* (parse-dem (aoc:input)))
(defparameter *start* (find-position #\S (aoc:input)))
(defparameter *end* (find-position #\E (aoc:input)))


;;; Part 1

(defparameter +cardinal-directions+
  (list #<-1 0> #<0 -1> #<0 1> #<1 0>))

(defun make-option-fn (dem &key reverse)
  (lambda (point)
    (mapcar (lambda (neighbor)
              (list 1 neighbor))
            (remove-if-not (lambda (neighbor)
                             (and (point:< #<-1 -1> neighbor (point:make-point (array-dimension dem 1) (array-dimension dem 0)))
                                  (let ((elevation-difference (- (point:aref dem neighbor) (point:aref dem point))))
                                    (if reverse
                                        (<= -1 elevation-difference)
                                        (<= elevation-difference 1)))))
                           (mapcar (alexandria:curry #'point:+ point) +cardinal-directions+)))))

(defun find-route (dem start end)
  (aoc:shortest-path start (make-option-fn dem) :end end))

(defun get-answer-1 (&key (dem *dem*) (start *start*) (end *end*))
  (nth-value 1 (find-route dem start end)))


;;; Part 2

(defun find-reverse-route (dem end)
  (aoc:shortest-path end (make-option-fn dem :reverse t)
                     :finishedp (lambda (point)
                                  (zerop (point:aref dem point)))))


(defun get-answer-2 (&key (dem *dem*) (end *end*))
  (nth-value 1 (find-reverse-route dem end)))


;;; Visualization

;; Copied from 2021-09
(defun save-dem (dem filename &key (cellsize 1.0))
  "Writes DEM to FILENAME in Arc/Info ASCII Grid format.  Use .grd extension."
  (with-open-file (output filename :direction :output :if-does-not-exist :create :if-exists :supersede)
    (format output "ncols ~A~%" (array-dimension dem 1))
    (format output "nrows ~A~%" (array-dimension dem 0))
    (format output "xllcorner 0.0 ~%yllcorner 0.0~%")
    (format output "cellsize ~F~%" cellsize)
    (format output "NODATA_value -9999~%")
    (dotimes (y (array-dimension dem 0))
      (dotimes (x (array-dimension dem 1))
        (format output "~F " (aref dem y x)))
      (format output "~%"))))

(defun clamped-aref (array y x)
  (let ((clamped-x (max 0 (min x (1- (array-dimension array 1)))))
        (clamped-y (max 0 (min y (1- (array-dimension array 0))))))
    (aref array clamped-y clamped-x)))

(defun current-cell-pct (delta)
  (min 1 (max 0 (/ (- 13 (if (< delta 5)
                             (- 9 delta)
                             delta))
                   7))))

(defun upscale-dem (dem &key (scale 10))
  (let ((result (make-array (list (* scale (array-dimension dem 0)) (* scale (array-dimension dem 1))))))
    (dotimes (y (array-dimension dem 0))
      (dotimes (x (array-dimension dem 1))
        (dotimes (dy scale)
          (dotimes (dx scale)
            ;; pct-x is what percentage of this cell should be included.
            ;; pct-y is what percentage of this cell should be included.
            (let ((pct-x (current-cell-pct dx))
                  (pct-y (current-cell-pct dy))
                  (xo (- x (truncate (signum (- 4.5 dx)))))
                  (yo (- y (truncate (signum (- 4.5 dy))))))
              (cond
                ;; Copy unaltered: centers of tiles
                ((or (and (<= 3 dx 6)
                          (<= 3 dy 6)))
                 (setf (aref result (+ (* scale y) dy) (+ (* scale x) dx))
                       (aref dem y x)))
                ;; Linear interpolation with adjacent tile, horizontally
                ((or (<= 3 dy 6)
                     (= (clamped-aref dem y x)
                        (clamped-aref dem yo x)))
                 (setf (aref result (+ (* scale y) dy) (+ (* scale x) dx))
                       (+ (* pct-x (clamped-aref dem y x))
                          (* (- 1 pct-x)
                             (clamped-aref dem y xo)))))
                ;; Linear interpolation with adjacent tile, vertically
                ((or (<= 3 dx 6)
                     (= (clamped-aref dem y x)
                        (clamped-aref dem y xo)))
                 (setf (aref result (+ (* scale y) dy) (+ (* scale x) dx))
                       (+ (* pct-y (clamped-aref dem y x))
                          (* (- 1 pct-y)
                             (clamped-aref dem yo x)))))
                (t
                 (setf (aref result (+ (* scale y) dy) (+ (* scale x) dx))
                       (/ (+ (* pct-x (clamped-aref dem y x))
                             (* (- 1 pct-x)
                                (clamped-aref dem y xo))
                             (* pct-y (clamped-aref dem y x))
                             (* (- 1 pct-y)
                                (clamped-aref dem yo x)))
                          2))
                 )))))))
    result))

(defun save-color-relief (dem filename &key (scale 10))
  (let ((upscaled-dem (upscale-dem dem :scale scale)))
    (cairo:with-png-file (filename
                          :rgb24
                          (array-dimension upscaled-dem 1)
                          (array-dimension upscaled-dem 0))
      (dotimes (y (array-dimension upscaled-dem 0))
        (dotimes (x (array-dimension upscaled-dem 1))
          (apply #'cairo:set-source-rgb
                 (colorcet:interpolate colorcet:+L10+
                                       (/ (aref upscaled-dem y x) 25)))
          (cairo:rectangle x y 1 1)
          (cairo:fill-path)))
      (let ((path (find-route dem *start* *end*)))
        (cairo:translate (/ scale 2) (/ scale 2))
        (cairo:set-line-width (* scale 0.2))
        (cairo:set-source-rgb 1 0 0)
        (cairo:set-line-cap :round)
        (cairo:set-line-join :round)
        (cairo:move-to (* scale (point:x (car path))) (* scale (point:y (car path))))
        (dolist (p (cdr path))
          (cairo:line-to (* scale (point:x p)) (* scale (point:y p))))
        (cairo:stroke)))))

#|
#!/bin/sh
dem=12.grd
hypsorelief=12-color.png
gdaldem hillshade $dem shade.tif
convert $hypsorelief -modulate 120 \( shade.tif -level 70,95% +level 0%,80% \) -compose screen -composite \( shade.tif -level 0,75% +level 40%,100% \) -compose multiply -composite -modulate 92 -define png:color-type=2 combined.png

|#

;; Also, you can `gdaltranslate` the Arc/Info file into a GeoTIFF, loat it
;; into Blender, and apply the hypsorelief image as a texture map.
