(in-package :aoc-2022-17)

(aoc:define-day 3224 1595988538691)


;;; Parsing

(parseq:defrule jet-pattern ()
    (+ push)
  (:vector))

(parseq:defrule push ()
    (or left right))

(parseq:defrule left ()
    "<"
  (:constant #<-1 0>))

(parseq:defrule right ()
    ">"
  (:constant #<1 0>))


;;; Input

(defparameter *example*
  (parseq:parseq 'jet-pattern ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"))
(defparameter *jet-pattern* (aoc:input :parse-line 'jet-pattern))

;;; Part 1

(defparameter *chamber-width* 7)

;; Useful value determined empirically.
(defparameter *prune-height* 64)

;; Initial positions are chosen so that the x values will line up
;; appropriately and the y values will work when added to the highest
;; exsting rock.  Rocks are in order of appearance.
(defparameter *rocks*
  (vector (fset:set #<2 -4> #<3 -4> #<4 -4> #<5 -4>)
          (fset:set         #<3 -6>
                            #<2 -5> #<3 -5> #<4 -5>
                            #<3 -4>)
          (fset:set         #<4 -6>
                            #<4 -5>
                            #<2 -4> #<3 -4> #<4 -4>)
          (fset:set #<2 -7>
                    #<2 -6>
                    #<2 -5>
                    #<2 -4>)
          (fset:set #<2 -5> #<3 -5>
                    #<2 -4> #<3 -4>)))

(defun print-rocks (obstacles &optional rock)
  (let ((rocks (if rock
                   (fset:union rock obstacles)
                   obstacles)))
    (multiple-value-bind (min-point max-point) (point:bbox rocks)
      (declare (ignore max-point))
      (let ((offset (point:make-point 0 (point:y min-point))))
        (aoc:draw-svg-grid
         7
         (abs (point:y min-point))
         (lambda (point)
           (when (fset:lookup obstacles (point:+ point offset))
             (if (and rock (fset:lookup rock (point:+ point offset)))
                 #\@
                 #\#)))
         :cell-size 16)))))

(defun move-rock-vector (obstacles vector rock)
  (let ((new-rock (fset:image (alexandria:curry #'point:+ vector) rock)))
    (multiple-value-bind (min-point max-point) (point:bbox new-rock)
      (if (or (< (point:x min-point) 0)
              (<= *chamber-width* (point:x max-point))
              (<= 0 (point:y min-point))
              (not (fset:empty? (fset:intersection new-rock obstacles))))
          rock
          new-rock))))

(defun move-rock-step (obstacles jet-vector rock)
  "Returns two values: the new position of the rock, and whether the rock
  is done moving (true) or not (false)."
  (let* ((shifted-rock (move-rock-vector obstacles jet-vector rock))
         (dropped-rock (move-rock-vector obstacles #<0 1> shifted-rock)))
    (values dropped-rock (fset:equal? dropped-rock shifted-rock))))

(defun move-rock (jet-patterns obstacles jet-index rock)
  "Returns two values: the new OBSTACLES set with the rock at rest and the
  new JET-INDEX."
  (multiple-value-bind (new-rock done-moving-p)
      (move-rock-step obstacles (svref jet-patterns jet-index) rock)
    (let ((new-index (mod (1+ jet-index) (length jet-patterns))))
      (if done-moving-p
          (values (fset:union new-rock obstacles)
                  new-index)
          (move-rock jet-patterns obstacles new-index new-rock)))))

(defun prune-obstacles (obstacles)
  (if (fset:empty? obstacles)
      obstacles
      (multiple-value-bind (min-point max-point) (point:bbox obstacles)
        (if (<= *prune-height* (- (point:y max-point) (point:y min-point)))
            (let ((y-limit (+ *prune-height* (point:y min-point))))
              (fset:filter (lambda (point)
                             (< (point:y point) y-limit))
                           obstacles))
            obstacles))))

(defun init-obstacle-history ()
  (cons (fset:empty-map) (fset:empty-seq)))

(defun normalize-obstacles (obstacles max-point)
  (fset:image (lambda (point)
                (point:make-point (point:x point)
                                  (- (point:y point)
                                     (point:y max-point))))
              obstacles))

(defun add-obstacle-history (past-obstacles obstacles)
  (if (fset:empty? obstacles)
      past-obstacles
      (multiple-value-bind (min-point max-point) (point:bbox obstacles)
        (destructuring-bind (past-map . past-info) past-obstacles
          (let ((normalized-obstacles (normalize-obstacles obstacles max-point)))
            (cons (fset:with past-map normalized-obstacles (fset:size past-info))
                  (fset:with-last past-info (point:y min-point))))))))

(defun last-obstacle-height (past-obstacles obstacles)
  (destructuring-bind (past-map . past-info) past-obstacles
    (multiple-value-bind (min-point max-point) (point:bbox obstacles)
      (declare (ignore min-point))
      (let* ((normalized-obstacles (normalize-obstacles obstacles max-point))
             (index (fset:lookup past-map normalized-obstacles)))
        (if index
            (values (fset:lookup past-info index)
                    (- (fset:size past-info) index)))))))

(defun obstacle-height (obstacles)
  (point:y (point:bbox obstacles)))

(defun add-height (obstacles height)
  (fset:image (lambda (point)
                (point:make-point (point:x point) (+ height (point:y point))))
              obstacles))

(defun add-rocks-step (jet-patterns rock-index obstacles jet-index count past-obstacles)
  (if (zerop count)
      obstacles
      (multiple-value-bind (last-height steps-back)
          (last-obstacle-height past-obstacles obstacles)
        (if (and last-height
                 (< steps-back count))
            (add-rocks-step jet-patterns
                            rock-index
                            (add-height obstacles (* (- (obstacle-height obstacles)
                                                        last-height)
                                                     (truncate count steps-back)))
                            jet-index
                            (mod count steps-back)
                            (init-obstacle-history))
            (multiple-value-bind (min-point max-point) (point:bbox obstacles)
              (declare (ignore max-point))
              (multiple-value-bind (new-obstacles new-jet-index)
                  (move-rock jet-patterns
                             obstacles
                             jet-index
                             (if (fset:empty? obstacles)
                                 (svref *rocks* rock-index)
                                 (fset:image (lambda (point)
                                               (point:+ point (point:make-point 0 (point:y min-point))))
                                             (svref *rocks* rock-index))))
                (add-rocks-step jet-patterns
                                (mod (1+ rock-index) (length *rocks*))
                                (prune-obstacles new-obstacles)
                                new-jet-index
                                (1- count)
                                (add-obstacle-history past-obstacles obstacles))))))))

(defun add-rocks (jet-patterns count)
  (add-rocks-step jet-patterns 0 (fset:empty-set) 0 count (init-obstacle-history)))

(defun get-answer-1 (&optional (jet-patterns *jet-pattern*) (count 2022))
  (multiple-value-bind (min-point max-point)
      (point:bbox (add-rocks jet-patterns count))
    (declare (ignore max-point))
    (- (point:y min-point))))


;;; Part 2

(defun get-answer-2 (&optional (jet-patterns *jet-pattern*))
  (get-answer-1 jet-patterns 1000000000000))
