(in-package :aoc-2022-25)

(aoc:define-day "2---1010-0=1220-=010" nil)


;;; Data Structures

(defparameter +snafu-digits+
  (fset:map (-2 "=") (-1 "-") (0 "0") (1 "1") (2 "2")))


;;; Parsing

(parseq:defrule snafu-number ()
    (+ snafu-digit)
  (:lambda (&rest digits)
    (reduce (lambda (result digit)
              (+ (* 5 result) digit))
            digits
            :initial-value 0)))

(parseq:defrule snafu-digit ()
    (char "-=012")
  (:lambda (c)
    (fset:lookup (fset:map (#\= -2) (#\- -1) (#\0 0) (#\1 1) (#\2 2))
                 c)))


;;; Input

(defparameter *fuel* (aoc:input :parse-line 'snafu-number))


;;; Part 1

(defun to-snafu (number)
  (if (zerop number)
      (fset:lookup +snafu-digits+ 0)
      (multiple-value-bind (q r) (truncate number 5)
        (if (<= r 2)
            (if (zerop q)
                (fset:lookup +snafu-digits+ r)
                (concatenate 'string (to-snafu q) (fset:lookup +snafu-digits+ r)))
            (concatenate 'string (to-snafu (1+ q)) (fset:lookup +snafu-digits+ (- r 5)))))))

(defun get-answer-1 ()
  (to-snafu (apply #'+ *fuel*)))
