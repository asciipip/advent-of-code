(in-package :aoc-2022-18)

(aoc:define-day 4456 2510)


;;; Parsing

(parseq:defrule point ()
    (aoc:comma-list aoc:integer-string)
  (:lambda (x y z) (point:make-point x y z)))


;;; Input

(defparameter *example* (fset:convert 'fset:set (aoc:input :parse-line 'point :file "examples/18.txt")))
(defparameter *cubes* (fset:convert 'fset:set (aoc:input :parse-line 'point)))


;;; Part 1

(defparameter +neighbors+
  (fset:set #<-1  0  0>
            #< 0 -1  0>
            #< 0  0 -1>
            #< 0  0  1>
            #< 0  1  0>
            #< 1  0  0>))

(defun exposed-side-count (cubes cube outside)
  (fset:reduce (lambda (count neighbor)
                 (if (and (not (fset:lookup cubes neighbor))
                          (or (not outside)
                              (fset:lookup outside neighbor)))
                     (1+ count)
                     count))
               (fset:image (alexandria:curry #'point:+ cube) +neighbors+)
               :initial-value 0))

(defun cubes-exposed-sides (cubes &optional outside)
  (fset:reduce (lambda (count cube)
                 (+ count (exposed-side-count cubes cube outside)))
               cubes
               :initial-value 0))

(defun get-answer-1 (&optional (cubes *cubes*))
  (cubes-exposed-sides cubes))


;;; Part 2

(defun add-fill-cube (cubes fill-min fill-max fill new-cube)
  (if (and (point:<= fill-min new-cube fill-max)
           (not (fset:lookup cubes new-cube))
           (not (fset:lookup fill new-cube)))
      (fill-cube cubes fill-min fill-max new-cube (fset:with fill new-cube))
      fill))

(defun fill-cube (cubes fill-min fill-max last-cube fill)
  (fset:reduce (alexandria:curry #'add-fill-cube cubes fill-min fill-max)
               (fset:image (alexandria:curry #'point:+ last-cube) +neighbors+)
               :initial-value fill))

(defun fill-outside (cubes)
  (multiple-value-bind (cube-min cube-max) (point:bbox cubes)
    (let ((fill-min (point:- cube-min #<1 1 1>))
          (fill-max (point:+ cube-max #<1 1 1>)))
      (fill-cube cubes fill-min fill-max fill-min (fset:set fill-min)))))

(defun get-answer-2 (&optional (cubes *cubes*))
  (cubes-exposed-sides cubes (fill-outside cubes)))
