(in-package :aoc-2022-23)

;; Part 2 is 968, but it takes a minute and a half
(aoc:define-day 4068 nil)


;;; Parsing

(defun parse-lines (lines)
  (let ((elves (fset:empty-set)))
    (iter
      (for line in lines)
      (for row from 0)
      (iter
        (for char in-string line)
        (for col from 0)
        (when (char= #\# char)
          (fset:adjoinf elves (point:make-point col row)))))
    elves))


;;; Input

(defparameter *small-example*
  (fset:set #<2 1> #<3 1> #<2 2> #<2 4> #<3 4>))
(defparameter *example* (parse-lines (aoc:input :file "examples/23.txt")))
(defparameter *elves* (parse-lines (aoc:input)))


;;; Part 1

(defparameter *moves*
  (vector #<0 -1> #<0 1> #<-1 0> #<1 0>))

(defparameter *neighbors*
  (gmap:gmap :vector
             (lambda (move)
               (if (zerop (point:x move))
                   (fset:set (point:+ #<-1 0> move)
                             move
                             (point:+ #<1 0> move))
                   (fset:set (point:+ #<0 -1> move)
                             move
                             (point:+ #<0 1> move))))
             (:vector *moves*)))

(defparameter *all-neighbors*
  (fset:reduce #'fset:union *neighbors*))

(defun elf-move-p (elves move-index elf)
  "If the elf can move in the indicated direction, returns the elf's new
  position.  Otherwise returns NIL."
  (when (fset:empty? (fset:intersection
                      (fset:image (alexandria:curry #'point:+ elf)
                                  (svref *neighbors* move-index))
                      elves))
    (point:+ (svref *moves* move-index)
             elf)))

(defun elf-proposed-move (elves move-index elf)
  (if (fset:empty? (fset:intersection elves
                                      (fset:image (alexandria:curry #'point:+ elf)
                                                  *all-neighbors*)))
      elf
      (or (gmap:gmap :or
                     (lambda (delta)
                       (elf-move-p elves
                                   (mod (+ delta move-index)
                                        (length *moves*))
                                   elf))
                     (:index 0 (length *moves*)))
          elf)))

(defun elves-proposed-moves (elves move-index)
  (fset:reduce (lambda (proposals elf)
                 (fset:with proposals elf (elf-proposed-move elves move-index elf)))
               elves
               :initial-value (fset:empty-map)))

(defun remove-move-collisions (proposals)
  (let ((targets (fset:reduce (lambda (targets elf target)
                                (declare (ignore elf))
                                (fset:with targets target))
                              proposals
                              :initial-value (fset:bag))))
    (fset:filter (lambda (elf target)
                   (declare (ignore elf))
                   (< (fset:multiplicity targets target) 2))
                 proposals)))

(defun apply-proposed-moves (elves proposals)
  (fset:image (lambda (elf)
                (or (fset:lookup proposals elf)
                    elf))
              elves))

(defun move-elves (elves move-index)
  (apply-proposed-moves elves (remove-move-collisions (elves-proposed-moves elves move-index))))

(defun make-moves (elves count)
  (labels ((move-r (new-elves new-count)
             (if (zerop new-count)
                 new-elves
                 (move-r (move-elves new-elves
                                     (mod (- count new-count)
                                          (length *moves*)))
                         (1- new-count)))))
    (move-r elves count)))

(defun count-empty-tiles (elves)
  (multiple-value-bind (min-point max-point) (point:bbox elves)
    (let ((span (point:+ #<1 1> (point:- max-point min-point))))
      (- (* (point:x span)
            (point:y span))
         (fset:size elves)))))

(defun get-answer-1 (&optional (elves *elves*) (count 10))
  (count-empty-tiles (make-moves elves count)))


;;; Part 2

(defun find-stasis-step (elves past-states step-count)
  (if (fset:lookup past-states elves)
      step-count
      (find-stasis-step (move-elves elves (mod step-count (length *moves*)))
                        (fset:with past-states elves)
                        (1+ step-count))))

(defun get-answer-2 (&optional (elves *elves*))
  (find-stasis-step elves (fset:empty-set) 0))
