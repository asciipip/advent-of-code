(in-package :aoc-2022-01)

(aoc:define-day 67450 199357)


;;; Parsing

(defun parse-ints (ints)
  (mapcar (lambda (e)
            (if (string= e "")
                nil
                (parse-integer e)))
          ints))

(defun group-calories (ints)
  (labels ((make-group (remaining-ints group)
             (if (null (car remaining-ints))
                 (values (reverse group)
                         (cdr remaining-ints))
                 (make-group (cdr remaining-ints)
                             (cons (car remaining-ints) group))))
           (make-groups (remaining-ints groups)
             (if (endp remaining-ints)
                 (reverse groups)
                 (multiple-value-bind (new-group next-ints)
                     (make-group remaining-ints nil)
                   (make-groups next-ints (cons new-group groups))))))
    (make-groups ints nil)))


;;; Input

(defparameter *calorie-groups* (group-calories (parse-ints (aoc:input))))


;;; Part 1

(defun sum-calories (calorie-groups)
  (mapcar (lambda (calories)
            (reduce #'+ calories))
          calorie-groups))

(defun find-max-calories (calorie-groups)
  (apply #'max (sum-calories calorie-groups)))

(defun get-answer-1 (&optional (calorie-groups *calorie-groups*))
  (find-max-calories calorie-groups))


;;; Part 2

(defun top-n-calories (n calorie-groups)
  (labels ((sum-n (np calories sum)
             (if (zerop np)
                 sum
                 (sum-n (1- np)
                        (cdr calories)
                        (+ sum (car calories))))))
    (let ((sorted (sort (sum-calories calorie-groups) #'>)))
      (sum-n n sorted 0))))

(defun get-answer-2 (&optional (calorie-groups *calorie-groups*))
  (top-n-calories 3 calorie-groups))
