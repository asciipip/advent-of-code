(in-package :aoc-2022-13)

(aoc:define-day 6076 24805)


;;; Parsing

;; In retrospect, I could have just used yason to parse the strings as
;; JSON.  I'm leaving this code for now.
;;
;; But for reference (make a flat list):
;;     (mapcar #'yason:parse (remove "" (aoc:input) :test #'string=))

(parseq:defrule packet-pair-list ()
    (+ packet-pair))

(parseq:defrule packet-pair ()
    (and packet-list #\newline packet-list (rep (0 2) #\newline))
  (:choose 0 2))

(parseq:defrule list-or-int ()
    (or packet-list aoc:integer-string))

(parseq:defrule packet-list ()
    (and "[" (? (aoc:comma-list list-or-int)) "]" )
  (:choose 1))



;;; Input

(defparameter *example* (aoc:input :parse 'packet-pair-list :file "examples/13.txt"))
(defparameter *packet-pairs* (aoc:input :parse 'packet-pair-list))


;;; Part 1

(defun packet-cmp (packet1 packet2)
  "Returns -1 if PACKET1 is less than PACKET2, 0 if they're equal, and 1
  if PACKET1 is greater than PACKET2."
  (cond
    ((and (endp packet1)
          (endp packet2))
     0)
    ((endp packet1)
     -1)
    ((endp packet2)
     1)
    (t (let ((car-result (packet-cmp-element (car packet1) (car packet2))))
         (if (zerop car-result)
             (packet-cmp (cdr packet1) (cdr packet2))
             car-result)))))

(defun packet-cmp-element (element1 element2)
  (if (and (typep element1 'integer)
           (typep element2 'integer))
      (cond
        ((< element1 element2)  -1)
        ((= element1 element2)   0)
        ((> element1 element2)   1)
        (t (assert nil)))
      (packet-cmp (listify element1) (listify element2))))

(defun listify (object)
  "Puts OBJECT in a list if isn't a list already."
  (if (typep object 'list)
      object
      (list object)))

(defun packet<= (packet1 packet2)
  (not (plusp (packet-cmp packet1 packet2))))

(defun packet= (packet1 packet2)
  (zerop (packet-cmp packet1 packet2)))

(defun positions-if (predicate list)
  "Returns a list of all positions where PREDICATE returns true."
  (labels ((pos-r (remaining-list index)
             (if (endp remaining-list)
                 nil
                 (if (funcall predicate (car remaining-list))
                     (cons index (pos-r (cdr remaining-list) (1+ index)))
                     (pos-r (cdr remaining-list) (1+ index))))))
    (pos-r list 0)))

(defun get-answer-1 (&optional (pairs *packet-pairs*))
  (reduce (lambda (r i) (+ r (1+ i)))
          (positions-if (lambda (pair) (apply #'packet<= pair)) pairs)
          :initial-value 0))


;;; Part 2

(defparameter *divider1* '((2)))
(defparameter *divider2* '((6)))

(defun flatten-packet-list (packet-pairs)
  (reduce (lambda (result pair)
            (cons (first pair) (cons (second pair) result)))
          packet-pairs
          :initial-value nil))

(defun get-answer-2 (&optional (pairs *packet-pairs*))
  (let ((packets (sort (cons *divider1* (cons *divider2* (flatten-packet-list pairs)))
                       #'packet<=)))
    (* (1+ (position *divider1* packets :test #'packet=))
       (1+ (position *divider2* packets :test #'packet=)))))
