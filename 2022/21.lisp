(in-package :aoc-2022-21)

(aoc:define-day 21208142603224 3882224466191)


;;; Parsing

(parseq:defrule monkey ()
    (and monkey-name ": " (or aoc:integer-string monkey-operation))
  (:choose 0 2)
  (:function #'cons))

(parseq:defrule monkey-operation ()
    (and monkey-name " " operator " " monkey-name)
  (:choose 0 2 4)
  (:lambda (m1 op m2) (list op m1 m2)))

(parseq:defrule operator ()
    (char "-+*/")
  (:string)
  (:function #'intern))

(parseq:defrule monkey-name ()
    (+ (char "a-z"))
  (:string)
  (:function #'string-upcase)
  (:function #'intern))


;;; Input

(defparameter *example* (fset:convert 'fset:map (aoc:input :parse-line 'monkey :file "examples/21.txt")))
(defparameter *monkeys* (fset:convert 'fset:map (aoc:input :parse-line 'monkey)))


;;; Part 1

(defun add-monkey-number (monkeys numbers name)
  (if (fset:lookup numbers name)
      numbers
      (let* ((monkey-fn (fset:lookup monkeys name))
             (augmented-answers (fset:reduce (alexandria:curry #'add-monkey-number monkeys)
                                             (cdr monkey-fn)
                                             :initial-value numbers)))
        (fset:with augmented-answers
                   name
                   (apply (car monkey-fn)
                          (mapcar (alexandria:curry #'fset:lookup augmented-answers)
                                  (cdr monkey-fn)))))))

(defun get-monkey-number (monkeys name)
  (let ((numbers (add-monkey-number monkeys
                                    (fset:filter (lambda (mname action)
                                                   (declare (ignore mname))
                                                   (numberp action))
                                                 monkeys)
                                    name)))
    (fset:lookup numbers name)))

(defun get-answer-1 (&optional (monkeys *monkeys*))
  (get-monkey-number monkeys 'root))

(aoc:given 1
  (= 152 (get-answer-1 *example*)))


;;; Part 2

(defun human-tree-p (monkeys name)
  (let ((monkey-fn (fset:lookup monkeys name)))
    (or (eq name 'humn)
        (and (consp monkey-fn)
             (gmap:gmap :or
                        (alexandria:curry #'human-tree-p monkeys)
                        (:list (cdr monkey-fn)))))))

(defun invert-op (operator target monkey-value human-is-left)
  (ecase operator
    (+ (- target monkey-value))
    (- (if human-is-left
           (+ target monkey-value)
           (- monkey-value target)))
    (* (/ target monkey-value))
    (/ (if human-is-left
           (* target monkey-value)
           (/ monkey-value target)))))

(defun get-human-number (monkeys name target-value)
  (if (eq name 'humn)
      target-value
      (destructuring-bind (op lname rname) (fset:lookup monkeys name)
        (let ((left-is-human (human-tree-p monkeys lname)))
          (get-human-number monkeys
                            (if left-is-human
                                lname
                                rname)
                            (invert-op op
                                       target-value
                                       (get-monkey-number monkeys (if left-is-human
                                                                      rname
                                                                      lname))
                                       left-is-human))))))

(defun get-human-number-from-root (monkeys)
  (destructuring-bind (op lname rname) (fset:lookup monkeys 'root)
    (declare (ignore op))
    (get-human-number monkeys
                      (if (human-tree-p monkeys lname)
                          lname
                          rname)
                      (get-monkey-number monkeys
                                         (if (human-tree-p monkeys lname)
                                             rname
                                             lname)))))

(defun get-answer-2 (&optional (monkeys *monkeys*))
  (get-human-number-from-root monkeys))

(aoc:given 2
  (= 301 (get-answer-2 *example*)))


;;; Visualization

(defun write-dot (monkeys filename)
  (with-open-file (output filename
                          :direction :output
                          :if-does-not-exist :create
                          :if-exists :supersede)
    (format output "digraph aoc202221 {~%")
    (format output "    ROOT [shape=diamond,style=filled]~%")
    (format output "    HUMN [shape=square,style=filled]~%")
    (fset:do-map (name op monkeys)
      (format output "    ~A [label=\"~A\"]~%"
              name (if (consp op)
                       (car op)
                       op))
      (when (consp op)
        (dolist (other (cdr op))
          (format output "    ~A -> ~A~%" other name))))
    (format output "}~%")))
