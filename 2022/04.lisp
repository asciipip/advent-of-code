(in-package :aoc-2022-04)

(aoc:define-day 477 830)


;;; Parsing

(parseq:defrule pair ()
    (and range "," range)
  (:choose 0 2))

(parseq:defrule range ()
    (and aoc:integer-string "-" aoc:integer-string)
  (:choose 0 2))


;;; Input

(defparameter *assignment-pairs* (aoc:input :parse-line 'pair))


;;; Part 1

(defun pair-full-overlap-p (pair)
  (destructuring-bind ((l1 h1) (l2 h2)) pair
    (assert (and (<= l1 h1) (<= l2 h2)))
    (or (<= l1 l2 h2 h1)
        (<= l2 l1 h1 h2))))

(defun get-answer-1 (&optional (pairs *assignment-pairs*))
  (count-if #'pair-full-overlap-p pairs))


;;; Part 2

(defun pair-overlap-p (pair)
  (destructuring-bind ((l1 h1) (l2 h2)) pair
    (assert (and (<= l1 h1) (<= l2 h2)))
    (and (<= l1 h2)
         (<= l2 h1))))

(defun get-answer-2 (&optional (pairs *assignment-pairs*))
  (count-if #'pair-overlap-p pairs))
