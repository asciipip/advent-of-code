(in-package :aoc-2022-16)

;; Part 2 is 2576, but the code takes too long to get there.  Printing
;; best results as they're found takes 12 minutes to get to the answer.
;; Exhausting the search space (to prove it's the best answer) takes 18
;; minutes.
(aoc:define-day 1896 nil)


;;; Data Structures

(defstruct valve
  name
  flow
  connections)


;;; Parsing

(parseq:defrule room ()
    (and "Valve " valve-name " has flow rate=" aoc:integer-string
         "; tunnel" (? "s") " lead" (? "s") " to valve" (? "s") " " (aoc:comma-list valve-name))
  (:choose 1 3 11)
  (:lambda (name flow connections)
    (cons name
          (make-valve :name name
                      :flow flow
                      :connections (fset:convert 'fset:set connections)))))

(parseq:defrule valve-name ()
    (+ (char "A-Z"))
  (:string)
  (:function #'intern))


;;; Input

(defparameter *example* (fset:convert 'fset:map (aoc:input :parse-line 'room :file "examples/16.txt")))

;; The functions access *VALVES* and *TOTAL-MINUTES* as globals in order
;; to not have to pass them into all of the functions all the time.  Just
;; rebind them if needed.
(defparameter *valves* (fset:convert 'fset:map (aoc:input :parse-line 'room)))
(defparameter *total-minutes* 30)


;;;
;;; Part 1
;;;

(defparameter *start-room* 'aa)

(defun valve-special-p (valve)
  (with-slots (name flow connections) valve
    (or (eq name *start-room*)
        (plusp flow)
        (/= 2 (fset:size connections)))))

(defun build-graph (valve-map)
  (let ((adjacency (fset:empty-map (fset:empty-set))))
    (labels ((add-tunnel (start last current count)
               (let ((valve (fset:lookup valve-map current)))
                 (with-slots (connections) valve
                   (if (not (valve-special-p valve))
                       (add-tunnel start
                                   current
                                   (fset:arb (fset:less connections last))
                                   (1+ count))
                       (fset:adjoinf adjacency
                                     start
                                     (fset:with (fset:lookup adjacency start)
                                                (cons current count))))))))
      (fset:do-map (name valve valve-map)
        (when (valve-special-p valve)
          (fset:do-set (start (valve-connections valve))
            (add-tunnel name name start 1))))
      adjacency)))

(defun travel-times (adjacency-graph)
  (flet ((option-fn (room)
           (mapcar (lambda (pair)
                     (list (cdr pair) (car pair)))
                   (fset:convert 'list (fset:lookup adjacency-graph room)))))
    (let ((times (fset:empty-map)))
      (fset:do-set (start (fset:domain adjacency-graph))
        (fset:do-set (end (fset:less (fset:domain adjacency-graph)
                                     start))
          (fset:adjoinf times
                        (cons start end)
                        (nth-value 1 (aoc:shortest-path start
                                                        #'option-fn
                                                        :end end)))))
      times)))

(defun max-path-pressure (visited minutes)
  (fset:reduce (lambda (sum name valve)
                 (+ sum
                    (* (valve-flow valve)
                       (- *total-minutes*
                          (or (fset:lookup visited name)
                              minutes)))))
               *valves*
               :initial-value 0))

(defun path-pressure (visited)
  (fset:reduce (lambda (sum room minutes)
                 (if (< minutes *total-minutes*)
                     (+ sum (* (- *total-minutes* minutes)
                               (valve-flow (fset:lookup *valves* room))))
                     sum))
               visited
               :initial-value 0))

(defparameter *best-pres* 0)
(defun visit-room (travel-graph visited-rooms remaining-rooms minutes best-path)
  (let ((best-pressure (path-pressure best-path)))
    (when (< *best-pres* best-pressure)
      (setf *best-pres* best-pressure))
    (multiple-value-bind (new-minutes actor) (fpq:delete-root minutes)
      (destructuring-bind (actor-minutes . actor-room) actor
        (if (or (<= *total-minutes* actor-minutes)
                (fset:empty? remaining-rooms)
                (< (max-path-pressure visited-rooms actor-minutes) best-pressure))
            (if (< best-pressure (path-pressure visited-rooms))
                visited-rooms
                best-path)
            (fset:reduce (lambda (cur-best new-room)
                           (let ((arrival-time
                                   (+ 1
                                      actor-minutes
                                      (fset:lookup travel-graph
                                                   (cons actor-room new-room)))))
                             (visit-room travel-graph
                                         (fset:with visited-rooms new-room arrival-time)
                                         (fset:less remaining-rooms new-room)
                                         (fpq:insert new-minutes (cons arrival-time new-room))
                                         cur-best)))
                         (fset:sort (fset:convert 'fset:seq remaining-rooms)
                                    #'>=
                                    :key (lambda (room)
                                           (valve-flow (fset:lookup *valves* room))))
                         :initial-value best-path))))))

(defun actor-starting-minutes (actors)
  (let ((result (fpq:empty-queue :key #'car)))
    (dotimes (i actors)
      (setf result (fpq:insert result (cons 0 *start-room*))))
    result))

(defun visit-rooms (valves actors)
  (let* ((*valves* valves)
         (adjacency-graph (build-graph valves))
         (best-path (visit-room (travel-times adjacency-graph)
                                (fset:empty-map)
                                (fset:less (fset:domain adjacency-graph) *start-room*)
                                (actor-starting-minutes actors)
                                (fset:empty-map))))
    (values best-path (path-pressure best-path))))

(defun get-answer-1 (&optional (valves *valves*))
  (nth-value 1 (visit-rooms valves 1)))

(aoc:given 1
  (= 1651 (get-answer-1 *example*)))


;;; Part 2

(defun get-answer-2 (&optional (valves *valves*))
  (let ((*total-minutes* 26)
        (*best-pres* 0))
    (nth-value 1 (visit-rooms valves 2))))

(aoc:given 2
  (= 1707 (get-answer-2 *example*)))


;;; Visualization

(defun color-to-hex (color)
  (format nil "#~{~2,'0X~}" (mapcar (lambda (c) (round (* 255 c))) color)))

(defun write-dot (valves filename)
  (let ((max-flow (fset:reduce (lambda (result name valve)
                                 (declare (ignore name))
                                 (max result (valve-flow valve)))
                               valves
                               :initial-value 0)))
    (with-open-file (output filename
                            :direction :output
                            :if-does-not-exist :create
                            :if-exists :supersede)
      (format output "graph aoc202216 {~%")
      (format output "    edge [len=2]~%")
      (format output "    AA [shape=diamond]~%")
      (fset:do-map (name valve valves)
        (fset:do-set (target (valve-connections valve))
          (when (string< (symbol-name name) (symbol-name target))
            (format output "    ~A -- ~A~%" name target)))
        (when (plusp (valve-flow valve))
          (format output "    ~A [label=\"~A\\n~A\",style=\"filled\",fillcolor=\"~A\"]~%"
                  name name (valve-flow valve)
                  (color-to-hex (colorcet:interpolate colorcet:+L19+ (/ (valve-flow valve) max-flow))))))
      (format output "}~%"))))

(defun write-compact-dot (valves filename)
  (let ((adjacency-graph (build-graph valves))
        (max-flow (fset:reduce (lambda (result name valve)
                                 (declare (ignore name))
                                 (max result (valve-flow valve)))
                               valves
                               :initial-value 0)))
    (with-open-file (output filename
                            :direction :output
                            :if-does-not-exist :create
                            :if-exists :supersede)
      (format output "graph aoc202216 {~%")
      (format output "    maxiter=10000~%")
      (format output "    AA [shape=diamond]~%")
      (fset:do-map (name connections adjacency-graph)
        (fset:do-set (target connections)
          (destructuring-bind (other-name . cost) target
            (when (string< (symbol-name name) (symbol-name other-name))
              (format output "    ~A -- ~A [label=\"~A\",penwidth=2,len=~A,color=\"~A\",fontcolor=\"~A\"]~%"
                      name other-name cost cost
                      (color-to-hex (colorcet:interpolate colorcet:+L19+ (/ cost 3)))
                      (color-to-hex (colorcet:interpolate colorcet:+L19+ (/ cost 3)))))))
        (let ((valve (fset:lookup valves name)))
          (when (plusp (valve-flow valve))
            (format output "    ~A [label=\"~A\\n~A\",style=\"filled\",fillcolor=\"~A\"]~%"
                    name name (valve-flow valve)
                    (color-to-hex (colorcet:interpolate colorcet:+L19+ (/ (valve-flow valve) max-flow)))))))
      (format output "}~%"))))
