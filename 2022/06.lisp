(in-package :aoc-2022-06)

(aoc:define-day 1802 3551)


;;; Input

(defparameter *example-1* (fset:convert 'fset:seq "mjqjpqmgbljsphdztnvjfqwrcgsmlb"))
(defparameter *datastream* (fset:convert 'fset:seq (aoc:input)))


;;; Part 1

(defun packet-start-p (datastream pos window)
  (= window (fset:size (fset:convert 'fset:set (fset:subseq datastream (- pos window) pos)))))

(defun find-packet-start (datastream &optional (window 4))
  (dotimes (pos (fset:size datastream))
    (when (packet-start-p datastream pos window)
      (return pos))))

(defun get-answer-1 (&optional (datastream *datastream*))
  (find-packet-start datastream))


;;; Part 2

(defun get-answer-2 (&optional (datastream *datastream*))
  (find-packet-start datastream 14))
