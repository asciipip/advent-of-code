(in-package :aoc-2019-25)

;;; Part 1 answer is 229384.  Obtained by carrying:
;;;  * space law space brochure
;;;  * astrolabe
;;;  * antenna
;;;  * weather machine
;;; Part 2 star is awarded for getting all 49 other stars
(aoc:define-day nil nil)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))



;;;; Part 1

(defun make-input-fn ()
  (let ((input-string "")
        (input-pos 0))
    (lambda ()
      (when (<= (length input-string) input-pos)
        (setf input-string (concatenate 'string (read-line) (format nil "~%")))
        (setf input-pos 0))
      (prog1
          (char-code (schar input-string input-pos))
        (incf input-pos)))))

(defun output-fn (value)
  (princ (code-char value)))

(defun run-program ()
  (intcode:run *input* :input-fn (make-input-fn) :output-fn #'output-fn))
