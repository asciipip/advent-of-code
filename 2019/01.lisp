(in-package :aoc-2019-01)

(aoc:define-day 3520097 5277255)


;;; Input

(defparameter *input* (mapcar #'aoc:extract-ints (aoc:input)))


;;; Part 1

(defun calculate-fuel (mass)
  (- (truncate mass 3) 2))

(aoc:given 1
  (= 2 (calculate-fuel 12))
  (= 2 (calculate-fuel 13))
  (= 654 (calculate-fuel 1969))
  (= 33583 (calculate-fuel 100756)))

(defun get-answer-1 ()
  (reduce #'+ (mapcar #'calculate-fuel *input*)))


;;; Part 2

(defun calculate-fuel-with-fuel (mass)
  (let ((fuel (calculate-fuel mass)))
    (if (<= fuel 0)
        0
        (+ fuel (calculate-fuel-with-fuel fuel)))))

(aoc:given 2
  (= 2 (calculate-fuel-with-fuel 14))
  (= 966 (calculate-fuel-with-fuel 1969))
  (= 50346 (calculate-fuel-with-fuel 100756)))

(defun get-answer-2 ()
  (reduce #'+ (mapcar #'calculate-fuel-with-fuel *input*)))
