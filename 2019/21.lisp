(in-package :aoc-2019-21)

(aoc:define-day 19350375 1143990055)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))



;;;; Part 1

(defun string-to-int-list (string)
  (map 'list #'char-code string))

(defun int-list-to-string (int-list)
  (map 'string #'code-char int-list))

(defun springscript-run (program)
  (let* ((output (intcode:run *input* :input-list (string-to-int-list program)))
         (last-int (car (last output))))
    (if (< last-int 255)
        (int-list-to-string output)
        (values (int-list-to-string (butlast output))
                last-int))))

(defparameter *springscript-program-1*
  "OR A T
AND B T
AND C T
NOT D J
OR J T
NOT T J
WALK
")

(defun get-answer-1 ()
  (nth-value 1 (springscript-run *springscript-program-1*)))


;;;; Part 2


(defparameter *springscript-program-2*
  "OR A T
AND B T
AND C T
NOT T T
OR E J
OR H J
AND T J
AND D J
RUN
")

(defun get-answer-2 ()
  (nth-value 1 (springscript-run *springscript-program-2*)))
