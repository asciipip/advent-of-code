(in-package :aoc-2019-18)

(aoc:define-day 3270 1628)


;;;; Input

(defparameter *example-1-8* '("#########"
                              "#b.A.@.a#"
                              "#########"))
(defparameter *example-1-86* '("########################"
                               "#f.D.E.e.C.b.A.@.a.B.c.#"
                               "######################.#"
                               "#d.....................#"
                               "########################"))
(defparameter *example-1-132* '("########################"
                                "#...............b.C.D.f#"
                                "#.######################"
                                "#.....@.a.B.c.d.A.e.F.g#"
                                "########################"))
(defparameter *example-1-136* '("#################"
                                "#i.G..c...e..H.p#"
                                "########.########"
                                "#j.A..b...f..D.o#"
                                "########@########"
                                "#k.E..a...g..B.n#"
                                "########.########"
                                "#l.F..d...h..C.m#"
                                "#################"))
(defparameter *example-1-81* '("########################"
                               "#@..............ac.GI.b#"
                               "###d#e#f################"
                               "###A#B#C################"
                               "###g#h#i################"
                               "########################"))
(defparameter *example-2-32* '("#############"
                               "#DcBa.#.GhKl#"
                               "#.###@#@#I###"
                               "#e#d#####j#k#"
                               "###C#@#@###J#"
                               "#fEbA.#.FgHi#"
                               "#############"))
(defparameter *example-2-72* '("#############"
                               "#g#f.D#..h#l#"
                               "#F###e#E###.#"
                               "#dCba@#@BcIJ#"
                               "#############"
                               "#nK.L@#@G...#"
                               "#M###N#H###.#"
                               "#o#m..#i#jk.#"
                               "#############"))
(defparameter *input* (aoc:input))


;;;; Part 1

(defparameter +cardinal-vectors+ '(#C(0 1) #C(0 -1) #C(1 0) #C(-1 0)))

(defun element-key (element &optional (door-or-key :key))
  "If ELEMENT is a key (or door if DOOR-OR-KEY is :door), returns the key.
  Otherwise, returns false."
  (and (consp element)
       (eq (car element) door-or-key)
       (cdr element)))

(defun parse-input (input)
  (iter (with maze = (make-hash-table))
        (with key-locations = (make-hash-table))
        (for row from 0)
        (for line in input)
        (iter (for col from 0)
              (for char in-string line)
              (for position = (complex col row))
              (for element = (case char
                               (#\# :wall)
                               (#\. :open)
                               (#\@ :entrance)
                               (t (cons (if (upper-case-p char)
                                            :door
                                            :key)
                                        (intern (string (char-upcase char)))))))
              (setf (gethash position maze) element)
              (when (element-key element)
                (setf (gethash (element-key element) key-locations) position)))
        (finally (return (values maze key-locations)))))

(defun maze-element-char (element)
  (case element
    (:wall #\#)
    (:open #\.)
    (:entrance #\@)
    (t (ecase (car element)
         (:door (cdr element))
         (:key (string-downcase (string (cdr element))))))))

(defun print-maze (maze &key position)
  (aoc:print-2d-hash-table maze #'maze-element-char :highlight position))

(defun entrance-positions (maze)
  (sort (iter (for (position element) in-hashtable maze)
              (when (eq element :entrance)
                (collecting position)))
        #'aoc:row-order<))

;;; State for maze traversal will be a list:  The first element is the
;;; position, as a complex number.  Any remaining elements will be keys
;;; that have been picked up.

(defun can-enter-p (element)
  (if (consp element)
      t
      (ecase element
        (:wall nil)
        (:open t)
        (:entrance t))))

(defun make-path-options (maze)
  (lambda (position)
    (iter (for vector in +cardinal-vectors+)
          (for new-position = (+ position vector))
          (when (can-enter-p (gethash new-position maze))
            (collecting (list 1 new-position))))))

(defun make-path-heuristic (end)
  (lambda (position)
    (aoc:manhattan-distance position end)))

(defun path-to (start end maze)
  (aoc:shortest-path start
                     (make-path-options maze)
                     :end end
                     :heuristic (make-path-heuristic end)))

(defun add-key (new-key keys)
  (sort (cons new-key (copy-list keys))
        #'string<
        :key #'string))

(defun keys-needed-for-path (maze path)
  (iter (for position in path)
        (for element = (gethash position maze))
        (for key = (element-key element :door))
        (when key (collecting key))))

(defun make-key-path (maze start end)
  (multiple-value-bind (path new-cost) (path-to start end maze)
    (let ((new-keys-needed (keys-needed-for-path maze path)))
      (cons new-cost new-keys-needed))))

(defun make-key-path-cache (maze key-positions)
  (let ((cache (make-hash-table :test 'equalp)))
    (iter (for entrance-position in (entrance-positions maze))
          (for entrance-id from 0)
          (iter (for (key-id key-position) in-hashtable key-positions)
                (for key-path = (make-key-path maze entrance-position key-position))
                (setf (gethash (cons entrance-id key-id) cache) key-path)))
    (iter (for (key-id-1 key-position-1) in-hashtable key-positions)
          (iter (for (key-id-2 key-position-2) in-hashtable key-positions)
                (for key-path = (make-key-path maze key-position-1 key-position-2))
                (when (and (not (eq key-id-1 key-id-2))
                           (car key-path))
                  (setf (gethash (cons key-id-1 key-id-2) cache) key-path))))
    cache))

(defun make-key-options (maze key-positions key-quadrants)
  (let ((all-keys (alexandria:hash-table-keys key-positions))
        (key-paths (make-key-path-cache maze key-positions)))
    (lambda (state)
      (destructuring-bind (positions &rest keys) state
        (iter (for key in (set-difference all-keys keys))
              (for key-quadrant = (gethash key key-quadrants))
              (for start-key = (svref positions key-quadrant))
              (destructuring-bind (cost &rest keys-needed)
                  (gethash (cons start-key key) key-paths)
                (when (and cost
                           (endp (set-difference keys-needed keys)))
                  (for new-positions = (copy-seq positions))
                  (setf (svref new-positions key-quadrant) key)
                  (collecting (list cost (cons new-positions (add-key key keys)))))))))))

(defun make-key-finishedp (key-positions)
  (let ((all-keys (sort (alexandria:hash-table-keys key-positions)
                        #'string<
                        :key #'string)))
    (lambda (state)
      (destructuring-bind (positions &rest keys) state
        (declare (ignore positions))
        (equal keys all-keys)))))

(defun make-key-heuristic (maze key-positions key-quadrants)
  (let ((all-keys (alexandria:hash-table-keys key-positions))
        (key-paths (make-key-path-cache maze key-positions)))
    (lambda (state)
      (destructuring-bind (positions &rest keys) state
        (iter (with max-steps = (make-array (length positions) :initial-element 0))
              (for key in (set-difference all-keys keys))
              (for key-quadrant = (gethash key key-quadrants))
              (for start-key = (svref positions key-quadrant))
              (destructuring-bind (cost &rest keys-needed)
                  (gethash (cons start-key key) key-paths)
                (declare (ignore keys-needed))
                (when (and cost
                           (< (svref max-steps key-quadrant) cost))
                  (setf (svref max-steps key-quadrant) cost)))
              (finally (return (reduce #'+ max-steps))))))))

(defun path-to-get-all-keys (maze key-positions)
  (let ((entrances (iter (for i from 0 below (length (entrance-positions maze)))
                         (collecting i result-type 'vector)))
        (key-quadrants (classify-key-quadrants maze key-positions)))
    (aoc:shortest-path (list entrances)
                       (make-key-options maze key-positions key-quadrants)
                       :finishedp (make-key-finishedp key-positions)
                       :heuristic (make-key-heuristic maze key-positions key-quadrants))))

(defun get-answer-1 (&optional (input *input*))
  (nth-value 1 (multiple-value-call #'path-to-get-all-keys (parse-input input))))


;;;; Part 2

(defun classify-key-quadrants (maze key-positions)
  (let ((entrances (coerce (entrance-positions maze) 'vector))
        (key-quadrants (make-hash-table)))
    (iter (for (key position) in-hashtable key-positions)
          (iter (for entrance in-vector entrances)
                (for i from 0)
                (when (path-to entrance position maze)
                  (setf (gethash key key-quadrants) i)
                  (finish))))
    key-quadrants))

(defun fix-single-entrance (maze)
  (let ((initial-entrances (entrance-positions maze)))
    (when (endp (cdr initial-entrances))
      (setf (gethash (car initial-entrances) maze) :wall)
      (iter (for vector in +cardinal-vectors+)
            (setf (gethash (+ vector (car initial-entrances)) maze) :wall))
      (Iter (for vector in '(#C(1 1) #C(1 -1) #C(-1 -1) #C(-1 1)))
            (setf (gethash (+ vector (car initial-entrances)) maze) :entrance)))
    (values)))


(defun prune-dead-ends (maze start)
  (labels ((prune-r (position last-position)
             (let* ((element (gethash position maze))
                    (key (element-key element)))
               (if (eq element :wall)
                   0
                   (iter (for vector in +cardinal-vectors+)
                         (for new-position = (+ vector position))
                         (when (= new-position last-position)
                           (next-iteration))
                         (summing (prune-r new-position position) into key-count)
                         (finally
                          (when (and (zerop key-count)
                                     (not key))
                            (setf (gethash position maze) :wall))
                          (return (+ key-count (if key 1 0)))))))))
    (prune-r start start)))

(defun get-answer-2 (&optional (input *input*))
  (multiple-value-bind (maze key-positions) (parse-input input)
    (fix-single-entrance maze)
    (mapc (lambda (p) (prune-dead-ends maze p)) (entrance-positions maze))
    (nth-value 1 (path-to-get-all-keys maze key-positions))))
