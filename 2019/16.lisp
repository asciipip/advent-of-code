(in-package :aoc-2019-16)

(aoc:define-day 22122816 41402171)


;;;; Input

(defparameter *example-1a* "12345678")
(defparameter *example-1b* "80871224585914546619083218645595")
(defparameter *example-2a* "03036732577212944063491565474664")
(defparameter *input* (aoc:input))


;;;; Part 1

(defun signal-to-digits (signal-str)
  (iter (for char in-string signal-str)
        (collecting (- (char-code char) (char-code #\0)) result-type 'vector)))

(defun get-cache-sum (offset cache level start length)
  (let* ((offset-start (1- (- start offset)))
         (offset-end (min (+ offset-start length)
                          (1- (array-dimension cache 1)))))
    (cond
      ((<= offset-end offset-start)
       0)
      ((minusp offset-start)
       (aref cache level offset-end))
      (t
       (- (aref cache level offset-end)
          (aref cache level offset-start))))))

(defun calculate-digit (offset input-signal cache phase-number digit)
  (mod (abs
        (iter outer
              (for base-index from digit below (+ offset (length input-signal)) by (* 4 (1+ digit)))
              (summing (get-cache-sum offset cache (1- phase-number) base-index (1+ digit)))
              (summing (- (get-cache-sum offset cache (1- phase-number)
                                         (+ base-index (* 2 (1+ digit)))
                                         (1+ digit))))))
       10))

(defun get-answer-1 (&key (input-signal *input*) (phase-count 100) (digit-count 8))
  (let* ((input-digits (signal-to-digits input-signal))
         (cache (make-array (list phase-count (length input-digits)))))
    (init-cache cache input-digits)
    (iter (for level from 1 below phase-count)
          (fill-cache-level 0 cache input-digits level))
    (aoc:join-digit-list
     (iter (for digit from 0)
           (repeat digit-count)
           (collecting (calculate-digit 0 input-digits cache phase-count digit))))))

(aoc:given 1
  (= 01029498 (get-answer-1 :input-signal *example-1a* :phase-count 4 :digit-count 8)))


;;;; Part 2

(defun repeat-vector (vector repetitions)
  (iter (with result = (make-array (* repetitions (length vector))))
        (for rep-count from 0 below repetitions)
        (iter (for i from 0 below (length vector))
              (setf (svref result (+ (* rep-count (length vector)) i)) (svref vector i)))
        (finally (return result))))

(defun init-cache (cache input-digits)
  (iter (for digit in-vector input-digits with-index i)
        (summing digit into digit-sum)
        (setf (aref cache 0 i) digit-sum)))

(defun fill-cache-level (offset cache input-signal level)
  (iter (for i from 0 below (array-dimension cache 1))
        (summing (calculate-digit offset input-signal cache level (+ i offset)) into digit-sum)
        (setf (aref cache level i) digit-sum)))

(defun get-answer-2 (&optional (input-signal *input*))
  (let* ((base-signal (signal-to-digits input-signal))
         (offset (aoc:join-digit-list (subseq base-signal 0 7)))
         (cache (make-array (list 101 (- (* (length base-signal) 10000) offset))))
         (input-digits (subseq (repeat-vector base-signal 10000)
                               offset)))
    (init-cache cache input-digits)
    (iter (for level from 1 to 99)
          (fill-cache-level offset cache input-digits level))
    (aoc:join-digit-list
     (iter (for position from offset)
           (repeat 8)
           (for digit = (calculate-digit offset input-digits cache 100 position))
           (collecting digit)))))
