(in-package :aoc-2019-07)

(aoc:define-day 79723 70602018)


;;; Input

(defparameter *example-1a* (intcode:load-program "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"))
(defparameter *example-1b* (intcode:load-program "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"))
(defparameter *example-1c* (intcode:load-program "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"))
(defparameter *example-2a* (intcode:load-program "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"))
(defparameter *example-2b* (intcode:load-program "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"))

(defparameter *input* (intcode:load-program (aoc:input)))


;;; Part 1

(defun run-program-for-amplifier (program phase-setting input)
  (first (intcode:run program :input-list (list phase-setting input))))

(defun try-phase-sequence (program sequence)
  (iter (for phase in sequence)
        (for input first 0 then output)
        (for output = (run-program-for-amplifier program phase input))
        (finally (return output))))

(defun find-phase-sequence (program)
  (let ((max-value nil)
        (max-sequence))
    (flet ((test-sequence (sequence)
             (let ((output (try-phase-sequence program sequence)))
               (when (or (null max-value)
                         (< max-value output))
                 (setf max-sequence sequence)
                 (setf max-value output)))))
      (aoc:visit-permutations #'test-sequence '(0 1 2 3 4))
      (values max-sequence max-value))))

(aoc:given 1
  (equal '((4 3 2 1 0) 43210)
         (multiple-value-list (find-phase-sequence *example-1a*)))
  (equal '((0 1 2 3 4) 54321)
         (multiple-value-list (find-phase-sequence *example-1b*)))
  (equal '((1 0 4 3 2) 65210)
         (multiple-value-list (find-phase-sequence *example-1c*))))

(defun get-answer-1 ()
  (nth-value 1 (find-phase-sequence *input*)))


;;; Part 2

(defun make-input-output-pair (initial-phase initial-input)
  (let ((queue (lparallel.queue:make-queue
                :initial-contents (if initial-input
                                      (list initial-phase initial-input)
                                      (list initial-phase)))))
    (flet ((output (value)
             (lparallel.queue:push-queue value queue))
           (input ()
             (lparallel.queue:pop-queue queue)))
      (list #'input #'output))))

(defun try-phase-sequence-with-feedback (program sequence)
  (let* ((io-funcs (iter (for phase in sequence)
                         (collecting (make-input-output-pair phase
                                                             (if (first-iteration-p)
                                                               0
                                                               nil))
                                     result-type 'vector)))
         (threads (iter (for phase in sequence)
                        (for i from 0)
                        (collecting (bt:make-thread
                                     (lambda ()
                                       (intcode:run program
                                                    :input-fn (first (svref io-funcs i))
                                                    :output-fn (second (svref io-funcs (mod (1+ i) (length sequence))))))
                                     :name (format nil "AoC 2019-07 instance ~A" i)))
                        ;; Without this sleep instruction, I was running
                        ;; into issues where the initial communication
                        ;; between threads wasn't happening properly.
                        (sleep 0.01))))
    (unwind-protect
         (progn
           (iter (for thread in threads)
                 (bt:join-thread thread))
           (funcall (first (svref io-funcs 0))))
      ;; Make sure all of the threads have been cleaned up even if
      ;; execution was interrupted.
      (iter (for thread in threads)
            (when (bt:thread-alive-p thread)
              (bt:destroy-thread thread))))))

(defun find-phase-sequence-with-feedback (program)
  (let ((max-value nil)
        (max-sequence))
    (flet ((test-sequence (sequence)
             (let ((output (try-phase-sequence-with-feedback program sequence)))
               (when (or (null max-value)
                         (< max-value output))
                 (setf max-sequence sequence)
                 (setf max-value output)))))
      (aoc:visit-permutations #'test-sequence '(5 6 7 8 9))
      (values max-sequence max-value))))

(aoc:given 2
  (equal '((9 8 7 6 5) 139629729)
         (multiple-value-list (find-phase-sequence-with-feedback *example-2a*)))
  (equal '((9 7 8 5 6) 18216)
         (multiple-value-list (find-phase-sequence-with-feedback *example-2b*))))

(defun get-answer-2 ()
  (nth-value 1 (find-phase-sequence-with-feedback *input*)))
