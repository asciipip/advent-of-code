(in-package :aoc-2019-13)

(aoc:define-day 398 19447)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))
(defparameter *board-width* 40)
(defparameter *board-height* 24)


;;;;Part 1

(defconstant +tile-empty+  0)
(defconstant +tile-wall+   1)
(defconstant +tile-block+  2)
(defconstant +tile-paddle+ 3)
(defconstant +tile-ball+   4)

(defstruct game-state
  (score 0)
  (screen (make-array (list *board-height* *board-width*) :element-type '(integer 0 4)))
  paddle-pos
  last-ball-pos
  (ball-pos (point:make-point 17 18)))


(defun make-output-fn (state)
  (intcode:make-output-fn (x y id)
     (if (= x -1)
         (setf (game-state-score state) id)
         (let ((pos (point:make-point x y)))
           (setf (aref (game-state-screen state) y x)
                 id)
           (cond
             ((= id +tile-paddle+)
              (setf (game-state-paddle-pos state) pos))
             ((= id +tile-ball+)
              (setf (game-state-last-ball-pos state) (game-state-ball-pos state))
              (setf (game-state-ball-pos state) pos)))))))

(defun build-screen (program)
  (let ((state (make-game-state)))
    (intcode:run program :output-fn (make-output-fn state))
    state))

(defun tile-char (tile-id)
  (ecase tile-id
    (#.+tile-empty+  " ")
    (#.+tile-wall+   #\U2588)
    (#.+tile-block+  #\U2593)
    (#.+tile-paddle+ "-")
    (#.+tile-ball+   #\U25CF)))

(defun print-screen (screen)
  (iter (for y from 0 below (array-dimension screen 0))
        (iter (for x from 0 below (array-dimension screen 1))
              (format t "~A" (tile-char (aref screen y x))))
        (format t "~%")))

(defun print-state (state)
  (format t "Score: ~A~%" (game-state-score state))
  (print-screen (game-state-screen state)))

(defun count-block-tiles (state)
  (iter outer
        (with screen-array = (game-state-screen state))
        (for y from 0 below (array-dimension screen-array 0))
        (iter (for x from 0 below (array-dimension screen-array 1))
              (in outer (counting (= (aref screen-array y x) +tile-block+))))))

(defun get-answer-1 ()
  (count-block-tiles (build-screen *input*)))


;;;; Part 2

(defparameter *last-ball-row* 21)

(defun next-ball-pos (screen ball-pos ball-vector steps)
  "Really the next place the ball will be just above the paddle."
  ;; These tiles are relative to the ball's vector.  In other words, if
  ;; the ball is moving southwest, "forward" is down and left of it,
  ;; "right" is to its left, and "left" is below it.
  (let* ((left-vector (point:turn ball-vector 'left))
         (right-vector (point:turn ball-vector 'right))
         (reverse-vector (point:- ball-vector))
         (tile-forward (point:+ ball-pos ball-vector))
         (tile-left (point:+ ball-pos
                             (point:/ (point:+ ball-vector
                                               left-vector)
                                      2)))
         (tile-right (point:+ ball-pos
                              (point:/ (point:+ ball-vector
                                                right-vector)
                                       2)))
         (forward-id (point:aref screen tile-forward))
         (left-id (point:aref screen tile-left))
         (right-id (point:aref screen tile-right)))
    (cond
      ((and (= (point:elt ball-pos 1) *last-ball-row*)
            (plusp (point:elt ball-vector 1)))
       (values ball-pos steps))
      ((and (= forward-id +tile-empty+)
            (= left-id +tile-empty+)
            (= right-id +tile-empty+))
       (next-ball-pos screen (point:+ ball-pos ball-vector) ball-vector (1+ steps)))
      ((and (/= left-id +tile-empty+))
       (when (= left-id +tile-block+)
         (setf (point:aref screen tile-left) +tile-empty+))
       (next-ball-pos screen ball-pos right-vector steps))
      ((and (/= right-id +tile-empty+))
       (when (= right-id +tile-block+)
         (setf (point:aref screen tile-right) +tile-empty+))
       (next-ball-pos screen ball-pos left-vector steps))
      ((/= forward-id +tile-empty+)
       (when (= forward-id +tile-block+)
         (setf (point:aref screen tile-forward) +tile-empty+))
       (next-ball-pos screen ball-pos reverse-vector steps))
      (t (error "Unexpected tile configuration.")))))

(defun make-smart-input-fn (state &optional video)
  (let ((movements (list 0 0 0)))
    (lambda ()
      (when (endp movements)
        (let ((screen-copy (alexandria:copy-array (game-state-screen state))))
          (setf (point:aref screen-copy (game-state-ball-pos state)) +tile-empty+)
          (multiple-value-bind (target-pos steps)
              (next-ball-pos screen-copy
                             (game-state-ball-pos state)
                             (point:- (game-state-ball-pos state) (game-state-last-ball-pos state))
                             1)
            (let ((distance (abs (point:elt (point:- target-pos
                                                     (game-state-paddle-pos state))
                                            0)))
                  (direction (signum (point:elt (point:- target-pos
                                                         (game-state-paddle-pos state))
                                                0))))
              (setf movements (append (make-list distance :initial-element direction)
                                      (make-list (- steps distance) :initial-element 0)))))))
      (when video
        (setf (point:aref (game-state-screen state) (game-state-paddle-pos state)) +tile-empty+)
        (setf (point:aref (game-state-screen state)
                          (point:+ (game-state-paddle-pos state)
                                   (point:make-point (car movements) 0)))
              +tile-paddle+)
        (write-frame state video))
      (when (zerop (count-block-tiles state))
        (throw 'empty (game-state-score state)))
      (pop movements))))

(defun make-input-fn (state)
  (lambda ()
    (when (zerop (count-block-tiles state))
      (throw 'empty (game-state-score state)))
    (signum (- (point:elt (game-state-ball-pos state) 0)
               (point:elt (game-state-paddle-pos state) 0)))))

(defun get-answer-2 ()
  (let ((state (make-game-state))
        (modified-program (copy-seq *input*)))
    (setf (svref modified-program 0) 2)
    (catch 'empty
      (intcode:run modified-program
                   :input-fn (make-smart-input-fn state)
                   :output-fn (make-output-fn state))
      ;(assert (zerop (count-block-tiles state)))
      (game-state-score state))))


;;;; Visualization

(defparameter *frame-width* 640)
(defparameter *frame-height* 480)
(defparameter *cell-width* 16)
(defparameter *cell-height* 20)
(defparameter *score-height* 24)
(defparameter *digit-width* 10)
(defparameter *digit-height* 18)
(defparameter *digit-padding* (- *score-height* *digit-height*))

(defun tile-color (tile-id)
  (ecase tile-id
    (#.+tile-empty+  :dark-highlight)
    (#.+tile-wall+   :dark-background)
    (#.+tile-block+  :dark-primary)
    (#.+tile-paddle+ :dark-primary)
    (#.+tile-ball+   :dark-emphasized)))

(defun draw-segment ()
  (cairo:move-to  0  0)
  (cairo:line-to  1 -1)
  (cairo:line-to  1 -5)
  (cairo:line-to  0 -6)
  (cairo:line-to -1 -5)
  (cairo:line-to -1 -1)
  (cairo:close-path)
  (cairo:fill-path))

;;; Segments are numbered as follows:
;;;
;;;  0
;;; 5 1
;;;  6
;;; 4 2
;;;  3
;;;
(defun draw-segments (segments)
  (iter (for segment in segments)
        (cairo:save)
        (ecase segment
          (0 (cairo:translate 2 1)
             (cairo:rotate (/ pi 2)))
          (1 (cairo:translate 9 8))
          (2 (cairo:translate 9 16))
          (3 (cairo:translate 2 17)
             (cairo:rotate (/ pi 2)))
          (4 (cairo:translate 1 16))
          (5 (cairo:translate 1 8))
          (6 (cairo:translate 1 9)
             (cairo:rotate (/ pi 2))))
        (draw-segment)
        (cairo:restore)))

(defun draw-digit (digit x)
  (viz:set-color :red)
  (cairo:save)
  (cairo:translate x *digit-padding*)
  (draw-segments (ecase digit
                   (0 '(0 1 2 3 4 5))
                   (1 '(1 2))
                   (2 '(0 1 3 4 6))
                   (3 '(0 1 2 3 6))
                   (4 '(1 2 5 6))
                   (5 '(0 2 3 5 6))
                   (6 '(0 2 3 4 5 6))
                   (7 '(0 1 2))
                   (8 '(0 1 2 3 4 5 6))
                   (9 '(0 1 2 3 5 6))))
  (cairo:restore))

(defun draw-score (state)
  (viz:set-color :dark-background)
  (cairo:rectangle 0 0 *frame-width* *score-height*)
  (cairo:fill-path)
  (iter (for d digits-of (game-state-score state) from right)
        (for i from 1)
        (draw-digit d (- *frame-width*
                         (* i (+ *digit-width* *digit-padding*))))))

(defun draw-block (tile-id x y)
  (viz:set-color (tile-color tile-id))
  (cairo:rectangle x y *cell-width* *cell-height*)
  (cairo:fill-path))

(defun draw-ball (x y)
  (draw-block +tile-empty+ x y)
  (viz:set-color (tile-color +tile-ball+))
  (cairo:arc (+ x (/ *cell-width* 2))
             (+ y (/ *cell-height* 2))
             (/ (min *cell-width* *cell-height*) 2)
             0
             (* 2 pi))
  (cairo:fill-path))

(defun draw-paddle (x y)
  (draw-block +tile-empty+ x y)
  (viz:set-color (tile-color +tile-paddle+))
  (let ((fifth-height (truncate *cell-height* 5)))
    (cairo:rectangle x y
                     *cell-width* fifth-height))
  (cairo:fill-path))

(defun draw-tile (state col row)
  (let ((tile-id (aref (game-state-screen state) row col))
        (pixel-x (* col *cell-width*))
        (pixel-y (* row *cell-height*)))
    (case tile-id
      (#.+tile-ball+   (draw-ball pixel-x pixel-y))
      (#.+tile-paddle+ (draw-paddle pixel-x pixel-y))
      (otherwise       (draw-block tile-id pixel-x pixel-y)))))

(defun draw-screen (state surface)
  (cairo:with-context-from-surface (surface)
    (draw-score state)
    (cairo:translate 0 *score-height*)
    (let ((screen-array (game-state-screen state)))
      (iter (for row from 0 below (array-dimension screen-array 0))
            (iter (for col from 0 below (array-dimension screen-array 1))
                  (draw-tile state col row)))
      (values))))

(defun write-screen (state filename)
  (cairo:with-png-file (filename
                        :rgb24
                        *frame-width*
                        (+ *score-height* *frame-height*)
                        :surface surface)
    (draw-screen state surface)))

(defun write-frame (state video)
  (cairo:with-surface (surface (viz:create-video-surface video))
    (draw-screen state surface)
    (viz:write-video-surface video surface)))

(defun animate-game (filename)
  (viz:with-video (video (viz:create-video
                          filename
                          *frame-width*
                          (+ *score-height* *frame-height*)
                          :fps 30))
    (let ((state (make-game-state))
          (modified-program (copy-seq *input*)))
      (setf (svref modified-program 0) 2)
      (intcode:run modified-program
                   :input-fn (make-smart-input-fn state video)
                   :output-fn (make-output-fn state))
      (dotimes (i (* 5 30))
        (write-frame state video)))))
