(in-package :aoc-2019-24)

;;; Part 2 answer is 2097, but it takes about an hour on my laptop.
(aoc:define-day 28778811 nil)


;;;; Input

(defparameter *example-1a*
  '("....#"
    "#..#."
    "#..##"
    "..#.."
    "#...."))
(defparameter *input* (aoc:input))


;;;; Part 1

(defparameter +cardinal-vectors+
  (list #(1 0 0) #(0 1 0) #(-1 0 0) #(0 -1 0)))

(defun parse-board (input)
  (iter (with result = (make-hash-table :test 'equalp))
        (for line in input)
        (for row from 0)
        (iter (for char in-string line)
              (for col from 0)
              (when (char= char #\#)
                (setf (gethash (point:make-point col row 0) result) 1)))
        (finally (return result))))

(defun print-board (board)
  (point:print-3d-hash-table board (lambda (e) (if (plusp e) #\# #\.)) :unknown #\.))

(defun adjacent-base-indices (position)
  (mapcar (lambda (v) (point:+ position v)) +cardinal-vectors+))

(defun outer-nested-point (source x y)
  (point:make-point x y (1- (point:elt source 2))))

(defun inner-nested-points (source &key x y)
  (iter (for i from 0 to 4)
        (collecting (point:make-point
                     (or x i) (or y i) (1+ (point:elt source 2))))))

(defun add-adjacent-levels (source indices)
  (remove
   nil
   (iter outer
         (for index in indices)
         (cond
           ;; outside left edge
           ((= -1 (point:elt index 0))
            (collecting (outer-nested-point index 1 2)))
           ;; outside top edge
           ((= -1 (point:elt index 1))
            (collecting (outer-nested-point index 2 1)))
           ;; outside right edge
           ((= 5 (point:elt index 0))
            (collecting (outer-nested-point index 3 2)))
           ;; outside bottom edge
           ((= 5 (point:elt index 1))
            (collecting (outer-nested-point index 2 3)))
           ;; inside left edge
           ((and (= 2 (point:elt index 0))
                 (= 2 (point:elt index 1))
                 (= 1 (point:elt source 0)))
            (appending (inner-nested-points index :x 0)))
           ;; inside top edge
           ((and (= 2 (point:elt index 0))
                 (= 2 (point:elt index 1))
                 (= 1 (point:elt source 1)))
            (appending (inner-nested-points index :y 0)))
           ;; inside right edge
           ((and (= 2 (point:elt index 0))
                 (= 2 (point:elt index 1))
                 (= 3 (point:elt source 0)))
            (appending (inner-nested-points index :x 4)))
           ;; inside bottom edge
           ((and (= 2 (point:elt index 0))
                 (= 2 (point:elt index 1))
                 (= 3 (point:elt source 1)))
            (appending (inner-nested-points index :y 4)))
           (t (collecting index))))))

(defun prune-2d-indices (indices)
  (remove-if (lambda (p)
               (not (and (<= 0 (point:elt p 0) 4)
                         (<= 0 (point:elt p 1) 4))))
             indices))

(defun adjacent-indices (position &key recursive)
  (let ((base-indices (adjacent-base-indices position)))
    (if recursive
        (add-adjacent-levels position base-indices)
        (prune-2d-indices base-indices))))

(defun cell-adjacent-sum (board position &key recursive)
  (iter (for index in (adjacent-indices position :recursive recursive))
        (summing (gethash index board 0))))

(defun cell-dies-p (board position &key recursive)
  (and (plusp (gethash position board 0))
       (/= 1 (cell-adjacent-sum board position :recursive recursive))))

(defun cell-lives-p (board position &key recursive)
  (and (zerop (gethash position board 0))
       (<= 1 (cell-adjacent-sum board position :recursive recursive) 2)))

(defun next-value (board position &key recursive)
  (cond
    ((cell-dies-p board position :recursive recursive)
     0)
    ((cell-lives-p board position :recursive recursive)
     1)
    (t
     (gethash position board 0))))

(defun candidate-positions (board &key recursive)
  "Returns all positions that are either alive or are adjacent to an alive
  position"
  (iter (for position in (alexandria:hash-table-keys board))
        (nunioning (cons position (adjacent-indices position :recursive recursive))
                   test #'point:=)))

(defun iterate-board (board &key recursive)
  (iter (with new-board = (make-hash-table :test 'equalp))
        (for position in (candidate-positions board :recursive recursive))
        (when (plusp (next-value board position :recursive recursive))
          (setf (gethash position new-board) 1))
        (finally (return new-board))))

(defun find-repeated-board (board)
  (iter (with seen = (make-hash-table :test 'equalp))
        (for current-board first board then (iterate-board current-board))
        (finding current-board such-that (gethash current-board seen))
        (setf (gethash current-board seen) t)))

(defun biodiversity-rating (board)
  (iter (for position in (alexandria:hash-table-keys board))
        (summing (expt 2 (+ (* 5 (point:elt position 1))
                            (point:elt position 0))))))

(defun get-answer-1 (&optional (input *input*))
  (biodiversity-rating (find-repeated-board (parse-board input))))


;;;; Part 2

(defun print-time (&optional (stream t))
  (multiple-value-bind
        (s m h)
      (get-decoded-time)
    (format stream "~2D:~2D:~2D" h m s)))

(defun get-answer-2 (&key (input *input*) (iterations 200))
  (iter (for board first (parse-board input) then (iterate-board board :recursive t))
        (for i from 0)
        (print-time)
        (format t ": ~A~%" i)
        (repeat iterations)
        (finally (return (hash-table-count board)))))
