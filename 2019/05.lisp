(in-package :aoc-2019-05)

(aoc:define-day 16348437 6959377)


;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))


;;; Part 1

(defun get-answer-1 ()
  (car (last (intcode:run *input* :input-list '(1)))))


;;; Part 2

(defun get-answer-2 ()
  (car (intcode:run *input* :input-list '(5))))
