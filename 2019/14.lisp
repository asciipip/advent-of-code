(in-package :aoc-2019-14)

(aoc:define-day 654909 2876992)


;;;; Input

(parseq:defrule reaction ()
    (and (aoc:comma-list ingredient) " => " ingredient)
  (:choose 0 2)
  (:lambda (inputs output) (cons output inputs)))

(parseq:defrule ingredient ()
    (and aoc:integer-string " " chemical)
  (:choose 0 2)
  (:function #'cons))

(parseq:defrule chemical ()
    (+ (char "A-Z"))
  (:string)
  (:function #'intern))

(defun parse-reactions (lines)
  (fset:reduce (lambda (result line)
                 (let* ((reaction (parseq:parseq 'reaction line))
                        (output-chemical (cdr (first reaction))))
                   (assert (null (fset:lookup result output-chemical)))
                   (fset:with result output-chemical reaction)))
               lines
               :initial-value (fset:empty-map)))

(defparameter *example-1* (parse-reactions '("10 ORE => 10 A"
                                             "1 ORE => 1 B"
                                             "7 A, 1 B => 1 C"
                                             "7 A, 1 C => 1 D"
                                             "7 A, 1 D => 1 E"
                                             "7 A, 1 E => 1 FUEL")))
(defparameter *example-13312*
  (parse-reactions '("157 ORE => 5 NZVS"
                     "165 ORE => 6 DCFZ"
                     "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL"
                     "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ"
                     "179 ORE => 7 PSHF"
                     "177 ORE => 5 HKGWZ"
                     "7 DCFZ, 7 PSHF => 2 XJWVT"
                     "165 ORE => 2 GPVTF"
                     "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT")))
(defparameter *example-180697*
  (parse-reactions '("2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG"
                     "17 NVRVD, 3 JNWZP => 8 VPVL"
                     "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL"
                     "22 VJHF, 37 MNCFX => 5 FWMGM"
                     "139 ORE => 4 NVRVD"
                     "144 ORE => 7 JNWZP"
                     "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC"
                     "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV"
                     "145 ORE => 6 MNCFX"
                     "1 NVRVD => 8 CXFTF"
                     "1 VJHF, 6 MNCFX => 4 RFSQX"
                     "176 ORE => 6 VJHF")))
(defparameter *input* (parse-reactions (aoc:input)))


(defun reaction-count (reactions)
  (fset:size reactions))

;;;; Part 1

(defun find-most-demanded-chemical (reactions inventory)
  (cdr
   (fset:reduce (lambda (best chemical quantity)
                  (if (and (< quantity (car best))
                           (fset:lookup reactions chemical))
                      (cons quantity chemical)
                      best))
                inventory
                :initial-value (cons 0 nil))))

(defun apply-reaction (inventory reaction multiplier)
  (destructuring-bind ((output-quantity . output-chemical) &rest inputs)
      reaction
    (fset:reduce (lambda (new-inventory input)
                   (destructuring-bind (input-quantity . input-chemical) input
                     (fset:with new-inventory
                                input-chemical
                                (- (fset:lookup new-inventory input-chemical)
                                   (* input-quantity multiplier)))))
                 inputs
                 :initial-value (fset:with inventory
                                           output-chemical
                                           (+ (fset:lookup inventory output-chemical)
                                              (* output-quantity multiplier))))))

(defun make-chemicals (reactions inventory)
  (let* ((chemical-to-make (find-most-demanded-chemical reactions inventory))
         (reaction (fset:lookup reactions chemical-to-make))
         (quantity-to-make (if chemical-to-make
                               (- (fset:lookup inventory chemical-to-make))
                               0)))
    (if chemical-to-make
        (destructuring-bind ((output-quantity . output-chemical) &rest inputs)
            reaction
          (declare (ignore inputs))
          (assert (eq output-chemical chemical-to-make))
          (let ((multiplier (ceiling quantity-to-make output-quantity)))
            (make-chemicals reactions (apply-reaction inventory reaction multiplier))))
        inventory)))

(defun make-chemical (reactions chemical &optional (quantity 1))
  (let ((inventory (fset:empty-map 0)))
    (make-chemicals reactions (fset:with inventory chemical (- quantity)))))

(defun get-answer-1 (&optional (reactions *input*) (quantity 1))
  (let ((final-inventory (make-chemical reactions 'fuel quantity)))
    (- (fset:lookup final-inventory 'ore))))

(aoc:given 1
  (= 31 (get-answer-1 *example-1*))
  (= 13312 (get-answer-1 *example-13312*))
  (= 180697 (get-answer-1 *example-180697*)))

;;;; Part 2

(defparameter *ore-quantity* 1000000000000)

;;; FUEL-QUANTITY +- increment
(defun find-optimal-fuel-results (reactions fuel-quantity increment)
  (if (zerop increment)
      fuel-quantity
      (let* ((ore-quantity (get-answer-1 reactions fuel-quantity))
             (direction (signum (- *ore-quantity* ore-quantity))))
        (ecase direction
          (1 (find-optimal-fuel-results reactions (+ fuel-quantity increment) (truncate increment 2)))
          (-1 (find-optimal-fuel-results reactions (- fuel-quantity increment) (truncate increment 2)))))))

(defun get-answer-2 (&optional (reactions *input*))
  (let* ((upper-fuel-bound (iter (for fuel first 1 then (* 2 fuel))
                                 (finding fuel such-that (< *ore-quantity* (get-answer-1 reactions fuel)))))
         (optimal-quantity (find-optimal-fuel-results reactions (truncate upper-fuel-bound 2) (truncate upper-fuel-bound 2))))
    ;; Sometimes the above algorithm overshoots by one, so we double-check
    ;; here to make sure we got the right value.
    (if (< *ore-quantity* (get-answer-1 reactions optimal-quantity))
        (1- optimal-quantity)
        optimal-quantity)))

(aoc:given 2
  (= 82892753 (get-answer-2 *example-13312*))
  (= 5586022 (get-answer-2 *example-180697*)))
