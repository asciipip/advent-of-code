(in-package :aoc-2019-04)

(aoc:define-day 1890 nil)


;;; Input

(defparameter *input* (mapcar #'abs (aoc:extract-ints (aoc:input))))


;;; Part 1

;; The logic here is relatively simple.  `valid-r' is a tail-recursive
;; helper function that uses `(truncate remaining-password 10)` to examine
;; successive digits in the password, from right to left.
(defun valid-password-p (password &optional (strict-doubles nil))
  (labels ((has-double-p (last-digit this-digit double-seen dup-count)
             (or double-seen
                 (and (= 2 dup-count)
                      (or (not strict-doubles)
                          (/= this-digit last-digit)))))
           (valid-r (last-digit this-digit rest double-seen dup-count)
             (and (<= this-digit last-digit)
                  (if (and (zerop rest)
                           (zerop this-digit))
                      (has-double-p last-digit this-digit double-seen dup-count)
                      (multiple-value-bind (quotient remainder)
                          (truncate rest 10)
                        (valid-r this-digit
                                 remainder
                                 quotient
                                 (has-double-p last-digit this-digit double-seen dup-count)
                                 (if (= this-digit last-digit)
                                     (1+ dup-count)
                                     1)))))))
    (and (<= 100000 password 999999)
         ; Initialize `valid-r' with some values that won't interfere with
         ; its algorithm.
         (valid-r 11 10 password nil 0))))

(aoc:deftest given-1
  (5am:is-true (valid-password-p 111111))
  (5am:is-false (valid-password-p 223450))
  (5am:is-false (valid-password-p 123789)))

(defun count-passwords-in-range (range &optional (strict-doubles nil))
  (iter (for password from (first range) to (second range))
        (counting (valid-password-p password strict-doubles))))

(defun get-answer-1 ()
  (count-passwords-in-range *input*))


;;; Part 2

(aoc:deftest given-2
  (5am:is-true (valid-password-p 112233 t))
  (5am:is-false (valid-password-p 123444 t))
  (5am:is-true (valid-password-p 111122 t)))

(aoc:deftest extra-2-counterexamples
  (5am:is-true (valid-password-p 112345))
  (5am:is-true (valid-password-p 666799 t)))

(defun get-answer-2 ()
  (count-passwords-in-range *input* t))


;;; Visualization

(defparameter *margins* '(64 96 120 32))  ; left top right bottom
(defparameter *image-background* :dark-background)
(defparameter *invalid-color* :dark-highlight)
(defparameter *valid-1* :red)
(defparameter *valid-2* :green)

(defun draw-valid-passwords (filename)
  (let ((width (+ 1000 (first *margins*) (third *margins*)))
        (height (+ 900 (second *margins*) (fourth *margins*))))
    (cairo:with-png-file (filename
                          :rgb24
                          width
                          height)
      (viz:set-color *image-background*)
      (cairo:rectangle 0 0 width height)
      (cairo:fill-path)

      (cairo:translate (first *margins*) (second *margins*))

      (viz:set-color :dark-primary)
      (viz:draw-text "xxx000"    0 -8 :horizontal :left)
      (viz:draw-text "xxx999" 1000 -8 :horizontal :right)

      (viz:draw-text "100xxx" -8   0 :horizontal :right :vertical :top)
      (viz:draw-text "999xxx" -8 900 :horizontal :right :vertical :bottom)

      (viz:set-color *valid-1*)
      (cairo:rectangle 1016 32 8 8)
      (cairo:fill-path)
      (viz:set-color :dark-primary)
      (viz:draw-text "Part 1 Only" 1028 36 :horizontal :left :vertical :middle)
      
      (viz:set-color *valid-2*)
      (cairo:rectangle 1016 48 8 8)
      (cairo:fill-path)
      (viz:set-color :dark-primary)
      (viz:draw-text "Parts 1 and 2" 1028 52 :horizontal :left :vertical :middle)

      (viz:set-color :dark-primary)
      (viz:draw-text "Valid Passwords for Advent of Code 2019 Day 4" 500 -48
                               :size 24 :horizontal :center)
      
      (viz:set-color *invalid-color*)
      (cairo:rectangle 0 0 1000 900)
      (cairo:fill-path)

      (cairo:set-line-width 1)
      (viz:set-color *image-background*)
      (iter (for i from 100 to 900 by 100)
            (cairo:move-to    0 (+ i 1/2))
            (cairo:line-to 1000 (+ i 1/2))
            (cairo:stroke)
            (cairo:move-to (+ i 1/2)    0)
            (cairo:line-to (+ i 1/2) 1000)
            (cairo:stroke))

      (cairo:translate 0 -100)
      
      (iter (for col from 0 to 999)
            (iter (for row from 0 to 999)
                  (for password = (+ (* 1000 row) col))
                  (cond
                    ((valid-password-p password t)
                     (viz:set-color *valid-2*)
                     (cairo:rectangle col row 1 1)
                     (cairo:fill-path))
                    ((valid-password-p password)
                     (viz:set-color *valid-1*)
                     (cairo:rectangle col row 1 1)
                     (cairo:fill-path))))))))
