(in-package :aoc-2019-22)

(aoc:define-day 6417 98461321956136)


;;;; Input

(defparameter *input* (aoc:input))
(defparameter *example-1a*
  '("deal with increment 7"
    "deal into new stack"
    "deal into new stack"))
(defparameter *example-1b*
  '("cut 6"
    "deal with increment 7"
    "deal into new stack"))
(defparameter *example-1c*
  '("deal with increment 7"
    "deal with increment 9"
    "cut -2"))
(defparameter *example-1d*
  '("deal into new stack"
    "cut -2"
    "deal with increment 7"
    "cut 8"
    "cut -4"
    "deal with increment 7"
    "cut 3"
    "deal with increment 9"
    "deal with increment 3"
    "cut -1"))


;;;; Part 1

(defun undo-new-stack (deck-size position)
  (- (1- deck-size) position))

(defun undo-cut (deck-size n position)
  (mod (+ position n) deck-size))

(defun extended-euclidean (a b &optional (sp 1) (sc 0) (tp 0) (tc 1))
  (if (zerop b)
      (values a sp tp)
      (let ((q (floor a b)))
        (extended-euclidean b (- a (* q b))
                            sc (- sp (* q sc))
                            tc (- tp (* q tc))))))

(defun undo-increment (deck-size n position)
  (let ((mod-inverse (nth-value 1 (extended-euclidean n deck-size))))
    (mod (* position mod-inverse) deck-size)))

(defun undo-instruction (deck-size instruction)
  (cond
    ((string= "deal into new stack" instruction)
     (alexandria:curry #'undo-new-stack deck-size))
    ((alexandria:starts-with-subseq "cut" instruction)
     (alexandria:curry #'undo-cut deck-size (aoc:extract-ints instruction)))
    ((alexandria:starts-with-subseq "deal with increment" instruction)
     (alexandria:curry #'undo-increment deck-size (aoc:extract-ints instruction)))
    (t (assert nil))))

(defun undo-instruction-chain (deck-size instructions)
  (apply #'alexandria:multiple-value-compose
         (mapcar (alexandria:curry #'undo-instruction deck-size) instructions)))

(defun shuffle-deck (deck-size instructions)
  (iter (with undo = (undo-instruction-chain deck-size instructions))
        (for i from 0 below deck-size)
        (collecting (funcall undo i))))

(aoc:given 1
  (equal '(0 3 6 9 2 5 8 1 4 7) (shuffle-deck 10 *example-1a*))
  (equal '(3 0 7 4 1 8 5 2 9 6) (shuffle-deck 10 *example-1b*))
  (equal '(6 3 0 7 4 1 8 5 2 9) (shuffle-deck 10 *example-1c*))
  (equal '(9 2 5 8 1 4 7 0 3 6) (shuffle-deck 10 *example-1d*)))

(defun get-answer-1 ()
  (iter (with undo = (undo-instruction-chain 10007 *input*))
        (for i from 0)
        (finding i such-that (= 2019 (funcall undo i)))))


;;;; Part 2

(defun modpower (num power modulus)
  "Raises NUM to the POWER power, modulo MODULUS.  Faster than
   (mod (expt num power) modulus)."
  (declare (type (integer 0) power modulus)
           (type integer num)
           (values (integer 0)))
  (labels ((modpower-r (b e a)
             (declare (type (integer 0) b e a))
             (if (zerop e)
                 a
                 (modpower-r (mod (* b b) modulus)
                             (truncate e 2)
                             (if (oddp e)
                                 (mod (* b a) modulus)
                                 a)))))
    (modpower-r (mod num modulus) power 1)))

(defun stack-polynomial (deck-size a b)
  (values (- a)
          (mod (- (1- deck-size) b) deck-size)))

(defun cut-polynomial (deck-size n a b)
  (values a
          (mod (+ b n) deck-size)))

(defun increment-polynomial (deck-size n a b)
  (let ((mod-inverse (nth-value 1 (extended-euclidean n deck-size))))
    (values (mod (* mod-inverse a) deck-size)
            (mod (* mod-inverse b) deck-size))))

(defun parse-polynomial-instruction (deck-size a b instruction)
  (cond
    ((string= "deal into new stack" instruction)
     (stack-polynomial deck-size a b))
    ((alexandria:starts-with-subseq "cut" instruction)
     (cut-polynomial deck-size (aoc:extract-ints instruction) a b))
    ((alexandria:starts-with-subseq "deal with increment" instruction)
     (increment-polynomial deck-size (aoc:extract-ints instruction) a b))
    (t (assert nil))))

(defun parse-polynomial-instructions (deck-size instructions)
  (iter (for instruction in (reverse instructions))
        (for (values a b)
             first (parse-polynomial-instruction deck-size 1 0 instruction)
             then (parse-polynomial-instruction deck-size a b instruction))
        (finally (return (values a b)))))

(defun apply-polynomial (deck-size a b position)
  (mod (+ (* a position) b) deck-size))

(defun shuffle-deck-polynomial (deck-size instructions)
  (multiple-value-bind (a b) (parse-polynomial-instructions deck-size instructions)
    (iter (for i from 0 below deck-size)
          (collecting (apply-polynomial deck-size a b i)))))

(defun get-answer-2 (&key (input *input*) (deck-size 119315717514047) (repetitions 101741582076661) (position 2020))
  (multiple-value-bind (a b) (parse-polynomial-instructions deck-size input)
    (apply-polynomial deck-size
                      (modpower a repetitions deck-size)
                      (* b (* (1- (modpower a repetitions deck-size))
                              (nth-value 1 (extended-euclidean (1- a) deck-size))))
                      position)))
