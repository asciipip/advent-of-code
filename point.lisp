(in-package :point)

(deftype point (&optional dimension)
  `(vector real ,(or dimension '*)))

(defun make-point-array (length &optional initial-contents)
  (if initial-contents
      (make-array length :element-type 'real :initial-contents initial-contents)
      (make-array length :element-type 'real)))

(defun make-point (&rest coordinates)
  (declare (type (or null (cons real)) coordinates))
  (make-point-array (length coordinates) coordinates))

(defun read-delimiter (stream char)
  (declare (ignore stream))
  (error "Delimiter ~S shouldn't be read alone" char))

(set-dispatch-macro-character #\# #\<
  (lambda (stream subchar arg)
    (let ((*readtable* (copy-readtable)))
      (set-macro-character #\> 'read-delimiter)
      (let ((lst (read-delimited-list #\> stream t)))
        `(make-point ,@lst)))))

(defun list-coordinates (point)
  "Returns the coordinates of POINT as a list."
  (declare (type point point))
  (coerce point 'list))

(defun elt (point index)
  "Returns the coordinate at position INDEX."
  (declare (type point point)
           (type (integer 0 #.(1- array-total-size-limit)) index))
  (cl:aref point index))

;; Some convenience functions
(declaim (inline x))
(defun x (point)
  "Alias for (elt point 0)"
  (elt point 0))

(declaim (inline y))
(defun y (point)
  "Alias for (elt point 1)"
  (elt point 1))

(declaim (inline z))
(defun z (point)
  "Alias for (elt point 2)"
  (elt point 2))

(declaim (inline w))
(defun w (point)
  "Alias for (elt point 3)"
  (elt point 3))

(defun = (point &rest other-points)
  "Returns true if all points have the same dimension and coordinates;
  false otherwise."
  (declare (type point point)
           (type (or null (cons point)) other-points))
  (reduce #'equalp (cons point other-points)))

(defun sxhash-point (point)
  (declare (type point point))
  (sxhash point))
(sb-ext:define-hash-table-test = sxhash-point)


(defun matching-dimensions-p (points)
  "Returns true if and only if all points have the same number of
  dimensions."
  (declare (type (or null (cons point) fset:collection) points))
  (let ((dimensions (length (aoc:arb-element points))))
    (fset:reduce (lambda (result point)
                   (and result
                        (cl:= dimensions (length point))))
                 points
                 :initial-value t)))

(defun check-dimensions (points)
  (when (not (matching-dimensions-p points))
    (error "Point dimensions do not match.")))

(defun map-coordinates (function points)
  "Calls FUNCTION on each successive set of coordinates from the points.
  FUNCTION must take as many parameters as there are points.  POINTS may
  be any collection of point objects understood by FSet.  Returns a point
  made from the returns values of FUNCTION."
  (declare (type (or null (cons point) fset:collection) points))
  (check-dimensions points)
  (let* ((arb-point (if (or (typep points 'fset:set)
                            (typep points 'fset:bag))
                        (fset:arb points)
                        (fset:first points)))
         (dimension-count (length arb-point))
         (result (make-point-array dimension-count)))
    (case (fset:size points)
      (0)
      (1 (dotimes (i (length arb-point))
           (setf (cl:aref result i)
                 (funcall function (cl:aref arb-point i)))))
      (t (let ((args (make-array dimension-count :initial-element nil)))
           ;; We're using `reduce` for side effects here.  A `do-` macro
           ;; would be clearer, but those aren't polymorphic, and we want
           ;; to be able to handle any single-arity FSet collection.
           (fset:reduce (lambda (_ point)
                          (declare (ignore _))
                          (dotimes (i dimension-count)
                            (push (cl:aref point i) (cl:aref args i))))
                        points
                        :initial-value nil)
           (dotimes (i dimension-count)
             (setf (cl:aref result i)
                   (apply function (reverse (cl:aref args i))))))))
    result))

(defun + (&rest points)
  "Adds the points together, coordinatewise"
  (declare (type (or null (cons point)) points))
  (check-dimensions points)
  (cond
    ((endp points)
     (make-point))
    ((endp (cdr points))
     (car points))
    (t
     (map-coordinates #'cl:+ points))))

(defun - (point &rest more-points)
  "Subtracts MORE-POINTS from POINT, coordinatewise.  With only one
  parameter, negates POINT."
  (declare (type point point)
           (type (or null (cons point)) more-points))
  (if (endp more-points)
      (map-coordinates #'cl:- (list point))
      (progn
        (check-dimensions (cons point more-points))
        (map-coordinates #'cl:- (list point (apply #'+ more-points))))))

(defun * (point number)
  "Multiplies every coordinate in POINT by NUMBER."
  (declare (type point point)
           (type real number))
  (map-coordinates (lambda (c) (cl:* c number)) (list point)))

(defun / (point number)
  "Divides every coordinate in POINT by NUMBER."
  (declare (type point point)
           (type real number))
  (map-coordinates (lambda (c) (cl:/ c number)) (list point)))


(defun mod (point divisor-point)
  "Returns a point where each coordinate in POINT is modulo the
  corresponding coordinate in DIVISOR-POINT."
  (map-coordinates #'cl:mod (list point divisor-point)))


(defun compare (op &rest points)
  "Applies OP to every coordinate of each member of points."
  (check-dimensions points)
  (labels ((compare-r (point-a more-points)
             (or (endp more-points)
                 (destructuring-bind (point-b &rest remaining-points) more-points
                   (and (iter (for a in-vector point-a)
                              (for b in-vector point-b)
                              (always (funcall op a b)))
                        (compare-r point-b remaining-points))))))
    (if (endp (cdr points))      ; Implicitly handles the "no points" case
        t
        (compare-r (car points) (cdr points)))))

(defun < (&rest points)
  "Returns true if every coordinate of each member of POINTS is less than
  each corresponding coordinate of the next point, and so on."
  (apply #'compare #'cl:< points))

(defun <= (&rest points)
  (apply #'compare #'cl:<= points))

(defun total< (point1 point2)
  "Provides a total ordering of points.  Examines the first pair of
  unequal coordinates among POINT1 and POINT1 and returns true if POINT1's
  coordinate is less than POINT2's."
  (check-dimensions (list point1 point2))
  (labels ((<r (i)
             (cond
               ((cl:<= (length point1) i)
                nil)
               ((cl:< (elt point1 i) (elt point2 i))
                t)
               ((cl:= (elt point1 i) (elt point2 i))
                (<r (1+ i)))
               (t
                nil))))
    (<r 0)))
(defun norm (point)
  "Returns the magnitude of the vector from the origin to POINT."
  (sqrt (reduce (lambda (r c) (cl:+ r (cl:* c c)))
                point
                :initial-value 0)))

(defun unit-vector (point)
  "Returns a point that is a unit vector from the origin."
  (let ((magnitude (norm point)))
    (cond
      ((zerop magnitude)
       (error "Cannot scale point; its magnitude is zero: ~A" point))
      ((cl:= magnitude 1)
       point)
      (t
       (/ point magnitude)))))

(defun aref (array point)
  "Uses POINT's coordinates as indices into ARRAY.  The indexing is
  row-major; the last coordinate of POINT is used for ARRAY's first
  dimension, and so on."
  (declare (type point point))
  (apply #'cl:aref array (nreverse (list-coordinates point))))

(defun (setf aref) (new-value array point)
  (declare (type point point))
  (setf (apply #'cl:aref array (nreverse (list-coordinates point)))
        new-value))

(defun neighbor-set (point &key diagonals array)
  "Returns a set of points that are adjacent to POINT.  If ARRAY is non-nil,
  only valid indices for the array will be in the set; otherwise, all
  adjacent points will be returned.  If DIAGONALS is true, all eight
  neighbors will be considered; otherwise, only horizontal and vertical
  neighbors will be considered."
  (let ((result (fset:empty-set)))
    (dolist (dy '(-1 0 1))
      (dolist (dx '(-1 0 1))
        (let ((y (cl:+ dy (y point)))
              (x (cl:+ dx (x point))))
          (when (and (or (null array)
                         (cl:< -1 y (array-dimension array 0)))
                     (or (null array)
                         (cl:< -1 x (array-dimension array 1)))
                     (not (cl:= 0 dx dy))
                     (or diagonals
                         (cl:/= (abs dx) (abs dy))))
            (fset:adjoinf result (make-point x y))))))
    result))

(defun turn (point direction)
  "Rotates a 2D point 90 degrees.  DIRECTION should be either the symbol
  LEFT or RIGHT."
  (declare (type (point 2) point)
           (type symbol direction))
  (cond
    ((string= "RIGHT" (symbol-name direction))
     (make-point (cl:aref point 1) (cl:- (cl:aref point 0))))
    ((string= "LEFT" (symbol-name direction))
     (make-point (cl:- (cl:aref point 1)) (cl:aref point 0)))
    (t (error "POINT-TURN only accepts directions of RIGHT and LEFT, not \"~A\"." direction))))

(defun manhattan-distance (point-a point-b)
  (declare (type point point-a point-b))
  (check-dimensions (list point-a point-b))
  (iter (for a in-vector point-a)
        (for b in-vector point-b)
        (summing (abs (cl:- a b)))))

(defun bbox (points)
  "Returns two points.  The first contains the smallest values for each
  set of coordinates; the second, the largest."
  (declare (type (or null (cons point) fset:collection) points))
  (values (map-coordinates #'min points)
          (map-coordinates #'max points)))

(defun print-2d-map (map element-char-fn &key (stream t) (unknown #\ ) highlight)
  "Prints a representation of MAP to STREAM.  The keys of MAP should be
  two-coordinate points, with the first coordinate giving the
  column (increasing to the right) and the second coordinate giving the
  row (increasing *down*).

  ELEMENT-CHAR-FN should be a function of one argument.  It will be called
  for each value of MAP.  It should return a character, though anything
  that can be passed to FORMAT will work.

  UNKNOWN will be used for positions that are not present in MAP.

  HIGHLIGHT is a position that is to be marked in some way to make it stand out."
  (multiple-value-bind (min-point max-point) (bbox (fset:convert 'list (fset:domain map)))
    (declare (type (point 2) min-point max-point))
    (iter (for row from (elt min-point 1) to (elt max-point 1))
          (iter (for col from (elt min-point 0) to (elt max-point 0))
                (for position = (make-point col row))
                (for (values element foundp) = (fset:lookup map position))
                (format stream "~A" (if foundp
                                        (funcall element-char-fn element)
                                        unknown))
                (when (and highlight (= highlight position))
                  (princ #\U20DD stream)))
          (format stream "~%")))
  (values))

(defun print-2d-hash-table (hash-table element-char-fn &key (stream t) (unknown #\ ) highlight)
  "Prints a representation of HASH-TABLE to STREAM.  The keys of
  HASH-TABLE should be two-coordinate points, with the first coordinate
  giving the column (increasing to the right) and the second coordinate
  giving the row (increasing *down*).

  ELEMENT-CHAR-FN should be a function of one argument.  It will be called
  for each value of HASH-TABLE.  It should return a character, though
  anything that can be passed to FORMAT will work.

  UNKNOWN will be used for positions that are not present in HASH-TABLE.

  HIGHLIGHT is a position that is to be marked in some way to make it stand out."
  (multiple-value-bind (min-point max-point) (bbox (alexandria:hash-table-keys hash-table))
    (declare (type (point 2) min-point max-point))
    (iter (for row from (elt min-point 1) to (elt max-point 1))
          (iter (for col from (elt min-point 0) to (elt max-point 0))
                (for position = (make-point col row))
                (for (values element foundp) = (gethash position hash-table))
                (format stream "~A" (if foundp
                                        (funcall element-char-fn element)
                                        unknown))
                (when (and highlight (= highlight position))
                  (princ #\U20DD stream)))
          (format stream "~%")))
  (values))

(defun print-3d-hash-table (hash-table element-char-fn &key (stream t) (unknown #\ ) highlight)
  "Prints a representation of HASH-TABLE to STREAM.  The keys of
  HASH-TABLE should be three-coordinate points.  The first coordinate
  gives the column (increasing to the right), the second coordinate gives
  the row (increasing *down*), and the third coordinate gives the layer.

  ELEMENT-CHAR-FN should be a function of one argument.  It will be called
  for each value of HASH-TABLE.  It should return a character, though
  anything that can be passed to FORMAT will work.

  UNKNOWN will be used for positions that are not present in HASH-TABLE.

  HIGHLIGHT is a position that is to be marked in some way to make it stand out."
  (multiple-value-bind (min-point max-point) (bbox (alexandria:hash-table-keys hash-table))
    (declare (type (point 3) min-point max-point))
    (iter (for layer from (elt min-point 2) to (elt max-point 2))
          (format t "Layer: ~A~%" layer)
          (iter (for row from (elt min-point 1) to (elt max-point 1))
                (iter (for col from (elt min-point 0) to (elt max-point 0))
                      (for position = (make-point (list col row layer)))
                      (for (values element foundp) = (gethash position hash-table))
                      (format stream "~A" (if foundp
                                              (funcall element-char-fn element)
                                              unknown))
                      (when (and highlight (= highlight position))
                        (princ #\U20DD stream)))
                (format stream "~%"))
          (format t "~%")))
  (values))

(defun print-3d-set (set element-char-fn &key (stream t) (unknown #\ ) highlight)
  "Prints a representation of SET to STREAM.  The elements of SET should
  be three-coordinate points.  The first coordinate gives the
  column (increasing to the right), the second coordinate gives the
  row (increasing *down*), and the third coordinate gives the layer.

  ELEMENT-CHAR-FN should be a function of one argument.  It will be called
  for each value of SET.  It should return a character, though anything
  that can be passed to FORMAT will work.

  UNKNOWN will be used for positions that are not present in SET.

  HIGHLIGHT is a position that is to be marked in some way to make it stand out."
  (multiple-value-bind (min-point max-point) (bbox set)
    (declare (type (point 3) min-point max-point))
    (iter (for layer from (elt min-point 2) to (elt max-point 2))
          (format t "Layer: ~A~%" layer)
          (iter (for row from (elt min-point 1) to (elt max-point 1))
                (iter (for col from (elt min-point 0) to (elt max-point 0))
                      (for position = (make-point col row layer))
                      (format stream "~A" (if (fset:lookup set position)
                                              (funcall element-char-fn position)
                                              unknown))
                      (when (and highlight (= highlight position))
                        (princ #\U20DD stream)))
                (format stream "~%"))
          (format t "~%")))
  (values))
