(in-package :aoc-2017-10)

(aoc:define-day 52070 "7f94112db4e32e19cf6502073c66f9bb")


;;; Parsing

(parseq:defrule lengths ()
    (aoc:comma-list aoc:integer-string))


;;; Input

(defparameter *lengths* (aoc:input :parse-line 'lengths))
(defparameter *example* '(3 4 1 5))


;;; Part 1

(defun add-start-points (total-elements lengths)
  (labels ((add-r (remaining-lengths current-position step)
             (if (endp remaining-lengths)
                 nil
                 (let ((length (car remaining-lengths)))
                   (cons (list current-position length)
                         (add-r (cdr remaining-lengths)
                                (mod (+ current-position length step)
                                     total-elements)
                                (1+ step)))))))
    (add-r lengths 0 0)))

(defun reversal-applies-p (total-elements start length position)
  (if (< (+ start length) total-elements)
      (and (<= start position)
           (< position (+ start length)))
      (or (<= start position)
          (< position (mod (+ start length) total-elements)))))

(defun apply-reversal (total-elements reversal position)
  "Takes the start and length of a segment reversal, as well as the position
  you want to look up.  Returns the pre-reversal number that position
  refers to."
  (destructuring-bind (start length) reversal
    (if (reversal-applies-p total-elements start length position)
        (mod (+ (* 2 start) (- length 1 position))
             total-elements)
        position)))

(defun apply-reversals (total-elements reversals position)
  (fset:reduce (alexandria:curry #'apply-reversal total-elements)
               reversals
               :from-end t
               :initial-value position))

(defun get-answer-1 (&optional (total-elements 256) (lengths *lengths*))
  (let ((reversals (add-start-points total-elements lengths)))
    (* (apply-reversals total-elements reversals 0)
       (apply-reversals total-elements reversals 1))))

(aoc:given 1
  (= 12 (get-answer-1 5 *example*)))


;;; Part 2

(defparameter *byte-rounds* 64)
(defparameter *byte-suffix* '(17 31 73 47 23))
(defparameter *byte-lengths* (aoc:input))

(defun string-to-byte-lengths (string)
  (append (map 'list #'char-code string)
          *byte-suffix*))

(defun repeat-list (list count)
  (if (= 1 count)
      list
      (append list (repeat-list list (1- count)))))

(defun byte-permute (total-elements byte-lengths)
  (let ((reversals (add-start-points total-elements (repeat-list byte-lengths *byte-rounds*))))
    (fset:image (alexandria:curry #'apply-reversals total-elements reversals)
                (alexandria:iota total-elements))))

(defun make-dense-hash (bytes)
  (if (endp bytes)
      nil
      (destructuring-bind (block rest) (aoc:split-at 16 bytes)
        (cons (apply #'logxor block)
              (make-dense-hash rest)))))

(defun knot-hash (string)
  (make-dense-hash (byte-permute 256 (string-to-byte-lengths string))))

(defun format-dense-hash (hash)
  (string-downcase (format nil "~{~2,'0X~}" hash)))

(defun get-answer-2 (&optional (byte-lengths *byte-lengths*))
  (format-dense-hash (knot-hash byte-lengths)))

(aoc:given 2
  (string= "a2582a3a0e66e6e86e3812dcb672a272" (get-answer-2 ""))
  (string= "33efeb34ea91902bb2f59c9920caa6cd" (get-answer-2 "AoC 2017"))
  (string= "3efbe78a8d82f29979031a4aa0b16a9d" (get-answer-2 "1,2,3"))
  (string= "63960835bcdc130f0b66d7ff4f6a5a8e" (get-answer-2 "1,2,4")))
