(in-package :aoc-2017-20)

;; Part 2 is 502, but I don't have a good threshold for calculating
;; deterministically.
(aoc:define-day 300 nil)


;;; Data Structures

(defstruct particle
  id
  position
  velocity
  acceleration)


;;; Parsing

(parseq:defrule particle ()
    (and "p=" point ", v=" point ", a=" point)
  (:choose 1 3 5)
  (:lambda (position velocity acceleration)
    (make-particle :position position
                   :velocity velocity
                   :acceleration acceleration)))

(parseq:defrule point ()
    (and "<" (aoc:comma-list aoc:integer-string) ">")
  (:choose 1)
  (:function #'point:make-point))

(defun add-particle-ids (particles)
  (mapc (lambda (particle id) (setf (particle-id particle) id))
        particles
        (alexandria:iota (length particles)))
  (fset:convert 'fset:set particles))


;;; Input

(defparameter *particles* (add-particle-ids (aoc:input :parse-line 'particle)))
(defparameter *example*
  (fset:set (make-particle :id 0 :position #<3 0 0> :velocity #<2 0 0> :acceleration #<-1 0 0>)
            (make-particle :id 1 :position #<4 0 0> :velocity #<0 0 0> :acceleration #<-2 0 0>)))


;;; Part 1

(defun distance-from-origin (point)
  (point:manhattan-distance #<0 0 0> point))

(defun slowest-particle (particles)
  (fset:reduce (lambda (result particle)
                 (let ((slowest-acceleration (distance-from-origin (particle-acceleration result)))
                       (this-acceleration (distance-from-origin (particle-acceleration particle))))
                   (if (< this-acceleration slowest-acceleration)
                       particle
                       result)))
               particles))

(defun get-answer-1 (&optional (particles *particles*))
  (particle-id (slowest-particle particles)))


;;; Part 2

(defparameter *example2*
  (fset:set (make-particle :id 0 :position #<-6 0 0> :velocity #< 3 0 0> :acceleration #<0 0 0>)
            (make-particle :id 1 :position #<-4 0 0> :velocity #< 2 0 0> :acceleration #<0 0 0>)
            (make-particle :id 2 :position #<-2 0 0> :velocity #< 1 0 0> :acceleration #<0 0 0>)
            (make-particle :id 3 :position #< 3 0 0> :velocity #<-1 0 0> :acceleration #<0 0 0>)))

;; Didn't even need this elaborate finction, but might as well keep it
;; now.
(defun move-particle (time particle)
  (if (zerop time)
      particle
      (with-slots (id position velocity acceleration) particle
        (let ((new-velocity (point:+ velocity
                                     (point:* acceleration time)))
              (new-position (point:+ position
                                     (point:* velocity time)
                                     (point:* acceleration (/ (+ time (expt time 2)) 2)))))
          (make-particle :id id
                         :position new-position
                         :velocity new-velocity
                         :acceleration acceleration)))))

(defun move-particles (time particles)
  (fset:image (alexandria:curry #'move-particle time) particles))

(defun remove-collisions (particles)
  (let ((positions (fset:reduce (lambda (position-map particle)
                                  (aoc:map-set-with position-map
                                                    (particle-position particle)
                                                    particle))
                                particles
                                :initial-value (fset:empty-map (fset:empty-set)))))
    (fset:reduce (lambda (result position current-particles)
                   (declare (ignore position))
                   (if (= 1 (fset:size current-particles))
                       (fset:union result current-particles)
                       result))
                 positions
                 :initial-value (fset:empty-set))))

(defun simulate (particles ticks)
  (if (zerop ticks)
      particles
      (simulate (remove-collisions (move-particles 1 particles))
                (1- ticks))))

;; To get the answer, we need to simulate the particles until there are no
;; more collisions.  I faked this by running the simulation for 1000
;; ticks, which took 30 seconds, and submitting the answer based on that.
;; Since I was right, I stopped there.  (I actually had the right answer
;; avter 100 ticks, which took about 3 seconds.)
;;
;; Doing this properly would require looking at all of the particles'
;; vectors and ruling out the possibility of more collisions.  Doable, but
;; I'll only bother if I'm bored later and want to write general code for
;; this historic problem.
