(in-package :aoc-2017-14)

(aoc:define-day 8216 1139)


;;; Input

(defparameter *key* (aoc:input))
(defparameter *example* "flqrgnkx")


;;; Part 1

(defun octet-coordinates (row col octet)
  "Returns a set of coordinates corresponding to the one bits in OCTET,
  assuming OCTET's MSB is at #<COL ROW>."
  (cond
    ((zerop octet)
     (fset:empty-set))
    ((plusp (logand octet 1))
     (fset:with (octet-coordinates row (1- col) (ash octet -1))
                (point:make-point (+ 7 col) row)))
    (t
     (octet-coordinates row (1- col) (ash octet -1)))))

(defun row-coordinates (row string)
  (reduce #'fset:union
          (mapcar (alexandria:curry #'octet-coordinates row)
                  (alexandria:iota 16 :step 8)
                  (aoc-2017-10::knot-hash string))))

(defun grid-coordinates (key)
  (reduce #'fset:union
          (mapcar (lambda (i)
                    (row-coordinates i (format nil "~A-~A" key i)))
                  (alexandria:iota 128))))

(defun get-answer-1 (&optional (key *key*))
  (fset:size (grid-coordinates key)))

(aoc:given 1
  (= 8108 (get-answer-1 *example*)))


;;; Part 2

(defun group-neighbors (grid cell &optional (neighbors (fset:empty-set)))
  (fset:reduce (lambda (new-neighbors neighbor)
                 (if (fset:lookup new-neighbors neighbor)
                     new-neighbors
                     (group-neighbors grid neighbor new-neighbors)))
               (fset:intersection grid (point:neighbor-set cell))
               :initial-value (fset:with neighbors cell)))

(defun group-all-neighbors (grid &optional (groups (fset:empty-set)))
  (if (fset:empty? grid)
      groups
      (let ((new-group (group-neighbors grid (fset:arb grid))))
        (group-all-neighbors (fset:set-difference grid new-group)
                             (fset:with groups new-group)))))

(defun get-answer-2 (&optional (key *key*))
  (fset:size (group-all-neighbors (grid-coordinates key))))

(aoc:given 2
  (= 1242 (get-answer-2 *example*)))
