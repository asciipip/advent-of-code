(in-package :aoc-2017-15)

(aoc:define-day 577 316)


;;; Input

(defparameter *starting-values* (mapcar #'aoc:extract-ints (aoc:input)))
(defparameter *example* '(65 8921))


;;; Part 1

(defparameter *factors* '(16807 48271))
(defparameter *iterations* 40000000)

(defun make-generator (factor starting-value)
  (let ((last-value starting-value))
    (lambda ()
      (setf last-value (mod (* last-value factor)
                            2147483647)))))

(defun make-generators (starting-values)
  (mapcar #'make-generator *factors* starting-values))

(defun generators-match-p (generators)
  (apply #'=
         (mapcar (lambda (generator)
                   (logand (funcall generator)
                           #b1111111111111111))
                 generators)))

(defun count-matches (generators iterations &optional (result 0))
  (if (zerop iterations)
      result
      (count-matches generators
                     (1- iterations)
                     (if (generators-match-p generators)
                         (1+ result)
                         result))))

(defun get-answer-1 (&optional (starting-values *starting-values*))
  (count-matches (make-generators starting-values) *iterations*))

(aoc:given 1
  (= 588 (get-answer-1 *example*)))


;;; Part 2

(defparameter *selectors* '(4 8))
(defparameter *picky-iterations* 5000000)

(defun make-picky-generator (selector factor starting-value)
  (let ((base-generator (make-generator factor starting-value)))
    (lambda ()
      (do ((val (funcall base-generator)
                (funcall base-generator)))
          ((zerop (mod val selector)) val)))))

(defun make-picky-generators (starting-values)
  (mapcar #'make-picky-generator *selectors* *factors* starting-values))

(defun get-answer-2 (&optional (starting-values *starting-values*))
  (count-matches (make-picky-generators starting-values) *picky-iterations*))

(aoc:given 2
  (= 309 (get-answer-2 *example*)))
