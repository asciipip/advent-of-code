(in-package :aoc-2017-04)

(aoc:define-day 325 119)


;;; Input

(defparameter *passphrases* (aoc:input))


;;; Part 1

(defparameter *part2* nil)

(defun passphrase-to-bag (passphrase)
  (fset:convert 'fset:bag
                (if *part2*
                    (mapcar (alexandria:curry #'fset:convert 'fset:bag)
                            (ppcre:split " " passphrase))
                    (ppcre:split " " passphrase))))

(defun passphrase-valid-p (passphrase)
  (let ((pbag (passphrase-to-bag passphrase)))
    (= (fset:size pbag) (fset:set-size pbag))))

(defun get-answer-1 (&optional (passphrases *passphrases*))
  (reduce (lambda (count passphrase)
            (if (passphrase-valid-p passphrase)
                (1+ count)
                count))
          passphrases
          :initial-value 0))


;;; Part 2

(defun get-answer-2 (&optional (passphrases *passphrases*))
  (let ((*part2* t))
    (get-answer-1 passphrases)))
