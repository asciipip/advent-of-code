(in-package :aoc-2017-08)

(aoc:define-day 5143 6209)


;;; Data Structures

(defstruct instruction
  register
  delta
  test-register
  test-fn)


;;; Parsing

(parseq:defrule instruction ()
    (and register " " delta " if " register " " test-fn)
  (:choose 0 2 4 6)
  (:lambda (register delta test-register test-fn)
    (make-instruction :register register
                      :delta delta
                      :test-register test-register
                      :test-fn test-fn)))

(parseq:defrule register ()
    (+ (char "a-z"))
  (:string)
  (:function #'string-upcase)
  (:function #'intern))

(parseq:defrule delta ()
    (and (or "inc" "dec") " " aoc:integer-string)
  (:choose 0 2)
  (:lambda (dir value)
    (alexandria:eswitch (dir)
      ("inc" value)
      ("dec" (- value)))))

(parseq:defrule test-fn ()
    (and (rep (1 2) (char "<>=!"))
         " "
         aoc:integer-string)
  (:choose 0 2)
  (:lambda (op value)
    (let ((fn (alexandria:eswitch ((coerce op 'string) :test #'string=)
                ("<" #'<)
                (">" #'>)
                ("<=" #'<=)
                (">=" #'>=)
                ("==" #'=)
                ("!=" #'/=))))
      (lambda (register)
        (funcall fn register value)))))


;;; Input

(defparameter *instructions* (aoc:input :parse-line 'instruction))
(defparameter *example-instructions*
  (mapcar (alexandria:curry #'parseq:parseq 'instruction)
          '("b inc 5 if a > 1"
            "a inc 1 if b < 5"
            "c dec -10 if a >= 1"
            "c inc -20 if c == 10")))


;;; Part 1

(defparameter *max-register-value* 0)

(defun process-instruction (registers instruction)
  (when (and (plusp (fset:size registers))
             (< *max-register-value* (max-register-value registers)))
    (setf *max-register-value* (max-register-value registers)))
  (with-slots (register delta test-register test-fn) instruction
    (if (funcall test-fn (fset:lookup registers test-register))
        (fset:with registers
                   register
                   (+ (fset:lookup registers register)
                      delta))
        registers)))

(defun process-instructions (instructions)
  (fset:reduce #'process-instruction instructions
               :initial-value (fset:empty-map 0)))

(defun max-register-value (registers)
  (fset:greatest (fset:range registers)))

(defun get-answer-1 (&optional (instructions *instructions*))
  (max-register-value (process-instructions instructions)))


;;; Part 2

(defun get-answer-2 (&optional (instructions *instructions*))
  (let ((*max-register-value* 0))
    (get-answer-1 instructions)
    *max-register-value*))
