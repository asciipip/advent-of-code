(in-package :aoc-2017-05)

;; Note: part 2 takes about 10 seconds
(aoc:define-day 376976 29227751)


;;; Input

(defparameter *offsets*
  (fset:convert 'fset:seq (aoc:input :parse-line 'aoc:integer-string)))
(defparameter *example-offsets*
  (fset:seq 0 3 0 1 -3))


;;; Part 1

(defparameter *part2* nil)

(defun jump (offsets position)
  "Returns two values: the new offsets and the new position."
  (let* ((offset (fset:lookup offsets position))
         (new-offsets (fset:with offsets position (if (and *part2* (<= 3 offset))
                                                      (1- offset)
                                                      (1+ offset))))
         (new-position (+ position offset)))
    (values new-offsets
            (if (fset:lookup offsets new-position)
                new-position
                nil))))

(defun jump-to-end (offsets &optional (position 0) (steps 0))
  "Returns two values: the number of steps to get to the end, and the final
  list of jump offsets."
  (multiple-value-bind (new-offsets new-position)
      (jump offsets position)
    (if new-position
        (jump-to-end new-offsets new-position (1+ steps))
        (values (1+ steps) new-offsets))))

(defun get-answer-1 (&optional (offsets *offsets*))
  (nth-value 0 (jump-to-end offsets)))


;;; Part 2

(defun get-answer-2 (&optional (offsets *offsets*))
  (let ((*part2* t))
    (nth-value 0 (jump-to-end offsets))))
