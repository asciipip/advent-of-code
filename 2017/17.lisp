(in-package :aoc-2017-17)

(aoc:define-day 1244 11162912)


;;; Input

(defparameter *steps* (parse-integer (aoc:input)))
(defparameter *example* 3)


;;; Part 1

(defparameter *initial-buffer* (fset:seq 0))

(defun insert-value (steps buffer current-position value)
  (let ((insert-pos (1+ (mod (+ current-position steps)
                             (fset:size buffer)))))
    (values (fset:insert buffer insert-pos value)
            insert-pos)))

(defun build-buffer (steps iterations &optional (buffer *initial-buffer*) (position 0) (start 1))
  (if (zerop iterations)
      (values buffer position)
      (multiple-value-bind (new-buffer new-position)
          (insert-value steps buffer position start)
        (build-buffer steps (1- iterations) new-buffer new-position (1+ start)))))

(defun get-answer-1 (&optional (steps *steps*))
  (multiple-value-bind (full-buffer position) (build-buffer steps 2017)
    (fset:lookup full-buffer (mod (1+ position) (fset:size full-buffer)))))

(aoc:given 1
  (= 638 (get-answer-1 *example*)))


;;; Part 2
;;;
;;; Can't build the full buffer; it's too big.

(defun insert-value-2 (steps buffer-size position)
  "Returns the position at which the value would have been inserted."
  (1+ (mod (+ position steps) buffer-size)))

(defun build-buffer-2 (steps iterations &optional (buffer-size 1) (position 0) (step-number 0) second-element)
  "Returns the second element of the buffer after ITERATIONS insertions."
  (if (zerop iterations)
      (if (= 1 position)
          step-number
          second-element)
      (build-buffer-2 steps
                      (1- iterations)
                      (1+ buffer-size)
                      (insert-value-2 steps buffer-size position)
                      (1+ step-number)
                      (if (= 1 position)
                          step-number
                          second-element))))

(defun get-answer-2 (&optional (steps *steps*))
  (build-buffer-2 steps 50000000))
