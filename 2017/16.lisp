(in-package :aoc-2017-16)

(aoc:define-day "fgmobeaijhdpkcln" "lgmkacfjbopednhi")


;;; Parsing

(defun char-to-symbol (char)
  (intern (string-upcase (format nil "~A" char))))

(parseq:defrule dance ()
    (aoc:comma-list move))

(parseq:defrule move ()
    (or spin exchange partner))

(parseq:defrule spin ()
    (and "s" aoc:integer-string)
  (:choose 1)
  (:lambda (x) (list :spin x)))

(parseq:defrule exchange ()
    (and "x" aoc:integer-string "/" aoc:integer-string)
  (:choose 1 3)
  (:lambda (a b) (list :exchange a b)))

(parseq:defrule partner ()
    (and "p" char "/" char)
  (:choose 1 3)
  (:lambda (a b)
    (list :partner (char-to-symbol a) (char-to-symbol b))))


;;; Input

(defparameter *dance* (aoc:input :parse-line 'dance))
(defparameter *example* (parseq:parseq 'dance "s1,x3/4,pe/b"))


;;; Part 1

(defparameter *starting-position*
  (fset:convert 'fset:seq
                (mapcar (lambda (c)
                          (char-to-symbol (code-char c)))
                        (alexandria:iota 16 :start 97))))
(defparameter *example-starting-position*
  (fset:convert 'fset:seq
                (mapcar (lambda (c)
                          (char-to-symbol (code-char c)))
                        (alexandria:iota 5 :start 97))))

(defun do-spin (step-count dancers)
  (let ((pivot (- (fset:size dancers) step-count)))
    (fset:concat (fset:subseq dancers pivot)
                 (fset:subseq dancers 0 pivot))))

(defun do-exchange (a b dancers)
  (fset:with (fset:with dancers a (fset:lookup dancers b))
             b (fset:lookup dancers a)))

(defun do-partner (a b dancers)
  (fset:with (fset:with dancers (fset:position b dancers) a)
             (fset:position a dancers) b))

(defun do-move (move dancers)
  (let ((move-fn (ecase (car move)
                   (:spin #'do-spin)
                   (:exchange #'do-exchange)
                   (:partner #'do-partner))))
    (funcall (apply #'alexandria:curry move-fn (cdr move)) dancers)))

(defun do-moves (moves dancers)
  (if (endp moves)
      dancers
      (do-moves (cdr moves)
                (do-move (car moves) dancers))))

(defun get-answer-1 (&optional (dance *dance*) (starting-position *starting-position*))
  (string-downcase (format nil "~{~A~}" (fset:convert 'list (do-moves dance starting-position)))))

(aoc:given 1
  (string= "baedc" (get-answer-1 *example* *example-starting-position*)))


;;; Part 2

(defun repeat-dance (dance iterations starting-positions)
  (labels ((dance-r (positions repetitions)
             (cond
               ((= repetitions iterations)
                positions)
               ((fset:equal? positions starting-positions)
                (dance-r (do-moves dance positions)
                         (1+ (- iterations (mod iterations repetitions)))))
               (t
                (dance-r (do-moves dance positions)
                         (1+ repetitions))))))
    (dance-r (do-moves dance starting-positions) 1)))

(defun get-answer-2 (&optional (dance *dance*) (starting-position *starting-position*))
  (string-downcase (format nil "~{~A~}" (fset:convert 'list (repeat-dance dance 1000000000 starting-position)))))
