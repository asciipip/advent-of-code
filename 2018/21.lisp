(in-package :aoc-2018-21)

(aoc:define-day 13522479 14626276)


;;;; Inputer

(defparameter *program* (elfcode:compile (aoc:input)))

#|

Code analysis time again!

;; ip register is 4
#ip 4

;; bani verification
 0: seti 123   0 2  
 1: bani   2 456 2  
 2: eqri   2  72 2  ; If 123 & 456 == 72 ...
 3: addr   2   4 4  ; ...skip line 4
 4: seti   0   0 4  ; GOTO line 1 (if 123 & 456 != 72)
 5: seti   0   0 2  ; re-zero #R2

;; Main loop
 6: bori   2    65536  5  ; #R5 = #R2 | 65536  (turn on bit 17)
 7: seti 5234604    6  2  ; #R2 = 5234604 (0b010011111101111110101100)

 8: bani   5      255  3      
 9: addr   2        3  2  ; #R2 += #R5 & 255  (lower eight bits of #R5)
10: bani   2 16777215  2  ; Keep the lower 24 bits of #R2
11: muli   2    65899  2  ; #R2 *= 65899
12: bani   2 16777215  2  ; Keep the lower 24 bits of #R2
13: gtir 256        5  3      
14: addr   3        4  4        
15: addi   4        1  4        
16: seti  27        2  4  ; GOTO line 28 (if #R5 < 256)

;; #R5 = #R5 >> 8
17: seti   0   0  3        
18: addi   3   1  1        
19: muli   1 256  1      
20: gtrr   1   5  1        
21: addr   1   4  4        
22: addi   4   1  4        
23: seti  25   6  4  ; GOTO line 26 (if (#R3 + 1) << 8 > #R5)
24: addi   3   1  3  ; #R3 += 1
25: seti  17   7  4  ; GOTO line 18
26: setr   3   4  5  ; #R5 = #R3
27: seti   7   8  4  ; GOTO line 8

;; Termination condition; if #R0 == #R2, we get to stop.
28: eqrr   2   0  3
29: addr   3   4  4
30: seti   5   6  4  ; GOTO line 6 (if #R0 != #R2)

|#


;;;; Part 1

;; This is pretty easy.  We want the program to terminate as soon as
;; possible.  It only terminates when #R0 is equal to #R2 on line 28.  So
;; let's just run the program until it gets to line 28, and then see
;; what's in register 2.
(defun get-answer-1 ()
  (handler-case
      (elfcode:execute *program* :breakpoints '(28))
    (elfcode:breakpoint (b)
      (svref (elfcode:breakpoint-registers b) 2))))


;;;; Part 2

;; It looks like the values in #R2 compared against #R0 are cyclic.  Let's
;; record all of them (until they start repeating) and return the last
;; non-unique one.
(defun get-answer-2 ()
  (let ((values nil))
    (handler-bind ((elfcode:breakpoint
                    #'(lambda (b)
                        (let ((new-value (svref (elfcode:breakpoint-registers b) 2)))
                          (if (member new-value values)
                              (invoke-restart :halt)
                              (progn
                                (push new-value values)
                                (invoke-restart :continue)))))))
      (elfcode:execute *program* :breakpoints '(28)))
    (car values)))
