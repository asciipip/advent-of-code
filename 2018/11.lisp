(in-package :aoc-2018-11)

(aoc:define-day #C(20 58) '(#C(233 268) 13))


;;; Input

(defparameter *serial-number* (aoc:extract-ints (aoc:input)))


;;; Summed-area Table

;; See https://en.wikipedia.org/wiki/Summed-area_table

(defun make-summed-area-table (array)
  (when (/= 2 (array-rank array))
    (error "ARRAY must be two dimensional."))
  (let ((table (make-array (array-dimensions array))))
    (iter (for x below (array-dimension array 0))
      (iter (for y below (array-dimension array 1))
        (setf (aref table x y)
              (+ (aref array x y)
                 (if (< 0 x)
                     (aref table (1- x) y)
                     0)
                 (if (< 0 y)
                     (aref table x (1- y))
                     0)
                 (if (and (< 0 x) (< 0 y))
                     (- (aref table (1- x) (1- y)))
                     0)))))
    table))

(defun summed-area-table-get-sum (table x y width height)
  (declare (type (integer 0) x y)
           (type (integer 1) width height))
  (let ((upper-x (+ x width -1))
        (upper-y (+ y height -1))
        (lower-x (1- x))
        (lower-y (1- y)))
    (when (or (<= (array-dimension table 0) upper-x)
              (<= (array-dimension table 1) upper-y))
      (error "Area out of range."))
    (+ (aref table upper-x upper-y)
       (- (if (not (minusp lower-x))
              (aref table lower-x upper-y)
              0))
       (- (if (not (minusp lower-y))
              (aref table upper-x lower-y)
              0))
       (if (and (not (minusp lower-x))
                (not (minusp lower-y)))
           (aref table lower-x lower-y)
           0))))


;;; Part 1

(defun calculate-power-level (coordinate serial-number)
  (let ((rack-id (+ 10 (realpart coordinate))))
    (- (mod (truncate (* (+ (* rack-id (imagpart coordinate))
                            serial-number)
                         rack-id)
                      100)
            10)
       5)))

(defun make-battery (dimensions serial-number)
  "Dimensions are (X Y)."
  (assert (= 2 (length dimensions)))
  (let ((battery (make-array (reverse dimensions))))
    (iter (for x from 0 below (first dimensions))
          (iter (for y from 0 below (second dimensions))
                (setf (aref battery y x)
                      (calculate-power-level (complex (1+ x) (1+ y)) serial-number))))
    battery))

(defun print-battery (battery &key top-left dimensions)
  (declare (type (or complex null) top-left)
           (type list dimensions))
  (let ((startx (if top-left
                    (1- (realpart top-left))
                    0))
        (starty (if top-left
                    (1- (imagpart top-left))
                    0)))
    (let ((endx (1- (if dimensions
                        (+ startx (first dimensions))
                        (array-dimension battery 1))))
          (endy (1- (if dimensions
                        (+ starty (second dimensions))
                        (array-dimension battery 0)))))
      (iter (for y from starty to endy)
            (iter (for x from startx to endx)
                  (format t "~2D " (aref battery y x)))
            (format t "~%")))))

(defun calc-fuel-sum (battery coordinate dimensions &optional summed-table)
  (let ((startx (1- (realpart coordinate)))
        (starty (1- (imagpart coordinate))))
    (if summed-table
        (summed-area-table-get-sum summed-table
                                   starty startx
                                   (second dimensions) (first dimensions))
        (iter outer
           (for x from startx below (+ startx (first dimensions)))
           (iter (for y from starty below (+ starty (second dimensions)))
                 (in outer
                     (summing (aref battery y x))))))))

(defun find-max-square (battery dimensions &optional summed-table)
  (values-list
   (iter outer
         (for x from 0 to (- (array-dimension battery 1) (first dimensions)))
         (iter (for y from 0 to (- (array-dimension battery 0) (second dimensions)))
               (for fuel-sum = (calc-fuel-sum battery
                                              (complex (1+ x) (1+ y))
                                              dimensions
                                              summed-table))
               (in outer
                   (finding (list (complex (1+ x) (1+ y)) fuel-sum)
                            maximizing fuel-sum))))))

(aoc:given 1
  (= 4 (calculate-power-level #C(3 5) 8))
  (= -5 (calculate-power-level #C(122 79) 57))
  (= 0 (calculate-power-level #C(217 196) 39))
  (= 4 (calculate-power-level #C(101 153) 71))
  (= #C(33 45) (find-max-square (make-battery '(300 300) 18) '(3 3)))
  (= #C(21 61) (find-max-square (make-battery '(300 300) 42) '(3 3))))

(defun get-answer-1 ()
  (find-max-square (make-battery '(300 300) *serial-number*) '(3 3)))


;;; Part 2

(defun find-max-square-and-size (battery)
  (iter (with summed-table = (make-summed-area-table battery))
        (for size from 1 to (apply #'min (array-dimensions battery)))
        (for (values coords fuel-sum) = (find-max-square battery (list size size) summed-table))
        (finding (list coords size) maximizing fuel-sum)))

(aoc:given 2
  (equal '(#C(90 269) 16) (find-max-square-and-size (make-battery '(300 300) 18)))
  (equal '(#C(232 251) 12) (find-max-square-and-size (make-battery '(300 300) 42))))

(defun get-answer-2 ()
  (find-max-square-and-size (make-battery '(300 300) *serial-number*)))
