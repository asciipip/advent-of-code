(in-package :aoc-2018-15)

;; part 2 attack is 40
(aoc:define-day 225096 35354)


;;;; Macros

(defmacro with-png-for-state ((state filename) &body body)
  (let ((map (gensym "MAP")))
    `(if *visualize*
         (with-slots ((,map map)) ,state
           (cairo:with-png-file (,filename
                                 :rgb24
                                 (* +square-side+ (array-dimension ,map 1))
                                 (* +square-side+ (array-dimension ,map 0)))
             (cairo:translate (/ +square-side+ 2) (/ +square-side+ 2))
             (cairo:scale +square-side+ +square-side+)
             ,@body))
         (progn
           ,@body))))


;;;; Basic classes

(defparameter *elf-attack* 3)

(defclass entity ()
  ((char :initarg :char
         :reader entity-char)
   (position :initarg :pos
             :reader entity-position)))

(defmethod print-object ((entity entity) stream)
  (with-slots (char) entity
    (format stream "#<ENTITY ~A>" char)))

(defclass wall (entity) ())

(defmethod print-object ((entity wall) stream)
  (with-slots (char) entity
    (format stream "#<WALL>")))

(defclass actor (entity)
  ((team :initarg :team
         :reader actor-team)
   (attack :initform 3
           :initarg :attack)
   (hp :initform 200
       :accessor actor-hp)))

(defmethod print-object ((entity actor) stream)
  (with-slots (char position hp) entity
    (format stream "#<ACTOR ~A(~A)=~A>" char position hp)))

(defun entity< (entity-1 entity-2)
  (aoc:row-order< (slot-value entity-1 'position)
                  (slot-value entity-2 'position)))

(defun entity-from-char (char position)
  (ecase char
    (#\# (make-instance 'wall :pos position :char #.(code-char #x2593)))
    (#\G (make-instance 'actor :pos position :char #\G :team :goblin))
    (#\E (make-instance 'actor :pos position :char #\E :team :elf :attack *elf-attack*))
    (#\. nil)))

(defstruct state
  map
  actors)

(defmethod print-object ((state state) stream)
  (with-slots (map) state
    (format stream "#<STATE~%")
    (iter (for y from 0 below (array-dimension map 0))
          (format stream "  ")
          (for (entities actors) =
               (iter (for x from 0 below (array-dimension map 1))
                     (for e = (aref map y x))
                     (collecting e into entities)
                     (when (typep e 'actor)
                       (collecting e into actors))
                     (finally (return (list entities actors)))))
          (format stream "  ~{~A~}  ~:{~A(~A)~:^,~}~%"
                  (mapcar (lambda (e) (if e (entity-char e) " ")) entities)
                  (mapcar (lambda (a) (list (entity-char a) (actor-hp a))) actors)))
    (format stream "  >")))

(defun parse-state (string-list)
  (let ((map (make-array (list (length string-list) (length (first string-list)))
                         :element-type '(or null entity)
                         :initial-element nil)))
    (iter outer
          (for string in string-list)
          (for y from 0)
          (iter (for char in-string string with-index x)
                (for entity = (entity-from-char char (complex x y)))
                (setf (aref map y x) entity)
                (when (typep entity 'actor)
                  (in outer (collecting entity into actors))))
          (finally (return-from outer (make-state :map map :actors actors))))))

(defun other-team (team)
  (if (eq team :elf)
      :goblin
      :elf))

(define-condition one-team-eliminated ()
  ())

;;;; Input

(defparameter *test-basic-layout*
  '("#######"
    "#.G.E.#"
    "#E.G.E#"
    "#.G.E.#"
    "#######"))
(defparameter *test-movement-1*
  '("#######"
    "#E..G.#"
    "#...#.#"
    "#.G.#G#"
    "#######"))
(defparameter *test-movement-2*
  '("#######"
    "#.E...#"
    "#.....#"
    "#...G.#"
    "#######"))
(defparameter *test-movement-3*
  '("#########"
    "#G..G..G#"
    "#.......#"
    "#.......#"
    "#G..E..G#"
    "#.......#"
    "#.......#"
    "#G..G..G#"
    "#########"))
(defparameter *test-combat*
  '("#######"
    "#.G...#"
    "#...EG#"
    "#.#.#G#"
    "#..G#E#"
    "#.....#"
    "#######"))
(defparameter *test-combat-36334*
  '("#######"
    "#G..#E#"
    "#E#E.E#"
    "#G.##.#"
    "#...#E#"
    "#...E.#"
    "#######"))
(defparameter *test-combat-39514*
  '("#######"
    "#E..EG#"
    "#.#G.E#"
    "#E.##E#"
    "#G..#.#"
    "#..E#.#"
    "#######"))
(defparameter *test-combat-27755*
  '("#######"
    "#E.G#.#"
    "#.#G..#"
    "#G.#.G#"
    "#G..#.#"
    "#...E.#"
    "#######"))
(defparameter *test-combat-28944*
  '("#######"
    "#.E...#"
    "#.#..G#"
    "#.###.#"
    "#E#G#G#"
    "#...#G#"
    "#######"))
(defparameter *test-combat-18740*
  '("#########"
    "#G......#"
    "#.E.#...#"
    "#..##..G#"
    "#...##..#"
    "#...#...#"
    "#.G...G.#"
    "#.....G.#"
    "#########"))

(defparameter *input* (aoc:input))

(defparameter *additional-movement-test*
  '("#######"
    "#.....#"
    "#.G...#"
    "#....E#"
    "#.....#"
    "#E....#"
    "#######"))

;;;; Part 1

(defun living-actors (state)
  "Returns dead actors as its second value."
  (values-list
   (iter (for actor in (state-actors state))
         (if (plusp (actor-hp actor))
             (collecting actor into alive)
             (collecting actor into dead))
         (finally (return (list alive dead))))))

(defun teams-present (state)
  (remove-duplicates (mapcar #'actor-team (living-actors state))))

;; Note: These are in reading order.
(defparameter *neighbor-vectors* '(#C(0 -1) #C(-1 0) #C(1 0) #C(0 1)))

(defun neighbor-coords (position)
  (mapcar (lambda (v) (+ position v)) *neighbor-vectors*))

(defun neighbor-actors (position state)
  (remove-if-not (lambda (e) (and (typep e 'actor) (plusp (actor-hp e))))
                 (mapcar (lambda (p) (aref (state-map state) (imagpart p) (realpart p)))
                         (neighbor-coords position))))

(defun empty-neighbors (position state)
  (remove-if (lambda (p)
               (aoc:cref (state-map state) p))
             (neighbor-coords position)))

(defun opponent-adjacent-p (my-team position state)
  (member (other-team my-team) (neighbor-actors position state) :key #'actor-team))

(defun weakest-adjacent-opponent (my-team position state)
  (first (sort (remove my-team (neighbor-actors position state) :key #'actor-team)
               #'<
               :key #'actor-hp)))

(defun nearest-targets (actor state)
  (with-slots (position team) actor
    (let ((positions-visited (make-hash-table)))
      (labels ((search-distance (candidates)
                 (iter (for candidate in candidates)
                       (draw-cell candidate :dark-secondary))
                 (cond
                   ((endp candidates)
                    nil)
                   ((member-if (lambda (c) (opponent-adjacent-p team c state)) candidates)
                    (remove-if-not (lambda (c) (opponent-adjacent-p team c state)) candidates))
                   (t
                    (iter (for candidate in candidates)
                          (setf (gethash candidate positions-visited) t))
                    (search-distance
                     (remove-if (lambda (c) (gethash c positions-visited))
                                (remove-duplicates
                                 (apply #'append (mapcar (lambda (c) (empty-neighbors c state))
                                                         candidates)))))))))
        (search-distance (empty-neighbors position state))))))

(defun first-step-to-target (source target state)
  (values-list
   (iter (for first-step in (empty-neighbors source state))
         (for (values path cost) =
              (aoc:shortest-path first-step
                                 (lambda (position)
                                   (mapcar (lambda (p) (list 1 p)) (empty-neighbors position state)))
                                 :end target
                                 :heuristic (lambda (position)
                                              (+ (abs (realpart (- target position)))
                                                 (abs (imagpart (- target position)))))))
         (when cost
           (finding (list first-step path) minimizing cost)))))

(defun actor-move! (actor state)
  (with-slots (team position) actor
    (if (opponent-adjacent-p team position state)
        (draw-actors state)
        (let ((target (first (sort (nearest-targets actor state) #'aoc:row-order<))))
          (if target
              (progn
                (draw-line position target :magenta 2)
                (let ((first-step (first-step-to-target position target state)))
                  (if first-step
                      (progn
                        (draw-line position first-step :blue 4)
                        (draw-actors state)
                        (setf (aref (state-map state) (imagpart position) (realpart position)) nil)
                        (setf position first-step)
                        (setf (aref (state-map state) (imagpart position) (realpart position)) actor))
                      (draw-actors state))))
              (draw-actors state))))))

(defun actor-combat! (actor state)
  (with-slots (team position attack) actor
    (let ((opponent (weakest-adjacent-opponent team position state)))
      (when opponent
        (draw-line position (entity-position opponent) :orange 4)
        (decf (actor-hp opponent) attack)
        (when (<= (actor-hp opponent) 0)
          (setf (aoc:cref (state-map state) (entity-position opponent)) nil))))))

(defun advance-round! (state round-count)
  (with-slots (actors map) state
    (iter (for actor in actors)
          (when (<= (actor-hp actor) 0)
            (next-iteration))
          (for actor-count from 0)
          (when (< (length (teams-present state)) 2)
            (signal 'one-team-eliminated))
          (with-png-for-state (state (format nil "2018-15-~2,'0D-~2,'0D.png" round-count actor-count))
            (draw-walls state)
            (draw-cell (entity-position actor) :light-highlight)
            (actor-move! actor state)
            (actor-combat! actor state)))
    (setf actors (sort (living-actors state) #'entity<))))

(defun determine-outcome (input)
  (let ((state (parse-state input)))
    (with-slots (actors) state
      (values-list
       (iter (for round-count from 0)
             (handler-case
                 (advance-round! state round-count)
               (one-team-eliminated ()
                 (setf (state-actors state) (living-actors state))
                 (return
                   (list (* round-count
                            (apply #'+ (mapcar #'actor-hp (living-actors state))))
                         round-count
                         state)))))))))

(aoc:given 1
  (= 27730 (determine-outcome *test-combat*))
  (= 36334 (determine-outcome *test-combat-36334*))
  (= 39514 (determine-outcome *test-combat-39514*))
  (= 27755 (determine-outcome *test-combat-27755*))
  (= 28944 (determine-outcome *test-combat-28944*))
  (= 18740 (determine-outcome *test-combat-18740*)))

(defun get-answer-1 ()
  (determine-outcome *input*))


;;;; Part 2

(defun get-answer-2 (&optional (input *input*))
  (labels ((elf-count (state)
             (count :elf (state-actors state) :key #'actor-team)))
    (values-list
     (iter (for attack from 3)
           (for initial-state = (let ((*elf-attack* attack)) (parse-state input)))
           (for (values outcome rounds final-state) =
                (let ((*elf-attack* attack)) (determine-outcome input)))
           (for initial-elves = (elf-count initial-state))
           (for final-elves = (elf-count final-state))
           (finding (list outcome attack rounds final-state)
                    such-that (= initial-elves final-elves))))))

(aoc:given 2
  (= 4988  (get-answer-2 *test-combat*))
  (= 31284 (get-answer-2 *test-combat-39514*))
  (= 3478  (get-answer-2 *test-combat-27755*))
  (= 6474  (get-answer-2 *test-combat-28944*))
  (= 1140  (get-answer-2 *test-combat-18740*)))


;;;; Visualization

(defparameter *visualize* nil)

(defconstant +square-side+ 32)

(defun draw-walls (state)
  (when *visualize*
    (with-slots (map) state
      (viz:set-color :dark-highlight)
      (cairo:paint)
      (iter (for y from 0 below (array-dimension map 0))
            (iter (for x from 0 below (array-dimension map 1))
                  (for entity = (aref map y x))
                  (for x-base = (* x +square-side+))
                  (for y-base = (* y +square-side+))
                  (when (typep entity 'wall)
                    (viz:set-color :dark-background)
                    (cairo:rectangle (- x 0.5) (- y 0.5) 1 1)
                    (cairo:fill-path)))))))

(defun draw-actors (state)
  (when *visualize*
    (with-slots (actors) state
      (iter (for actor in actors)
            (when (not (plusp (actor-hp actor)))
              (next-iteration))
            (for x = (realpart (entity-position actor)))
            (for y = (imagpart (entity-position actor)))
            (ecase (actor-team actor)
              (:goblin
               (viz:set-color :violet)
               (cairo:move-to (- x (* 0.25 (sqrt 2))) y)
               (cairo:line-to x (+ y (* 0.25 (sqrt 2))))
               (cairo:line-to (+ x (* 0.25 (sqrt 2))) y)
               (cairo:line-to x (- y (* 0.25 (sqrt 2))))
               (cairo:close-path)
               (cairo:fill-path))
              (:elf
               (viz:set-color :green)
               (cairo:rectangle (- x 0.25) (- y 0.25) 0.5 0.5)
               (cairo:fill-path)))
            (viz:set-color :light-background)
            (cairo:rectangle (- x 1/4) (- y 15/32) 1/2 2/32)
            (cairo:fill-path)
            (viz:set-color :red)
            (cairo:rectangle (- x 1/4) (- y 15/32) (* 1/2 (/ (actor-hp actor) 200)) 2/32)
            (cairo:fill-path)))))

(defun draw-line (start end color width)
  (when *visualize*
    (cairo:set-line-width (/ width +square-side+))
    (viz:set-color color)
    (cairo:move-to (realpart start) (imagpart start))
    (cairo:line-to (realpart end) (imagpart end))
    (cairo:stroke)))

(defun draw-cell (position color)
  (when *visualize*
    (viz:set-color color)
    (cairo:rectangle (- (realpart position) 0.5)
                     (- (imagpart position) 0.5)
                     1 1)
    (cairo:fill-path)))
