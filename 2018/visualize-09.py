#!/usr/bin/env python3

import argparse
import cmath
import collections
import math

import cairo
import numpy as np
from tqdm import tqdm

IMAGE_WIDTH = 1024
IMAGE_BORDER = 16
MARBLE_RADIUS = 8
MARBLE_BORDER = 1
INITIAL_RADIUS = (IMAGE_WIDTH - 2 * IMAGE_BORDER - 2 * MARBLE_RADIUS) / 2
NEWEST_MARBLE_COLOR = np.array([204, 236, 230]) / 255
OLDEST_MARBLE_COLOR = np.array([0, 68, 27]) / 255

def render_board(board, frames):
    max_n = max([m[0] for m in board])

    min_phi = min([(board[i][1] - board[i-1][1]) % (2 * cmath.pi)
                   for i in range(len(board))])

    if min_phi >= cmath.pi / 4 or min_phi == 0:
        radius = INITIAL_RADIUS
    else:
        ideal_radius = MARBLE_RADIUS / math.sin(min_phi / 2)
        radius = max(INITIAL_RADIUS, ideal_radius)

    surface = cairo.ImageSurface(cairo.FORMAT_RGB24, IMAGE_WIDTH, IMAGE_WIDTH)

    ctx = cairo.Context(surface)
    ctx.set_source_rgb(1, 1, 1)
    ctx.rectangle(0, 0, IMAGE_WIDTH, IMAGE_WIDTH)
    ctx.fill()

    ctx.translate(IMAGE_WIDTH / 2, IMAGE_WIDTH / 2)
    ctx.scale(INITIAL_RADIUS / radius, INITIAL_RADIUS / radius)

    # Draw indicator
    ctx.set_line_width(2.0 * radius / INITIAL_RADIUS)
    ctx.set_source_rgb(0.9, 0.1, 0.1)
    n, phi = board[-1]
    coords = cmath.rect(radius - 3 * MARBLE_RADIUS, phi)
    ctx.move_to(coords.real, coords.imag)
    coords = cmath.rect(radius + 3 * MARBLE_RADIUS, phi)
    ctx.line_to(coords.real, coords.imag)
    ctx.stroke()
    
    for (n, phi) in board:
        if max_n == 0:
            rgb = NEWEST_MARBLE_COLOR
        else:
            rgb = (NEWEST_MARBLE_COLOR - OLDEST_MARBLE_COLOR) * n / max_n \
                  + OLDEST_MARBLE_COLOR
        coords = cmath.rect(radius, phi)
        ctx.set_source_rgb(0, 0, 0)
        ctx.arc(coords.real, coords.imag, MARBLE_RADIUS + MARBLE_BORDER, 0, 2*cmath.pi)
        ctx.fill()
        ctx.set_source_rgb(*list(rgb))
        ctx.arc(coords.real, coords.imag, MARBLE_RADIUS, 0, 2*cmath.pi)
        ctx.fill()

    for frame in frames:
        surface.write_to_png('/tmp/v/{:05d}.png'.format(frame))
        

def play_marble(board, marble):
    if marble % 23 == 0:
        board.rotate(7)
        board.pop()
        board.rotate(-1)
    else:
        board.rotate(-1)
        mln, mlp = board[-1]
        mrn, mrp = board[0]
        if mrn == 0:
            mrp = 2 * cmath.pi
        phi = (mlp + mrp) / 2
        board.append((marble, phi))

parser = argparse.ArgumentParser()
parser.add_argument('-n', type=int, default=645)
parser.add_argument('-f', '--fps', type=int, default=60)
args = parser.parse_args()

board = collections.deque([(0, 0)])

max_fpb = int(args.fps / 4)

frame = 0
frames = range(frame, frame + int(max_fpb / 4))
render_board(board, frames)
frame = 1 + max(frames)
for marble in tqdm(range(1, args.n)):
    play_marble(board, marble)
    frames = range(frame, frame + max(1, int(max_fpb * (1 - math.log(marble, args.n)))))
    render_board(board, frames)
    frame = 1 + max(frames)

frames = range(frame, frame + 3 * args.fps)
render_board(board, frames)
