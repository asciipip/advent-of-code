(in-package :aoc-2018-14)

(aoc:define-day #(9 4 1 1 1 3 7 1 3 3) 20317612)


;;;; Input

(defparameter *threshold* (aoc:extract-ints (aoc:input)))


;;;; Part 1

;;; We're working with mutable state today!

(defstruct state
  (scoreboard (make-array 2 :element-type '(integer 0 9)
                          :initial-contents '(3 7)
                          :adjustable t
                          :fill-pointer t))
  (elves (list 0 1)))

(defun print-state (state)
  (with-slots (scoreboard elves) state
    (iter (for recipe in-vector scoreboard with-index i)
          (format t
                  (cond
                    ((= i (first elves))
                     "(~A) ")
                    ((= i (second elves))
                     "[~A] ")
                    (t
                     "~A "))
                  recipe))
    (format t "~%")))

(defun combine-recipes (state)
  (with-slots (scoreboard elves) state
    (let ((new-recipe (+ (aref scoreboard (first elves))
                         (aref scoreboard (second elves)))))
      (if (< new-recipe 10)
          (vector-push-extend new-recipe scoreboard)
          (multiple-value-bind (new-recipe-1 new-recipe-2)
              (truncate new-recipe 10)
            (vector-push-extend new-recipe-1 scoreboard)
            (vector-push-extend new-recipe-2 scoreboard)))
      (setf elves
            (mapcar (lambda (pos)
                      (mod (+ pos 1 (aref scoreboard pos)) (length scoreboard)))
                    elves))))
  state)

(defun make-recipes (state recipes-to-make)
  (if (<= recipes-to-make (length (state-scoreboard state)))
      state
      (make-recipes (combine-recipes state) recipes-to-make)))

(defun recipes-after (n &optional (count 10))
  (let ((state (make-recipes (make-state) (+ n count))))
    (subseq (state-scoreboard state) n (+ n count))))

(aoc:given 1
  (equalp #(5 1 5 8 9 1 6 7 7 9) (recipes-after 9))
  (equalp #(0 1 2 4 5 1 5 8 9 1) (recipes-after 5))
  (equalp #(9 2 5 1 0 7 1 0 8 5) (recipes-after 18))
  (equalp #(5 9 4 1 4 2 9 8 8 2) (recipes-after 2018)))

(defun get-answer-1 ()
  (recipes-after *threshold*))


;;;; Part 2

(defun find-recipes-before (pattern state)
  (with-slots (scoreboard) state
    (if (and (< (length pattern) (length scoreboard))
             (or (equalp pattern (subseq scoreboard (- (length scoreboard) (length pattern))))
                 (equalp pattern (subseq scoreboard (- (length scoreboard) (length pattern) 1) (1- (length scoreboard))))))
        (subseq (state-scoreboard state) 0 (search pattern scoreboard))
        (find-recipes-before pattern (combine-recipes state)))))

(defun get-answer-2 (&optional (threshold *threshold*))
  (length (find-recipes-before (if (integerp threshold)
                                   (aoc:digit-vector threshold)
                                   threshold)
                               (make-state))))

(aoc:given 2
  (= 9 (get-answer-2 51589))
  (= 5 (get-answer-2 #(0 1 2 4 5)))
  (= 18 (get-answer-2 92510))
  (= 2018 (get-answer-2 59414)))
