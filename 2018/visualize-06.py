#!/usr/bin/env python3

import argparse
import datetime
import re
from enum import Enum
from functools import reduce

import igraph
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

COLORS = np.array([
    (166, 206, 227),  # light blue
    (251, 154, 153),  # light red
    (253, 191, 111),  # light orange
    (202, 178, 214),  # light purple
    ( 31, 120, 180),  # blue
    (227,  26,  28),  # red
    (255, 127,   0),  # orange
    (106,  61, 154),  # purple
    ( 51, 160,  44),  # green
    (224, 224, 224),  # light yellow
    (  0,   0,   0),  # black
])
COLOR_COUNT = 4

LEAST_SAFE_COLOR = np.array([237, 248, 233])
MOST_SAFE_COLOR = np.array([0, 109, 44])
LEAST_DANGEROUS_COLOR = np.array([251, 106, 74])
MOST_DANGEROUS_COLOR = np.array([103, 0, 13])

parser = argparse.ArgumentParser()
parser.add_argument('--test', action='store_true')
args = parser.parse_args()

def distance(c1, c2):
    return abs(c1[0] - c2[0]) + abs(c1[1] - c2[1])

def closest_target(c, targets):
    ti = 0
    dist = distance(c, targets[ti])
    multiple = False
    for t in range(1, len(targets)):
        d = distance(c, targets[t])
        if d < dist:
            ti = t
            dist = d
            multiple = False
        elif d == dist:
            multiple = True
    if multiple:
        return -1
    else:
        return ti

if args.test:
    targets = np.array([(1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)])
    canvas = np.empty((10, 10), np.int8)
    distance_threshold = 32
else:
    target_list = []
    with open('/home/user/.cache/aoc/input.2018-06') as ipt:
        for line in ipt:
            xstr, ystr = line.split(', ')
            target_list.append((int(xstr), int(ystr)))

    targets = np.array(target_list)
    canvas = np.empty((400, 400), np.int8)
    distance_threshold = 10000

target_overlay = np.zeros((canvas.shape[0], canvas.shape[1], 4), np.uint8)
for t in targets:
    target_overlay[t[1], t[0]] = (0, 0, 0, 255)


for x in range(0, canvas.shape[1]):
    for y in range(0, canvas.shape[0]):
        canvas[y, x] = closest_target((x, y), targets)


def copy_graph_colors(dest, source):
    for v in source.vs:
        dest.vs.select(id=v['id'])['color_id'] = v['color_id']


def find_coloring(graph):
    if len(graph.vs) == 0:
        return graph
    min_degree = min(graph.degree())
    if min_degree < COLOR_COUNT:
        v_to_remove_i = graph.degree().index(min_degree)
        v_to_remove = graph.vs[v_to_remove_i]
        subgraph = graph.subgraph(set(range(len(graph.vs))) - set([v_to_remove_i]))
        colored_subgraph = find_coloring(subgraph)
        copy_graph_colors(graph, subgraph)
        available_colors = set(range(COLOR_COUNT)) - \
                           set([n['color_id'] for n in v_to_remove.neighbors()])
        v_to_remove['color_id'] = min(available_colors)
        return graph
    print(graph)
    assert False, 'Need a more robust coloring algorithm.'

g = igraph.Graph()
g.add_vertices(len(targets))
g.vs['id'] = range(len(targets))
for x in range(0, canvas.shape[1]):
    for y in range(0, canvas.shape[0]):
        this_cell = canvas[y, x]
        if this_cell < 0:
            continue
        for delta in [1, -1, 1j, -1j]:
            pp = x + y * 1j + delta
            xp = int(pp.real)
            yp = int(pp.imag)
            if 0 <= xp and 0 <= yp and \
               xp < canvas.shape[1] and yp < canvas.shape[0]:
                other_cell = canvas[yp, xp]
                if other_cell < 0:
                    pp = x + y * 1j + delta * 2
                    xp = int(pp.real)
                    yp = int(pp.imag)
                    other_cell = canvas[yp, xp]
                if this_cell != other_cell and other_cell >= 0 \
                   and g.get_eid(this_cell, other_cell, directed=False, error=False) == -1:
                    g.add_edge(this_cell, other_cell)

g.vs['color_id'] = -1
internal_targets = reduce(np.setdiff1d,
                          [np.arange(len(targets)),
                           canvas[0], canvas[-1], canvas[:,0], canvas[:,-1]])
internal_cells = canvas.flatten()[np.in1d(canvas.flatten(), internal_targets)]
biggest_area = np.bincount(internal_cells).argmax()
g.vs[biggest_area]['color_id'] = len(COLORS) - 3 - 4
color_graph = find_coloring(g.subgraph(set(range(len(g.vs))) - set([biggest_area])))
copy_graph_colors(g, color_graph)

colored_canvas = np.empty((canvas.shape[0], canvas.shape[1], 3), np.uint8)
for x in range(0, canvas.shape[1]):
    for y in range(0, canvas.shape[0]):
        target_group = canvas[y, x]
        if target_group >= 0:
            color_id = g.vs[target_group]['color_id']
            if target_group in internal_cells:
                color_id += 4
            colored_canvas[y, x] = COLORS[color_id]
        elif target_group == -1:  # boundary
            colored_canvas[y, x] = COLORS[-2]

patches = [
    mpatches.Patch(color=COLORS[0] / 255, label='Infinite'),
    mpatches.Patch(color=COLORS[4] / 255, label='Finite'),
    mpatches.Patch(color=COLORS[8] / 255, label='Largest Finite'),
    mpatches.Patch(color=COLORS[9] / 255, label='Equidistant'),
    mpatches.Patch(color=COLORS[1] / 255),
    mpatches.Patch(color=COLORS[5] / 255),
    mpatches.Patch(color=(0, 0, 0, 0)),
    mpatches.Patch(color=COLORS[2] / 255),
    mpatches.Patch(color=COLORS[6] / 255),
    mpatches.Patch(color=(0, 0, 0, 0)),
    mpatches.Patch(color=COLORS[3] / 255),
    mpatches.Patch(color=COLORS[7] / 255),
    mpatches.Patch(color=(0, 0, 0, 0)),
]

fig = plt.figure(figsize=(8.89, 8.89))
fig.add_axes((0.05, 0.04, 0.9, 0.9))
plt.title('Advent of Code 2018, Day 6, Part 1\nAssuming the Coordinates Are Dangerous')
plt.imshow(colored_canvas)
plt.imshow(target_overlay)
plt.legend(handles=patches, ncol=4, markerfirst=False, columnspacing=0)
plt.savefig('2018-06-1.png', dpi=100)


def total_distance(c, targets):
    return sum([distance(c, t) for t in targets])

distances = np.empty(canvas.shape, np.int32)
for x in range(0, canvas.shape[1]):
    for y in range(0, canvas.shape[0]):
        distances[y, x] = total_distance((x, y), targets)

max_distance = distances.max()
min_distance = distances.min()

# From 0 to min_distance; nothing uses these
colormap_ultrasafe = np.array([MOST_SAFE_COLOR] * min_distance).astype(np.uint8)
# From min_distance to distance_threshold
safe_bins = distance_threshold - min_distance
safe_vector = MOST_SAFE_COLOR - LEAST_SAFE_COLOR
colormap_safe = (np.expand_dims(np.linspace(1, 0, safe_bins), axis=1) * \
                 np.array([safe_vector] * safe_bins) + \
                 LEAST_SAFE_COLOR).astype(np.uint8)
# From distance_threshold to max_distance
dangerous_bins = max_distance - distance_threshold + 1
dangerous_vector = MOST_DANGEROUS_COLOR - LEAST_DANGEROUS_COLOR
colormap_dangerous = (np.expand_dims(np.linspace(0, 1, dangerous_bins), axis=1) * \
                      np.array([dangerous_vector] * dangerous_bins) + \
                      LEAST_DANGEROUS_COLOR).astype(np.uint8)
colormap = np.append(colormap_ultrasafe,
                     np.append(colormap_safe, colormap_dangerous, axis=0),
                     axis=0)

fig = plt.figure(figsize=(8.89, 8.89))
fig.add_axes((0.05, 0.04, 0.9, 0.9))
plt.title('Advent of Code 2018, Day 6, Part 2\nAssuming the Coordinates Are Safe')
plt.imshow(colormap[distances])
plt.imshow(target_overlay)
plt.savefig('2018-06-2.png', dpi=100)
