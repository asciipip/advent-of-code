(in-package :aoc-2018-01)

(aoc:define-day 486 69285)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Part 1

(defun calibrate-frequency (changes)
  (apply #'+ (mapcar #'parse-integer changes)))

(aoc:given 1
  (= 3 (calibrate-frequency '("+1" "-2" "+3" "+1")))
  (= 3 (calibrate-frequency '("+1" "+1" "+1")))
  (= 0 (calibrate-frequency '("+1" "+1" "-2")))
  (= -6 (calibrate-frequency '("-1" "-2" "-3"))))

(defun get-answer-1 ()
  (calibrate-frequency *input*))


;;;; Part 2

(defun find-duplicate-frequency (changes)
  (let ((seen (make-hash-table))
        (cycle-func (make-cycle-func changes)))
    (setf (gethash 0 seen) t)
    (values-list
     (iter (for frequency = (funcall cycle-func))
           (for iterations from 1)
           (finding (list frequency iterations) such-that (gethash frequency seen))
           (setf (gethash frequency seen) t)))))

(aoc:given 2
  (= 2 (find-duplicate-frequency '("+1" "-2" "+3" "+1")))
  (= 0 (find-duplicate-frequency '("+1" "-1")))
  (= 10 (find-duplicate-frequency '("+3" "+3" "+4" "-2" "-4")))
  (= 5 (find-duplicate-frequency '("-6" "+3" "+8" "+5" "-6")))
  (= 14 (find-duplicate-frequency '("+7" "+7" "-2" "-7" "-4"))))

(defun make-cycle-func (changes-str)
  (let ((changes (apply #'vector (mapcar #'parse-integer changes-str)))
        (i 0)
        (sum 0))
    (lambda ()
      (prog1
          (incf sum (svref changes i))
        (setf i (mod (1+ i) (length changes)))))))


(defun get-answer-2 ()
  (find-duplicate-frequency *input*))


;;;; Visualization

(defun max-total (change-strings iterations)
  (iter (with cycle-func = (make-cycle-func change-strings))
        (repeat iterations)
        (for frequency = (funcall cycle-func))
        (maximizing frequency)))

(defconstant +image-border+ 16)
(defconstant +max-height+ 1024)
(defconstant +max-width+ 1820)

(defun draw-frequency-changes (change-strings filename)
  (let ((changes (mapcar #'parse-integer change-strings)))
    (multiple-value-bind (duplicate-frequency iterations)
        (find-duplicate-frequency change-strings)
      (destructuring-bind (miny maxy)
          (iter (for change in changes)
                (maximizing change into maxy)
                (minimizing change into miny)
                (finally (return (list miny (max maxy (max-total change-strings iterations))))))
        (let ((height (if (< (- maxy miny) +max-height+)
                          (1+ (- maxy miny))
                          +max-height+))
              (y-scale (if (< (- maxy miny) +max-height+)
                           1
                           (/ +max-height+ (1+ (- maxy miny)))))
              (width (if (<= iterations +max-width+)
                         iterations
                         +max-width+))
              (x-scale (if (<= iterations +max-width+)
                           1
                           (/ +max-width+ iterations))))
          (cairo:with-png-file (filename
                                :rgb24
                                (+ (* 2 +image-border+) width)
                                (+ (* 2 +image-border+) height))
            (viz:set-color :light-background)
            (cairo:paint)
            (cairo:translate (+ +image-border+ 0.5) (+ +image-border+ 0.5))
            (viz:set-color :light-secondary)
            (cairo:set-line-width 1)
            (cairo:move-to 0 (* maxy y-scale))
            (cairo:line-to width (* maxy y-scale))
            (cairo:stroke)
            (iter (for x from 0 by (length changes))
                  (while (< (* x x-scale) width))
                  (cairo:move-to (* x x-scale) 0)
                  (cairo:line-to (* x x-scale) height)
                  (cairo:stroke))
            (viz:set-color :green)
            (iter (with change-clist = (clist:make-circular-list changes))
                  (repeat iterations)
                  (for change = (clist:focused change-clist))
                  (for y = (* (- maxy change) y-scale))
                  (for x from 0 by x-scale)
                  (cairo:arc x y 1 0 (* 2 pi))
                  (cairo:fill-path)
                  (clist:rotate change-clist 1))
            (viz:set-color :red)
            (cairo:set-line-width 1)
            (cairo:move-to 0 (* maxy y-scale))
            (iter (repeat iterations)
                  (with cycle-func = (make-cycle-func change-strings))
                  (for running-total = (funcall cycle-func))
                  (for x from 0 by x-scale)
                  (for y = (* (- maxy running-total) y-scale))
                  (cairo:line-to x y))
            (cairo:stroke)
            (viz:set-color :violet)
            (cairo:set-line-width 2)
            (iter (repeat iterations)
                  (with cycle-func = (make-cycle-func change-strings))
                  (for running-total = (funcall cycle-func))
                  (for x from 0 by x-scale)
                  (for y = (* (- maxy running-total) y-scale))
                  (when (= running-total duplicate-frequency)
                    (cairo:arc x y 4 0 (* 2 pi))
                    (cairo:stroke)))))))))
