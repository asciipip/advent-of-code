(in-package :aoc-2018-19)

(aoc:define-day 968 10557936)


;;;; Input

(defparameter *test-program*
  (elfcode:compile '("#ip 0"
                     "seti 5 0 1"
                     "seti 6 0 2"
                     "addi 0 1 0"
                     "addr 1 2 3"
                     "setr 1 0 0"
                     "seti 8 0 4"
                     "seti 9 0 5")))
(defparameter *program* (elfcode:compile (aoc:input)))


;;;; Part 1

(defun get-answer-1 ()
  (svref (elfcode:execute *program*) 0))


;;;; Part 2

#|

The program takes too long to run.  Let's try to reverse engineer it.

With comments:

#ip 3
 0: addi 3 16 3  ; Jump to line 17

;;; Main loop
 1: seti 1 8 1  ; Start here the first time
 2: seti 1 3 4  ; Start here every time #R4 > #R5
 3: mulr 1 4 2  ; Start here the rest of the time
 4: eqrr 2 5 2  ; If #R1 * #R4 == #R5 ...
 5: addr 2 3 3  ; ...skip line 6
 6: addi 3 1 3  ; skip line 7
 7: addr 1 0 0  ; Increment #R1 (only if #R1 * #R4 == #R5)
 8: addi 4 1 4  ; Increment #R4
 9: gtrr 4 5 2  ; If #R4 > #R5 ...
10: addr 3 2 3  ; ...skip line 11
11: seti 2 6 3  ; GOTO line 3 (only if #R4 <= #R5)
12: addi 1 1 1  ; Increment #R1
13: gtrr 1 5 2  ; If #R1 > #R5 ...
14: addr 2 3 3  ; ...skip line 15
15: seti 1 5 3  ; GOTO line 2 (only if #R1 <= #R5)
16: mulr 3 3 3  ; Terminate

;;; Initialize
;; #R5 = (2**2 * 19 * 11) + (5 * 22 + 21) = 967
17: addi 5 2 5
18: mulr 5 5 5
19: mulr 3 5 5
20: muli 5 11 5
21: addi 2 5 2
22: mulr 2 3 2
23: addi 2 21 2
24: addr 5 2 5
25: addr 3 0 3  ; Skip next part for part 2
26: seti 0 4 3  ; GOTO line 1 (if doing part 1)
;; #R5 = #R5(967) + (27 * 28 + 29) * 30 * 14 * 32 = 10551367
27: setr 3 1 2
28: mulr 2 3 2
29: addr 3 2 2
30: mulr 3 2 2
31: muli 2 14 2
32: mulr 2 3 2
33: addr 5 2 5

34: seti 0 3 0  ; Zero out #R0
35: seti 0 6 3  ; GOTO line 1; start calculating


In pseudocode:

reference = 10551367
count = 0
for i from 1 to reference:
  for j from 1 to reference:
    if i * j == reference:
      count += i


In other words, the output is the sum of the integer factors of the
reference number.

|#

(defun get-answer-2 ()
  (let ((reference 10551367))  ; Would be nice to calculate this from input
    (iter (for i from 1 to reference)
          (when (zerop (mod reference i))
            (summing i)))))
