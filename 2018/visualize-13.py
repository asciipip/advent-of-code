#!/usr/bin/env python3

import argparse
import collections
import cmath
import math
import subprocess

import cairocffi as cairo
import numpy as np
from tqdm import tqdm


TILE_WIDTH = 7
IMAGE_BORDER = 2 * TILE_WIDTH
TRAIL_ALPHA = 0.25
FPS = 60

COLORS = np.array([
    (251, 180, 174), # red
    (179, 205, 227), # blue
    (204, 235, 197), # green
    (222, 203, 228), # purple
    (254, 217, 166), # orange
    (255, 255, 204), # yellow
    (229, 216, 189), # brown
    (253, 218, 236), # pink
    (242, 242, 242), # grey
]) / 255


class Track:
    def __init__(self, segments, carts):
        self.segments = segments
        self.carts = carts
        self.wrecks = []
        self.segment_surface = None

    def __str__(self):
        str_array = np.frompyfunc(str, 1, 1)(self.segments)
        for wreck in self.wrecks:
            str_array[int(wreck.position.imag), int(wreck.position.real)] = '░'
        for cart in self.carts:
            if cart.visible:
                str_array[int(cart.position.imag), int(cart.position.real)] = str(cart)
        return '\n'.join([
            ''.join([str(s) for s in row])
            for row in str_array])

    def _segment_at(self, coords):
        return self.segments[int(coords.imag), int(coords.real)]

    def draw_segment_surface(self):
        if self.segment_surface is not None:
            return

        self.segment_surface = cairo.ImageSurface(
            cairo.FORMAT_ARGB32,
            track.segments.shape[1] * TILE_WIDTH + IMAGE_BORDER * 2,
            track.segments.shape[0] * TILE_WIDTH + IMAGE_BORDER * 2)
        ctx = cairo.Context(self.segment_surface)
        
        ctx.set_source_rgb(0.2, 0.2, 0.2)
        ctx.set_line_width(1)
        for y in range(self.segments.shape[0]):
            for x in range(self.segments.shape[1]):
                self.segments[y, x].draw_segment(ctx)
        
    def draw(self, ffmpeg):
        if self.segment_surface is None:
            self.draw_segment_surface()
        segment_ctx = cairo.Context(self.segment_surface)
            
        surface = cairo.ImageSurface(cairo.FORMAT_RGB24,
                                     track.segments.shape[1] * TILE_WIDTH + IMAGE_BORDER * 2,
                                     track.segments.shape[0] * TILE_WIDTH + IMAGE_BORDER * 2)
        ctx = cairo.Context(surface)
        ctx.set_source_rgb(0, 0, 0)
        ctx.rectangle(0, 0, surface.get_width(), surface.get_height())
        ctx.fill()
        
        ctx.translate(IMAGE_BORDER, IMAGE_BORDER)

        # Wrecks
        for wreck in self.wrecks:
            if wreck.at_fault.color is not None:
                ctx.set_source_rgb(*wreck.at_fault.color)
            else:
                ctx.set_source_rgb(*COLORS[0])
            ctx.arc(wreck.position.real * TILE_WIDTH + TILE_WIDTH / 2,
                    wreck.position.imag * TILE_WIDTH + TILE_WIDTH / 2,
                    TILE_WIDTH / 2 * math.sqrt(2), 0, 2 * math.pi)
            ctx.fill()

        # Track
        for cart in self.carts:
            if cart.last_position is not None:
                self._segment_at(cart.last_position).draw_path(
                    segment_ctx, cart.position - cart.last_position, cart.color)
                self._segment_at(cart.position).draw_path(
                    segment_ctx, cart.last_position - cart.position, cart.color)
        ctx.set_source_surface(self.segment_surface)
        ctx.paint()

        # Carts
        for cart in self.carts:
            cart.draw(ctx)

        ffmpeg.stdin.write(surface.get_data())
        #surface.write_to_png(filename)
        
    def tick(self):
        for cart in self.carts:
            if not cart.visible:
                continue
            cart.last_position = cart.position
            cart.position += cart.direction
            for other_cart in self.carts:
                if cart.position == other_cart.position \
                   and other_cart.visible \
                   and other_cart is not cart:
                    cart.visible = False
                    other_cart.visible = False
                    self.wrecks.append(Wreck(cart, other_cart))
            self._segment_at(cart.position).rotate_cart(cart)
        self.carts = sorted([c for c in self.carts if c.visible],
                            key=lambda c: (c.position.imag, c.position.real))
            
            
class TrackSegment:
    @staticmethod
    def make_segment(char, position):
        if char in ['|', 'v', 'V', '^']:
            return VerticalTrackSegment(position)
        elif char in ['-', '>', '<']:
            return HorizontalTrackSegment(position)
        elif char == '/':
            return ReverseCurve(position)
        elif char == '\\':
            return ForwardCurve(position)
        elif char == '+':
            return Intersection(position)
        else:
            return EmptySegment(position)

    def __init__(self, position):
        self.char = '?'
        self.position = position

    def __str__(self):
        return self.char
    
    def __repr__(self):
        return '<Segment "{}" >'.format(self.char)

    def draw_segment(self, context):
        pass

    def draw_path(self, context, vector, color):
        pass
    
    def rotate_cart(self, cart):
        pass

    @property
    def x(self):
        return self.position.real * TILE_WIDTH
    
    @property
    def y(self):
        return self.position.imag * TILE_WIDTH
    
    @property
    def left_exit(self):
        return None
    
    @property
    def right_exit(self):
        return None
    
    @property
    def top_exit(self):
        return None
    
    @property
    def bottom_exit(self):
        return None
    
class EmptySegment(TrackSegment):
    def __init__(self, position):
        super().__init__(position)
        self.char = ' '

    @property
    def right_exit(self):
        return False
    
class StraightTrackSegment(TrackSegment):
    def draw_path(self, context, vector, color):
        context.save()
        context.set_source_rgba(*color, TRAIL_ALPHA)
        context.move_to(self.x + 3.5 + 3.5 * vector.real,
                        self.y + 3.5 + 3.5 * vector.imag)
        context.line_to(self.x + 3.5, self.y + 3.5)
        context.stroke()
        context.restore()
    
class HorizontalTrackSegment(StraightTrackSegment):
    def __init__(self, position):
        super().__init__(position)
        self.char = '═'

    def draw_segment(self, context):
        context.move_to(self.x + 0, self.y + 2.5)
        context.line_to(self.x + 7, self.y + 2.5)
        context.move_to(self.x + 0, self.y + 4.5)
        context.line_to(self.x + 7, self.y + 4.5)
        context.stroke()

    @property
    def left_exit(self):
        return True
    
    @property
    def right_exit(self):
        return True
    
    @property
    def top_exit(self):
        return False
    
    @property
    def bottom_exit(self):
        return False
    
class VerticalTrackSegment(StraightTrackSegment):
    def __init__(self, position):
        super().__init__(position)
        self.char = '║'

    def draw_segment(self, context):
        context.move_to(self.x + 2.5, self.y + 0)
        context.line_to(self.x + 2.5, self.y + 7)
        context.move_to(self.x + 4.5, self.y + 0)
        context.line_to(self.x + 4.5, self.y + 7)
        context.stroke()
        
    @property
    def left_exit(self):
        return False
    
    @property
    def right_exit(self):
        return False
    
    @property
    def top_exit(self):
        return True
    
    @property
    def bottom_exit(self):
        return True
    
class Curve(TrackSegment):
    pass

class ForwardCurve(Curve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '\\'
        
    def draw_segment(self, context):
        context.arc(self.x + 7, self.y, 2.5, cmath.pi / 2, cmath.pi)
        context.stroke()
        context.arc(self.x + 7, self.y, 4.5, cmath.pi / 2, cmath.pi)
        context.stroke()
        context.arc(self.x, self.y + 7, 2.5, cmath.pi * 3 / 2, 2 * cmath.pi)
        context.stroke()
        context.arc(self.x, self.y + 7, 4.5, cmath.pi * 3 / 2, 2 * cmath.pi)
        context.stroke()
        
    def draw_path(self, context, vector, color):
        context.save()
        context.set_source_rgba(*color, TRAIL_ALPHA)
        if cmath.phase(vector) > 0:
            # bottom left curve
            if vector == -1:
                context.arc(self.x, self.y + 7, 3.5, cmath.pi * 3 / 2, cmath.pi * 7 / 4)
            else:
                context.arc(self.x, self.y + 7, 3.5, cmath.pi * 7 / 4, cmath.pi * 2)
        else:
            # top right curve
            if vector == 1:
                context.arc(self.x + 7, self.y, 3.5, cmath.pi / 2, cmath.pi * 3 / 4)
            else:
                context.arc(self.x + 7, self.y, 3.5, cmath.pi * 3 / 4, cmath.pi)
        context.stroke()
        context.restore()
    
    def rotate_cart(self, cart):
        cart.direction = complex(cart.direction.imag, cart.direction.real)

class BottomLeftCurve(ForwardCurve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '╗'
        
    def draw_segment(self, context):
        context.arc(self.x, self.y + 7, 2.5, cmath.pi * 3 / 2, 2 * cmath.pi)
        context.stroke()
        context.arc(self.x, self.y + 7, 4.5, cmath.pi * 3 / 2, 2 * cmath.pi)
        context.stroke()
        
    @property
    def left_exit(self):
        return True
    
    @property
    def right_exit(self):
        return False
    
    @property
    def top_exit(self):
        return False
    
    @property
    def bottom_exit(self):
        return True
    
class TopRightCurve(ForwardCurve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '╚'
        
    def draw_segment(self, context):
        context.arc(self.x + 7, self.y, 2.5, cmath.pi / 2, cmath.pi)
        context.stroke()
        context.arc(self.x + 7, self.y, 4.5, cmath.pi / 2, cmath.pi)
        context.stroke()
        
    @property
    def left_exit(self):
        return False
    
    @property
    def right_exit(self):
        return True
    
    @property
    def top_exit(self):
        return True
    
    @property
    def bottom_exit(self):
        return False
    
class ReverseCurve(Curve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '/'

    def draw_segment(self, context):
        context.arc(self.x, self.y, 2.5, 0, cmath.pi / 2)
        context.stroke()
        context.arc(self.x, self.y, 4.5, 0, cmath.pi / 2)
        context.stroke()
        context.arc(self.x + 7, self.y + 7, 2.5, cmath.pi, cmath.pi * 3 / 2)
        context.stroke()
        context.arc(self.x + 7, self.y + 7, 4.5, cmath.pi, cmath.pi * 3 / 2)
        context.stroke()
        
    def draw_path(self, context, vector, color):
        context.save()
        context.set_source_rgba(*color, TRAIL_ALPHA)
        if vector.real >= 0 and vector.imag >= 0:
            # bottom right curve
            if vector == 1:
                context.arc(self.x + 7, self.y + 7, 3.5, cmath.pi * 5 / 4, cmath.pi * 6 / 4)
            else:
                context.arc(self.x + 7, self.y + 7, 3.5, cmath.pi * 4 / 4, cmath.pi * 5 / 4)
        else:
            # top left curve
            if vector == -1:
                context.arc(self.x, self.y, 3.5, cmath.pi / 1 / 4, cmath.pi * 2 / 4)
            else:
                context.arc(self.x, self.y, 3.5, cmath.pi * 0 / 4, cmath.pi * 1 / 4)
        context.stroke()
        context.restore()
    
    def rotate_cart(self, cart):
        cart.direction = - complex(cart.direction.imag, cart.direction.real)

class TopLeftCurve(ReverseCurve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '╝'
        
    def draw_segment(self, context):
        context.arc(self.x, self.y, 2.5, 0, cmath.pi / 2)
        context.stroke()
        context.arc(self.x, self.y, 4.5, 0, cmath.pi / 2)
        context.stroke()
        
    @property
    def left_exit(self):
        return True
    
    @property
    def right_exit(self):
        return False
    
    @property
    def top_exit(self):
        return True
    
    @property
    def bottom_exit(self):
        return False
    
class BottomRightCurve(ReverseCurve):
    def __init__(self, position):
        super().__init__(position)
        self.char = '╔'

    def draw_segment(self, context):
        context.arc(self.x + 7, self.y + 7, 2.5, cmath.pi, cmath.pi * 3 / 2)
        context.stroke()
        context.arc(self.x + 7, self.y + 7, 4.5, cmath.pi, cmath.pi * 3 / 2)
        context.stroke()
        
    @property
    def left_exit(self):
        return False
    
    @property
    def right_exit(self):
        return True
    
    @property
    def top_exit(self):
        return False
    
    @property
    def bottom_exit(self):
        return True
    
class Intersection(StraightTrackSegment):
    def __init__(self, position):
        super().__init__(position)
        self.char = "╬"

    def draw_segment(self, context):
        context.move_to(self.x + 0,   self.y + 2.5)
        context.line_to(self.x + 2.5, self.y + 2.5)
        context.line_to(self.x + 2.5, self.y + 0)
        context.move_to(self.x + 4.5, self.y + 0)
        context.line_to(self.x + 4.5, self.y + 2.5)
        context.line_to(self.x + 7,   self.y + 2.5)
        context.move_to(self.x + 7,   self.y + 4.5)
        context.line_to(self.x + 4.5, self.y + 4.5)
        context.line_to(self.x + 4.5, self.y + 7)
        context.move_to(self.x + 2.5, self.y + 7)
        context.line_to(self.x + 2.5, self.y + 4.5)
        context.line_to(self.x + 0,   self.y + 4.5)
        context.stroke()

    def rotate_cart(self, cart):
        cart.direction = cart.direction * cart.next_turn[0]
        cart.next_turn.rotate(-1)

    @property
    def left_exit(self):
        return True
    
    @property
    def right_exit(self):
        return True
    
    @property
    def top_exit(self):
        return True
    
    @property
    def bottom_exit(self):
        return True


class Cart:
    @classmethod
    def from_char(cls, cart_id, char, x, y):
        if char == '>':
            direction = 1
        elif char == '<':
            direction = -1
        elif char in ['v', 'V']:
            direction = 1j
        elif char == '^':
            direction = -1j
        else:
            return None
        return cls(x + y * 1j, direction, cart_id)
    
    def __init__(self, position, direction, cart_id, next_turn=None):
        self.last_position = None
        self.position = position
        self.direction = direction
        if next_turn is not None:
            self.next_turn = next_turn
        else:
            self.next_turn = collections.deque([-1j, 1, 1j])
        self.visible = True
        self.id = cart_id
        self.color = None

    def __repr__(self):
        return '<Cart(#{} {} {})>'.format(self.id, self.position, str(self))
    
    def __str__(self):
        if self.direction == 1:
            return '▶'
        elif self.direction == 1j:
            return '▼'
        elif self.direction == -1:
            return '◀'
        elif self.direction == -1j:
            return '▲'
        
    def draw(self, context):
        if not self.visible:
            return
        context.save()
        if self.color is not None:
            context.set_source_rgb(*self.color)
        else:
            context.set_source_rgb(*COLORS[-1])
        context.translate(self.position.real * TILE_WIDTH + TILE_WIDTH / 2,
                          self.position.imag * TILE_WIDTH + TILE_WIDTH / 2)
        context.rotate(cmath.phase(self.direction))
        context.move_to(- TILE_WIDTH / 2, - TILE_WIDTH / 2)
        context.line_to(TILE_WIDTH / 2, 0)
        context.line_to(- TILE_WIDTH / 2, TILE_WIDTH / 2)
        context.close_path()
        context.fill()
        context.restore()

class Wreck:
    def __init__(self, cart_at_fault, other_cart):
        self.at_fault = cart_at_fault
        self.other_cart = other_cart
        self.position = self.at_fault.position

    def __repr__(self):
        return '<Wreck({}x{}@{})>'.format(self.at_fault.id, self.other_cart.id, self.position)
            
def parse_track(segment_list, cart_colors=None):
    segments = np.empty([len(segment_list), len(segment_list[0])],
                        object)
    carts = []
    for y, string in enumerate(segment_list):
        for x, char in enumerate(string):
            segments[y, x] = TrackSegment.make_segment(char, complex(x, y))
            cart = Cart.from_char(len(carts), char, x, y)
            if cart is not None:
                carts.append(cart)
                if cart_colors is not None:
                    cart.color = cart_colors[cart.id]
    carts.sort(key=lambda c: (c.position.imag, c.position.real))
    for y in range(segments.shape[0]):
        for x in range(segments.shape[1]):
            s = segments[y, x]
            if isinstance(s, ForwardCurve):
                if 0 < y and segments[y-1, x].bottom_exit:
                    segments[y, x] = TopRightCurve(s.position)
                elif 0 < x and segments[y, x-1].right_exit:
                    segments[y, x] = BottomLeftCurve(s.position)
                elif y < segments.shape[0] and segments[y+1, x].top_exit:
                    segments[y, x] = BottomLeftCurve(s.position)
                elif x < segments.shape[1] and segments[y, x+1].left_exit:
                    segments[y, x] = TopRightCurve(s.position)
            elif isinstance(s, ReverseCurve):
                if 0 < y and segments[y-1, x].bottom_exit:
                    segments[y, x] = TopLeftCurve(s.position)
                elif 0 < x and segments[y, x-1].right_exit:
                    segments[y, x] = TopLeftCurve(s.position)
                elif y < segments.shape[0] and segments[y+1, x].top_exit:
                    segments[y, x] = BottomRightCurve(s.position)
                elif x < segments.shape[1] and segments[y, x+1].left_exit:
                    segments[y, x] = BottomRightCurve(s.position)
    return Track(segments, carts)

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--test', action='store_true')
args = parser.parse_args()

if args.test:
    segment_list = [
        '/->-\\        ',
        '|   |  /----\\',
        '| /-+--+-\\  |',
        '| | |  | v  |',
        '\\-+-/  \\-+--/',
        '  \\------/   ',
    ]
else:
    with open('/home/user/.cache/aoc/input.2018-13') as ipt:
        segment_list = [line for line in ipt]

track = parse_track(segment_list)
cart_colors = np.empty((len(track.carts), 3))
cart_colors[:] = COLORS[-1]
steps = 0
while len(track.carts) > 1:
    track.tick()
    steps += 1
for i, wreck in enumerate(track.wrecks):
    cart_colors[wreck.at_fault.id] = COLORS[i]
    cart_colors[wreck.other_cart.id] = COLORS[i]

track = parse_track(segment_list, cart_colors)
ffmpeg_command = [
    'ffmpeg',
    '-loglevel', 'error',
    '-nostats',
    '-nostdin',
    '-f', 'rawvideo',
    '-s', '{}x{}'.format(track.segments.shape[1] * TILE_WIDTH + IMAGE_BORDER * 2,
                         track.segments.shape[0] * TILE_WIDTH + IMAGE_BORDER * 2),
    '-pix_fmt', 'rgba',
    '-r', str(FPS),
    '-i', '-',
    '-an',
    '-y',
    '2018-13.mp4',
]
ffmpeg = subprocess.Popen(ffmpeg_command, stdin=subprocess.PIPE)
track.draw(ffmpeg)
for i in tqdm(range(steps)):
    track.tick()
    track.draw(ffmpeg)
for i in range(3 * FPS):
    track.draw(ffmpeg)
ffmpeg.stdin.close()
ffmpeg.wait()
