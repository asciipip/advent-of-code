(in-package :aoc-2018-25)

(aoc:define-day 386 nil)


;;;; Input

(defun parse-input (input)
  (mapcar (lambda (l) (apply #'point:make-point (aoc:extract-ints l))) input))

(defparameter *test-2*
  (parse-input '("0,0,0,0"
                 "3,0,0,0"
                 "0,3,0,0"
                 "0,0,3,0"
                 "0,0,0,3"
                 "0,0,0,6"
                 "9,0,0,0"
                 "12,0,0,0")))
(defparameter *test-4*
  (parse-input '("-1,2,2,0"
                 "0,0,2,-2"
                 "0,0,0,-2"
                 "-1,2,0,0"
                 "-2,-2,-2,2"
                 "3,0,2,-1"
                 "-1,3,2,2"
                 "-1,0,-1,0"
                 "0,2,1,-2"
                 "3,0,0,0")))
(defparameter *test-3*
  (parse-input '("1,-1,0,1"
                 "2,0,-1,0"
                 "3,2,-1,0"
                 "0,0,3,1"
                 "0,0,-1,-1"
                 "2,3,-2,0"
                 "-2,2,0,0"
                 "2,-2,0,-1"
                 "1,-1,0,-1"
                 "3,2,0,2")))
(defparameter *test-8*
  (parse-input '("1,-1,-1,-2"
                 "-2,-2,0,1"
                 "0,2,1,3"
                 "-2,3,-2,1"
                 "0,2,3,-2"
                 "-1,-1,1,-2"
                 "0,-2,-1,0"
                 "-2,2,3,-1"
                 "1,2,2,0"
                 "-1,-2,0,-2")))
(defparameter *points* (parse-input (aoc:input)))


;;;; Part 1

;;; Ripped from day 10.

(defparameter *adjacency-threshold* 3)

(defun find-adjacent-points (p points)
  (iter (for candidate in points)
        (when (<= (point:manhattan-distance p candidate)
                  *adjacency-threshold*)
          (collecting candidate))))

(defun find-grouped-points (seed points)
  (let ((candidates (find-adjacent-points seed points)))
    (if (endp candidates)
        (values (list seed) points)
        (values-list
         (iter (for candidate in candidates)
               (for (values subgroup remaining)
                    first (find-grouped-points candidate (remove candidate points
                                                                 :test #'point:=))
                    then (find-grouped-points candidate (remove candidate remaining
                                                                :test #'point:=)))
               (appending subgroup into result)
               (finally (return (list (cons seed result) remaining))))))))

(defun group-all-points (points)
  (multiple-value-bind (group1 remaining)
      (find-grouped-points (car points) (cdr points))
    (if (endp remaining)
        (list group1)
        (cons group1 (group-all-points remaining)))))

(defun get-answer-1 (&optional (points *points*))
  (length (group-all-points points)))

(aoc:given 1
  (= 2 (get-answer-1 *test-2*))
  (= 4 (get-answer-1 *test-4*))
  (= 3 (get-answer-1 *test-3*))
  (= 8 (get-answer-1 *test-8*)))
