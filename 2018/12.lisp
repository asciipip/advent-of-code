(in-package :aoc-2018-12)

(aoc:define-day 2736 3150000000905)


(defstruct state
  (offset 0)
  pots)


;;; Input

;; The rules are represented as a 32-element bit vector.  The pattern for
;; each element is the bit-pattern for the element's index.  Thus, the
;; rule represented by "...## => #" would have a 1 in the element with
;; index 3.
;;
;; This allows for efficient state processing.  Rather than grabbing
;; multiple subwindows across the state array, we just take the current
;; pattern, left-shift its bits, add in the next bit in the state vector,
;; and remove the top bit.  (i.e. "pattern << 1 | next-bit & 0b11111")

(defparameter *state-prefix-length* 15)
(defparameter *plant-char* #\#)
(defparameter *empty-char* #\.)

(defun parse-bit-vector (string one-char)
  (let ((bit-vector (make-array (length string)
                                :element-type 'bit
                                :initial-element 0)))
    (iter (for c in-string string with-index i)
          (when (char= c one-char)
            (setf (sbit bit-vector i) 1)))
    bit-vector))

(defun parse-initial-state (initial-state-string)
  (let* ((bit-string (subseq initial-state-string *state-prefix-length*))
         (first-plant (position *plant-char* bit-string))
         (last-plant (position *plant-char* bit-string :from-end t)))
    (make-state
     :offset (min 0 (- first-plant 4))
     :pots (parse-bit-vector
            (format nil "~{~A~}~A~{~A~}"
                    (make-list (max 0 (- 4 first-plant))
                               :initial-element *empty-char*)
                    bit-string
                    (make-list (max 0 (- last-plant (length bit-string) -5))
                               :initial-element *empty-char*))
            *plant-char*))))

(defun pattern-to-int (pattern)
  (if (zerop (length pattern))
      0
      (+ (ash (pattern-to-int (subseq pattern 0 (1- (length pattern))))
              1)
         (if (char= (schar pattern (1- (length pattern)))
                    *plant-char*)
             1
             0))))

(defun parse-rules (rule-string-list)
  (iter (with result = (make-array (expt 2 5) :element-type 'bit :initial-element 0))
        (for rule-string in rule-string-list)
        (for (pattern-string outcome-string) = (ppcre:split " => " rule-string))
        (when (char= (schar outcome-string 0) *plant-char*)
          (setf (sbit result (pattern-to-int pattern-string)) 1))
        (finally (return result))))

(defparameter *initial-state* (parse-initial-state (first (aoc:input))))
(defparameter *rules* (parse-rules (nthcdr 2 (aoc:input))))

(defparameter *test-initial-state*
  (parse-initial-state "initial state: #..#.#..##......###...###"))
(defparameter *test-rules* (parse-rules
                            '("...## => #"
                              "..#.. => #"
                              ".#... => #"
                              ".#.#. => #"
                              ".#.## => #"
                              ".##.. => #"
                              ".#### => #"
                              "#.#.# => #"
                              "#.### => #"
                              "##.#. => #"
                              "##.## => #"
                              "###.. => #"
                              "###.# => #"
                              "####. => #")))


;;; Part 1

(defun spread-plants-once (state rules)
  (with-slots ((current-offset offset) (current-pots pots)) state
    (let* ((first-plant (position 1 current-pots))
           (last-plant (position 1 current-pots :from-end t))
           (new-offset (- current-offset (- 6 first-plant)))
           (new-length (+ last-plant 7 (- current-offset new-offset)))
           (new-pots (make-array new-length :element-type 'bit :initial-element 0)))
      (iter (for bit in-vector current-pots
                 from first-plant to (+ last-plant 4)
                 with-index i)
            (for pattern first 1 then (logand (logior (ash pattern 1)
                                                      bit)
                                              #b11111))
            (when (plusp (sbit rules pattern))
              (setf (sbit new-pots (+ i -2 (- current-offset new-offset)))
                    1)))
      (make-state :offset new-offset :pots new-pots))))

(defun spread-plants (initial-state rules generations)
  (iter (for generation from 1 to generations)
        (for new-state first (spread-plants-once initial-state rules)
             then (spread-plants-once new-state rules))
        (for previous-state previous new-state)
        (when (and previous-state
                   (equal (state-pots new-state) (state-pots previous-state)))
          (leave (make-state
                  :offset (+ (state-offset new-state)
                             (* (- (state-offset new-state) (state-offset previous-state))
                                (- generations generation)))
                  :pots (state-pots new-state))))
        (finally (return new-state))))

(defun add-plant-numbers (state)
  (with-slots (offset pots) state
    (iter (for p in-vector pots with-index i)
          (when (= p 1)
            (summing (+ i offset))))))
(aoc:given 1
  (equal #*010000110000111110001111111000010100110
         (let ((state (spread-plants *test-initial-state* *test-rules* 20)))
           (subseq (state-pots state)
                   (- -3 (state-offset state))
                   (- 36 (state-offset state)))))
  (= 325 (add-plant-numbers (spread-plants *test-initial-state* *test-rules* 20))))

(defun get-answer-1 ()
  (add-plant-numbers (spread-plants *initial-state* *rules* 20)))


;;; Part 2

(defun get-answer-2 ()
  (add-plant-numbers (spread-plants *initial-state* *rules* 50000000000)))
