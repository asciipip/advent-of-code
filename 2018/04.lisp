(in-package :aoc-2018-04)

(aoc:define-day 118599 33949)


;;;; Input

(defparameter *test-input*
  '("[1518-11-01 00:00] Guard #10 begins shift"
    "[1518-11-01 00:05] falls asleep"
    "[1518-11-01 00:25] wakes up"
    "[1518-11-01 00:30] falls asleep"
    "[1518-11-01 00:55] wakes up"
    "[1518-11-01 23:58] Guard #99 begins shift"
    "[1518-11-02 00:40] falls asleep"
    "[1518-11-02 00:50] wakes up"
    "[1518-11-03 00:05] Guard #10 begins shift"
    "[1518-11-03 00:24] falls asleep"
    "[1518-11-03 00:29] wakes up"
    "[1518-11-04 00:02] Guard #99 begins shift"
    "[1518-11-04 00:36] falls asleep"
    "[1518-11-04 00:46] wakes up"
    "[1518-11-05 00:03] Guard #99 begins shift"
    "[1518-11-05 00:45] falls asleep"
    "[1518-11-05 00:55] wakes up"))

(defstruct event
  minute
  type
  guard-id)

(defun parse-event (event-str)
  (multiple-value-bind (match groups)
      (ppcre:scan-to-strings "^\\[(\\d+)-(\\d+)-(\\d+) (\\d+):(\\d+)\\] (.*)$"
                             event-str)
    (assert match)
    (let ((hour (parse-integer (svref groups 3)))
          (minute (parse-integer (svref groups 4)))
          (event-type
           (cond
             ((string= (svref groups 5) "falls asleep") :asleep)
             ((string= (svref groups 5) "wakes up")     :awake)
             (t :new-guard))))
      (make-event :minute (if (= hour 0)
                              minute
                              (- (- (* 24 60) (* hour 60) minute)))
                  :type event-type
                  :guard-id (if (eq event-type :new-guard)
                                (aoc:extract-ints (svref groups 5))
                                nil)))))

(defparameter *test-events* (mapcar #'parse-event *test-input*))
(defparameter *events* (mapcar #'parse-event (sort (aoc:input) #'string<)))


;;;; Part 1

(defun add-sleep-time (guard-id sleep-start sleep-end histogram)
  (when (not (nth-value 1 (gethash guard-id histogram)))
    (setf (gethash guard-id histogram)
          (make-array 60 :initial-element 0)))
  (let ((guard-times (gethash guard-id histogram)))
    (iter (for m from (max 0 sleep-start)
                 below (if (<= 0 sleep-end)
                           sleep-end
                           60))
          (incf (svref guard-times m)))))

(defun make-guard-histogram (events)
  (let ((histogram (make-hash-table))
        (current-guard nil)
        (asleep-at nil))
    (iter (for event in events)
          (cond
            ((eq (event-type event) :new-guard)
             (when asleep-at
               (add-sleep-time current-guard
                               asleep-at (event-minute event)
                               histogram))
             (psetf current-guard (event-guard-id event)
                    asleep-at nil))
            ((eq (event-type event) :asleep)
             (setf asleep-at (event-minute event)))
            ((eq (event-type event) :awake)
             (add-sleep-time current-guard
                             asleep-at (event-minute event)
                             histogram)
             (setf asleep-at nil))
            (t (assert nil
                       (event)
                       "Unknown event type: ~A" event))))
    histogram))

(defun guard-with-most-sleep (histogram)
  (iter (for (guard guard-times) in-hashtable histogram)
        (finding guard maximizing (reduce #'+ guard-times))))

(defun sleepiest-minute (guard-histogram)
  (iter (for count in-vector guard-histogram with-index i)
        (finding i maximizing count)))

(aoc:deftest given-1
  (let* ((histogram (make-guard-histogram *test-events*))
         (guard (guard-with-most-sleep histogram)))
    (5am:is (= 10 guard))
    (5am:is (= 24 (sleepiest-minute (gethash guard histogram))))))

(defun get-answer-1 ()
  (let* ((histogram (make-guard-histogram *events*))
         (guard (guard-with-most-sleep histogram)))
    (* guard (sleepiest-minute (gethash guard histogram)))))


;;;; Part 2

(defun guard-sleepiest-minute (histogram)
  (iter outer
        (for (guard guard-times) in-hashtable histogram)
        (iter (for times in-vector guard-times with-index minute)
              (in outer
                  (finding (list guard minute) maximizing times)))))

(aoc:deftest given-2
  (let ((histogram (make-guard-histogram *test-events*)))
    (destructuring-bind (guard minute)
        (guard-sleepiest-minute histogram)
      (5am:is (= 99 guard))
      (5am:is (= 45 minute)))))

(defun get-answer-2 ()
  (let ((histogram (make-guard-histogram *events*)))
    (apply #'* (guard-sleepiest-minute histogram))))


;;;; Visualization

(defconstant +image-border+ 16)
(defconstant +label-margin+ 8)
(defconstant +square-size+ 16)

(defun max-id-width (ids)
  (iter (for id in ids)
        (maximizing (nth-value 0 (viz:text-bbox id)))))

(defun draw-histogram (events filename)
  (let* ((histogram (make-guard-histogram events))
         (guards (sort (alexandria:hash-table-keys histogram) #'<))
         (id-width (truncate (max-id-width guards)))
         (minute-height (truncate (nth-value 3 (text-dims "0"))))
         (width (+ (* 2 +image-border+) id-width +label-margin+ (* 60 +square-size+)))
         (height (+ (* 2 +image-border+) minute-height +label-margin+ (* (hash-table-count histogram) +square-size+))))
    (destructuring-bind (min-times max-times)
        (iter outer
              (for (id guard-histogram) in-hashtable histogram)
              (iter (for times in-vector guard-histogram)
                    (in outer
                        (maximizing times into max-times)
                        (minimizing times into min-times)))
              (finally (return-from outer (list min-times max-times))))
      (let ((color-func (viz:make-color-func :GnBu min-times max-times)))
        (cairo:with-png-file (filename :rgb24 width height)
          (viz:set-color :light-background)
          (cairo:paint)
          (cairo:translate (+ +image-border+ id-width +label-margin+) +image-border+)
          (iter (for guard-id in guards)
                (for (values text-width text-height) = (text-dims guard-id))
                (for y from 0)
                (viz:set-color :light-primary)
                (viz:draw-text guard-id (- +label-margin+) (* (+ y 0.5) +square-size+)
                               :horizontal :right :vertical :middle)
                (iter (for times in-vector (gethash guard-id histogram) with-index x)
                      (apply #'cairo:set-source-rgb (funcall color-func times))
                      (cairo:rectangle (* x +square-size+) (* y +square-size+)
                                       +square-size+ +square-size+)
                      (cairo:fill-path)))
          (iter (for minute from 0 below 60 by 10)
                (for (values text-width text-height) = (text-dims minute))
                (viz:set-color :light-primary)
                (viz:draw-text minute
                               (* (+ minute 0.5) +square-size+)
                               (+ (* (length guards) +square-size+)
                                  +label-margin+)
                               :horizontal :center
                               :vertical :top)))))))
