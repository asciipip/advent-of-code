#!/usr/bin/env python3

import argparse

import cairocffi as cairo
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

BATTERY_SIZE = (300, 300)
IMAGE_SCALE = 4

COLORS = np.array([
    ( 38,   0, 165, 255),
    ( 39,  48, 215, 255),
    ( 67, 109, 244, 255),
    ( 97, 174, 253, 255),
    (139, 224, 254, 255),
    (139, 239, 217, 255),
    (106, 217, 166, 255),
    ( 99, 189, 102, 255),
    ( 80, 152,  26, 255),
    ( 55, 104,   0, 255),
], np.uint8)

parser = argparse.ArgumentParser()
parser.add_argument('-n', type=int, default=6392)
parser.add_argument('--all', action='store_true')
parser.add_argument('-s', '--with-solutions', action='store_true')
parser.add_argument('--visualize-battery', action='store_true')
parser.add_argument('--visualize-max-squares', action='store_true')
args = parser.parse_args()

def power_level(coord, serial):
    rack_id = coord[0] + 10
    return (rack_id * coord[1] + serial) * rack_id // 100 % 10 - 5

def make_battery(serial):
    battery = np.empty((BATTERY_SIZE[1], BATTERY_SIZE[0]), np.int8)
    for x in range(BATTERY_SIZE[0]):
        for y in range(BATTERY_SIZE[1]):
            battery[y, x] = power_level((x+1, y+1), serial)
    return battery

def max_square(table, size):
    if size == min(table.shape):
        return (np.array([0, 0]), table[-1, -1])
    sums = table[size:, size:] \
           - table[size:, :-size] \
           - table[:-size, size:] \
           + table[:-size, :-size]
    fuel_coords_unadjusted = np.array(np.unravel_index(sums.argmax(), sums.shape))
    fuel_coords = np.flip(fuel_coords_unadjusted + 2, 0)
    max_fuel = sums[tuple(fuel_coords_unadjusted)]
    return (fuel_coords, max_fuel)

def max_square_and_size(table):
    max_fuel = None
    max_coords = None
    max_size = None
    for size in range(1, min(BATTERY_SIZE)):
        coords, fuel = max_square(table, size)
        if max_fuel is None or max_fuel < fuel:
            max_fuel = fuel
            max_coords = coords
            max_size = size
        if fuel < 0:
            break
    return (max_coords, max_size, max_fuel)

def make_sum_table(array):
    assert array.ndim == 2
    return array.cumsum(axis=0).cumsum(axis=1)

def visualize_battery():
    if args.all:
        serials = range(1000)
    else:
        serials = [args.n]

    for serial in tqdm(serials):
        battery = make_battery(serial)
        image = COLORS[np.kron(battery, np.ones((IMAGE_SCALE, IMAGE_SCALE), np.int8)) + 5].flatten()
    
        surface = cairo.ImageSurface.create_for_data(
            image.data,
            cairo.FORMAT_ARGB32,
            BATTERY_SIZE[0] * IMAGE_SCALE,
            BATTERY_SIZE[1] * IMAGE_SCALE)
        ctx = cairo.Context(surface)
        
        if args.with_solutions:
            sum_table = make_sum_table(battery)
            
            ctx.set_source_rgb(0, 0, 1)
            ctx.set_line_width(1)
            
            part_1_coords = max_square(sum_table, 3)[0]
            ctx.rectangle(part_1_coords[0] * IMAGE_SCALE + 0.5,
                          part_1_coords[1] * IMAGE_SCALE + 0.5,
                          3 * IMAGE_SCALE - 1,
                          3 * IMAGE_SCALE - 1)
            ctx.stroke()
            
            part_2_coords, part_2_size, _ = max_square_and_size(sum_table)
            ctx.rectangle(part_2_coords[0] * IMAGE_SCALE + 0.5,
                          part_2_coords[1] * IMAGE_SCALE + 0.5,
                          part_2_size * IMAGE_SCALE - 1,
                          part_2_size * IMAGE_SCALE - 1)
            ctx.stroke()
        
        surface.write_to_png('2018-11-battery-{:03d}.png'.format(serial))

def visualize_max_squares():
    histogram = np.zeros((10, 1000), np.int64)
    max_squares = np.zeros((300, 1000), np.int64)
    for serial in tqdm(range(1000)):
        battery = make_battery(serial)
        histogram[:, serial] = np.histogram(battery)[0]
        sum_table = make_sum_table(battery)
        for size in range(300):
            coords, fuel = max_square(sum_table, size + 1)
            max_squares[size, serial] = fuel
    
    fig = plt.figure(figsize=(11.11, 4.2))
    fig.add_axes((0.06, 0.05, 0.9, 0.9))
    plt.title('Advent of Code 2018, Day 11\nMaximum Fuel by Square Size')
    plt.imshow(max_squares)
    plt.xlabel('Serial Number')
    plt.ylabel('Square Size')
    plt.savefig('2018-11-squares.png', dpi=100)

    plt.figure(figsize=(12.8, 4.8))
    plt.imshow(histogram)
    plt.savefig('2018-11-histogram.png')
    
    
if args.visualize_battery:
    visualize_battery()
if args.visualize_max_squares:
    visualize_max_squares()
