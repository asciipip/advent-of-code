(in-package :aoc-2018-24)

(aoc:define-day 21127 2456)


;;;; Input

(defparameter +base-regex+ "^(\\d+) units each with (\\d+) hit points (?:\\(([^)]+)\\) )?with an attack that does (\\d+) ([a-z]+) damage at initiative (\\d+)$")

(defstruct army
  team
  units
  hp
  attack
  initiative
  strength
  weaknesses
  immunities)

(defmethod print-object ((army army) stream)
  (with-slots (team units hp attack initiative strength weaknesses immunities) army
    (format stream "#<ARMY(~A ~Ax hp:~A a:~A/~A i:~A w:~{~A~^,~}/~{~A~^,~})>" team units hp attack strength initiative weaknesses immunities)))

(defun army-effective-power (army)
  (* (army-attack army) (army-units army)))

(defun make-keyword (string)
  (intern (string-upcase string) (find-package :keyword)))

(defun parse-weaknesses (army weakness-string)
  (labels ((list-types (string)
             (mapcar #'make-keyword (ppcre:split ", " string))))
    (iter (for part in (ppcre:split "; " weakness-string))
          (for groups = (nth-value 1 (ppcre:scan-to-strings "^(immune|weak) to (.*)$" part)))
          (if (string= "immune" (svref groups 0))
              (setf (army-immunities army) (list-types (svref groups 1)))
              (setf (army-weaknesses army) (list-types (svref groups 1)))))))

(defun parse-armies (lines)
  (labels ((parse-lines-r (team remaining-lines armies)
             (let ((groups (nth-value 1 (ppcre:scan-to-strings
                                         +base-regex+
                                         (car remaining-lines)))))
               (cond
                 ((endp remaining-lines)
                  armies)
                 ((string= "Immune System:" (car remaining-lines))
                  (parse-lines-r :immune (cdr remaining-lines) armies))
                 ((string= "Infection:" (car remaining-lines))
                  (parse-lines-r :infection (cdr remaining-lines) armies))
                 (groups
                  (let ((army (make-army :team team
                                         :units (parse-integer (svref groups 0))
                                         :hp (parse-integer (svref groups 1))
                                         :attack (parse-integer (svref groups 3))
                                         :strength (make-keyword (svref groups 4))
                                         :initiative (parse-integer (svref groups 5)))))
                    (parse-weaknesses army (svref groups 2))
                    (parse-lines-r team
                                   (cdr remaining-lines)
                                   (cons army armies))))
                 (t
                  (parse-lines-r team (cdr remaining-lines) armies))))))
    (parse-lines-r nil lines nil)))

(defparameter *test-input*
  '("Immune System:"
    "17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2"
    "989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3"
    ""
    "Infection:"
    "801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1"
    "4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4"))
(defparameter *input* (aoc:input))


;;; Part 1

(defun damage-dealt (attacker defender)
  (with-slots ((a-strength strength)) attacker
    (with-slots ((d-immmunities immunities) (d-weaknesses weaknesses)) defender
      (* (army-effective-power attacker)
         (cond
           ((member a-strength d-immmunities)
            0)
           ((member a-strength d-weaknesses)
            2)
           (t
            1))))))

(defun attack (attacker defender)
  "Returns a replacement DEFENDER with reduced units."
  (let ((new-units (- (army-units defender) (truncate (damage-dealt attacker defender)
                                                      (army-hp defender)))))
    (make-army :team (army-team defender)
               :units new-units
               :hp (army-hp defender)
               :attack (army-attack defender)
               :initiative (army-initiative defender)
               :strength (army-strength defender)
               :weaknesses (army-weaknesses defender)
               :immunities (army-immunities defender))))

(defun best-target (attacker armies)
  (let ((candidates
         (sort (remove (army-team attacker) armies :key #'army-team)
               (lambda (a b)
                 (or (> (damage-dealt attacker a) (damage-dealt attacker b))
                     (and (= (damage-dealt attacker a) (damage-dealt attacker b))
                          (or (> (army-effective-power a) (army-effective-power b))
                              (and (= (army-effective-power a) (army-effective-power b))
                                   (> (army-initiative a) (army-initiative b))))))))))
    (if (and (first candidates)
             (plusp (damage-dealt attacker (first candidates))))
        (car candidates)
        nil)))

(defun assign-targets (armies)
  (labels ((assign-r (remaining-attackers remaining-defenders)
             (if (endp remaining-attackers)
                 (mapcar (lambda (d) (list nil d)) remaining-defenders)
                 (let* ((attacker (car remaining-attackers))
                        (defender (best-target attacker remaining-defenders)))
                   ;(format t "~A -> ~A~%" attacker defender)
                   (cons (list attacker defender)
                         (assign-r (cdr remaining-attackers)
                                   (remove defender remaining-defenders :test 'eq)))))))
    (let ((by-power (sort (copy-list armies)
                          (lambda (a b)
                            (or (> (army-effective-power a) (army-effective-power b))
                                (and (= (army-effective-power a) (army-effective-power b))
                                     (> (army-initiative a) (army-initiative b))))))))
      (assign-r by-power by-power))))

(defun fight (armies)
  (labels ((fight-r (remaining-targets new-defenders)
             (if (endp remaining-targets)
                 new-defenders
                 (destructuring-bind (attacker-maybe defender) (car remaining-targets)
                   (let ((attacker (and attacker-maybe
                                        (or (car (member (army-initiative attacker-maybe)
                                                         new-defenders
                                                         :key #'army-initiative))
                                            attacker-maybe))))
                     (cond
                       ((and attacker defender)
                        (if (plusp (army-units attacker))
                            (fight-r (cdr remaining-targets) (cons (attack attacker defender) new-defenders))
                            (fight-r (cdr remaining-targets) (cons defender new-defenders))))
                       ((and defender (not attacker))
                        (fight-r (cdr remaining-targets) (cons defender new-defenders)))
                       (t
                        (fight-r (cdr remaining-targets) new-defenders))))))))
    (let* ((targets (sort (assign-targets armies)
                          (lambda (a b)
                            (let ((attacker1 (first a))
                                  (attacker2 (first b)))
                              (cond
                                ((and (null attacker1) (null attacker2))
                                 nil)
                                ((null attacker1)
                                 nil)
                                ((null attacker2)
                                 t)
                                (t
                                 (> (army-initiative (first a)) (army-initiative (first b)))))))))
           (survivors (remove-if-not #'plusp (fight-r targets nil) :key #'army-units)))
      (cond
        ((equalp survivors armies)
         (values survivors (remove-duplicates (mapcar #'army-team survivors))))
        ((< 1 (length (remove-duplicates (mapcar #'army-team survivors))))
         (fight survivors))
        (t
         (values survivors (army-team (first survivors))))))))

(defun get-answer-1 (&optional (input *input*))
  (reduce #'+ (fight (parse-armies input)) :key #'army-units))

(aoc:given 1
  (= 5216 (get-answer-1 *test-input*)))


;;;; Part 2

(defun boost (armies team amount)
  (labels ((boost-r (remaining)
             (unless (endp remaining)
               (let ((army (car remaining)))
                 (cons (if (eq (army-team army) team)
                           (make-army :team (army-team army)
                                      :units (army-units army)
                                      :hp (army-hp army)
                                      :attack (+ amount (army-attack army))
                                      :initiative (army-initiative army)
                                      :strength (army-strength army)
                                      :weaknesses (army-weaknesses army)
                                      :immunities (army-immunities army))
                           army)
                       (boost-r (cdr remaining)))))))
    (boost-r armies)))

(defun get-answer-2 (&optional (input *input*))
  (iter (for boost from 0)
        (for (values survivors teams) = (fight (boost (parse-armies input) :immune boost)))
        (finding (reduce #'+ survivors :key #'army-units) such-that (eq teams :immune))))
