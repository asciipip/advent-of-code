(in-package :aoc-2018-07)

(aoc:define-day "IBJTUWGFKDNVEYAHOMPCQRLSZX" 1118)

;;; Digraphs

;; For this problem, we need a digraph.  There's a cl-digraph library,
;; but it's not packaged for Debian, and I'm lazy.
;;
;; Our digraph will be a simple hash table in adjacency-list form.  Each
;; key will be a vertex and each value will be a list of adjacent
;; vertices.  We'll keep track of edges from both ends for easier
;; addition and removal.  Adding an edge will implicitly add the edge's
;; vertices.
;;
;; Vertices should be symbols, for simplicity

(defstruct graph
  (forward-edges (make-hash-table))
  (reverse-edges (make-hash-table)))

(defun remove-vertex! (graph vertex)
  (let ((predecessors (predecessors graph vertex))
        (successors (successors graph vertex)))
    (iter (for predecessor in predecessors)
          (setf (gethash predecessor (graph-forward-edges graph))
                (remove vertex (gethash predecessor (graph-forward-edges graph)))))
    (iter (for successor in successors)
          (setf (gethash successor (graph-reverse-edges graph))
                (remove vertex (gethash successor (graph-reverse-edges graph))))))
  (remhash vertex (graph-forward-edges graph))
  (remhash vertex (graph-reverse-edges graph))
  graph)

(defun insert-edge! (graph from to)
  (let ((forward (gethash from (graph-forward-edges graph) nil))
        (reverse (gethash to (graph-reverse-edges graph) nil)))
    (setf (gethash from (graph-forward-edges graph))
          (cons to forward))
    (setf (gethash to (graph-reverse-edges graph))
          (cons from reverse))
    graph))

(defun remove-edge! (graph from to)
  (setf (gethash from (graph-forward-edges graph))
        (remove to (gethash from (graph-forward-edges graph))))
  (setf (gethash to (graph-reverse-edges graph))
        (remove from (gethash to (graph-reverse-edges graph))))
  graph)

(defun predecessors (graph vertex)
  (gethash vertex (graph-reverse-edges graph)))

(defun successors (graph vertex)
  (gethash vertex (graph-forward-edges graph)))

(defun degree-in (graph vertex)
  (length (predecessors graph vertex)))

(defun vertices (graph)
  (union (iter (for (v ignored) in-hashtable (graph-forward-edges graph))
               (collecting v))
         (iter (for (v ignored) in-hashtable (graph-reverse-edges graph))
               (collecting v))))

(defun edges (graph)
  (iter outer
        (for (from tos) in-hashtable (graph-forward-edges graph))
        (iter (for to in tos)
              (in outer
                  (collecting (list from to))))))

(defun find-paths (graph from to)
  "Returns all paths between vertices FROM and TO."
  (if (eql from to)
      (list (list to))
      (iter outer
            (for intermediate in (gethash from (graph-forward-edges graph)))
            (iter (for path in (find-paths graph intermediate to))
                  (in outer
                      (collecting (cons from path)))))))

(defun transitive-reduce! (graph)
  "Removes edges from GRAPH when those edges are implied by the transitive
  property of one or more other edges."
  (iter (for (from tos) in-hashtable (graph-forward-edges graph))
        (iter (for to in tos)
              (when (iter (for path in (find-paths graph from to))
                          (thereis (< 2 (length path))))
                (remove-edge! graph from to))))
  graph)


;;; Input

(defparameter *input-regex*
  "^Step (\\w+) must be finished before step (\\w+) can begin.$")

(defun parse-input (string)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings *input-regex* string))))
    (assert groups (string)
            "Unable to parse string: ~A" string)
    (list (intern (svref groups 0)) (intern (svref groups 1)))))

(defparameter *edges* (mapcar #'parse-input (aoc:input)))

(defparameter *test-edges*
  '((C A)
    (C F)
    (A B)
    (A D)
    (B E)
    (D E)
    (F E)))

(defun make-graph-from-edges (edges)
  (let ((graph (make-graph)))
    (iter (for (from to) in edges)
          (insert-edge! graph from to))
    graph))


;;; Part 1

(defun step-candidates (graph)
  (sort (iter (for v in (vertices graph))
              (when (= 0 (degree-in graph v))
                (collecting v)))
        (lambda (c1 c2)
          (string< (symbol-name c1) (symbol-name c2)))))

(defun next-step (graph)
  (first (step-candidates graph)))

(defun step-order! (graph)
  "WARNING: Destructively modifies GRAPH."
  (format nil "~{~A~}"
          (iter (for step = (next-step graph))
                (while step)
                (collecting step)
                (remove-vertex! graph step))))

(defun get-answer-1 (&optional (edges *edges*))
  (step-order! (make-graph-from-edges edges)))

(aoc:given 1
  (string= "CABDFE" (get-answer-1 *test-edges*)))


;;; Part 2

(defparameter *step-base-time* 60)

(defun step-time (step)
  (let ((name (symbol-name step)))
    (assert (= 1 (length name)) (step)
            "Step name is longer than one character: ~A" step)
    (+ (1+ (- (char-code (schar name 0)) (char-code #\A)))
       *step-base-time*)))

(defstruct (task
             (:constructor make-task
                           (name worker start
                            &optional (end (+ start (step-time name))))))
  name
  worker
  start
  end)

(defun lambda-task< (accessor)
  "Returns a function that accepts two tasks and returns true if the first
   is less than the second.  \"Is less than\" is a function first of the
   value returned by applying ACCESSOR to the tasks and then, if the
   values are the same, checking the task names.

   The value returned by ACCESSOR is assumed to be numerical."
  (lambda (t1 t2)
    (let ((v1 (funcall accessor t1))
          (v2 (funcall accessor t2)))
      (or (< v1 v2)
          (and (= v1 v2)
               (string< (symbol-name (task-name t1))
                        (symbol-name (task-name t2))))))))

(defun parallel-task-step (graph workers tasks time)
  (cond
    ; No more tasks to do...
    ((= 0 (length (vertices graph)))
     ; ...so start building the task list.
     nil)
    ; There are available tasks and idle workers...
    ((and (< (length tasks) workers)
          (< 0 (length (set-difference (step-candidates graph)
                                       (mapcar #'task-name tasks)))))
     ; ...so start another task.
     (parallel-task-step
      graph
      workers
      (cons (make-task (first (sort (set-difference (step-candidates graph)
                                                    (mapcar #'task-name tasks))
                                    (lambda (s1 s2)
                                      (string< (symbol-name s1)
                                               (symbol-name s2)))))
                       (apply #'min (set-difference (iter (for i from 0 below workers)
                                                          (collecting i))
                                                    (mapcar #'task-worker tasks)))
                       time)
            tasks)
      time))
    ; Every worker is busy...
    (t
     ; ...so finish the next task.
     (let* ((tasks-by-finish-time (sort (copy-list tasks)
                                        (lambda-task< #'task-end)))
            (finished-task (first tasks-by-finish-time)))
       (cons finished-task
             (parallel-task-step (remove-vertex! graph (task-name finished-task))
                                 workers
                                 (rest tasks-by-finish-time)
                                 (task-end finished-task)))))))

(defun parallel-task-order-by-start (graph workers)
  (sort (parallel-task-step graph workers nil 0)
        (lambda-task< #'task-start)))

(defun parallel-task-order-by-end (graph workers)
  (sort (parallel-task-step graph workers nil 0)
        (lambda-task< #'task-end)))

(defun get-answer-2 (&optional (edges *edges*) (workers 5) (step-base-time *step-base-time*))
  (let ((*step-base-time* step-base-time))
    (task-end (car (last (parallel-task-order-by-end (make-graph-from-edges edges)
                                                     workers))))))

(aoc:given 2
  (string= "CABFDE"
           (let ((*step-base-time* 0))
             (format nil "~{~A~}"
                     (mapcar #'task-name
                             (parallel-task-order-by-end (make-graph-from-edges *test-edges*)
                                                         2)))))
  (= 15 (get-answer-2 *test-edges* 2 0)))


;;; Visualization

(defun write-dot-simple (&key (edges *edges*) reduce (in-order t))
  (let ((edges-to-graph (if reduce
                            (edges (transitive-reduce! (make-graph-from-edges edges)))
                            edges)))
    (with-open-file (dotfile "2018-07-1.dot"
                                     :direction :output
                                     :if-exists :supersede)
            (format dotfile "digraph aoc_2018_07_1 {~%")
            (format dotfile "    rankdir=\"LR\"~%")
            (format dotfile "~:{    ~A -> ~A~%~}~%" edges-to-graph)
            (when in-order
              (iter (for c in-string (step-order! (make-graph-from-edges edges)))
                    (for cp previous c initially nil)
                    (when cp
                      (format dotfile "    ~A -> ~A [style=\"invis\"]~%" cp c))))
            (format dotfile "}~%"))))

(defun write-dot-timeline (&optional (edges *edges*) (workers 5) (step-base-time *step-base-time*))
  (let* ((*step-base-time* step-base-time)
         (timeline (parallel-task-order-by-start (make-graph-from-edges edges)
                                                 workers))
         (workers (1+ (apply #'max (mapcar #'task-worker timeline))))
         (end-time (apply #'max (mapcar #'task-end timeline))))
    (with-open-file (dotfile "2018-07-2.dot"
                             :direction :output
                             :if-exists :supersede)
      (format dotfile "digraph aoc_2018_07_2 {~%")
      (format dotfile "    rankdir=\"LR\"~%")
      (format dotfile "    node [shape=\"box\"]~%")
      (iter (for task in timeline)
            (format dotfile "    ~A [pos=\"~A,~A!\",width=\"~A\"]~%"
                    (task-name task)
                    (/ (+ (task-start task) (float (/ (- (task-end task) (task-start task)) 2)))
                       (max 1 step-base-time))
                    (task-worker task)
                    (/ (float (- (task-end task) (task-start task)))
                       (max 1 step-base-time))))
      (iter (for worker from 1 to workers)
            (format dotfile "    worker~A [shape=\"plaintext\",label=\" Worker ~:*~A\",pos=\"~A,~A!\"]~%"
                    worker -0.7 (1- worker)))
      (iter (for time from 0 to end-time by (max 1 step-base-time))
            (format dotfile "    time~A [shape=\"plaintext\",label=\"~:*~A\",pos=\"~F,-1!\"]~%"
                    time (/ time (max 1 step-base-time))))
      (format dotfile "    time_label [shape=\"plaintext\",label=\"Seconds\",pos=\"~A,~A!\"]~%"
              (float (/ (/ end-time 2) (max 1 step-base-time))) -1.5)
      (format dotfile "}~%"))))
