(in-package :aoc-2018-20)

(aoc:define-day 3046 8545)


;;;; Input

(defun char-to-vector (char)
  (ecase char
    (#\N #C( 0 -1))
    (#\E #C( 1  0))
    (#\W #C(-1  0))
    (#\S #C( 0  1))))

(defun parse-paren (string string-pos graph coordinates)
  (destructuring-bind (next-coords new-pos)
      (iter (for (values coords new-pos)
                 first (parse-char string (1+ string-pos) graph coordinates)
                 then (parse-char string (1+ new-pos) graph coordinates))
            (unioning coords into next-coords)
            (until (char= #\) (schar string new-pos)))
            (finally (return (list next-coords new-pos))))
    (values-list (iter (for start-coords in next-coords)
                       (for (values coords end-pos)
                            = (parse-char string (1+ new-pos) graph start-coords))
                       (unioning coords into final-coords)
                       (finally (return (list final-coords end-pos)))))))

(defun parse-single-char (string string-pos graph coordinates)
  (let ((new-coordinates (+ coordinates (char-to-vector (schar string string-pos)))))
    (graph:add-edge graph (list coordinates new-coordinates))
    (parse-char string (1+ string-pos) graph new-coordinates)))

(defun parse-char (string string-pos graph coordinates)
  (if (<= (length string) string-pos)
      (values (list coordinates) string-pos)
      (case (schar string string-pos)
        (#\( (parse-paren string string-pos graph coordinates))
        (#\| (values (list coordinates) string-pos))
        (#\) (values (list coordinates) string-pos))
        (t   (parse-single-char string string-pos graph coordinates)))))

(defun parse-regex (regex)
  (let ((graph (make-instance 'graph:graph)))
    (parse-char (subseq regex 1 (1- (length regex))) 0 graph #C(0 0))
    graph))

(defun print-graph (graph)
  (destructuring-bind (minx miny maxx maxy) (bbox (graph:nodes graph))
    (format t "~v@{~A~:*~}~%" (1+ (* 2 (1+ (- maxx minx)))) "█")
    (iter (for y from miny to maxy)
          (format t "█")
          (iter (for x from minx to maxx)
                (if (member (complex x y) (graph:nodes graph))
                    (format t "~A~A"
                            (if (zerop (complex x y))
                                "╳"
                                " ")
                            (if (member (complex (1+ x) y) (graph:neighbors graph (complex x y)))
                                "│"
                                "█"))
                    (format t "██")))
          (format t "~%█")
          (iter (for x from minx to maxx)
                (if (member (complex x y) (graph:nodes graph))
                    (format t "~A█" (if (member (complex x (1+ y)) (graph:neighbors graph (complex x y)))
                                        "─"
                                        "█"))
                    (format t "██")))
          (format t "~%"))))

(defparameter *test-3* (parse-regex "^WNE$"))
(defparameter *test-10* (parse-regex "^ENWWW(NEEE|SSE(EE|N))$"))
(defparameter *test-18* (parse-regex "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"))
(defparameter *test-23* (parse-regex "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$"))
(defparameter *test-31* (parse-regex "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"))
(defparameter *graph* (parse-regex (aoc:input)))


;;;; Part 1

(defun measure-distance (graph &optional (start #C(0 0)))
  (let ((distance (make-hash-table)))
    (labels ((next-neighbors (current)
               (iter (for node in current)
                     (unioning (remove-if (lambda (n) (gethash n distance))
                                          (graph:neighbors graph node)))))
             (record-distance (nodes d)
               (when (not (endp nodes))
                 (iter (for node in nodes)
                       (setf (gethash node distance) d))
                 (record-distance (next-neighbors nodes) (1+ d)))))
      (record-distance (list start) 0)
      distance)))

(defun furthest-distance (graph &optional (start #C(0 0)))
  (iter (for (node distance) in-hashtable (measure-distance graph start))
        (maximizing distance)))

(aoc:given 1
  (= 3 (furthest-distance *test-3*))
  (= 10 (furthest-distance *test-10*))
  (= 18 (furthest-distance *test-18*))
  (= 23 (furthest-distance *test-23*))
  (= 31 (furthest-distance *test-31*)))

(defun get-answer-1 ()
  (furthest-distance *graph*))


;;;; Part 2

(defun get-answer-2 ()
  (iter (for (node distance) in-hashtable (measure-distance *graph*))
        (counting (<= 1000 distance))))
