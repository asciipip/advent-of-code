(in-package :aoc-2020-16)

(aoc:define-day 21980 1439429522627)


;;;; Parsing

(defstruct tickets
  rules
  yours
  nearby)

(parseq:defrule tickets ()
  (and (+ (and field #\Newline))
       (and #\Newline "your ticket:" #\Newline)
       ticket
       (and #\Newline #\Newline "nearby tickets:" #\Newline)
       (+ (and ticket (? #\Newline))))
  (:choose 0 2 4)
  (:lambda (rules yours nearby)
    (make-tickets :rules (mapcar #'first rules)
                  :yours yours
                  :nearby (mapcar #'first nearby))))

(parseq:defrule field ()
  (and field-name ": " field-range)
  (:choose 0 2)
  (:function #'cons))

(parseq:defrule field-name ()
  (+ (char "a-z "))
  (:string))

;; All of my ranges are of the same format: two contiguous ranges with the
;; word "or" between them.  The following parsing isn't reallt general,
;; but it's enough for my input.
(parseq:defrule field-range ()
  (and aoc:integer-string "-" aoc:integer-string " or " aoc:integer-string "-" aoc:integer-string)
  (:choose 0 2 4 6)
  (:lambda (lo1 hi1 lo2 hi2)
    (lambda (value)
      (or (<= lo1 value hi1)
          (<= lo2 value hi2)))))

(parseq:defrule ticket ()
  (+ (and aoc:integer-string (? ",")))
  (:lambda (&rest values) (mapcar #'first values)))


;;;; Input

(defparameter *example-str*
  "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12")
(defparameter *example* (parseq:parseq 'tickets *example-str*))

(defparameter *example2-str*
  "class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9")
(defparameter *example2* (parseq:parseq 'tickets *example2-str*))

(defparameter *tickets* (aoc:input :parse 'tickets))


;;;; Part 1

(defun value-error-rate (rules value)
  (if (endp rules)
      value
      (destructuring-bind ((field-name . valid-range-fn) . other-rules) rules
        (declare (ignore field-name))
        (if (funcall valid-range-fn value)
            0
            (value-error-rate other-rules value)))))

(defun ticket-error-rate (rules ticket)
  (reduce #'+ (mapcar (alexandria:curry #'value-error-rate rules) ticket)))

(defun all-tickets-error-rate (rules tickets)
  (reduce #'+ (mapcar (alexandria:curry #'ticket-error-rate rules) tickets)))

(defun get-answer-1 (&optional (tickets *tickets*))
  (all-tickets-error-rate (tickets-rules tickets) (tickets-nearby tickets)))

(aoc:given 1
  (= 71 (get-answer-1 *example*)))


;;;; Part 2

(defun valid-value-p (rules value)
  (if (endp rules)
      nil
      (destructuring-bind ((field-name . valid-range-fn) . other-rules) rules
        (declare (ignore field-name))
        (or (funcall valid-range-fn value)
            (valid-value-p other-rules value)))))

(defun valid-ticket-p (rules ticket)
  (if (endp ticket)
      t
      (and (valid-value-p rules (car ticket))
           (valid-ticket-p rules (cdr ticket)))))

(defun valid-tickets (rules tickets)
  (remove-if-not (alexandria:curry #'valid-ticket-p rules)
                 tickets))

(defun field-candidates (rules value &optional (accumulated-candidates (fset:empty-set)))
  (if (endp rules)
      accumulated-candidates
      (destructuring-bind ((field-name . valid-range-fn) . other-rules) rules
        (field-candidates other-rules
                          value
                          (if (funcall valid-range-fn value)
                              (fset:with accumulated-candidates field-name)
                              accumulated-candidates)))))

(defun ticket-field-candidates (rules ticket)
  (mapcar (alexandria:curry #'field-candidates rules) ticket))

(defun all-ticket-field-candidates (rules tickets)
  (reduce (alexandria:curry #'mapcar #'fset:intersection) tickets
          :key (alexandria:curry #'ticket-field-candidates rules)))

(defun remove-candidate (candidates field)
  (fset:image (lambda (candidate-set)
                (and candidate-set (fset:less candidate-set field)))
              candidates))

(defun find-field-mapping (candidates results)
  (fset:do-seq (candidate-set candidates :index i)
    (when (and candidate-set (= 1 (fset:size candidate-set)))
      (let ((field (fset:arb candidate-set)))
        (return-from find-field-mapping
          (values (remove-candidate (fset:with candidates i nil)
                                    field)
                  (fset:with results i field))))))
  (assert nil () "find-field-mapping failed to find a field with only one candidate: ~A" candidates))

(defun determine-field-mappings (candidates &optional results)
  (cond
    ((not (typep candidates 'fset:seq))
     (determine-field-mappings (fset:convert 'fset:seq candidates) results))
    ((not (typep results 'fset:seq))
     (determine-field-mappings candidates (fset:empty-seq)))
    ;; Something of a hack to see if all candidates are NIL
    ((not (fset:position-if #'identity candidates))
     (fset:convert 'list results))
    (t
     (multiple-value-call #'determine-field-mappings (find-field-mapping candidates results)))))

(defun map-fields (mapping ticket)
  (reduce #'fset:map-union (mapcar (lambda (field value) (fset:map (field value))) mapping ticket)))

(defun find-and-map-fields (tickets)
  (with-slots (rules yours nearby) tickets
    (let* ((valid-tickets (valid-tickets rules nearby))
           (field-candidates (all-ticket-field-candidates rules valid-tickets))
           (field-mappings (determine-field-mappings field-candidates)))
      (map-fields field-mappings yours))))

(defun get-answer-2 ()
  (let* ((fields (find-and-map-fields *tickets*))
         (answer-fields (fset:filter (lambda (field-name)
                                       (string= "departure"
                                                (subseq field-name 0 (min 9 (length field-name)))))
                                     (fset:domain fields))))
    (fset:reduce (lambda (result field-name)
                   (* result (fset:lookup fields field-name)))
                 answer-fields
                 :initial-value 1)))
