(in-package :aoc-2020-24)

(aoc:define-day 391 3876)


;;;; Parsing

(parseq:defrule tiles ()
  (+ tile))

(parseq:defrule tile ()
  (and (+ direction) (? #\Newline))
  (:choose 0))

(parseq:defrule direction ()
  (or "se" "sw" "ne" "nw" "e" "w")
  (:lambda (direction)
    (ecase direction
      ("nw" (point:make-point  0  1 -1))
      ("se" (point:make-point  0 -1  1))
      ("ne" (point:make-point  1  0 -1))
      ("sw" (point:make-point -1  0  1))
      ("e"  (point:make-point  1 -1  0))
      ("w"  (point:make-point -1  1  0)))))


;;;; Input

(defparameter *example* (parseq:parseq 'tiles (aoc:read-example)))

(defparameter *tiles* (aoc:input :parse 'tiles))


;;;; Part 1

(defun tile-coordinates (directions)
  (reduce #'point:+ directions))

(defun tile-flips (tiles)
  (reduce (lambda (result tile)
            (fset:with result tile (mod (1+ (fset:lookup result tile)) 2)))
          (mapcar #'tile-coordinates tiles)
          :initial-value (fset:empty-map 0)))

(defun count-color (tiles color)
  (let ((result 0))
    (fset:do-map (tile flips (tile-flips tiles))
      (declare (ignore tile))
      (when (= flips color)
        (incf result)))
    result))

(defun get-answer-1 (&optional (tiles *tiles*))
  (count-color tiles 1))


;;;; Part 2

(defparameter +neighbor-vectors+
  (list (point:make-point  0  1 -1)
        (point:make-point  0 -1  1)
        (point:make-point  1  0 -1)
        (point:make-point -1  0  1)
        (point:make-point  1 -1  0)
        (point:make-point -1  1  0)))

(defun make-floor (tiles)
  (fset:domain (fset:filter (lambda (tile flips)
                              (declare (ignore tile))
                              (= flips 1))
                            (tile-flips tiles))))

(defun tile-adjacent-count (counts tile)
  (fset:reduce (lambda (new-counts vector)
                 (let ((new-tile (point:+ tile vector)))
                   (fset:with new-counts new-tile (1+ (fset:lookup new-counts new-tile)))))
               +neighbor-vectors+
               :initial-value counts))

(defun count-adjacent (floor)
  (fset:reduce #'tile-adjacent-count
               floor
               :initial-value (fset:empty-map 0)))

(defun next-day (floor)
  (let ((adjacent (count-adjacent floor))
        (new-floor (fset:empty-set)))
    (fset:do-map (tile count adjacent)
      (when (or (= 2 count)
                (and (= 1 count)
                     (fset:member? tile floor)))
        (fset:adjoinf new-floor tile)))
    new-floor))

(defun nth-day (nth floor)
  (if (zerop nth)
      floor
      (nth-day (1- nth) (next-day floor))))

(defun get-answer-2 (&optional (tiles *tiles*) (day 100))
  (fset:size (nth-day day (make-floor tiles))))
