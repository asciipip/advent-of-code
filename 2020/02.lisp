(in-package :aoc-2020-02)

(aoc:define-day 383 272)


;;;; Parsing

(defstruct policy
  min-count
  max-count
  char
  password)

(parseq:defrule policy ()
  (and aoc:integer-string "-" aoc:integer-string " " char ": " password)
  (:choose 0 2 4 6)
  (:lambda (min-count max-count char password)
    (make-policy :min-count min-count
                 :max-count max-count
                 :char char
                 :password password)))

(parseq:defrule password ()
  (+ alpha)
  (:string))


;;;; Input

(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'policy)
          '("1-3 a: abcde"
            "1-3 b: cdefg"
            "2-9 c: ccccccccc")))
(defparameter *policies* (aoc:input :parse-line 'policy))


;;;; Part 1

(defun valid-policy-p (policy)
  (with-slots (min-count max-count char password) policy
    (<= min-count (count char password) max-count)))

(defun get-answer-1 (&optional (policies *policies*))
  (count-if #'valid-policy-p policies))


;;;; Part 2

(defun xor (a b)
  (and (or a b)
       (not (and a b))))

(defun valid-policy-2-p (policy)
  (with-slots (min-count max-count char password) policy
    (let ((char1 (schar password (1- min-count)))
          (char2 (schar password (1- max-count))))
      (xor (char= char1 char)
           (char= char2 char)))))

(defun get-answer-2 (&optional (policies *policies*))
  (count-if #'valid-policy-2-p policies))
