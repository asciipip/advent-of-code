(in-package :aoc-2020-11)

(aoc:define-day 2319 2117)


;;;; Parsing

(defun parse-lines (lines &optional (row 0) (result (fset:empty-map)))
  (if (endp lines)
      result
      (parse-lines (cdr lines) (1+ row)
                   (parse-line (car lines) row 0 result))))

(defun parse-line (line row &optional (column 0) (result (fset:empty-map)))
  (if (<= (length line) column)
      result
      (parse-line line row (1+ column)
                  (ecase (schar line column)
                    (#\L (fset:with result (point:make-point column row) nil))
                    (#\. result)))))


;;;; Input

(defparameter *example*
  (parse-lines
   '("L.LL.LL.LL"
     "LLLLLLL.LL"
     "L.L.L..L.."
     "LLLL.LL.LL"
     "L.LL.LL.LL"
     "L.LLLLL.LL"
     "..L.L....."
     "LLLLLLLLLL"
     "L.LLLLLL.L"
     "L.LLLLL.LL")))

(defparameter *seats* (parse-lines (aoc:input)))


;;;; Printing

(defun print-seats (seats)
  (flet ((seat-char (seat)
           (if seat
               #\u25CF
               #\u25CB)))
    (point:print-2d-map seats #'seat-char :unknown #\u25CC)))


;;;; Part 1

(defparameter +adjacent-seats+
  (fset:set (point:make-point -1 -1) (point:make-point 0 -1) (point:make-point  1 -1)
            (point:make-point -1  0)                         (point:make-point  1  0)
            (point:make-point -1  1) (point:make-point 0  1) (point:make-point  1  1)))

(defun count-adjacent-occupied (seats seat)
  (fset:reduce (lambda (result offset)
                 (if (fset:lookup seats (point:+ seat offset))
                     (1+ result)
                     result))
               +adjacent-seats+
               :initial-value 0))

(defun seat-next-state (seats seat currently-occupied-p)
  (let ((adjacent-occupied (count-adjacent-occupied seats seat)))
    (cond
      ((and (not currently-occupied-p)
            (zerop adjacent-occupied))
       t)
      ((and currently-occupied-p
            (<= 4 adjacent-occupied))
       nil)
      (t
       currently-occupied-p))))

(defparameter *next-state* #'seat-next-state)

(defun step-one-round (seats)
  (flet ((reduce-seat (result seat currently-occupied-p)
           (fset:with result seat (funcall *next-state* seats seat currently-occupied-p))))
    (fset:reduce #'reduce-seat seats :initial-value (fset:empty-map))))

(defun find-stable-state (seats &optional (last-seats (fset:empty-map)))
  (if (fset:equal? seats last-seats)
      seats
      (find-stable-state (step-one-round seats) seats)))

(defun count-occupied (seats)
  (flet ((add-occupied (result seat occupiedp)
           (declare (ignore seat))
           (if occupiedp
               (1+ result)
               result)))
    (fset:reduce #'add-occupied seats :initial-value 0)))

(defun get-answer-1 (&optional (seats *seats*))
  (count-occupied (find-stable-state seats)))

(aoc:given 1
  (= 37 (get-answer-1 *example*)))


;;;; Part 2

(defun seat-limit (seats)
  "Returns a point that is one step beyond the maximum row and column in
  SEATS."
  (flet ((max-point (a b)
           (cond
             ((point:< b a) a)
             ((point:< a b) b)
             (t (point:make-point (max (point:elt a 0) (point:elt b 0))
                                  (max (point:elt a 1) (point:elt b 1)))))))
    (let ((max-seat (fset:reduce #'max-point (fset:domain seats))))
      (point:+ max-seat (point:make-point 1 1)))))

(defun seat-in-bounds (seat-limit seat)
  (point:< #.(point:make-point -1 -1) seat seat-limit))

(defun occupied-in-direction-p (seat-limit seats origin direction)
  (let ((seat (point:+ origin direction)))
    (and (seat-in-bounds seat-limit seat)
         (multiple-value-bind (occupiedp foundp) (fset:lookup seats seat)
           (if foundp
               occupiedp
               (occupied-in-direction-p seat-limit seats seat direction))))))

(defun count-adjacent-occupied-real (seat-limit seats seat)
  (fset:reduce (lambda (result offset)
                 (if (occupied-in-direction-p seat-limit seats seat offset)
                     (1+ result)
                     result))
               +adjacent-seats+
               :initial-value 0))

(defun seat-next-state-real (seat-limit seats seat currently-occupied-p)
  (let ((adjacent-occupied (count-adjacent-occupied-real seat-limit seats seat)))
    (cond
      ((and (not currently-occupied-p)
            (zerop adjacent-occupied))
       t)
      ((and currently-occupied-p
            (<= 5 adjacent-occupied))
       nil)
      (t
       currently-occupied-p))))

(defun get-answer-2 (&optional (seats *seats*))
  (let ((*next-state* (alexandria:curry #'seat-next-state-real (seat-limit seats))))
    (count-occupied (find-stable-state seats))))

(aoc:given 2
  (= 26 (get-answer-2 *example*)))


;;;; Visualization

(defun draw-seat (seat)
  (cairo:save)
  (cairo:translate (point:elt seat 0) (point:elt seat 1))
  (viz:set-color :dark-highlight)
  (cairo:rectangle 0.1 0.2 0.8 0.7)
  (cairo:fill-path)
  (viz:set-color :dark-secondary)
  (cairo:set-line-width 0.1)
  (cairo:move-to 0.1 0.3)
  (cairo:line-to 0.1 0.9)
  (cairo:line-to 0.9 0.9)
  (cairo:line-to 0.9 0.3)
  (cairo:stroke)
  (cairo:restore)) 

(defun draw-person (seat)
  (cairo:save)
  (cairo:translate (point:elt seat 0) (point:elt seat 1))
  (viz:set-color :green)
  (cairo:arc 0.5 0.5 0.3 0 (* 2 pi))
  (cairo:fill-path)
  (cairo:restore))

(defun draw-seats-on-surface (surface seats cell-size top-margin left-margin)
  (cairo:with-context-from-surface (surface)
    (cairo:translate left-margin top-margin)
    (cairo:scale cell-size cell-size)
    (fset:do-map (seat occupiedp seats)
      (declare (ignore occupiedp))
      (draw-seat seat))))

(defun draw-people-on-surface (surface seats cell-size top-margin left-margin)
  (cairo:with-context-from-surface (surface)
    (cairo:translate left-margin top-margin)
    (cairo:scale cell-size cell-size)
    (fset:do-map (seat occupiedp seats)
      (when occupiedp
        (draw-person seat)))))

(defun draw-seats (seats filename &key (cell-size 8) (margin 8))
  (let* ((seat-limit (seat-limit seats))
         (width (+ (* 2 margin) (* cell-size (point:elt seat-limit 0))))
         (height (+ (* 2 margin) (* cell-size (point:elt seat-limit 1)))))
    (cairo:with-png-file (filename :rgb24 width height :surface surface)
      (viz:clear-surface surface :dark-background)
      (draw-seats-on-surface surface seats cell-size margin margin)
      (draw-people-on-surface surface seats cell-size margin margin))))

(defun draw-seat-video (seats filename &key (cell-size 10) (margin 8) (fps 5) (outro 10))
  (let* ((seat-limit (seat-limit seats))
         (seat-width (* cell-size (point:elt seat-limit 0)))
         (width (+ (* 4 margin) (* 2 seat-width)))
         (height (+ (* 2 margin) (* cell-size (point:elt seat-limit 1)))))
    (viz:with-video (video (viz:create-video filename width height :fps fps))
      (let ((base-image (viz:create-video-surface video)))
        (viz:clear-surface base-image :dark-background)
        (draw-seats-on-surface base-image seats cell-size margin margin)
        (draw-seats-on-surface base-image seats cell-size margin
                               (+ (* 3  margin) seat-width))
        (iter (for current-seats
                   first seats
                   then (step-one-round (step-one-round current-seats)))
              (let ((*next-state* (alexandria:curry #'seat-next-state-real (seat-limit seats))))
                (for current-seats-2
                     first seats
                     then (step-one-round (step-one-round current-seats-2))))
              (for previous-seats-2 previous current-seats-2)
              (for previous-seats previous current-seats)
              (let ((current-image (viz:clone-surface base-image)))
                (draw-people-on-surface current-image current-seats cell-size margin margin)
                (draw-people-on-surface current-image current-seats-2 cell-size margin
                                        (+ (* 3  margin) seat-width))
                (viz:write-video-surface video current-image))
              (until (and (fset:equal? current-seats previous-seats)
                          (fset:equal? current-seats-2 previous-seats-2)))
              (finally
               (let ((current-image (viz:clone-surface base-image)))
                 (draw-people-on-surface current-image current-seats cell-size margin margin)
                 (draw-people-on-surface current-image current-seats-2 cell-size margin
                                         (+ (* 3  margin) seat-width))
                 (dotimes (i (* fps outro))
                   (viz:write-video-surface video current-image)))))))))
