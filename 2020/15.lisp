(in-package :aoc-2020-15)

;;; Part 2 is 8546398, but takes just over a minute to calculate.
(aoc:define-day 257 nil)


;;;; Parsing

(parseq:defrule numbers ()
  (+ (and aoc:integer-string (? ",")))
  (:lambda (&rest pairs) (mapcar #'first pairs)))


;;;; Input

(defparameter *example-436* '(0 3 6))

(defparameter *numbers* (aoc:input :parse-line 'numbers))


;;;; Part 1

(defun init-seen (numbers &optional (index 1) (seen (fset:empty-map)))
  (if (endp (cdr numbers))
      seen
      (init-seen (cdr numbers)
                 (1+ index)
                 (fset:with seen (car numbers) index))))

(defun next-number (index last-number seen)
  (multiple-value-bind (last-index foundp) (fset:lookup seen last-number)
    (if foundp
        (- index last-index 1)
        0)))

(defun nth-number (target numbers)
  (labels ((take-turn (turn-number last-number seen)
             (let ((next-number (next-number turn-number last-number seen)))
               (if (= turn-number target)
                   next-number
                   (take-turn (1+ turn-number)
                              next-number
                              (fset:with seen last-number (1- turn-number)))))))
    (if (<= target (length numbers))
        (nth (1- target) numbers)
        (take-turn (1+ (length numbers))
                   (car (last numbers))
                   (init-seen numbers)))))

(defun get-answer-1 (&optional (numbers *numbers*))
  (nth-number 2020 numbers))


;;;; Part 2

(defun get-answer-2 (&optional (numbers *numbers*))
  (nth-number 30000000 numbers))
