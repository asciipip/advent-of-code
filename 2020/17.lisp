(in-package :aoc-2020-17)

(aoc:define-day 284 2240)


;;;; Parsing

(defun parse-state (state)
  (iter (with cells = (fset:empty-set))
        (for line in state)
        (for y from 0)
        (iter (for c in-string line with-index x)
              (when (char= c #\#)
                (for point = (point:make-point x y))
                (fset:adjoinf cells point)))
        (finally (return cells))))


;;;; Input

(defparameter *example*
  (parse-state '(".#."
                 "..#"
                 "###")))

(defparameter *initial-state* (parse-state (aoc:input)))


;;;; Part 1

(defparameter *neighbor-vectors* (fset:empty-map))

(defun make-neighbor-vectors (dimensions)
  (labels ((make-pos-list (d)
             (if (zerop d)
                 (list nil)
                 (let ((lower-vectors (make-pos-list (1- d))))
                   (append (mapcar (alexandria:curry #'cons -1) lower-vectors)
                           (mapcar (alexandria:curry #'cons  0) lower-vectors)
                           (mapcar (alexandria:curry #'cons  1) lower-vectors))))))
    (mapcar (lambda (coords) (apply #'point:make-point coords))
            (remove (make-list dimensions :initial-element 0)
                    (make-pos-list dimensions)
                    :test 'equal))))

(defun get-neighbor-vectors (dimensions)
  (multiple-value-bind (vectors foundp) (fset:lookup *neighbor-vectors* dimensions)
    (if foundp
        vectors
        (let ((new-vectors (make-neighbor-vectors dimensions)))
          (fset:adjoinf *neighbor-vectors* dimensions new-vectors)
          new-vectors))))

(defun add-neighbors (cell neighbors)
  (dolist (vector (get-neighbor-vectors (length cell)))
    (incf (gethash (point:+ cell vector) neighbors 0))))

;;; If I use an fset:map insteaf of a hash table, the process takes about
;;; ten times longer.
(defun cycle-step (cells)
  (let ((neighbors (make-hash-table :test 'equalp))
        (new-cells (fset:empty-set)))
    (fset:do-set (cell cells)
      (add-neighbors cell neighbors))
    (flet ((add-cell (new-cell adjacent-count)
             (when (or (= adjacent-count 3)
                       (and (= 2 adjacent-count)
                            (fset:member? new-cell cells)))
               (fset:adjoinf new-cells new-cell))))
      (maphash #'add-cell neighbors))
    new-cells))

(defun nth-cycle (nth cells)
  (if (zerop nth)
      cells
      (nth-cycle (1- nth) (cycle-step cells))))

(defun force-dimensionality (total-dimensions point)
  (let ((difference (- total-dimensions (length point))))
    (cond
      ((plusp difference)
       (apply #'point:make-point (append (point:list-coordinates point)
                                         (make-list difference :initial-element 0))))
      ((zerop difference)
       point)
      ((minusp difference)
       (apply #'point:make-point (subseq (point:list-coordinates point) 0 total-dimensions))))))

(defun cells-with-dimension (dimensionality cells)
  (fset:image (alexandria:curry #'force-dimensionality dimensionality) cells))

(defun get-answer-1 (&optional (cells *initial-state*))
  (fset:size (nth-cycle 6 (cells-with-dimension 3 cells))))

(aoc:given 1
  (= 112 (get-answer-1 *example*)))


;;;; Part 2

(defun get-answer-2 (&optional (cells *initial-state*))
  (fset:size (nth-cycle 6 (cells-with-dimension 4 cells))))

(aoc:given 2
  (= 848 (get-answer-2 *example*)))

;;;; Visualization

(defun oblique-trans-matrices-for-bbox (min-point max-point width height &optional (margin 0))
  (let* ((min-x (point:elt min-point 0))
         (min-y (point:elt min-point 1))
         (min-z (if (< 2 (length min-point))
                    (point:elt min-point 2)
                    0))
         (min-w (if (< 3 (length min-point))
                    (point:elt min-point 3)
                    0))
         (canvas-width (- width (* 2 margin)))
         (canvas-height (- height (* 2 margin)))
         (raw-data-width (1+ (- (point:elt max-point 0) min-x)))
         (raw-data-height (1+ (- (point:elt max-point 1) min-y)))
         (raw-data-z-range (if (< 2 (length min-point))
                               (1+ (- (point:elt max-point 2) min-z))
                               1))
         (raw-data-w-range (if (< 3 (length min-point))
                               (1+ (- (point:elt max-point 3) min-w))
                               1))
         (oblique-data-width (+ raw-data-width (/ raw-data-height (sqrt 2d0))))
         (oblique-data-height (/ raw-data-height (sqrt 2d0)))
         (oblique-data-z-height (+ (* oblique-data-height raw-data-z-range)
                                   (1- raw-data-z-range)))
         (oblique-data-w-width (+ (* oblique-data-width raw-data-w-range)
                                  (1- raw-data-w-range)))
         (trial-width-scale (/ canvas-width oblique-data-w-width))
         (trial-height-scale (/ canvas-height oblique-data-z-height))
         (scale (min trial-width-scale trial-height-scale))
         (canvas-x-offset (- (/ canvas-width 2) (/ (* oblique-data-w-width scale) 2)))
         (canvas-y-offset (- (/ canvas-height 2) (/ (* oblique-data-z-height scale) 2))))
    (let ((result (fset:empty-map)))
      (dotimes (dz raw-data-z-range)
        (dotimes (dw raw-data-w-range)
          (fset:adjoinf
           result
           (ecase (length min-point)
             (2 nil)
             (3 (list (+ dz min-z)))
             (4 (list (+ dz min-z) (+ dw min-w))))
           (cairo:make-trans-matrix
            :xx scale
            :xy (- (/ scale (sqrt 2d0)))
            :x0 (+ margin canvas-x-offset
                   (* dw (* scale (1+ oblique-data-width)))
                   (* scale (/ raw-data-height (sqrt 2d0)))
                   (* scale (- min-x))
                   (* scale (/ min-y (sqrt 2d0))))
            :yx 0d0
            :yy (/ scale (sqrt 2d0))
            :y0 (+ margin canvas-y-offset
                   (* dz (* scale (1+ oblique-data-height)))
                   (* scale (/ (- min-y) (sqrt 2d0))))))))
      result)))

(defun partition-cells-by-plane (cells)
  (let ((result (fset:empty-map (fset:empty-set))))
    (fset:do-set (cell cells)
      (let ((key (subseq (point:list-coordinates cell) 2)))
        (setf result (fset:with result key (fset:with (fset:lookup result key) cell)))))
    result))

(defun draw-plane-grid (min-point max-point)
  (let* ((scale (cairo:trans-matrix-xx (cairo:get-trans-matrix)))
         (alpha (+ 0.05 (alexandria:clamp (/ (- scale 4) (- 40 4)) 0.0 0.95))))
    (cairo:set-line-width (/ 2 scale))
    (let ((x-min (point:elt min-point 0))
          (x-max (1+ (point:elt max-point 0)))
          (y-min (point:elt min-point 1))
          (y-max (1+ (point:elt max-point 1))))
      (dotimes (dx (1+ (- x-max x-min)))
        (if (zerop (+ x-min dx))
            (viz:set-color :dark-primary :alpha alpha)
            (viz:set-color :dark-secondary :alpha alpha))
        (cairo:move-to (+ x-min dx) y-min)
        (cairo:line-to (+ x-min dx) y-max)
        (cairo:stroke))
      (dotimes (dy (1+ (- y-max y-min)))
        (if (zerop (+ y-min dy))
            (viz:set-color :dark-primary :alpha alpha)
            (viz:set-color :dark-secondary :alpha alpha))
        (cairo:move-to x-min (+ y-min dy))
        (cairo:line-to x-max (+ y-min dy))
        (cairo:stroke)))))

(defun draw-full-grid (min-point max-point trans-matrices)
  (flet ((draw-for (plane-key)
           (let ((at-origin-p (iter (for i in plane-key) (always (zerop i)))))
             (cairo:save)
             (cairo:set-trans-matrix (fset:lookup trans-matrices plane-key))
             (draw-plane-grid min-point max-point)
             (draw-label (point:elt min-point 0)
                         (/ (+ (point:elt min-point 1)
                               (point:elt max-point 1)
                               1)
                            2)
                         plane-key
                         :origin at-origin-p)
             (cairo:restore))))
    ;; Hardcoded loops.  Not hte most elegant, but I'm not planning to
    ;; draw more than four dimensions at a time.
    (ecase (length min-point)
      (2 (draw-for '()))
      (3 (iter (for z from (point:elt min-point 2) to (point:elt max-point 2))
               (draw-for (list z))))
      (4 (iter (for z from (point:elt min-point 2) to (point:elt max-point 2))
               (iter (for w from (point:elt min-point 3) to (point:elt max-point 3))
                     (draw-for (list z w))))))))

(defun draw-origin (min-point max-point trans-matrices)
  (let ((x-min (point:elt min-point 0))
        (x-max (1+ (point:elt max-point 0)))
        (y-min (point:elt min-point 1))
        (y-max (1+ (point:elt max-point 1))))
    (cairo:save)
    (cairo:set-trans-matrix (fset:lookup trans-matrices (make-list (- (length min-point) 2) :initial-element 0)))
    (viz:set-color :blue)
    (cairo:set-line-width (/ 2 (cairo:trans-matrix-xx (cairo:get-trans-matrix))))
    (cairo:rectangle x-min y-min (- x-max x-min) (- y-max y-min))
    (cairo:stroke)
    (cairo:arc 0 0
               (min 0.25 (/ 8 (cairo:trans-matrix-xx (cairo:get-trans-matrix))))
               0 (* 2 pi))
    (cairo:fill-path)
    (cairo:restore)))

(defun draw-cells (cells)
  (viz:set-color :dark-primary)
  (let ((size (max 1 (/ (cairo:trans-matrix-xx (cairo:get-trans-matrix))))))
    (fset:do-set (cell cells)
      (cairo:rectangle (point:elt cell 0) (point:elt cell 1)
                       size size)
      (cairo:fill-path))))

(defun draw-label (anchor-x anchor-y plane-key &key origin)
  (let* ((label (case (length plane-key)
                  (0 "")
                  (1 (format nil "z = ~A" (car plane-key)))
                  (t (format nil "(~{~A~^,~})" plane-key))))
         (scale (cairo:trans-matrix-xx (cairo:get-trans-matrix)))
         (text-height (min 1 (/ 32 scale)))
         (offset (min 0.5 (/ 16 scale))))
    (cairo:save)
    (cairo:translate (- anchor-x offset) anchor-y)
    (cairo:rotate (- (/ pi 2)))
    (if origin
        (viz:set-color :blue)
        (viz:set-color :dark-secondary))
    (viz:draw-text label
                   0 0
                   :size text-height
                   :horizontal :center
                   :vertical :baseline))
  (cairo:restore))

(defun write-image (cells filename &key (width 1280) (height 720) (margin 16))
  (assert (<= (length (fset:arb cells)) 4) ()
          "Cannot draw cells with more than four dimensions.")
  (multiple-value-bind (min-point max-point)
      (point:bbox (cons (apply #'point:make-point (make-list (length (fset:arb cells)) :initial-element 0))
                        (fset:convert 'list cells)))
    (let ((trans-matrices (oblique-trans-matrices-for-bbox min-point max-point width height margin)))
      (cairo:with-png-file (filename :rgb24 width height :surface surface)
        (viz:clear-surface surface :dark-background)
        (draw-full-grid min-point max-point trans-matrices)
        (fset:do-map (plane-key plane-cells (partition-cells-by-plane cells))
          (cairo:set-trans-matrix (fset:lookup trans-matrices plane-key))
          (draw-cells plane-cells))
        (draw-origin min-point max-point trans-matrices)))))

(defun find-total-bbox (steps cells)
  (if (zerop steps)
      (point:bbox cells)
      (point:bbox (nconc (multiple-value-list (find-total-bbox (1- steps) (cycle-step cells)))
                         (fset:convert 'list cells)))))

(defun write-video-frames (video base-image trans-matrices cells steps outro)
  (let ((working-image (viz:clone-surface base-image)))
    (cairo:with-context-from-surface (working-image)
      (fset:do-map (plane-key plane-cells (partition-cells-by-plane cells))
        (cairo:set-trans-matrix (fset:lookup trans-matrices plane-key))
        (draw-cells plane-cells)))
    (viz:write-video-surface video working-image)
    (if (zerop steps)
        (dotimes (i outro)
          (viz:write-video-surface video working-image))
        (write-video-frames video base-image trans-matrices
                            (cycle-step cells)
                            (1- steps)
                            outro))))

(defun write-video (cells filename &key (width 1280) (height 720) (margin 16) (fps 1) (outro 10) (steps 6))
  (assert (<= (length (fset:arb cells)) 4) ()
          "Cannot draw cells with more than four dimensions.")
  (multiple-value-bind (min-point max-point) (find-total-bbox steps cells)
    (let ((trans-matrices (oblique-trans-matrices-for-bbox min-point max-point width height margin)))
      (viz:with-video (video (viz:create-video filename width height :fps fps))
        (let ((base-image (viz:create-video-surface video)))
          (viz:clear-surface base-image :dark-background)
          (cairo:with-context-from-surface (base-image)
            (draw-full-grid min-point max-point trans-matrices))
          (write-video-frames video base-image trans-matrices cells steps (* fps outro)))))))
