(in-package :aoc-2020-21)

(aoc:define-day 2317 "kbdgs,sqvv,slkfgq,vgnj,brdd,tpd,csfmb,lrnz")


;;;; Data Structures

(defstruct food
  (ingredients (fset:empty-set) :type fset:set)
  (allergens (fset:empty-set) :type fset:set))


;;;; Parsing

(parseq:defrule foods ()
  (+ (and food (? #\Newline)))
  (:lambda (&rest foods) (mapcar #'first foods)))

(parseq:defrule food ()
  (and ingredients " " allergens)
  (:choose 0 2)
  (:lambda (ingredients allergens) (make-food :ingredients ingredients :allergens allergens)))

(parseq:defrule ingredients ()
  (and alpha-symbol (* (and " " alpha-symbol)))
  (:lambda (first rest) (fset:convert 'fset:set (cons first (mapcar #'second rest)))))

(parseq:defrule allergens ()
  (and "(contains " (aoc:comma-list alpha-symbol) ")")
  (:choose 1)
  (:lambda (&rest rest) (fset:convert 'fset:set rest)))

(parseq:defrule alpha-symbol ()
  (+ alpha)
  (:string)
  (:function #'string-upcase)
  (:function #'intern))


;;;; Input

(defparameter *example* (parseq:parseq 'foods (aoc:read-example)))

(defparameter *foods* (aoc:input :parse 'foods))


;;;; Part 1

;;; Allergen index (maps allergens to potential ingredients)

(defun build-allergen-index (foods &optional (index (fset:empty-map)))
  (if (endp foods)
      index
      (build-allergen-index (cdr foods) (add-food-to-allergen-index (car foods) index))))

(defun add-food-to-allergen-index (food index)
  (with-slots (ingredients allergens) food
    (fset:reduce (lambda (new-index allergen)
                   (add-ingredients-to-allergen-index allergen ingredients new-index))
                 allergens
                 :initial-value index)))

(defun add-ingredients-to-allergen-index (allergen ingredients index)
  (multiple-value-bind (existing-ingredients foundp) (fset:lookup index allergen)
    (fset:with index allergen (if foundp
                                  (fset:intersection ingredients existing-ingredients)
                                  ingredients))))

;;; Ingredient allergens

(defun find-allergen-ingredients (allergen-index &optional (allergen-map (fset:empty-map)))
  (if (fset:equal? (fset:domain allergen-index) (fset:range allergen-map))
      allergen-map
      (find-allergen-ingredients allergen-index (add-allergen-to-map allergen-index allergen-map))))

(defun add-allergen-to-map (allergen-index allergen-map)
  (let ((found-ingredients (fset:domain allergen-map)))
    (fset:do-map (allergen ingredients allergen-index)
      (let ((ingredient-candidates (fset:set-difference ingredients found-ingredients)))
        (when (= 1 (fset:size ingredient-candidates))
          (return-from add-allergen-to-map (fset:with allergen-map (fset:arb ingredient-candidates) allergen)))))
    (assert nil () "No ingredient candidates found.")))

;;; Allergen-free ingredients

(defun find-allergen-free-ingredients (foods)
  (let ((allergen-map (find-allergen-ingredients (build-allergen-index foods))))
    (reduce #'fset:bag-sum
            (mapcar (alexandria:compose (alexandria:curry #'fset:convert 'fset:bag)
                                        (alexandria:rcurry #'fset:set-difference (fset:domain allergen-map))
                                        #'food-ingredients)
                    foods))))

;;; Solve the problem

(defun get-answer-1 (&optional (foods *foods*))
  (fset:size (find-allergen-free-ingredients foods)))

(aoc:given 1
  (= 5 (get-answer-1 *example*)))


;;;; Part 2

(defun invert-allergen-map (allergen-map)
  (fset:image (lambda (a i) (values i a)) allergen-map))

(defun sort-allergens (ingredient-map)
  (fset:sort (fset:convert 'fset:seq (fset:domain ingredient-map))
             #'string<
             :key #'string))

(defun dangerous-ingredient-list (foods)
  (let* ((allergen-map (find-allergen-ingredients (build-allergen-index foods)))
         (ingredient-map (invert-allergen-map allergen-map))
         (sorted-allergens (sort-allergens ingredient-map)))
    (fset:convert 'list (fset:image (alexandria:curry #'fset:lookup ingredient-map) sorted-allergens))))

(defun get-answer-2 (&optional (foods *foods*))
  (format nil "~{~(~A~)~^,~}" (dangerous-ingredient-list foods)))

(aoc:given 2
  (string= "mxmxvkd,sqjhc,fvjkl" (get-answer-2 *example*)))
