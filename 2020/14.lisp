(in-package :aoc-2020-14)

(aoc:define-day 12610010960049 3608464522781)


;;;; Parsing

(defstruct mask
  (and 0 :type (integer 0 #.(1- (expt 2 36))))
  (or 0 :type (integer 0 #.(1- (expt 2 36))))
  (float 0 :type (integer 0 #.(1- (expt 2 36)))))

(parseq:defrule program ()
  (+ line))

(parseq:defrule line ()
  (and (or mask mem) (? #\Newline))
  (:choose 0))

(defun parse-mask (chars &optional (and 0) (or 0) (float 0))
  (if (endp chars)
      (make-mask :and and :or or :float float)
      (destructuring-bind (char . other-chars) chars
        (parse-mask other-chars
                    (if (char= char #\0)
                        (* 2 and)
                        (1+ (* 2 and)))
                    (if (char= char #\1)
                        (1+ (* 2 or))
                        (* 2 or))
                    (if (char= char #\X)
                        (1+ (* 2 float))
                        (* 2 float))))))

(parseq:defrule mask ()
  (and "mask = " (rep 36 (char  "X10")))
  (:choose 1)
  (:lambda (&rest chars) (list 'set-mask (parse-mask chars))))

(parseq:defrule mem ()
  (and "mem[" aoc:integer-string "] = " aoc:integer-string)
  (:choose 1 3)
  (:lambda (address value) (list 'set-mem address value)))


;;;; Input

(defparameter *example*
  (parseq:parseq 'program "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"))

(defparameter *example2*
  (parseq:parseq 'program "mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"))

(defparameter *program* (aoc:input :parse 'program))


;;;; Part 1

(defstruct context
  (version 1)
  (mask (make-mask) :type mask)
  (memory (fset:empty-map) :type fset:map))

(defun update-context (context &key mask memory)
  (with-slots (version (old-mask mask) (old-memory memory)) context
    (make-context :version version
                  :mask (or mask old-mask)
                  :memory (or memory old-memory))))

(defun set-mask (context mask)
  (update-context context :mask mask))

(defun apply-mask-to-value (mask integer)
  (logand (mask-and mask) (logior (mask-or mask) integer)))

(defun apply-mask-to-address (mask address)
  (with-slots (and or float) mask
    (labels ((expand-addresses (addresses add-one-p)
               (fset:image (lambda (a)
                             (if add-one-p
                                 (1+ (* 2 a))
                                 (* 2 a)))
                           addresses))
             (generate (bit addresses)
               (if (minusp bit)
                   addresses
                   (let ((next-addresses (if (logbitp bit float)
                                             (fset:union (expand-addresses addresses t)
                                                         (expand-addresses addresses nil))
                                             (expand-addresses addresses
                                                               (or (logbitp bit or)
                                                                   (logbitp bit address))))))
                     (generate (1- bit) next-addresses)))))
      (generate 35 (fset:set 0)))))

(defun set-mem (context address value)
  (with-slots (mask memory version) context
    (ecase version
      (1
       (let ((masked-value (apply-mask-to-value mask value)))
         (update-context context :memory (fset:with memory address masked-value))))
      (2
       (let ((addresses (apply-mask-to-address mask address)))
         (update-context context
                         :memory (fset:reduce (lambda (r a)
                                                (fset:with r a value))
                                              addresses
                                              :initial-value memory)))))))

(defun execute-instruction (context instruction)
  (destructuring-bind (fn . args) instruction
    (apply fn (cons context args))))

(defun execute (instructions &optional (context (make-context)))
  (if (endp instructions)
      context
      (destructuring-bind (instruction . other-instructions) instructions
        (execute other-instructions (execute-instruction context instruction)))))

(defun sum-memory (context)
  (fset:reduce (lambda (result address value)
                 (declare (ignore address))
                 (+ result value))
               (context-memory context)
               :initial-value 0))

(defun get-answer-1 (&optional (program *program*))
  (sum-memory (execute program)))

(aoc:given 1
  (= 165 (get-answer-1 *example*)))


;;;; Part 2

(defun get-answer-2 (&optional (program *program*))
  (sum-memory (execute program (make-context :version 2))))

(aoc:given 2
  (= 208 (get-answer-2 *example2*)))
