(in-package :aoc-2020-05)

(aoc:define-day 930 515)


;;;; Parsing

(parseq:defrule boarding-pass ()
  (and (rep 7 (char "FB")) (rep 3 (char "RL")))
  (:flatten)
  (:lambda (&rest chars)
    (reduce (lambda (n c)
              (+ (* 2 n)
                 (ecase c
                   ((#\B #\R) 1)
                   ((#\F #\L) 0))))
            chars
            :initial-value 0)))


;;;; Input

(defparameter *boarding-passes* (aoc:input :parse-line 'boarding-pass))


;;;; Part 1

(defun get-answer-1 (&optional (boarding-passes *boarding-passes*))
  (apply #'max boarding-passes))


;;;; Part 2

(defun get-answer-2 (&optional (boarding-passes *boarding-passes*))
  (- (aoc:sum-from-to (apply #'min boarding-passes)
                      (apply #'max boarding-passes))
     (reduce #'+ boarding-passes)))


;;;; Visualization

;; NB: Dynamically rebinding these gets a little tricky because of the
;; parallel processing.  `let' bindings are thread-local.  To propagate
;; custom bindings to the rendering threads you need to use an `lparallel'
;; kernel created with the appropriate parameters.
(defparameter *plane-width-pct* 0.9)
(defparameter *boarding-width-pct* 0.05)
(defparameter *seat-width* 1.2)
(defparameter *luggage-time* 4)
(defparameter *special-boarder* (get-answer-2))
(defparameter *seat-layout* #(0 0 0 0 t 0 0 0 0))    ; 4-4
;(defparameter *seat-layout* #(0 0 t 0 0 1 1 t 1 1))  ; 2-4-2
;(defparameter *seat-layout* #(0 0 0 t 0 1 t 1 1 1))  ; 3-2-3
(defparameter *special-boarding* nil)

(defstruct boarder
  (id 0 :type (integer 0 1023))
  (position (point:make-point 0 0) :type point:point)
  (targets nil :type list))

(defun pass-label (seat-id)
  (iter (for current-id first seat-id then (truncate current-id 2))
        (for chars in '("LR" "LR" "LR" "FB" "FB" "FB" "FB" "FB" "FB" "FB"))
        (collecting (schar chars (mod current-id 2)) at beginning result-type 'string)))

(defun move-boarder (boarder speed)
  (with-slots (id position targets) boarder
    (cond
      ((zerop (point:norm (point:- (first targets) position)))
       (make-boarder :id id
                     :position position
                     :targets (rest targets)))
      (t
       (let* ((vector (point:* (point:unit-vector (point:- (first targets) position))
                               speed))
              (new-position (point:+ position vector))
              (new-distance-to-target (point:norm (point:- (first targets) new-position)))
              (at-target (<= new-distance-to-target (/ speed 2))))
         (make-boarder :id id
                       :position (if at-target
                                     (first targets)
                                     new-position)
                       :targets (if at-target
                                    (rest targets)
                                    targets)))))))

(defun intersect-boarders-p (ba bb)
  (< (point:norm (point:- (boarder-position ba) (boarder-position bb)))
     1.0))

(defstruct boarders
  (all (fset:empty-set) :type fset:set)
  (seated (fset:empty-set) :type fset:set)
  (moving (fset:empty-seq) :type fset:seq))

(defun aisle-pos (aisle-index)
  (if (zerop aisle-index)
      (position t *seat-layout*)
      (position t *seat-layout* :start (1+ (aisle-pos (1- aisle-index))))))

(defun aisle-count ()
  (count t *seat-layout*))

(defun target-aisle (column)
  (svref (remove t *seat-layout*) column))

(defun seat-pos-y (column)
  (iter (for n in-vector *seat-layout* with-index i)
        (with c = -1)
        (when (not (eq n t))
          (incf c))
        (finding i such-that (= c column))))

(defun seat-pos (max-row seat-id)
  ;; "row" and "column" are from the problem definition.  The drawing is
  ;; rotated 90 degrees from that!
  (multiple-value-bind (row column) (truncate seat-id 8)
    (point:make-point (+ (* *seat-width* (- max-row row))
                         (1- *seat-width*))
                      (seat-pos-y column))))

(defun seat-targets-normal (max-row seat-id speed)
  (multiple-value-bind (row column) (truncate seat-id 8)
    (let ((aisle-y (aisle-pos (target-aisle column))))
      (nconc (list
              ;; First, a point just inside the plane.  Further aisles go
              ;; further before they turn.
              (point:make-point (- (* 2 *seat-width*))
                                (- (length *seat-layout*)
                                   (- (aisle-count) (target-aisle column))))
              ;; Next, a point at the beginning of the appropriate aisle.
              (point:make-point (- *seat-width*)
                                aisle-y))
             ;; Next, the aisle spot closest to the seat.  Repeat the
             ;; aisle spot to simulate putting up luggage.
             (make-list (truncate *luggage-time* speed)
                        :initial-element (point:make-point (* *seat-width* (- max-row row))
                                                           aisle-y))
             ;; Finally, the seat itself.
             (list (seat-pos max-row seat-id))))))

(defun seat-targets-special (max-row seat-id speed)
  (declare (ignore speed))
  (let ((row (truncate seat-id 8))
        (base (* *seat-width* max-row)))
    (cons (point:make-point (- base (* *seat-width* 63.5))
                            (+ 33 (length *seat-layout*)))
          (if (plusp (logand row #b1000000))
              (list (point:make-point (- base (* *seat-width* (logand #b1000000 row)))
                                      (+ 32 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1100000 row)))
                                      (+ 16 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1110000 row)))
                                      (+ 8 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1111000 row)))
                                      (+ 4 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1111100 row)))
                                      (+ 2 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1111110 row)))
                                      (+ 1 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logand #b1111111 row)))
                                      (+ 0 (length *seat-layout*)))
                    (seat-pos max-row seat-id))
              (list (point:make-point (- base (* *seat-width* (logior #b0111111 row)))
                                      (+ 32 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0011111 row)))
                                      (+ 16 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0001111 row)))
                                      (+ 8 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0000111 row)))
                                      (+ 4 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0000011 row)))
                                      (+ 2 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0000001 row)))
                                      (+ 1 (length *seat-layout*)))
                    (point:make-point (- base (* *seat-width* (logior #b0000000 row)))
                                      (+ 0 (length *seat-layout*)))
                    (seat-pos max-row seat-id))))))

(defun boarder-start-normal (max-row i)
  (declare (ignore max-row))
  (point:make-point (- (* 2 *seat-width*)) i))

(defun boarder-start-special (max-row i)
  (let ((base (* *seat-width* max-row)))
    (point:make-point (- base (* *seat-width* 63.5))
                      (+ i 34 (length *seat-layout*)))))

(defun seat-targets (max-row seat-id speed)
  (if *special-boarding*
      (seat-targets-special max-row seat-id speed)
      (seat-targets-normal max-row seat-id speed)))

(defun boarder-start (max-row i)
  (if *special-boarding*
      (boarder-start-special max-row i)
      (boarder-start-normal max-row i)))

(defun init-boarders (boarding-passes speed)
  (let ((boarders (make-boarders :all (fset:convert 'fset:set boarding-passes))))
    (setf (boarders-moving boarders)
          (fset:convert 'fset:seq
                        (iter (for seat-id in boarding-passes)
                              (for i from (1+ (length *seat-layout*)))
                              (collecting
                                (make-boarder :id seat-id
                                              :position (boarder-start (max-row boarders) i)
                                              :targets (seat-targets (max-row boarders) seat-id speed))))))
    boarders))

(defun min-seat (boarders)
  (fset:least (boarders-all boarders)))

(defun max-seat (boarders)
  (fset:greatest (boarders-all boarders)))

(defun min-row (boarders)
  (floor (min-seat boarders) 8))

(defun max-row (boarders)
  (floor (max-seat boarders) 8))

(defun seat-rows (boarders)
  (1+ (- (max-row boarders)
         (min-row boarders))))

(defun apply-trans-matrix (boarders width height)
  (cairo:transform (make-trans-matrix boarders width height)))

(defun make-trans-matrix (boarders width height)
  (let* ((seat-height (/ (* width *plane-width-pct*)
                         (seat-rows boarders)
                         *seat-width*))
         (origin-x (* width (+ *boarding-width-pct*
                               (/ (- 1 *plane-width-pct* *boarding-width-pct*)
                                  2))))
         (origin-y (if *special-boarding*
                       (* 20 seat-height)
                       (- (/ height 2)
                          (* (/ (length *seat-layout*) 2) seat-height)))))
    (cairo:make-trans-matrix :xx (float seat-height 1.0d0)
                             :yy (float seat-height 1.0d0)
                             :x0 (float origin-x 1.0d0)
                             :y0 (float origin-y 1.0d0))))

(defun draw-seat (boarders seat-id)
  (cairo:save)
  (let ((pos (seat-pos (max-row boarders) seat-id)))
    (cairo:translate (point:elt pos 0) (point:elt pos 1)))
  (cairo:set-line-width 0.1)
  (viz:set-color :dark-highlight)
  (cairo:rectangle 0 0 1 1)
  (cairo:fill-path)
  (viz:set-color :dark-secondary)
  (cairo:move-to 0.2 0)
  (cairo:line-to 1 0)
  (cairo:line-to 1 1)
  (cairo:line-to 0.2 1)
  (cairo:stroke)
  (cairo:restore))

(defun draw-seats (boarders &key door-closed)
  (cairo:save)
  (let* ((plane-height (length *seat-layout*))
         (front-start (- (* 2 *seat-width*)))
         (back-end (* *seat-width* (+ 5 (seat-rows boarders))))
         (top-y -0.1)
         (bottom-y (+ plane-height 0.1))
         (wing-inner-left 30)
         (wing-outer-left 40)
         (wing-inner-right 90)
         (wing-outer-right 95)
         (wing-width 10))
    #|
    ;; Wing shading
    (viz:set-color :dark-highlight)
    (cairo:move-to wing-inner-left top-y)
    (cairo:line-to wing-outer-left (- top-y wing-width))
    (cairo:line-to wing-outer-right (- top-y wing-width))
    (cairo:line-to wing-inner-right top-y)
    (cairo:close-path)
    (cairo:fill-path)
    (cairo:move-to wing-inner-left bottom-y)
    (cairo:line-to wing-outer-left (+ bottom-y wing-width))
    (cairo:line-to wing-outer-right (+ bottom-y wing-width))
    (cairo:line-to wing-inner-right bottom-y)
    (cairo:close-path)
    (cairo:fill-path)
    |#
    (cairo:set-line-width 0.2)
    (viz:set-color :dark-emphasized)
    ;; Front
    (cairo:curve-to front-start top-y
                    -10 top-y
                    -12 4)
    (cairo:stroke)
    (cairo:curve-to front-start bottom-y
                    -10 bottom-y
                    -12 (- plane-height 4))
    (cairo:stroke)
    ;; Middle/Back
    (cairo:move-to front-start top-y)
    (cairo:line-to back-end top-y)
    (cairo:stroke)
    (if (or door-closed *special-boarding*)
        (progn
          (cairo:move-to front-start bottom-y)
          (cairo:line-to back-end bottom-y)
          (cairo:stroke))
        (progn
          (cairo:move-to (1+ front-start) bottom-y)
          (cairo:line-to back-end bottom-y)
          (cairo:stroke)))
    ;; Wings
    (cairo:move-to wing-inner-left top-y)
    (cairo:line-to wing-outer-left (- top-y wing-width))
    (cairo:stroke)
    (cairo:move-to wing-inner-left bottom-y)
    (cairo:line-to wing-outer-left (+ bottom-y wing-width))
    (cairo:stroke)
    (cairo:move-to wing-inner-right top-y)
    (cairo:line-to wing-outer-right (- top-y wing-width))
    (cairo:stroke)
    (cairo:move-to wing-inner-right bottom-y)
    (cairo:line-to wing-outer-right (+ bottom-y wing-width))
    (cairo:stroke)

    ;; Fade out wings
    ;; TODO  (Looks like cl-cairo2 doesn't support gradients?)
    )
  (cairo:restore)
  (iter (for seat-id from (min-seat boarders) to (max-seat boarders))
        (draw-seat boarders seat-id)))

(defun draw-boarder (pos id &key (dx 0) (dy 0))
  (cairo:save)
  (let ((x (point:elt pos 0))
        (y (point:elt pos 1)))
    (cairo:translate (+ x dx) (+ y dy))
    (let ((min-y-for-label (+ (length *seat-layout*)
                              (if *special-boarding*
                                  33
                                  0)
                              0.5)))
      (when (and (<= min-y-for-label y)
                 (/= id *special-boarder*))
        (viz:set-color :dark-primary)
        (viz:draw-text (pass-label id)
                       1.5 0.5
                       :size 0.9
                       :vertical :middle))))
  (viz:set-color (if (= id *special-boarder*)
                     :red
                     :green))
  (cairo:arc 0.5 0.5 0.4 0 (* 2 pi))
  (cairo:fill-path)
  (cairo:restore))

(defun draw-boarders (boarders base-image)
  (let ((image (viz:clone-surface base-image)))
    (cairo:with-context-from-surface (image)
      (apply-trans-matrix boarders
                          (cairo:image-surface-get-width image)
                          (cairo:image-surface-get-height image))
      (fset:do-set (boarder (boarders-seated boarders))
        (draw-boarder (seat-pos (max-row boarders) boarder)
                      boarder))
      (fset:do-seq (boarder (boarders-moving boarders))
        (draw-boarder (boarder-position boarder)
                      (boarder-id boarder))))
    image))

(defun move-boarders (boarders speed)
  (with-slots (all seated moving) boarders
    (flet ((move-r (new-moving boarder)
             (let ((new-boarder (move-boarder boarder speed)))
               (fset:with-last
                 new-moving
                 (if (fset:find-if (alexandria:curry #'intersect-boarders-p new-boarder)
                                   new-moving)
                     boarder
                     new-boarder)))))
      (let ((new-moving (fset:reduce #'move-r
                                     moving
                                     :initial-value (fset:empty-seq))))
        (make-boarders :all all
                       :seated seated
                       :moving new-moving)))))

(defun seat-boarders (boarders)
  (with-slots (all seated moving) boarders
    (multiple-value-bind (to-be-seated still-moving)
        (fset:partition (lambda (b) (endp (boarder-targets b)))
                        moving)
      (make-boarders :all all
                     :seated (fset:union
                              (fset:convert 'fset:set (fset:image #'boarder-id to-be-seated))
                              seated)
                     :moving still-moving))))

(defun make-video (filename &key (boarding-passes *boarding-passes*) (width 1920) (height 1080) (fps 60) (outro-len 10) (speed 0.2) stop-after)
  "SPEED is how far a person moves in a frame, in fractions of a person's
  size."
  (viz:with-video (video (viz:create-video filename width height :fps fps))
    (let* ((*special-boarder* (get-answer-2 boarding-passes))
           (futures-queue (lparallel.queue:make-queue :fixed-capacity 16))
           (render-thread (bt:make-thread
                           (lambda ()
                             (iter (for future = (lparallel.queue:pop-queue futures-queue))
                                   (while future)
                                   (for image = (lparallel:force future))
                                   (viz:write-video-surface video image)))))
           (boarders (init-boarders (append boarding-passes (list *special-boarder*)) speed))
           (base-image (viz:create-video-surface video)))
      (viz:clear-surface base-image :dark-background)
      (cairo:with-context-from-surface (base-image)
        (apply-trans-matrix boarders width height)
        (draw-seats boarders))
      (iter (lparallel.queue:push-queue (lparallel:future (draw-boarders boarders base-image))
                                        futures-queue)
            (setf boarders (seat-boarders (move-boarders boarders speed)))
            (for i from 0)
            (when stop-after
              (while (< i (* fps stop-after))))
            (until (fset:empty? (boarders-moving boarders))))
      (cairo:with-context-from-surface (base-image)
        (apply-trans-matrix boarders width height)
        (draw-seats boarders :door-closed t))
      (let ((final-image (draw-boarders boarders base-image)))
        (dotimes (i (* outro-len fps))
          (lparallel.queue:push-queue (lparallel:future final-image)
                                      futures-queue)))
      (lparallel.queue:push-queue nil
                                  futures-queue)
      (when (bt:thread-alive-p render-thread)
        (bt:join-thread render-thread)))))
