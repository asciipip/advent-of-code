(in-package :aoc-2020-06)

(aoc:define-day 6726 3316)


;;;; Parsing

(parseq:defrule everyone ()
  (+ (and group (? #\Newline)))
  (:lambda (&rest pairs) (mapcar #'first pairs)))

(parseq:defrule group ()
  (+ (and person (? #\Newline)))
  (:lambda (&rest pairs) (mapcar #'first pairs)))

(parseq:defrule person ()
  (+ (char "a-z"))
  (:lambda (&rest chars)
    (fset:convert 'fset:set (mapcar #'aoc:make-keyword chars))))


;;;; Input

(defparameter *example*
  (parseq:parseq 'everyone "abc

a
b
c

ab
ac

a
a
a
a

b"))

(defparameter *everyone* (aoc:input :parse 'everyone))


;;;; Part 1

(defun combine-group-answers (fn group)
  (reduce fn group))

(defun combine-all-group-answers (everyone)
  (mapcar (alexandria:curry #'combine-group-answers #'fset:union) everyone))

(defun get-answer-1 (&optional (everyone *everyone*))
  (reduce #'+ (mapcar #'fset:size (combine-all-group-answers everyone))))


;;;; Part 2

(defun intersect-all-group-answers (everyone)
  (mapcar (alexandria:curry #'combine-group-answers #'fset:intersection) everyone))

(defun get-answer-2 (&optional (everyone *everyone*))
  (reduce #'+ (mapcar #'fset:size (intersect-all-group-answers everyone))))
