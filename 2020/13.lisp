(in-package :aoc-2020-13)

(aoc:define-day 2215 1058443396696792)


;;;; Parsing

(defun condense-schedule (ids &optional (pos 0))
  (cond
    ((endp ids)
     nil)
    ((null (car ids))
     (condense-schedule (cdr ids) (1+ pos)))
    (t
     (cons (cons (car ids) pos)
           (condense-schedule (cdr ids) (1+ pos))))))

(parseq:defrule schedule ()
  (+ bus-id)
  (:lambda (&rest ids) (condense-schedule ids)))

(parseq:defrule bus-id ()
  (and (or aoc:integer-string empty) (? ","))
  (:choose 0))

(parseq:defrule empty ()
  "x"
  (:constant nil))


;;;; Input

(defparameter *example-start* 939)
(defparameter *example-schedule*
  (parseq:parseq 'schedule "7,13,x,x,59,x,31,19"))

(defparameter *start* (parseq:parseq 'aoc:integer-string (first (aoc:input))))
(defparameter *schedule* (parseq:parseq 'schedule (second (aoc:input))))


;;;; Part 1

(defun earliest-bus (start schedule)
  (let* ((bus (car (car schedule)))
         (wait (- bus (mod start bus)))
         (other-buses (cdr schedule)))
    (if (endp other-buses)
        (values bus wait)
        (multiple-value-bind (min-bus min-wait) (earliest-bus start other-buses)
          (if (< wait min-wait)
              (values bus wait)
              (values min-bus min-wait))))))

(defun get-answer-1 (&optional (start *start*) (schedule *schedule*))
  (multiple-value-bind (bus wait) (earliest-bus start schedule)
    (* bus wait)))

(aoc:given 1
  (= (* 59 5) (get-answer-1 *example-start* *example-schedule*)))


;;;; Part 2

(defun combine-buses (bus1 bus2)
  (if (< (car bus1) (car bus2))
      (combine-buses bus2 bus1)
      (destructuring-bind (id1 . delay1) bus1
        (destructuring-bind (id2 . delay2) bus2
          (let ((product (* id1 id2))
                (remainder2 (mod (- id2 delay2) id2)))
            (labels ((find-remainder (multiple1)
                       (let ((remainder (mod multiple1 id2)))
                         (if (= remainder2 remainder)
                             (cons product (- product multiple1))
                             (find-remainder (+ multiple1 id1))))))
              (find-remainder (- id1 delay1))))))))

(defun get-answer-2 (&optional (schedule *schedule*))
  (destructuring-bind (id . delay)
      (reduce #'combine-buses (sort (copy-list schedule) #'> :key #'car))
    (- id delay)))

(aoc:given 2
  (= 1068781 (get-answer-2 *example-schedule*)))
