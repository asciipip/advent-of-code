(in-package :aoc-2020-12)

(aoc:define-day 319 50157)


;;;; Parsing

;;; I could probably operate on a more direct representation of the input
;;; actions, but the following changes will make later operations easier:
;;;
;;; [NSEW]<number> -> (:move . <point>)
;;; [LR]180 -> (:turnaround)
;;; [LR]<number> -> (:turn . <direction>), where <direction> is :left or :right
;;; F<number> -> (:forward . distance)

(parseq:defrule action ()
  (or move turnaround turn forward))

(parseq:defrule move ()
  (and (char "NSEW") aoc:integer-string)
  (:lambda (dir-char distance)
    (cons :move (point:* (ecase dir-char
                           (#\N #.(point:make-point  0  1))
                           (#\S #.(point:make-point  0 -1))
                           (#\E #.(point:make-point  1  0))
                           (#\W #.(point:make-point -1  0)))
                         distance))))

(parseq:defrule turnaround ()
  (and (char "LR") aoc:integer-string)
  (:choose 1)
  (:test (degrees) (= 180 (mod degrees 360)))
  (:constant (cons :turnaround nil)))

(defun parse-turn (dir-char degrees)
  (if (= 270 (mod degrees 360))
      (parse-turn (ecase dir-char
                    (#\L #\R)
                    (#\R #\L))
                  90)
      (cons :turn (ecase dir-char
                    (#\L :left)
                    (#\R :right)))))

(parseq:defrule turn ()
  (and (char "LR") aoc:integer-string)
  (:test (dir-char degrees)
         (declare (ignore dir-char))
         (= 90 (mod degrees 180)))
  (:function #'parse-turn))

(parseq:defrule forward ()
  (and "F" aoc:integer-string)
  (:choose 1)
  (:lambda (distance) (cons :forward distance)))


;;;; Input

(defparameter *example-instructions*
  '("F10" "N3" "F7" "R90" "F11"))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'action)
          *example-instructions*))

(defparameter *instructions* (aoc:input))
(defparameter *actions* (aoc:input :parse-line 'action))


;;;; Part 1

(defparameter *use-waypoint* t)

(defstruct ferry-state
  (position #.(point:make-point 0 0) :type point:point)
  (heading #.(point:make-point 1 0) :type point:point))

(defun change-ferry-state (ferry-state &key position heading)
  (with-slots ((old-position position) (old-heading heading)) ferry-state
    (make-ferry-state :position (or position old-position)
                      :heading (or heading old-heading))))

(defun take-action (ferry-state action)
  (with-slots (position heading) ferry-state
    (destructuring-bind (action-type . action-param) action
      (ecase action-type
        (:move
         (if *use-waypoint*
             (change-ferry-state ferry-state :heading (point:+ heading action-param))
             (change-ferry-state ferry-state :position (point:+ position action-param))))
        (:turnaround
         (change-ferry-state ferry-state :heading (point:* heading -1)))
        (:turn
         (change-ferry-state ferry-state :heading (point:turn heading action-param)))
        (:forward
         (change-ferry-state ferry-state :position (point:+ position (point:* heading action-param))))))))

(defun take-actions (ferry-state actions)
  (if (endp actions)
      ferry-state
      (take-actions (take-action ferry-state (car actions))
                    (cdr actions))))

(defun get-answer-1 (&optional (actions *actions*))
  (let ((*use-waypoint* nil))
    (point:manhattan-distance (ferry-state-position (take-actions (make-ferry-state)
                                                                  actions))
                              #.(point:make-point 0 0))))

(aoc:given 1
  (= 25 (get-answer-1 *example*)))


;;;; Part 2

;;; For part 2, I'll just repurpose the FERRY-STATE's HEADING slot as the
;;; location of the ship's waypoint.

(defun initial-ferry-state ()
  (if *use-waypoint*
      (make-ferry-state :heading #.(point:make-point 10 1))
      (make-ferry-state)))

(defun get-answer-2 (&optional (actions *actions*))
  (point:manhattan-distance
   (ferry-state-position (take-actions (initial-ferry-state)
                                       actions))
   #.(point:make-point 0 0)))

(aoc:given 2
  (= 286 (get-answer-2 *example*)))


;;;; Visualization

(defparameter *text-height* 32)

(defun collect-ferry-positions (actions &optional (ferry-state (initial-ferry-state)) (previous-positions (fset:empty-seq)) (previous-waypoints (fset:empty-seq)))
  (with-slots (position heading) ferry-state
    (if (endp actions)
        (values (fset:with-last previous-positions position)
                (fset:with-last previous-waypoints (point:+ position heading)))
        (let ((next-state (take-action ferry-state (car actions))))
          (collect-ferry-positions (cdr actions)
                                   next-state
                                   (fset:with-last previous-positions position)
                                   (fset:with-last previous-waypoints (point:+ position heading)))))))

(defun ferry-distance (actions)
  (let ((positions (collect-ferry-positions actions)))
    (gmap:gmap :sum
               (lambda (p1 p2)
                 (let ((vector (point:- p2 p1)))
                   (sqrt (+ (expt (point:elt vector 0) 2)
                            (expt (point:elt vector 1) 2)))))
               (:seq positions)
               (:seq (fset:less-first positions)))))

(defun draw-ferry-path (actions)
  (let ((scale (cairo:trans-matrix-xx (cairo:get-trans-matrix)))
        (positions (collect-ferry-positions actions)))
    (cairo:translate 0.5 0.5)
    (cairo:set-line-width (/ 1 scale))
    (viz:set-color :dark-secondary)
    (cairo:move-to 0 0)
    (fset:do-seq (p positions)
      (cairo:line-to (point:elt p 0) (point:elt p 1)))
    (cairo:stroke)))

(defun invert-vertical-position (point)
  (point:make-point (point:elt point 0) (- (point:elt point 1))))

(defun ferry-trans-matrix (actions width height margin)
  (multiple-value-bind (raw-positions raw-waypoints)
      (collect-ferry-positions actions)
    (let* ((all-points (fset:image #'invert-vertical-position
                                   (fset:union (fset:convert 'fset:set raw-positions)
                                               (fset:convert 'fset:set raw-waypoints))))
           (trans-matrix (viz:trans-matrix-for-points (fset:convert 'list all-points)
                                                      width height
                                                      :margin margin)))
      (setf (cairo:trans-matrix-yy trans-matrix)
            (* -1 (cairo:trans-matrix-yy trans-matrix)))
      trans-matrix)))

(defun write-ferry-path-image (actions filename &key (width 1024) (height 1024) (margin 16))
  (cairo:with-png-file (filename :rgb24 width height :surface surface)
    (viz:clear-surface surface :dark-background)
    (cairo:set-trans-matrix (ferry-trans-matrix actions width height margin))
    (draw-ferry-path actions)))

(defun draw-ferry (rotation)
  "ROTATION is in radians."
  (let ((current-scale (cairo:trans-matrix-xx (cairo:get-trans-matrix))))
    (cairo:save)
    (cairo:scale (/ 10 current-scale) (/ 10 current-scale))
    (cairo:rotate rotation)
    (viz:set-color :dark-primary)
    (cairo:move-to 1 0.5)
    (cairo:curve-to 1 0.5
                    2 0
                    1 -0.5)
    (cairo:line-to -1 -0.5)
    (cairo:curve-to -1 -0.5
                    -0.5 -0.5
                    -1 0.5)
    (cairo:close-path)
    (cairo:fill-path)
    (cairo:restore)))

(defun draw-waypoint ()
  (let ((current-scale (cairo:trans-matrix-xx (cairo:get-trans-matrix))))
    (cairo:save)
    (cairo:scale (/ 4 current-scale) (/ 4 current-scale))
    (viz:set-color :dark-secondary)
    (cairo:move-to 2 0)
    (cairo:line-to 0.5 0.5)
    (cairo:line-to 0 2)
    (cairo:line-to -0.5 0.5)
    (cairo:line-to -2 0)
    (cairo:line-to -0.5 -0.5)
    (cairo:line-to 0 -2)
    (cairo:line-to 0.5 -0.5)
    (cairo:close-path)
    (cairo:fill-path)
    (cairo:restore)))

(defun vector-angle (point)
  (atan (point:elt point 1)
        (point:elt point 0)))

(defparameter *draw-waypoint* nil)

(defun draw-ferry-state (ferry-state &optional (rotate 0))
  (with-slots (position heading) ferry-state
    (cairo:save)
    (cairo:translate (+ 0.5 (point:elt position 0))
                     (+ 0.5 (point:elt position 1)))
    (draw-ferry (+ rotate (vector-angle heading)))
    (when *draw-waypoint*
      (cairo:translate (point:elt heading 0)
                       (point:elt heading 1))
      (draw-waypoint))
    (cairo:restore)))

(defun output-frame (video base-image ferry-state trans-matrix &key (rotate 0))
  (let ((frame (viz:clone-surface base-image)))
    (cairo:with-context-from-surface (frame)
      (cairo:set-trans-matrix trans-matrix)
      (draw-ferry-state ferry-state rotate))
    (viz:write-video-surface video frame)))

(defun move-ferry-forward (ferry-state distance)
  (with-slots (position heading) ferry-state
    (let* ((unit-heading (point:unit-vector heading))
           (new-position (point:+ position (point:* unit-heading distance))))
      (change-ferry-state ferry-state :position new-position))))

(defun draw-forward (video base-image ferry-state target speed start-state trans-matrix)
  (draw-path base-image start-state ferry-state trans-matrix)
  (with-slots (position) ferry-state
    (if (< (point:norm (point:- target position)) (* 0.75 speed))
        (output-frame video base-image (change-ferry-state ferry-state :position target) trans-matrix)
        (progn
          (output-frame video base-image ferry-state trans-matrix)
          (draw-forward video base-image (move-ferry-forward ferry-state speed) target speed start-state trans-matrix)))))

(defun draw-rotation (video base-image target-state current-angle speed trans-matrix)
  (with-slots (heading) target-state
    (let ((target-bearing (vector-angle heading)))
      (if (< (abs (- target-bearing current-angle)) (* 0.75 speed))
          (output-frame video base-image target-state trans-matrix)
          (let* ((rotation (- current-angle target-bearing))
                 (increment (if (minusp rotation)
                                speed
                                (- speed))))
            (output-frame video base-image target-state trans-matrix :rotate rotation)
            (draw-rotation video base-image target-state (+ current-angle increment) speed trans-matrix))))))

(defun draw-instructions (base-image last-instructions next-instructions)
  (let ((image (viz:clone-surface base-image))
        (height (cairo:image-surface-get-height base-image))
        (line-height (* *text-height* 1.2))
        (line-fudge (* *text-height* 0.2))
        (text-left 16))
    (cairo:with-context-from-surface (image)
      (viz:set-color :dark-primary)
      (viz:draw-text (car next-instructions) text-left (/ height 2) :size *text-heght* :vertical :middle)
      (iter (for instruction in (cdr next-instructions))
            (for top from (+ (/ height 2) (/ line-height 2) line-fudge) by line-height)
            (for alpha from 1 downto 0 by 0.1)
            (viz:set-color :dark-secondary :alpha alpha)
            (viz:draw-text instruction text-left top :size *text-heght* :vertical :top))
      (iter (for instruction in last-instructions)
            (for bottom downfrom (- (/ height 2) (/ line-height 2) line-fudge) by line-height)
            (for alpha from 1 downto 0 by 0.1)
            (viz:set-color :dark-secondary :alpha alpha)
            (viz:draw-text instruction text-left bottom :size *text-heght* :vertical :bottom)))
    image
    ))

(defun draw-action (video base-image last-instructions next-instructions ferry-state action distance-per-frame radians-per-frame trans-matrix)
  (let ((instruction-surface (draw-instructions base-image last-instructions next-instructions)))
    (with-slots (position heading) ferry-state
      (destructuring-bind (action-type . action-param) action
        (ecase action-type
          (:forward
           (let ((target (point:+ position (point:* heading action-param))))
             (draw-forward video instruction-surface ferry-state target distance-per-frame ferry-state trans-matrix)))
          ((:move :turn :turnaround)
           (let* ((new-waypoint (ecase action-type
                                  (:move (point:+ heading action-param))
                                  (:turn (point:turn heading action-param))
                                  (:turnaround (point:* heading -1))))
                  (new-state (change-ferry-state ferry-state :heading new-waypoint))
                  (old-angle (vector-angle heading)))
             (draw-rotation video instruction-surface new-state old-angle radians-per-frame trans-matrix))))))))

(defun draw-outro (video base-image ferry-state frames trans-matrix)
  (let ((surface (viz:clone-surface base-image)))
    (cairo:with-context-from-surface (surface)
      (cairo:set-trans-matrix trans-matrix)
      (draw-ferry-state ferry-state))
    (dotimes (i frames)
      (viz:write-video-surface video surface))))

(defun draw-path (image start-state end-state trans-matrix)
  (cairo:with-context-from-surface (image)
    (cairo:set-trans-matrix trans-matrix)
    (viz:set-color :red)
    (cairo:set-line-cap :round)
    (cairo:set-line-width (/ 5 (cairo:trans-matrix-xx (cairo:get-trans-matrix))))
    (with-slots (position) start-state
      (cairo:move-to (+ 0.5 (point:elt position 0)) (+ 0.5 (point:elt position 1))))
    (with-slots (position) end-state
      (cairo:line-to (+ 0.5 (point:elt position 0)) (+ 0.5 (point:elt position 1))))
    (cairo:stroke)))

;;; Let's make some assumptions here.  My ferry travels about 550,000
;;; units of distance.  Let's say that those are feet, so that translates
;;; into about 107 miles.  A ferry's top speed might be around 30 mph, so
;;; that's about 3.6 hours.  With a 300:1 speedup, that gives us a 43
;;; second video.  So the video speed needs to be 13,200 units per second.
;;;
;;; We also need to consider rotational speed.  Ships don't really rotate
;;; in place, but we're going to pretend this ferry can.  Normal ship
;;; rates of turn can vary from 0 to 60 degrees per minute (with the
;;; actual rate depending on speed, and 60 being an extreme outlier).
;;; Let's say our ferry can do 15 degrees per minute.  That's 1/4 of a
;;; degrees per second, which, with our 300:1 speedup, is 75 degrees per
;;; video second.  Unfortunately, that ends up being really slow relative
;;; to the horizontal travel speed.  Running the rotation at four times
;;; the horizontal rate comes out a bit better.
;;;
;;; The example's distance is only ~300 units, which is short enough to
;;; not need a speedup.  Its parameters should then be a forward speed of
;;; 44 and a rotational speed of 1/4.  As before, the rotational speed
;;; benefits from a boost.  A value of 75 seems reasonable, but using the
;;; same value as the full example works, too.

(defun write-ferry-video (instructions filename &key (width 1920) (height 1080) (margin 16) (fps 60) (outro 10) (speed (* 44 300)) (rot (* 1/4 1200)))
  "SPEED is units traveled per video second.  ROT is degrees turned per
  video second."
  (viz:with-video (video (viz:create-video filename width height :fps fps))
    (let* ((actions (mapcar (alexandria:curry #'parseq:parseq 'action) instructions))
           (trans-matrix (ferry-trans-matrix actions width height margin))
           (base-image (viz:create-video-surface video)))
      (viz:clear-surface base-image :dark-background)
      (iter (for ferry-state
                 first (initial-ferry-state)
                 then (take-action ferry-state action))
            (for last-state previous ferry-state)
            (when last-state
              (draw-path base-image last-state ferry-state trans-matrix))
            (output-frame video base-image ferry-state trans-matrix)
            (for last-instructions first nil then (cons instruction last-instructions))
            (for instruction in instructions)
            (for next-instructions on instructions)
            (for action in actions)
            (for i from 0)
            (format t "~A: ~A~%" i action)
            (draw-action video
                         base-image
                         last-instructions
                         next-instructions
                         ferry-state
                         action
                         (/ speed fps)
                         (/ (* pi (/ rot fps)) 180)
                         trans-matrix)
            ;; The frames for the video don't get properly
            ;; garbage-collected for some reason.
            (sb-ext:gc)
            (finally (draw-outro video base-image ferry-state (* outro fps) trans-matrix))))))
