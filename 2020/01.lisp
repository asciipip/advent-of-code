(in-package :aoc-2020-01)

(aoc:define-day 1018336 288756720)


;;;; Input

(defparameter *example*
  '(1721
    979
    366
    299
    675
    1456))
(defparameter *input* (aoc:input :parse-line 'aoc:integer-string))


;;;; Part 1

(define-condition value-found (condition)
  ((subset :initarg :subset :reader subset)))

(defun get-answer-1 (&key (input *input*) (subset-size 2) (target-sum 2020))
  (handler-case
      (aoc:visit-subsets (lambda (subset)
                           (when (= target-sum (reduce #'+ subset))
                             (signal 'value-found :subset subset)))
                         input
                         subset-size)
    (value-found (vf) (reduce #'* (subset vf)))))

(aoc:given 1
  (= 514579 (get-answer-1 :input *example*)))


;;;; Part 2

(defun get-answer-2 (&key (input *input*))
  (get-answer-1 :input input :subset-size 3))

(aoc:given 2
  (= 241861950 (get-answer-2 :input *example*)))
