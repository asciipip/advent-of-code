(in-package :aoc-2020-23)

(aoc:define-day "78569234" 565615814504)


;;;; Parsing

(parseq:defrule cups ()
  (and (rep 9 cup) (? #\Newline))
  (:choose 0)
  (:lambda (&rest cups) (fset:convert 'fset:seq cups)))

(parseq:defrule cup ()
  digit
  (:string)
  (:function #'parse-integer))


;;;; Input

(defparameter *example* (parseq:parseq 'cups "389125467"))

(defparameter *cups* (aoc:input :parse 'cups))


;;;; Part 1

;; Assumptions based on the data
(defparameter *min-cup* 1)
(defparameter *max-cup* 9)

(defparameter *take-cups* 3)

(defstruct game-state
  current
  pointers)

(defun cup-seq-to-vector (cup-sequence)
  (let* ((max-cup (fset:reduce #'max cup-sequence))
         (result (make-array (1+ max-cup))))
    ;; Hack to map across the sequence's adjacent pairs.
    (gmap:gmap :and
               (lambda (from to)
                 (setf (svref result from) to))
               (:seq cup-sequence)
               (:seq (aoc:rotate-seq cup-sequence)))
    result))

(defun init-game-state (cup-sequence)
  (make-game-state :current (fset:first cup-sequence)
                   :pointers (cup-seq-to-vector cup-sequence)))

(defun print-game-state (game-state &optional (stream t))
  (with-slots (current pointers) game-state
    (labels ((print-r (cup)
               (when (/= cup current)
                 (format stream " ~A" cup)
                 (print-r (svref pointers cup)))))
      (princ current stream)
      (print-r (svref pointers current))
      (princ #\Newline)))
  (values))

(defun make-cup-current (cup game-state)
  (with-slots (pointers) game-state
    (make-game-state :current cup
                     :pointers pointers)))

(defun nth-cup-from (cup nth pointers)
  (if (zerop nth)
      cup
      (nth-cup-from (svref pointers cup) (1- nth) pointers)))

(defun move (game-state)
  (with-slots (current pointers) game-state
    (let ((next-current (nth-cup-from current (1+ *take-cups*) pointers))
          (destination-cup (find-destination (1- current) game-state)))
      (move-cups! destination-cup next-current game-state)
      (make-game-state :current next-current
                       :pointers pointers))))

(defun find-destination (candidate game-state)
  (if (< candidate *min-cup*)
      (find-destination *max-cup* game-state)
      (with-slots (current pointers) game-state
        (labels ((find-r (steps moving-cup)
                   (cond
                     ((< *take-cups* steps)
                      candidate)
                     ((= candidate moving-cup)
                      (find-destination (1- candidate) game-state))
                     (t
                      (find-r (1+ steps) (nth-cup-from moving-cup 1 pointers))))))
          (find-r 1 (nth-cup-from current 1 pointers))))))

(defun move-cups! (destination-cup next-current game-state)
  (with-slots (current pointers) game-state
    ;; Point moved tail at new destination
    (setf (svref pointers (nth-cup-from current *take-cups* pointers))
          (svref pointers destination-cup))
    ;; Insert moved cups at appropriate position
    (setf (svref pointers destination-cup) (nth-cup-from current 1 pointers))
    ;; Skip over moved cups
    (setf (svref pointers current) next-current))
  (values))

(defun make-moves (cups moves)
  (if (zerop moves)
      cups
      (make-moves (move cups) (1- moves))))

(defun get-answer-1 (&optional (cups *cups*) (moves 100))
  (let ((final-state (make-moves (init-game-state cups) moves)))
    (with-slots (pointers) final-state
      (format nil "~{~A~}" (mapcar (lambda (n)
                                     (nth-cup-from 1 n pointers))
                                   (alexandria:iota (- *max-cup* *min-cup*)
                                                    :start *min-cup*))))))

(aoc:given 1
  (string= "92658374" (get-answer-1 *example* 10))
  (string= "67384529" (get-answer-1 *example*)))


;;;; Part 2

(defun last-cup (game-state)
  (with-slots (current pointers) game-state
    (position current pointers)))

(defun augment-cups (game-state max-cup)
  (with-slots (current pointers) game-state
    (let ((last-cup (last-cup game-state))
          (new-pointers (make-array (1+ max-cup))))
      (do ((cup 1 (1+ cup)))
          ((<= (length pointers) cup))
        (setf (svref new-pointers cup) (svref pointers cup)))
      (setf (svref new-pointers last-cup) (length pointers))
      (do ((cup (1+ (length pointers)) (1+ cup)))
          ((< max-cup cup))
        (setf (svref new-pointers (1- cup)) cup))
      (setf (svref new-pointers max-cup) current)
      (make-game-state :current current
                       :pointers new-pointers))))

(defun get-answer-2 (&key (cups *cups*) (max-cup 1000000) (moves 10000000))
  (let* ((*max-cup* max-cup)
         (final-state (make-moves (augment-cups (init-game-state cups) max-cup) moves)))
    (with-slots (pointers) final-state
      (* (nth-cup-from 1 1 pointers)
         (nth-cup-from 1 2 pointers)))))

(aoc:given 2
  (= 149245887792 (get-answer-2 :cups *example*)))
