(in-package :aoc-2020-18)

(aoc:define-day 31142189909908 323912478287549)


;;;; Parsing

(parseq:defrule expression ()
  (or compound-expression
      value))

(parseq:defrule compound-expression ()
  (and value " " operator " " expression)
  (:choose 0 2 4)
  (:lambda (value operator rest)
    (if (consp rest)
        (cons value (cons operator rest))
        (list value operator rest)) ))

(parseq:defrule operator ()
  (char "+*")
  (:string)
  (:function #'intern))

(parseq:defrule value ()
  (or aoc:integer-string parenthesized-expression))

(parseq:defrule parenthesized-expression ()
  (and "(" expression ")")
  (:choose 1)
  (:lambda (&rest e) (list e)))


;;;; Input

(defparameter *example-71*
  (parseq:parseq 'expression "1 + 2 * 3 + 4 * 5 + 6"))

(defparameter *example-51*
  (parseq:parseq 'expression "1 + (2 * 3) + (4 * (5 + 6))"))

(defparameter *input* (aoc:input :parse-line 'expression))


;;;; Part 1

(defparameter *default-precedence* (list (fset:set '+ '*)))

(defun evaluate-operators (precedence operators expression)
  (if (endp (cdr expression))
      expression
      (destructuring-bind (e1 op e2 &rest rest) expression
        (if (fset:member? op operators)
            (evaluate-operators precedence
                                operators
                                (cons (funcall op
                                               (evaluate-infix precedence e1)
                                               (evaluate-infix precedence e2))
                                      rest))
            (cons e1 (cons op (evaluate-operators precedence operators (cons e2 rest))))))))

(defun apply-operator-precedence (precedence expression)
  (labels ((apply-r (remaining-precedence reduced-expression)
             (if (endp remaining-precedence)
                 reduced-expression
                 (apply-r (cdr remaining-precedence)
                          (evaluate-operators precedence
                                              (car remaining-precedence)
                                              reduced-expression)))))
    (apply-r precedence expression)))

(defun evaluate-infix (precedence expression)
  (cond
    ((not (consp expression))
     expression)
    ((endp (cdr expression))
     (evaluate-infix precedence (car expression)))
    (t
     (evaluate-infix precedence (apply-operator-precedence precedence expression)))))

(defun get-answer-1 (&optional (input *input*))
  (reduce #'+ (mapcar (alexandria:curry #'evaluate-infix *default-precedence*) input)))

(aoc:given 1
  (= 71 (get-answer-1 (list *example-71*)))
  (= 51 (get-answer-1 (list *example-51*))))


;;;; Part 2

(defparameter *advanced-precedence* (list (fset:set '+) (fset:set '*)))

(defun get-answer-2 (&optional (input *input*))
  (reduce #'+ (mapcar (alexandria:curry #'evaluate-infix *advanced-precedence*) input)))
