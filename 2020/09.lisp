(in-package :aoc-2020-09)

(aoc:define-day 27911108 4023754)


;;;; Input

(defparameter *example-5*
  (vector 35 20 15 25 47 40 62 55 65 95 102 117 150 182 127 219 299 277 309 576))
(defparameter *input* (coerce (aoc:input :parse-line 'aoc:integer-string)
                              'vector))


;;;; Part 1

(defun valid-p (prefix n)
  (flet ((test-pair (pair)
           (when (= n (fset:reduce #'+ pair))
             (return-from valid-p pair))))
    (aoc:visit-subsets #'test-pair prefix 2)
    nil))

(defun find-first-invalid-number (numbers preamble-size)
  (iter (for n in-vector numbers with-index i from preamble-size)
        (for valid = (valid-p (aoc:vector-shared-subseq numbers (- i preamble-size) i)
                              n))
        (finding n such-that (not valid))))

(defun get-answer-1 ()
  (find-first-invalid-number *input* 25))


;;;; Part 2

(defun find-contiguous-summing-subset (numbers target)
  (labels ((find-r (lower-bound upper-bound total)
             (cond
               ((= total target)
                (aoc:vector-shared-subseq numbers lower-bound upper-bound))
               ((< total target)
                (find-r lower-bound
                        (1+ upper-bound)
                        (+ total (aref numbers upper-bound))))
               ((> total target)
                (find-r (1+ lower-bound)
                        upper-bound
                        (- total (aref numbers lower-bound)))))))
    (find-r 0 0 0)))

(defun get-answer-2 (&optional (numbers *input*) (preamble-size 25))
  (let* ((invalid-number (find-first-invalid-number numbers preamble-size))
         (contiguous-set (find-contiguous-summing-subset numbers invalid-number)))
    (+ (reduce #'min contiguous-set) (reduce #'max contiguous-set))))
