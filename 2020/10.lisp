(in-package :aoc-2020-10)

(aoc:define-day 2232 173625106649344)


;;;; Parsing

(defun adapter-bag-to-array (adapter-list)
  (let ((result (make-array (1+ (reduce #'max adapter-list))
                            :element-type 'bit
                            :initial-element 0)))
    (setf (sbit result 0) 1)
    (dolist (adapter adapter-list)
      (setf (sbit result adapter) 1))
    result))


;;;; Input

(defparameter *example-1*
  (adapter-bag-to-array '(16 10 15 5 1 11 7 19 6 12 4)))

(defparameter *example-2*
  (adapter-bag-to-array '(28 33 18 42 31 14 46 20 48 47 24 23 49 45 19 38 39 11 1 32 25 35 8 17 7 9 4 2 34 10 3 )))

(defparameter *adapters*
  (adapter-bag-to-array (aoc:input :parse-line 'aoc:integer-string)))

;; For the challenge from https://www.reddit.com/r/adventofcode/comments/kaenbn/
(defparameter *evil* (make-array 0 :element-type 'bit))
(defmacro do-file-lines ((line filename) &body body)
  "The body is run ones for each line of the file, with LINE bound to a string containing that line."
  (alexandria:with-gensyms (file)
    `(with-open-file (,file ,filename)
       (do ((,line (read-line ,file) (read-line ,file nil nil)))
           ((null ,line))
         ,@body))))
(defun load-evil (filename)
  (let ((max-adapter 0))
    (do-file-lines (line filename)
      (let ((adapter (parse-integer line)))
        (when (< max-adapter adapter)
          (setf max-adapter adapter))))
    (setf *evil* (make-array (1+ max-adapter) :element-type 'bit :initial-element 0)))
  (setf (sbit *evil* 0) 1)
  (do-file-lines (line filename)
    (let ((adapter (parse-integer line)))
      (setf (sbit *evil* adapter) 1)))
  (values))


;;;; Part 1

(defun count-joltage-differences (adapters)
  (labels ((count-r (last-adapter cur-position results)
             (cond
               ((<= (length adapters) cur-position)
                ;; Add a 3 for the final device connection.
                (fset:with results 3))
               ((plusp (sbit adapters cur-position))
                (count-r cur-position (1+ cur-position) (fset:with results (- cur-position last-adapter))))
               (t
                (count-r last-adapter (1+ cur-position) results)))))
    (count-r 0 1 (fset:empty-bag))))

(defun get-answer-1 (&optional (adapters *adapters*))
  (let ((differences (count-joltage-differences adapters)))
    (assert (fset:empty? (fset:set-difference (fset:convert 'fset:set differences)
                                              (fset:set 1 2 3))))
    (* (fset:multiplicity differences 1) (fset:multiplicity differences 3))))

(aoc:given 1
  (= (* 7 5) (get-answer-1 *example-1*))
  (= (* 22 10) (get-answer-1 *example-2*)))


;;;; Part 2

(defun count-arrangements (adapters max-span)
  (declare (type (simple-bit-vector) adapters)
           (type (integer 0 #.(1- array-total-size-limit)) max-span)
           (optimize (speed 3)))
  (let ((previous-counts (make-array max-span :initial-element 0)))
    (labels ((pc-index (adapter)
               (declare (type (integer 0 #.(1- array-total-size-limit)) adapter))
               (mod adapter max-span))
             (add-previous-values (adapter offset total)
               (declare (type (integer 0 #.(1- array-total-size-limit)) adapter offset)
                        (type (integer 0) total))
               (let ((successor (+ adapter offset)))
                 (declare (type (integer 0 #.(1- array-total-size-limit)) successor))
                 (if (and (<= offset max-span)
                          (< successor (length adapters)))
                     (add-previous-values
                      adapter
                      (1+ offset)
                      (if (plusp (sbit adapters successor))
                          (if (plusp total)
                              (+ total (svref previous-counts (pc-index successor)))
                              (svref previous-counts (pc-index successor)))
                          total))
                     total)))
             (count-r (adapter)
               (declare (type (integer 0 #.(1- array-total-size-limit)) adapter))
               (if (zerop adapter)
                   (add-previous-values adapter 1 0)
                   (progn
                     (setf (svref previous-counts (pc-index adapter))
                           (if (plusp (sbit adapters adapter))
                               (add-previous-values adapter 1 0)
                               0))
                     (count-r (1- adapter))))))
      (declare (inline pc-index))
      (setf (svref previous-counts (pc-index (1- (length adapters)))) 1)
      (count-r (- (length adapters) 2)))))

(defun get-answer-2 (&optional (adapters *adapters*))
  (count-arrangements adapters 3))

(aoc:given 2
  (= 8 (get-answer-2 *example-1*))
  (= 19208 (get-answer-2 *example-2*)))

(defun strip-trailing-zeroes (n)
  (multiple-value-bind (q r) (truncate n 10)
    (if (zerop r)
        (strip-trailing-zeroes q)
        n)))

(defun get-answer-evil (&optional (adapters *evil*))
  (let ((result (strip-trailing-zeroes (get-answer-2 adapters))))
    (mod result (expt 10 10))))
