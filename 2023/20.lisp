(in-package :aoc-2023-20)

;;; Part 2 is 229414480926893, which I calculated somewhat directly from
;;; the structure of the modules.
(aoc:define-day 898731036 nil)


;;; Data Structures

(defclass module ()
  ((name
    :initarg :name
    :reader module-name)
   (targets
    :initarg :targets
    :initform nil
    :reader module-targets)))

(defmethod fset:compare ((m1 module) (m2 module))
  (fset:compare-slots m1 m2 'name))

(defmethod print-object ((module module) stream)
  (with-slots (name targets) module
    (print-unreadable-object (module stream :type t)
      (format stream "~A -> ~{~A~^, ~}" name targets))))

(defclass broadcast (module)
  ())

(defclass output (module)
  ((pulses
    :initarg :pulses
    :initform (fset:empty-bag)
    :reader output-pulses)))

(defmethod print-object ((module output) stream)
  (with-slots (name targets pulses) module
    (print-unreadable-object (module stream :type t)
      (format stream "[~A] ~A" pulses name))))

(defun add-output (output pulse)
  (with-slots (name targets pulses) output
    (make-instance 'output
                   :name name
                   :targets targets
                   :pulses (fset:with pulses pulse))))

(defclass flip-flop (module)
  ((state
    :initarg :state
    :initform nil
    :reader flip-flop-state)))

(defmethod print-object ((module flip-flop) stream)
  (with-slots (name targets) module
    (print-unreadable-object (module stream :type t)
      (format stream "[~A] ~A -> ~{~A~^, ~}" (flip-flop-state module) name targets))))

(defun toggle-flip-flop (module)
  (with-slots (name targets state) module
    (make-instance 'flip-flop
                   :name name
                   :targets targets
                   :state (not state))))

(defclass conjunction (module)
  ((last-inputs
    :initarg :last-inputs
    :initform (fset:empty-map)
    :reader conjunction-inputs)))

(defun compact-conjunction-inputs-string (conjunction)
  (with-slots (last-inputs) conjunction
    (format nil "~{~A~^ ~}"
            (reverse
             (fset:reduce (lambda (result name value)
                            (cons (format nil "~A:~A" name value) result))
                          last-inputs
                          :initial-value nil)))))

(defmethod print-object ((module conjunction) stream)
  (with-slots (name targets) module
    (print-unreadable-object (module stream :type t)
      (format stream "[~A] ~A -> ~{~A~^, ~}"
              (compact-conjunction-inputs-string module) name targets))))

(defun conjunction-remember-pulse (conjunction source pulse)
  (with-slots (name targets last-inputs) conjunction
    (make-instance 'conjunction
                   :name name
                   :targets targets
                   :last-inputs (fset:with last-inputs source pulse))))


;;; Parsing

(parseq:defrule module ()
    (and (? (char "%&")) module-name " -> " (aoc:comma-list module-name))
  (:choose 0 1 3)
  (:lambda (type-symbol name targets)
    (let ((class
            (cond
              (type-symbol
               (ecase type-symbol
                 (#\% 'flip-flop)
                 (#\& 'conjunction)))
              ((string= name "broadcaster")
               'broadcast)
              ((string= name "output")
               'output)
              (t (assert nil)))))
      (make-instance class :name name :targets targets))))

(parseq:defrule module-name ()
    (+ (char "a-z"))
  (:string))

(defun add-missing-modules (module-map)
  (let* ((all-module-names (fset:reduce (lambda (result module)
                                          (fset:union (fset:convert 'fset:set (module-targets module))
                                                      result))
                                        (fset:range module-map)
                                        :initial-value (fset:empty-set))))
    (fset:reduce (lambda (result name)
                   (fset:with result name (make-instance 'output :name name)))
                 (fset:set-difference all-module-names (fset:domain module-map))
                 :initial-value module-map)))

(defun add-conjunction-states (module-map)
  (let ((target-map (fset:reduce
                     (lambda (result module)
                       (reduce (lambda (inner-result target)
                                 (aoc:map-set-with inner-result target (module-name module)))
                               (module-targets module)
                               :initial-value result))
                     (fset:range module-map)
                     :initial-value (fset:empty-map (fset:empty-set)))))
    (fset:image (lambda (key module)
                  (values key
                          (if (typep module 'conjunction)
                              (with-slots (name targets) module
                                (make-instance 'conjunction
                                               :name name
                                               :targets targets
                                               :last-inputs (fset:reduce (lambda (result target-name)
                                                                           (fset:with result target-name nil))
                                                                         (fset:lookup target-map name)
                                                                         :initial-value (fset:empty-map))))
                              module)))
                module-map)))

(defun combine-modules (modules)
  (let ((module-map (reduce (lambda (result module)
                              (fset:with result (module-name module) module))
                            modules
                            :initial-value (fset:empty-map))))
    (add-missing-modules (add-conjunction-states module-map))))


;;; Input

(defparameter *input* (combine-modules (aoc:input :parse-line 'module)))
(defparameter *example-1* (combine-modules (aoc:input :parse-line 'module :file "examples/20-1.txt")))
(defparameter *example-2* (combine-modules (aoc:input :parse-line 'module :file "examples/20-2.txt")))


;;; Part 1

(defparameter *low-pulse-count* 0)
(defparameter *high-pulse-count* 0)

(defun outgoing-pulse-seq (module pulse)
  (fset:image (lambda (module-name) (list (module-name module) module-name pulse))
              (fset:convert 'fset:seq (module-targets module))))

(defgeneric module-send-pulse (module sender pulse)
  (:documentation "Returns two values: the new module state, and a sequence of pulses to
   enqueue.  Each enqueued pulse will be a list of (source-module-name
   target-module-name value)."))

(defmethod module-send-pulse ((module broadcast) sender pulse)
  (declare (ignore sender))
  (values module (outgoing-pulse-seq module pulse)))

(defmethod module-send-pulse ((module output) sender pulse)
  (declare (ignore sender))
  (values (add-output module pulse)
          (fset:empty-seq)))

(defmethod module-send-pulse ((module flip-flop) sender pulse)
  (declare (ignore sender))
  (if pulse
      (values module (fset:empty-seq))
      (let ((new-module (toggle-flip-flop module)))
        (values new-module (outgoing-pulse-seq new-module (flip-flop-state new-module))))))

(defmethod module-send-pulse ((module conjunction) sender pulse)
  (let* ((new-module (conjunction-remember-pulse module sender pulse))
         (remembered-inputs (fset:range (conjunction-inputs new-module)))
         (outgoing-pulse (not (and (= 1 (fset:size remembered-inputs))
                                   (fset:arb remembered-inputs)))))
    (values new-module (outgoing-pulse-seq module outgoing-pulse))))

(defun send-pulse (modules source-module-name target-module-name pulse)
  "Returns two values: the new set of modules, and a sequence of pulses to
  be enqueued for sending."
  (let ((module (fset:lookup modules target-module-name)))
    (multiple-value-bind (new-module queue) (module-send-pulse module source-module-name pulse)
      (values (if (eq module new-module)
                  modules
                  (fset:with modules target-module-name new-module))
              queue))))

(defun send-pulses (modules queue)
  (if (fset:empty? queue)
      modules
      (destructuring-bind (source-module-name target-module-name pulse) (fset:first queue)
        (if pulse
            (incf *high-pulse-count*)
            (incf *low-pulse-count*))
        (multiple-value-bind (new-modules additional-queue)
            (send-pulse modules source-module-name target-module-name pulse)
          (send-pulses new-modules (fset:concat (fset:less-first queue) additional-queue))))))

(defun push-button (modules &optional (count 1))
  (if (zerop count)
      modules
      (push-button (send-pulses modules (fset:seq (list "button" "broadcaster" nil)))
                   (1- count))))

(defun get-answer-1 (&optional (modules *input*))
  (let ((*low-pulse-count* 0)
        (*high-pulse-count* 0))
    (push-button modules 1000)
    (* *low-pulse-count* *high-pulse-count*)))

(aoc:given 1
  (= 32000000 (get-answer-1 *example-1*))
  (= 11687500 (get-answer-1 *example-2*)))


;;; Part 2

;; Too slow
(defun find-low-rx-presses (modules &optional (count 0))
  (let ((output-bag (output-pulses (fset:lookup modules "rx"))))
    (when (zerop (mod count 100000))
      (:printv :ts output-bag count))
    (if (plusp (fset:multiplicity output-bag nil))
        count
        (find-low-rx-presses (push-button modules) (1+ count)))))

;(lcm #b111100100101 #b111011010101 #b111100000111 #b111111010011)

;; TODO: Work backwards from "rx" to the four inputs that all need to be
;; high; calculate their periods; LCM the periods.


;;;; Visualization

(defun modules-to-dot (modules stream)
  (format stream "digraph modules {~%")
  (fset:do-set (module (fset:range modules))
    (format stream "  ~A -> {~{~A~^ ~}}~%" (module-name module) (module-targets module)))
  (format stream "}~%"))
