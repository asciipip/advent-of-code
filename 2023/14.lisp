(in-package :aoc-2023-14)

;; Part two is 89845, but it took about an hour.
(aoc:define-day 110565 nil)


;;; Parsing

(defun parse-line (line row)
  (reduce (lambda (result col)
            (ecase (schar line col)
              (#\O (fset:with result (point:make-point col row) :round))
              (#\# (fset:with result (point:make-point col row) :cube))
              (#\. result)))
          (alexandria:iota (length line))
          :initial-value (fset:empty-map)))

(defun parse-lines (lines)
  (reduce #'fset:map-union (mapcar #'parse-line lines (alexandria:iota (length lines)))))


;;; Input

(defparameter *rocks* (parse-lines (aoc:input)))
(defparameter *example*
  (parse-lines '("O....#...."
                 "O.OO#....#"
                 ".....##..."
                 "OO.#O....O"
                 ".O.....O#."
                 "O.#..O.#.#"
                 "..O..#O..O"
                 ".......O.."
                 "#....###.."
                 "#OO..#....")))


;;; Part 1

(defun out-of-bounds (points point)
  (multiple-value-bind (min-point max-point) (point:bbox points)
    (or (< (point:x point) (point:x min-point))
        (< (point:y point) (point:y min-point))
        (< (point:x max-point) (point:x point))
        (< (point:y max-point) (point:y point)))))

(defun next-rock (rocks vector start)
  "Returns two values: the rock and its position (or NIL and NIL if there are no more rocks)"
  (if (fset:lookup rocks start)
      (values (fset:lookup rocks start)
              start)
      (if (out-of-bounds (fset:domain rocks) start)
          (values nil nil)
          (next-rock rocks vector (point:+ start vector)))))

(defun fill-row (rocks vector start)
  (multiple-value-bind (next-rock next-pos) (next-rock rocks vector start)
    (if (null next-rock)
        rocks
        (ecase next-rock
          (:cube
           (fill-row rocks vector (point:+ next-pos vector)))
          (:round
           (fill-row (fset:with (fset:less rocks next-pos)
                                start next-rock)
                     vector
                     (point:+ start vector)))))))

(defun fill-rows (rocks row-vector vector start)
  (if (out-of-bounds (fset:domain rocks) start)
      rocks
      (fill-rows (fill-row rocks vector start)
                 row-vector
                 vector
                 (point:+ start row-vector))))

(defun tilt-rocks (rocks direction)
  (multiple-value-bind (min-point max-point) (point:bbox (fset:domain rocks))
    (ecase direction
      (:north (fill-rows rocks #< 1  0> #< 0  1> min-point))
      (:south (fill-rows rocks #<-1  0> #< 0 -1> max-point))
      (:west  (fill-rows rocks #< 0  1> #< 1  0> min-point))
      (:east  (fill-rows rocks #< 0 -1> #<-1  0> max-point)))))

(defun calculate-load (rocks)
  (let ((result 0)
        (max-point (nth-value 1 (point:bbox (fset:domain rocks)))))
    (fset:do-map (point rock rocks)
      (when (eq rock :round)
        (incf result (1+ (point:y (point:- max-point point))))))
    result))

(defun get-answer-1 (&optional (rocks *rocks*))
  (calculate-load (tilt-rocks rocks :north)))


;;; Part 2

(defun spin (rocks)
  (tilt-rocks (tilt-rocks (tilt-rocks (tilt-rocks rocks
                                                  :north)
                                      :west)
                          :south)
              :east))

(defun spin-multiple (rocks count &optional (steps 0) (seen (fset:empty-map)))
  (if (zerop count)
      rocks
      (let ((last-cycle (fset:lookup seen rocks)))
        (if last-cycle
            (spin-multiple rocks (mod count (- steps last-cycle)))
            (spin-multiple (spin rocks)
                           (1- count)
                           (1+ steps)
                           (fset:with seen rocks steps))))))

(defun get-answer-2 (&optional (rocks *rocks*))
  (calculate-load (spin-multiple rocks 1000000000)))


;;; Visualization

(defun draw-rocks (rocks)
  (aoc:draw-map-svg-grid rocks
                         :key (lambda (rock)
                                (when rock
                                  (ecase rock
                                    (:round '(:shape :circle))
                                    (:cube '(:shape :square)))))))
