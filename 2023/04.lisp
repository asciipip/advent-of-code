(in-package :aoc-2023-04)

(aoc:define-day 21821 5539496)


;;; Parsing

(parseq:defrule cards ()
    (+ card)
  (:lambda (&rest cards)
    (fset:reduce #'fset:map-union cards)))

(parseq:defrule card ()
    (and "Card" (+ " ") aoc:integer-string ":" number-list " |" number-list (? #\Newline))
  (:choose 2 4 6)
  (:lambda (id winners card-numbers)
    (fset:map (id (list winners card-numbers)))))

(parseq:defrule number-list ()
    (+ (and (+ " ") aoc:integer-string))
  (:lambda (&rest matches)
    (fset:convert 'fset:set (mapcar #'second matches))))


;;; Input

(defparameter *cards* (aoc:input :parse 'cards))
(defparameter *example*
  (fset:reduce #'fset:map-union
               (mapcar (alexandria:curry #'parseq:parseq 'card)
                       '("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
                         "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"
                         "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"
                         "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"
                         "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"
                         "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"))))


;;; Part 1

(defun card-score (card)
  (destructuring-bind (winners card-numbers) card
    (let ((winner-count (fset:size (fset:intersection winners card-numbers))))
      (if (zerop winner-count)
          0
          (expt 2 (1- winner-count))))))

(defun get-answer-1 (&optional (cards *cards*))
  (fset:reduce (lambda (result id card)
                 (declare (ignore id))
                 (+ result (card-score card)))
               cards
               :initial-value 0))

(aoc:given 1
  (= 13 (get-answer-1 *example*)))


;;; Part 2

(defun score-card-with-copies (card-counts card-id card)
  (destructuring-bind (winners card-numbers) card
    (let* ((winner-count (fset:size (fset:intersection winners card-numbers)))
           (this-card-count (fset:lookup card-counts card-id)))
      (fset:reduce (lambda (new-counts new-id)
                     (fset:with new-counts new-id
                                (+ (fset:lookup new-counts new-id)
                                   this-card-count)))
                   (alexandria:iota winner-count :start (1+ card-id))
                   :initial-value card-counts))))

(defun score-cards-with-copies (cards)
  (let ((card-counts (fset:image (lambda (id card)
                                   (declare (ignore card))
                                   (values id 1))
                                 cards)))
    (fset:reduce (lambda (new-counts card-id)
                   (score-card-with-copies new-counts card-id (fset:lookup cards card-id)))
                 (fset:sort (fset:convert 'fset:seq (fset:domain cards))
                            #'<)
                 :initial-value card-counts)))

(defun get-answer-2 (&optional (cards *cards*))
  (fset:reduce (lambda (result id count)
                 (declare (ignore id))
                 (+ result count))
               (score-cards-with-copies cards)
               :initial-value 0))

(aoc:given 2
  (= 30 (get-answer-2 *example*)))
