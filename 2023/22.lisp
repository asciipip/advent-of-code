(in-package :aoc-2023-22)

;;; Part two is 57770, but it takes about ten minutes.
(aoc:define-day 448 nil)


;;; Data Structures

;; We'll represent bricks as cons pairs of points representing the bricks'
;; bounding boxes.  Note that we're using the standard definition of
;; bounding box, where the brick is fully enclosed.  A 1×2x3 brick at the
;; origin would be `1,0,0~1,1,2` in the problem input, but `(#<0 0 0>
;; . #<1 2 3>)` in this notation.


;;; Parsing

(parseq:defrule brick ()
    (and (aoc:comma-list aoc:integer-string) "~" (aoc:comma-list aoc:integer-string))
  (:choose 0 2)
  (:lambda (start end)
    (destructuring-bind (x1 y1 z1) start
      (destructuring-bind (x2 y2 z2) end
        (cons (point:make-point (min x1 x2)
                                (min y1 y2)
                                (1- (min z1 z2)))
              (point:make-point (1+ (max x1 x2))
                                (1+ (max y1 y2))
                                (max z1 z2)))))))

(defun brick< (brick1 brick2)
  (labels ((compare-r (l1 l2)
             (cond
               ((endp l1) nil)
               ((< (car l1) (car l2)) t)
               ((= (car l1) (car l2)) (compare-r (cdr l1) (cdr l2)))
               (t nil))))
    (compare-r (list (point:z (car brick1))
                     (point:y (car brick1))
                     (point:x (car brick1))
                     (point:z (cdr brick1))
                     (point:y (cdr brick1))
                     (point:x (cdr brick1)))
               (list (point:z (car brick2))
                     (point:y (car brick2))
                     (point:x (car brick2))
                     (point:z (cdr brick2))
                     (point:y (cdr brick2))
                     (point:x (cdr brick2))))))

(defun parse-bricks (lines)
  (fset:sort (fset:convert 'fset:seq (mapcar (alexandria:curry #'parseq:parseq 'brick) lines))
             #'brick<))

;;; Input

(defparameter *bricks* (parse-bricks (aoc:input)))
(defparameter *example*
  (parse-bricks '("1,0,1~1,2,1"
                  "0,0,2~2,0,2"
                  "0,2,3~2,2,3"
                  "0,0,4~0,2,4"
                  "2,0,5~2,2,5"
                  "0,1,6~2,1,6"
                  "1,1,8~1,1,9")))


;;; Part 1

(defun brick-top (brick)
  (point:z (cdr brick)))

(defun brick-bottom (brick)
  (point:z (car brick)))

(defun brick-footprint (brick)
  (destructuring-bind (brick-min . brick-max) brick
    (cons (point:make-point (point:x brick-min)
                            (point:y brick-min))
          (point:make-point (point:x brick-max)
                            (point:y brick-max)))))

(defun footprints-overlap-p (footprint1 footprint2)
  (destructuring-bind (min1 . max1) footprint1
    (destructuring-bind (min2 . max2) footprint2
      (and (< (point:x min1) (point:x max2))
           (< (point:x min2) (point:x max1))
           (< (point:y min1) (point:y max2))
           (< (point:y min2) (point:y max1))))))

(defun max-z-in-range (bricks bbox)
  (let ((bbox-footprint (brick-footprint bbox)))
   (fset:reduce (lambda (z brick)
                  (if (footprints-overlap-p bbox-footprint (brick-footprint brick))
                      (max z (brick-top brick))
                      z))
                bricks
                :initial-value 0)))

(defun shift-brick-to-z (brick z)
  (destructuring-bind (brick-min . brick-max) brick
    (let ((delta (point:make-point 0 0 (- z (brick-bottom brick)))))
      (cons (point:+ brick-min delta)
            (point:+ brick-max delta)))))

(defun settle-bricks (bricks &optional (active-brick 0))
  (if (<= (fset:size bricks) active-brick)
      bricks
      (let ((max-z (max-z-in-range (fset:subseq bricks 0 active-brick)
                                   (fset:lookup bricks active-brick))))
        (settle-bricks (fset:with bricks active-brick (shift-brick-to-z (fset:lookup bricks active-brick)
                                                                        max-z))
                       (1+ active-brick)))))

;; TODO: Use `group-bricks-by-z' to speed up the
;; supported/supporting/unsupported brick selection.
(defun group-bricks-by-z (bricks &key (key #'cdr))
  (fset:reduce (lambda (result brick)
                 (aoc:map-set-with result (point:z (funcall key brick)) brick))
               bricks
               :initial-value (fset:empty-map (fset:empty-set))))

(defun select-supporting-bricks (brick support-candidates)
  (let ((brick-footprint (brick-footprint brick)))
    (fset:filter (lambda (candidate)
                   (and (= (brick-top candidate) (brick-bottom brick))
                        (footprints-overlap-p brick-footprint (brick-footprint candidate))))
                 support-candidates)))

(defun select-supported-bricks (brick supported-candidates)
  (let ((brick-footprint (brick-footprint brick)))
    (fset:filter (lambda (candidate)
                   (and (= (brick-bottom candidate) (brick-top brick))
                        (footprints-overlap-p brick-footprint (brick-footprint candidate))))
                 supported-candidates)))

(defun disintegrable-brick-p (bricks brick)
  (let ((supported-bricks (select-supported-bricks brick bricks)))
    (fset:reduce (lambda (result supported-brick)
                   (and result (<= 2 (fset:size (select-supporting-bricks supported-brick bricks)))))
                 supported-bricks
                 :initial-value t)))

(defun disintegrable-bricks (bricks)
  (fset:filter (alexandria:curry #'disintegrable-brick-p bricks) bricks))

(defun get-answer-1 (&optional (bricks *bricks*))
  (fset:size (disintegrable-bricks (settle-bricks bricks))))

(aoc:given 1
  (= 5 (get-answer-1 *example*)))


;;; Part 2

(defun select-unsupported-bricks (bricks)
  (fset:filter (lambda (brick)
                 (and (plusp (brick-bottom brick))
                      (fset:empty? (select-supporting-bricks brick bricks))))
               bricks))

(defun remove-brick (bricks bricks-to-remove)
  (cond
    ((not (typep bricks 'fset:set))
     (remove-brick (fset:convert 'fset:set bricks) bricks-to-remove))
    ((not (typep bricks-to-remove 'fset:set))
     (remove-brick bricks (fset:set bricks-to-remove)))
    (t
     (let* ((new-bricks (fset:set-difference bricks bricks-to-remove))
            (now-unsupported (select-unsupported-bricks new-bricks)))
       (if (fset:empty? now-unsupported)
           new-bricks
           (remove-brick new-bricks now-unsupported))))))

(defun get-answer-2 (&optional (bricks *bricks*))
  (let ((settled-bricks (settle-bricks bricks))
        (completed 0))
    (fset:reduce #'+
                 (fset:image (lambda (brick)
                               (:printv :ts (incf completed))
                               (- (fset:size settled-bricks)
                                  (fset:size (remove-brick settled-bricks brick))
                                  1))
                             settled-bricks))))

(aoc:given 2
  (= 7 (get-answer-2 *example*)))
