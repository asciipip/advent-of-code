(in-package :aoc-2023-24)

(aoc:define-day 29142 nil)


;;; Data Structures

;; A line is a cons pair of point plus a vector.
;;
;; So the following particle: 19, 13, 30 @ -2,  1, -2
;; is: (#<19 13 30> . #<-2 1 -2>)


;;; Parsing

(parseq:defrule line ()
    (and (aoc:comma-list aoc:integer-string) " @" (+ " ") (aoc:comma-list aoc:integer-string))
  (:choose 0 3)
  (:lambda (position vector)
    (cons (apply #'point:make-point position)
          (apply #'point:make-point vector))))


;;; Input

(defparameter *hail* (aoc:input :parse-line 'line))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'line)
          '("19, 13, 30 @ -2,  1, -2"
            "18, 19, 22 @ -1, -1, -2"
            "20, 25, 34 @ -2, -2, -4"
            "12, 31, 28 @ -1, -2, -1"
            "20, 19, 15 @  1, -5, -3")))

;;; Part 1

;; See notebook notes for the equations here.  Basically, we solve the
;; intersection in the XY plane and then see if the intersection holds for
;; all point dimensions.
(defun intersection-point (line1 line2)
  "Returns three values:
    * The point of intersection or NIL if the lines do not intersect
    * The time between the start and the first particle reaching the intersection point
    * The time between the start and the second particle reaching the intersection point"
  (destructuring-bind (p1 . v1) line1
    (destructuring-bind (p2 . v2) line2
      (if (or (point:= (point:unit-vector v1) (point:unit-vector v2))
              (point:= (point:unit-vector v1) (point:- (point:unit-vector v2))))
          (values nil nil nil)
          (let* ((s (/ (- (point:y p2) (point:y p1) (* (- (point:x p2) (point:x p1))
                                                       (/ (point:y v1) (point:x v1))))
                       (- (* (point:x v2) (/ (point:y v1) (point:x v1)))
                          (point:y v2))))
                 (u (/ (- (+ (point:x p2) (* s (point:x v2)))
                          (point:x p1))
                       (point:x v1)))
                 (r1 (point:+ p1 (point:* v1 u)))
                 (r2 (point:+ p2 (point:* v2 s))))
            (values (if (point:= r1 r2)
                        r1
                        nil)
                    u
                    s))))))

(defparameter *part-1-min-point* #<7 7>)
(defparameter *part-1-max-point* #<27 27>)

(defun part-1-intersection-p (line1 line2)
  (multiple-value-bind (intersection dt1 dt2)
      (intersection-point line1 line2)
    (and intersection
         (point:<= *part-1-min-point* intersection *part-1-max-point*)
         (plusp dt1)
         (plusp dt2))))

(defun part-1-project (line)
  "Projects a line onto the XY axis."
  (destructuring-bind (p . v) line
    (cons (point:make-point (point:x p) (point:y p))
          (point:make-point (point:x v) (point:y v)))))

(defun find-part-1-intersections (hail)
  (let ((result))
    (labels ((visit-fn (pair)
               (destructuring-bind (l1 l2) pair
                 (when (part-1-intersection-p (part-1-project l1)
                                              (part-1-project l2))
                   (push pair result)))))
      (aoc:visit-subsets #'visit-fn hail 2))
    result))

(defun get-answer-1 (&optional (hail *hail*) (min-pos 200000000000000) (max-pos 400000000000000))
  (let ((*part-1-min-point* (point:make-point min-pos min-pos))
        (*part-1-max-point* (point:make-point max-pos max-pos)))
    (length (find-part-1-intersections hail))))

(aoc:given 1
  (= 2 (get-answer-1 *example* 7 27)))
