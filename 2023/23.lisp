(in-package :aoc-2023-23)

;; Part 2 is 6630, but my code doesn't calculate it properly.  I got the
;; answer by taking the path my code *did* calculate and making manual
;; tweaks to it that demonstrably increased the length of the path
;; generated.
(aoc:define-day 2406 nil)


;;; Parsing

(defun char-to-symbol (char)
  (ecase char
    (#\. t)
    (#\# nil)
    (#\< #<-1 0>)
    (#\> #<1 0>)
    (#\^ #<0 -1>)
    (#\v #<0 1>)))


;;; Input

(defparameter *map* (aoc:parse-grid-to-array (aoc:input) :key #'char-to-symbol))
(defparameter *example* (aoc:parse-grid-to-array (aoc:input :file "examples/23.txt")
                                                 :key #'char-to-symbol))


;;; Part 1

(defun start-pos (map)
  (dotimes (x (array-dimension map 1))
    (let ((point (point:make-point x 0)))
      (when (point:aref map point)
        (return-from start-pos point))))
  (assert nil))

(defun end-pos (map)
  (dotimes (x (array-dimension map 1))
    (let ((point (point:make-point x (1- (array-dimension map 0)))))
      (when (point:aref map point)
        (return-from end-pos point))))
  (assert nil))

(defun find-longest-path-step (with-slope map target position visited)
  (cond
    ((point:= position target)
     visited)
    ((and with-slope
          (typep (point:aref map position) 'point:point))
     (let ((neighbor (point:+ position (point:aref map position))))
       (if (fset:lookup visited neighbor)
           (fset:empty-set)
           (find-longest-path-step with-slope
                                   map
                                   target
                                   neighbor
                                   (fset:with visited position)))))
    (t
     (let ((neighbors (fset:filter (lambda (neighbor)
                                     (and (point:aref map neighbor)
                                          (not (fset:lookup visited neighbor))))
                                   (point:neighbor-set position :array map))))
       (fset:reduce (lambda (best-path neighbor)
                      (let ((new-path (find-longest-path-step with-slope
                                                              map
                                                              target
                                                              neighbor
                                                              (fset:with visited position))))
                        (if (< (fset:size best-path) (fset:size new-path))
                            new-path
                            best-path)))
                    neighbors
                    :initial-value (fset:empty-set))))))

(defun find-longest-path (map &key (with-slope t))
  (find-longest-path-step with-slope map (end-pos map) (start-pos map) (fset:empty-set)))

(defun get-answer-1 (&optional (map *map*))
  (fset:size (find-longest-path map)))

(aoc:given 1
  (= 94 (get-answer-1 *example*)))


;;; Part 2

;; The code below does not get the right answer.  In theory, Dijkstra
;; should get the right answer if you (1) give negative edge weights and
;; (2) ensure that no node is revisited (avoiding cycles).  In practice,
;; something isn't working right and it's missing steps.  I got the right
;; answer by drawing out the anser my code found, and then manually
;; removing and adding edges to increase the score.  Not really ideal.

(defun point-neighbors (map point)
  (fset:filter (alexandria:curry #'point:aref map)
               (point:neighbor-set point :array map)))

(defun find-graph-segment (map segment-start position last-position segment-length)
  "Returns two values: a map from points to cons pairs of a point and the
  distance to the point, and the next neighbors of the end point."
  (let ((neighbors (fset:less (point-neighbors map position)
                              last-position)))
    (if (= 1 (fset:size neighbors))
        (find-graph-segment map
                            segment-start
                            (fset:arb neighbors)
                            position
                            (1+ segment-length))
        (values (fset:with-default
                    (fset:map (segment-start (fset:map (position segment-length)))
                              (position (fset:map (segment-start segment-length))))
                  (fset:empty-map))
                position
                neighbors))))

(defun seen-segment-p (segment-map start end)
  (nth-value 1 (fset:lookup (fset:lookup segment-map start)
                            end)))

(defun find-graph-segments (map position neighbor &optional (segment-map (fset:empty-map (fset:empty-map))))
  (multiple-value-bind (new-segment-map new-position new-neighbors)
      (find-graph-segment map position neighbor position 1)
    (if (seen-segment-p segment-map position new-position)
        segment-map
        (fset:reduce (lambda (result new-neighbor)
                       (find-graph-segments map new-position new-neighbor result))
                     new-neighbors
                     :initial-value (fset:map-union segment-map
                                                    new-segment-map
                                                    #'fset:map-union)))))

(defun build-segment-map (map)
  (find-graph-segments map (start-pos map) (start-pos map)))

(defstruct state
  position
  distance
  visited)

(defmethod fset:compare ((s1 state) (s2 state))
  (fset:compare-slots s1 s2 'position 'distance 'visited))

(defun make-option-fn (segment-map)
  (lambda (state)
    (with-slots (position distance visited) state
      (let ((target-map (fset:filter (lambda (key value)
                                       (declare (ignore value))
                                       (not (fset:lookup visited key)))
                                     (fset:lookup segment-map position))))
        (fset:reduce (lambda (result next-position next-distance)
                       (cons (list (- next-distance)
                                   (make-state :position next-position
                                               :distance (+ distance next-distance)
                                               :visited (fset:with visited position)))
                             result))
                     target-map
                     :initial-value nil)))))

(defun find-longest-segment-path (map)
  (let ((segment-map (build-segment-map map))
        (end-pos (end-pos map)))
    (aoc:shortest-path (make-state :position (start-pos map)
                                   :distance 0
                                   :visited (fset:empty-set))
                       (make-option-fn segment-map)
                       :finishedp (lambda (state)
                                    (point:= end-pos (state-position state))))))

(defun get-answer-2 (&optional (map *map*))
  (- (1+ (nth-value 1 (find-longest-segment-path map)))))

(aoc:given 2
  (= 154 (get-answer-2 *example*)))


;;; Visualization

(defun make-map-cell-format-fn (map)
  (let ((longest-path (find-longest-path map)))
    (lambda (point)
      (let ((cell (point:aref map point))
            (color (if (fset:lookup longest-path point)
                       :green
                       :dark-primary)))
        (cond
          ((typep cell 'point:point)
           (alexandria:eswitch (cell :test #'point:=)
             (#<1 0> (list :color color :shape :triangle))
             (#<0 1> (list :color color :shape :triangle :rotation 90))
             (#<-1 0> (list :color color :shape :triangle :rotation 180))
             (#<0 -1> (list :color color :shape :triangle :rotation 270))))
          (cell
           (list :color color :shape :square))
          (t nil))))))

(defun draw-map (map)
  (aoc:draw-svg-grid (array-dimension map 1)
                     (array-dimension map 0)
                     (make-map-cell-format-fn map)))

(defun dot-node (point)
  (format nil "p~A_~A" (point:x point) (point:y point)))

(defun visited-edges (map)
  (let ((path (mapcar #'state-position (find-longest-segment-path map))))
    (fset:convert 'fset:set (mapcar #'cons path (cdr path)))))

(defun write-dot (map filename &key show-path)
  (multiple-value-bind (path distance) (find-longest-segment-path map)
    (let ((visited (fset:convert 'fset:set (mapcar #'state-position path)))
          (visited-edges (visited-edges map)))
      (with-open-file (output filename
                              :direction :output
                              :if-does-not-exist :create
                              :if-exists :supersede)
        (format output "graph aoc202323 {~%")
        (let ((mapped (fset:empty-set)))
          (fset:do-map (start target-map (build-segment-map map))
            (format output "    ~A [label=\"~A, ~A\"~A]~%"
                    (dot-node start) (point:x start) (point:y start)
                    (if (and show-path
                             (fset:lookup visited start))
                        ",style=\"filled\""
                        ""))
            (fset:do-map (end distance target-map)
              (when (not (fset:lookup mapped end))
                (format output "    ~A -- ~A [label=\"~A\",penwidth=~A]~%"
                        (dot-node start) (dot-node end) distance
                        (if (or (fset:lookup visited-edges (cons start end))
                                (fset:lookup visited-edges (cons end start)))
                            2
                            1))))
            (fset:adjoinf mapped start)))
        (format output "}~%")))
    distance))
