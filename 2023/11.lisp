(in-package :aoc-2023-11)

(aoc:define-day 9947476 519939907614)


;;; Parsing

(defun parse-line (line row)
  (reduce (lambda (result col)
            (if (char= #\# (schar line col))
                (fset:with result (point:make-point col row))
                result))
          (alexandria:iota (length line))
          :initial-value (fset:empty-set)))

(defun parse-lines (lines)
  (fset:reduce #'fset:union (mapcar #'parse-line lines (alexandria:iota (length lines)))))


;;; Input

(defparameter *galaxies* (parse-lines (aoc:input)))
(defparameter *example*
  (parse-lines '("...#......"
                 ".......#.."
                 "#........."
                 ".........."
                 "......#..."
                 ".#........"
                 ".........#"
                 ".........."
                 ".......#.."
                 "#...#.....")))


;;; Part 1

(defun project-galaxies (galaxies axis)
  "Returns a set of indices along the given axis that correspond to
  galaxies' positions."
  (fset:image (alexandria:rcurry #'point:elt axis) galaxies))

(defun missing-indices (indices)
  "Takes a set of integers.  Returns a set of integers between 0 and
  max(INDICES) that are not in INDICES."
  (fset:set-difference (aoc:iota (fset:greatest indices) :type 'fset:set)
                       indices))

(defun expansion-table (indices scale)
  "Given a set of indices, returns a seq from 0 to max(INDICES) giving the
  number of missing indices below each index."
  (let ((missing (missing-indices indices)))
    (fset:image (lambda (i)
                  (+ i (* (1- scale) (fset:count-if (alexandria:curry #'> i) missing))))
                (aoc:iota (1+ (fset:greatest indices)) :type 'fset:seq))))

(defun expand-galaxies (galaxies &optional (scale 2))
  (let ((x-expansion (expansion-table (project-galaxies galaxies 0) scale))
        (y-expansion (expansion-table (project-galaxies galaxies 1) scale)))
    (fset:image (lambda (galaxy)
                  (point:make-point (fset:lookup x-expansion (point:x galaxy))
                                    (fset:lookup y-expansion (point:y galaxy))))
                galaxies)))

(defun get-answer-1 (&optional (galaxies *galaxies*))
  (let ((result 0)
        (expanded-galaxies (fset:convert 'list (expand-galaxies galaxies))))
    (flet ((visit-pair (pair)
             (incf result (apply #'point:manhattan-distance (fset:convert 'list pair)))))
      (aoc:visit-subsets #'visit-pair expanded-galaxies 2))
    result))

(aoc:given 1
  (= 374 (get-answer-1 *example*)))


;;; Part 2

(defun get-answer-2 (&optional (galaxies *galaxies*) (scale 1000000))
  (let ((result 0)
        (expanded-galaxies (fset:convert 'list (expand-galaxies galaxies scale))))
    (flet ((visit-pair (pair)
             (incf result (apply #'point:manhattan-distance (fset:convert 'list pair)))))
      (aoc:visit-subsets #'visit-pair expanded-galaxies 2))
    result))

(aoc:given 2
  (= 1030 (get-answer-2 *example* 10))
  (= 8410 (get-answer-2 *example* 100)))
