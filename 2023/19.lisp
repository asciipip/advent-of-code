(in-package :aoc-2023-19)

(aoc:define-day 449531 122756210763577)


;;; Data Structures

(defstruct part
  x m a s)

(defmethod fset:compare ((p1 part) (p2 part))
  (fset:compare-slots p1 p2 'x 'm 'a 's))

(defun update-part (part &key x m a s)
  (with-slots ((old-x x) (old-m m) (old-a a) (old-s s)) part
    (make-part :x (or x old-x)
               :m (or m old-m)
               :a (or a old-a)
               :s (or s old-s))))

(defun update-part-slot (part slot value)
  (update-part part (aoc:make-keyword slot) value))


;;; Parsing

(defun parse-workflows (lines)
  (let ((workflow-lines (subseq lines 0 (position "" lines :test #'string=))))
    (reduce #'fset:map-union (mapcar (alexandria:curry #'parseq:parseq 'workflow) workflow-lines))))

(parseq:defrule workflow ()
    (and workflow-name "{" (aoc:comma-list rule) "}")
  (:choose 0 2)
  (:lambda (name rules)
    (fset:map (name rules))))

(parseq:defrule rule ()
    (or compare-rule workflow-name))

(parseq:defrule compare-rule ()
    (and (char "xmas") (char "<>") aoc:integer-string ":" workflow-name)
  (:choose 0 1 2 4)
  (:lambda (slot compare value target)
    (list (intern (string-upcase (string slot)))
          (ecase compare
            (#\< '<)
            (#\> '>))
          value
          target)))

(parseq:defrule workflow-name ()
    (+ (char "ARa-z"))
  (:string)
  (:lambda (name)
    (alexandria:switch (name :test #'string=)
      ("A" t)
      ("R" nil)
      (t name))))

(defun parse-parts (lines)
  (let ((part-lines (subseq lines (1+ (position "" lines :test #'string=)))))
    (mapcar (alexandria:curry #'parseq:parseq 'part) part-lines)))

(parseq:defrule part ()
    (and "{x=" aoc:integer-string ",m=" aoc:integer-string ",a=" aoc:integer-string ",s="  aoc:integer-string "}")
  (:choose 1 3 5 7)
  (:lambda (x m a s) (make-part :x x :m m :a a :s s)))


;;; Input

(defparameter *workflows* (parse-workflows (aoc:input)))
(defparameter *parts* (parse-parts (aoc:input)))
(defparameter *example-workflows* (parse-workflows (aoc:input :file "examples/19.txt")))
(defparameter *example-parts* (parse-parts (aoc:input :file "examples/19.txt")))


;;; Part 1

(defun rule-applies-p (rule part)
  (destructuring-bind (slot compare value target) rule
    (declare (ignore target))
    (funcall compare (slot-value part slot) value)))

(defun apply-rules (rules part)
  (assert (not (endp rules)))
  (cond
    ((or (symbolp (car rules))
         (stringp (car rules)))
     (car rules))
    ((rule-applies-p (car rules) part)
     (fset:last (car rules)))
    (t
     (apply-rules (cdr rules) part))))

(defun next-workflow (workflows workflow part)
  (apply-rules (fset:lookup workflows workflow) part))

(defun process-part (workflows part)
  (do ((workflow "in" (next-workflow workflows workflow part)))
      ((or (null workflow)
           (eq workflow t))
       workflow)))

(defun filter-parts (workflows parts)
  (fset:filter (alexandria:curry #'process-part workflows) parts))

(defun part-score (part)
  (with-slots (x m a s) part
    (+ x m a s)))

(defun get-answer-1 (&optional (workflows *workflows*) (parts *parts*))
  (reduce #'+ (mapcar #'part-score (filter-parts workflows parts))))

(aoc:given 1
  (= 19114 (get-answer-1 *example-workflows* *example-parts*)))


;;; Part 2

(defparameter *start-ranges*
  (fset:set
   (cons "in"
         (make-part :x (fset:set '(1 4001))
                    :m (fset:set '(1 4001))
                    :a (fset:set '(1 4001))
                    :s (fset:set '(1 4001))))))

(defun split-range (compare value range)
  "Returns two values: the portion of RANGE that matches the comparison, and
  the portion that doesn't.  Either portion might be NIL."
  (destructuring-bind (min-value max-value) range
    (cond
      ((and (funcall compare min-value value)
            (funcall compare max-value value))
       (values range nil))
      ((and (not (funcall compare min-value value))
            (not (funcall compare max-value value)))
       (values nil range))
      ((eq compare '<)
       (values (list min-value value)
               (list value max-value)))
      ((eq compare '>)
       (values (list (1+ value) max-value)
               (list min-value (1+ value))))
      (t (assert nil)))))

(defun split-ranges (compare value ranges)
  "Returns two values: a set of ranges included by the rule, and a set of
  ranges excluded."
  (values-list (fset:reduce (lambda (results range)
                              (multiple-value-bind (r1 r2) (split-range compare value range)
                                (list (if r1
                                          (fset:with (first results) r1)
                                          (first results))
                                      (if r2
                                          (fset:with (second results) r2)
                                          (second results)))))
                            ranges
                            :initial-value (list (fset:empty-set) (fset:empty-set)))))

(defun apply-rule-range (rule range)
  "Returns two values: a new list of workflow name and range set for the
  workflow, and the remaining range set.  Either might be NIL."
  (if (or (symbolp rule)
          (stringp rule))
      (values (cons rule range)
              nil)
      (destructuring-bind (slot compare value target) rule 
        (let ((slot-ranges (slot-value range slot)))
          (multiple-value-bind (included-ranges excluded-ranges)
              (split-ranges compare value slot-ranges)
            (values (if (fset:empty? included-ranges)
                        nil
                        (cons target (update-part-slot range slot included-ranges)))
                    (if (fset:empty? excluded-ranges)
                        nil
                        (update-part-slot range slot excluded-ranges))))))))

(defun apply-rules-range (rules range)
  (assert (not (endp rules)))
  (multiple-value-bind (new-range remaining-range)
      (apply-rule-range (car rules) range)
    (if (null remaining-range)
        (fset:set new-range)
        (fset:with (apply-rules-range (cdr rules) remaining-range)
                   new-range))))

(defun apply-rules-ranges (workflows ranges)
  (fset:filter #'car
               (fset:reduce (lambda (result full-range)
                              (destructuring-bind (name . range) full-range
                                (fset:union result (apply-rules-range (fset:lookup workflows name)
                                                                      range))))
                            ranges
                            :initial-value (fset:empty-set))))

(defun find-final-ranges (workflows ranges)
  (if (fset:empty? ranges)
      (fset:empty-set)
      (let* ((next-ranges (apply-rules-ranges workflows ranges))
             (new-finished (fset:filter (lambda (range) (eq t (car range))) next-ranges))
             (new-unfinished (fset:set-difference next-ranges new-finished)))
        (fset:union new-finished
                    (find-final-ranges workflows new-unfinished)))))

(defun range-combinations (ranges)
  (flet ((range-size (range-set)
           (fset:reduce (lambda (sum range)
                          (+ sum (- (second range) (first range))))
                        range-set
                        :initial-value 0)))
    (* (range-size (part-x ranges))
       (range-size (part-m ranges))
       (range-size (part-a ranges))
       (range-size (part-s ranges)))))

(defun total-range-combinations (ranges)
  (fset:reduce (lambda (sum full-range) (+ sum (range-combinations (cdr full-range))))
               ranges
               :initial-value 0))

(defun get-answer-2 (&optional (workflows *workflows*))
  (total-range-combinations (find-final-ranges workflows *start-ranges*)))

(aoc:given 2
  (= 167409079868000 (get-answer-2 *example-workflows*)))
