(in-package :aoc-2023-07)

(aoc:define-day 250957639 251515496)


;;; Parsing
;;;
;;; Hands parse into a list of cards and the bid.

(parseq:defrule hand ()
    (and cards " " aoc:integer-string)
  (:choose 0 2))

(parseq:defrule cards ()
    (rep 5 (or face-card ten-card number-card)))

(parseq:defrule face-card ()
    (char "AKQJ")
  (:lambda (char) (intern (format nil "~A" char))))

(parseq:defrule ten-card ()
    "T"
  (:constant 10))

(parseq:defrule number-card ()
    (char "987654321")
  (:lambda (char) (- (char-code char) (char-code #\0))))


;;; Input

(defparameter *hands* (aoc:input :parse-line 'hand))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'hand)
          '("32T3K 765"
            "T55J5 684"
            "KK677 28"
            "KTJJT 220"
            "QQQJA 483")))


;;; Part 1

(defparameter *score-rules* 1)

(defun hand-type-score-1 (cards)
  "Returns a numeric score for the hand type, with higher scores
  representing stronger hand types."
  (let* ((card-bag (fset:convert 'fset:bag cards))
         (max-multiplicity (fset:reduce (lambda (result card)
                                          (max result (fset:multiplicity card-bag card)))
                                        (fset:convert 'fset:set cards)
                                        :initial-value 0)))
    (ecase (fset:set-size card-bag)
      (1 7)                             ; Five of a kind
      (2 (ecase max-multiplicity
           (4 6)                        ; Four of a kind
           (3 5)))                      ; Full house
      (3 (ecase max-multiplicity
           (3 4)                        ; Three of a kind
           (2 3)))                      ; Two pair
      (4 2)                             ; One pair
      (5 1))))                          ; High card

(defun hand-type-score (cards)
  (ecase *score-rules*
    (1 (hand-type-score-1 cards))
    (2 (hand-type-score-2 cards))))

(defun card-score-1 (card)
  (case card
    (a 14)
    (k 13)
    (q 12)
    (j 11)
    (t card)))

(defun card-score (card)
  (ecase *score-rules*
    (1 (card-score-1 card))
    (2 (card-score-2 card))))

(defun hand-card-< (cards1 cards2)
  (assert (and (not (endp cards1))
               (not (endp cards2))))
  (or (< (card-score (car cards1))
         (card-score (car cards2)))
      (and (= (card-score (car cards1))
              (card-score (car cards2)))
           (hand-card-< (cdr cards1) (cdr cards2)))))

(defun hand< (cards1 cards2)
  (or (< (hand-type-score cards1)
         (hand-type-score cards2))
      (and (= (hand-type-score cards1)
              (hand-type-score cards2))
           (hand-card-< cards1 cards2))))

(defun total-winnings (hands)
  (let ((rank-ordered-hands (fset:sort hands #'hand< :key #'first)))
    (reduce #'+
            (mapcar #'*
                    (mapcar #'second rank-ordered-hands)
                    (alexandria:iota (length hands) :start 1)))))

(defun get-answer-1 (&optional (hands *hands*))
  (total-winnings hands))

(aoc:given 1
  (= 6440 (get-answer-1 *example*)))


;;; Part 2

(defun hand-type-score-2 (cards)
  "Returns a numeric score for the hand type, with higher scores
  representing stronger hand types."
  (let* ((card-bag (fset:bag-difference (fset:convert 'fset:bag cards) (fset:bag 'j 'j 'j 'j 'j)))
         (max-multiplicity (fset:reduce (lambda (result card)
                                          (max result (fset:multiplicity card-bag card)))
                                        (fset:convert 'fset:set cards)
                                        :initial-value 0)))
    (ecase (fset:set-size card-bag)
      (0 7)                             ; Five of a kind (all jokers)
      (1 7)                             ; Five of a kind
      (2 (ecase max-multiplicity
           (4 6)                        ; Four of a kind (no jokers)
           (3 (ecase (fset:size card-bag)
                (4 6)                   ; Four of a kind (one joker)
                (5 5)))                 ; Full house (no jokers)
           (2 (ecase (fset:size card-bag)
                (3 6)                   ; Four of a kind (two jokers)
                (4 5)))                 ; Full house (one joker)
           (1 6)))                      ; Four of a kind (three jokers)
      (3 (ecase max-multiplicity
           (3 4)                        ; Three of a kind (no jokers)
           (2 (ecase (fset:size card-bag)
                (4 4)                   ; Three of a kind (one joker)
                (5 3)))                 ; Two pair (no jokers)
           (1 4)))                      ; THree of a kind (two jokers)
      (4 2)                             ; One pair
      (5 1))))                          ; High card

(defun card-score-2 (card)
  (case card
    (a 14)
    (k 13)
    (q 12)
    (j 1)
    (t card)))

(defun get-answer-2 (&optional (hands *hands*))
  (let ((*score-rules* 2))
    (total-winnings hands)))

(aoc:given 2
  (= 5905 (get-answer-2 *example*)))
