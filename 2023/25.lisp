(in-package :aoc-2023-25)

;; As usual, part 2 is just to get all the other stars.
(aoc:define-day 613870 nil)


;;; Parsing

(parseq:defrule connection ()
    (and component ": " component-list)
  (:choose 0 2)
  (:lambda (source targets)
    (reduce (lambda (result target)
              (fset:with result (if (string< (string target) (string source))
                                    (cons target source)
                                    (cons source target))))
            targets
            :initial-value (fset:empty-set))))

(parseq:defrule component-list ()
    (and component (* (and " " component)))
  (:lambda (first rest)
    (cons first (mapcar #'second rest))))

(parseq:defrule component ()
    (+ (char "a-z"))
  (:string)
  (:function #'string-upcase)
  (:function #'intern))

(defun parse-connections (lines)
  (fset:reduce #'fset:union (mapcar (alexandria:curry #'parseq:parseq 'connection) lines)))


;;; Input

(defparameter *connections* (parse-connections (aoc:input)))
(defparameter *example*
  (parse-connections '("jqt: rhn xhk nvd"
                       "rsh: frs pzl lsr"
                       "xhk: hfx"
                       "cmg: qnr nvd lhk bvb"
                       "rhn: xhk bvb hfx"
                       "bvb: xhk hfx"
                       "pzl: lsr hfx nvd"
                       "qnr: nvd"
                       "ntq: jqt hfx bvb xhk"
                       "nvd: lhk"
                       "lsr: lhk"
                       "rzs: qnr cmg lsr rsh"
                       "frs: qnr lhk lsr")))


;;; Part 1

;; TODO: Determine the connections to remove programatically.
(defparameter *connections-to-remove*
  (fset:set '(ccp . fvm) '(fvm . ccp)
            '(lhg . llm) '(llm . lhg)
            '(frl . thx) '(thx . frl))
  "From inspection of the graph as drawn by dot.")

(defun make-connection-map (connections)
  (fset:reduce (lambda (result connection)
                 (destructuring-bind (n1 . n2) connection
                   (aoc:map-set-with (aoc:map-set-with result n2 n1)
                                     n1 n2)))
               connections
               :initial-value (fset:empty-map (fset:empty-set))))

(defun build-group (connection-map node group candidate-nodes)
  (let ((neighbors (fset:intersection (fset:lookup connection-map node)
                                      candidate-nodes)))
    (if (fset:empty? neighbors)
        (fset:with group node)
        (fset:reduce (lambda (result neighbor)
                       (if (fset:lookup result neighbor)
                           result
                           (fset:union result (build-group connection-map
                                                           neighbor
                                                           (fset:with group node)
                                                           (fset:less candidate-nodes node)))))
                     neighbors
                     :initial-value group))))

(defun build-groups (connection-map candidate-nodes)
  (if (fset:empty? candidate-nodes)
      nil
      (let ((group (build-group connection-map
                                (fset:arb candidate-nodes)
                                (fset:empty-set)
                                candidate-nodes)))
        (cons group (build-groups connection-map (fset:set-difference candidate-nodes group))))))

(defun get-answer-1 (&optional (connections *connections*) (to-remove *connections-to-remove*))
  (let ((connection-map (make-connection-map (fset:set-difference connections to-remove))))
    (apply #'* (mapcar #'fset:size (build-groups connection-map (fset:domain connection-map))))))


;;; Visualization

(defun write-dot (connections filename)
  (with-open-file (output filename
                          :direction :output
                          :if-does-not-exist :create
                          :if-exists :supersede)
    (format output "graph aoc202324 {~%")
    (format output "    overlap=false~%")
    (fset:do-set (connection connections)
      (destructuring-bind (n1 . n2) connection
        (format output "    ~A -- ~A~%" n1 n2)))
    (format output "}~%")))
